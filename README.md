# Travail de diplome

Ce git contient la documentation ainsi que le code réalisé pendant mon diplôme de technicien ES

## Dates importantes

* Début du travail : Lundi 4 avril 2022
* Evaluation intermédiaire 1 : 26 avril 2022
* Rapport intermédiaire + Poster : 9 mai 2022
* Evaluation intermédiaire 2 : 10 mai 2022
* Evaluation intermédiaire 3 : 24 mai 2022
* Fin du travail : Lundi 10 juin 2022

## Plannings

[Planning prévisionnel](https://docs.google.com/spreadsheets/d/1oBoMLqBCg3QjMgRj6gTaeiQAAlch0rYUdZ7_olKJr3E/edit?usp=sharing)


## journal de bord

Le [journal de bord](./docs/docs/JournalDeBord.md) ce trouve dans le dossier docs/docs.
