﻿/* 
 * App : SecuWallet
 * Auteur : Thomas Tissot
 * Org : CFPT
 * Date : 07.06.2022
 * brief : Classe permettant de stocker un portefeuille.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravailDiplome_TISSOTT
{
    public class Wallet
    {
        string name;
        string publicKey;
        string path;

        public string Name { get => name; set => name = value; }
        public string PublicKey { get => publicKey; set => publicKey = value; }
        public string Path { get => path; set => path = value; }

        public Wallet(string name, string path)
        {
            Name = name;
            Path = path;
        }
    }
}
