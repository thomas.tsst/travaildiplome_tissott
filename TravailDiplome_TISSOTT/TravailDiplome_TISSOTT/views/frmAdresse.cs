﻿/* 
 * App : SecuWallet
 * Auteur : Thomas Tissot
 * Org : CFPT
 * Date : 07.06.2022
 * brief : Vue permettant l'affichage et la copie de l'adresse publique d'un portefeuille.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroSet_UI.Forms;
using QRCoder;
namespace TravailDiplome_TISSOTT
{
    public partial class frmAdresse : MetroSetForm
    {
        public frmAdresse(string publicKey)
        {
            InitializeComponent();
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(publicKey, QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);
            Bitmap qrCodeImage = qrCode.GetGraphic(20);
            pbxQrCode.Image = qrCodeImage;
            lblPublicAddress.Text = publicKey;
        }
        /// <summary>
        /// Copie de l'adresse publique dans le presse-papiers.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void metroSetEllipse1_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(lblPublicAddress.Text);
        }

        private void frmAdresse_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }
    }
}
