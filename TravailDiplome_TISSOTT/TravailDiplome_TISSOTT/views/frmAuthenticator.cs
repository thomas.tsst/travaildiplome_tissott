﻿/* 
 * App : SecuWallet
 * Auteur : Thomas Tissot
 * Org : CFPT
 * Date : 07.06.2022
 * brief : Vue permettant à l'utilisateur d'entrer son code Google Authenticator.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroSet_UI.Forms;

namespace TravailDiplome_TISSOTT
{
    public partial class frmAuthenticator : MetroSetForm
    {
        AppController app;
        public frmAuthenticator(AppController app)
        {
            InitializeComponent();
            this.app = app;
        }

        private void btnCode_Click(object sender, EventArgs e)
        {
            //Le code Google Authenticator doit toujours être égale à 6 chiffres.
            if (tbxCode.Text.Length == 6)
            {
                //Demande à l'API si le code entré par l'utilisateur est valide
                object reponse = JwtController.DeserializeResponse(JwtController.CheckUserAuthenticator(app.User.Email, tbxCode.Text));
                if (reponse.ToString() == "ok")
                {
                    frmMain frmGestionPortefeuilles = new frmMain(app);
                    this.Hide();
                    frmGestionPortefeuilles.Closed += (s, args) => this.Close();
                    frmGestionPortefeuilles.Show();
                }
                else
                {
                    MessageBox.Show("Code erroné.");
                }
            }
        }
    }
}
