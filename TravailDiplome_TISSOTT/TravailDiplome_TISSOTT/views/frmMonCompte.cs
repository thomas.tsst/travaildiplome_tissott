﻿/* 
 * App : SecuWallet
 * Auteur : Thomas Tissot
 * Org : CFPT
 * Date : 07.06.2022
 * brief : Vue permettant l'accès au vue Gestion des portefeuilles, Paramètres ainsi qu'à la déconnexion de l'utilisateur.
 */
using MetroSet_UI.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TravailDiplome_TISSOTT
{
    public partial class frmMonCompte : MetroSetForm
    {
        AppController app;
        public frmMonCompte(AppController app)
        {
            this.app = app;
            InitializeComponent();
        }
        /// <summary>
        /// Redirige sur la vue Gestion des portefeuilles.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGestionPortefeuille_Click(object sender, EventArgs e)
        {
            frmGestionPortefeuilles frmGestionPortefeuilles = new frmGestionPortefeuilles(app);
            this.Hide();
            frmGestionPortefeuilles.Closed += (s, args) => this.Close();
            frmGestionPortefeuilles.Show();
        }
        /// <summary>
        /// Redirige sur la vue frmParametres.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnParametres_Click(object sender, EventArgs e)
        {
            frmParametres frm = new frmParametres(app);
            this.Hide();
            frm.Closed += (s, args) => this.Close();
            frm.Show();
        }
        /// <summary>
        /// Déconnecte l'utilisateur et redirection sur la vue Login.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDeconnexion_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }
    }
}
