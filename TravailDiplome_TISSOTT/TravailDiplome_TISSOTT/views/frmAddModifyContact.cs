﻿/* 
 * App : SecuWallet
 * Auteur : Thomas Tissot
 * Org : CFPT
 * Date : 07.06.2022
 * brief : Vue permettant de modifier ou d'ajouter un contact.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroSet_UI.Forms;
namespace TravailDiplome_TISSOTT
{
    public partial class frmAddModifyContact : MetroSetForm
    {
        AppController app;
        private bool modify;
        private int id;
        /// <summary>
        /// Constructeur nécessaire pour modifier un contact.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="frmTitle"></param>
        /// <param name="contactIndex"></param>
        public frmAddModifyContact(AppController app, string frmTitle, int contactIndex)
        {
            InitializeComponent();
            this.app = app;
            this.Text = frmTitle;
            this.id = contactIndex;
            modify = true;
            tbxName.Text = app.Settings.Contacts[contactIndex].Name;
            tbxAddress.Text = app.Settings.Contacts[contactIndex].PublicKey;
        }

        /// <summary>
        /// Constructeur nécessaire pour la création d'un nouveau contact.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="frmTitle"></param>
        public frmAddModifyContact(AppController app, string frmTitle)
        {
            InitializeComponent();
            this.app = app;
            this.Text = frmTitle;
            modify = false;
        }

        private void btnValidate_Click(object sender, EventArgs e)
        {
            bool isExist = false;
            //Vérification du type d'action de la page et vérifie si le nom ou l'adresse du contact est déjà existante.
            if (modify)
            {
                foreach (Contact contact in app.Settings.Contacts)
                {
                    if (app.Settings.Contacts.IndexOf(contact) != id)
                    {
                        if (tbxName.Text == contact.Name || tbxAddress.Text == contact.PublicKey)
                        {
                            isExist = true;
                        }
                    }
                }
                if (!isExist)
                {
                    if (app.Bc.isAdressValid(tbxAddress.Text))
                    {
                        app.Settings.Contacts[id].Name = tbxName.Text;
                        app.Settings.Contacts[id].PublicKey = tbxAddress.Text;
                        MessageBox.Show("Contact modifié");
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Adresse publique invalide");
                    }
                    
                }
                else
                {
                    MessageBox.Show("Nom de contact ou adresse publique déjà existante");
                }
            }
            else
            {
                foreach (Contact contact in app.Settings.Contacts)
                {
                    if (tbxName.Text == contact.Name || tbxAddress.Text == contact.PublicKey)
                    {
                        isExist = true;
                    }
                }
                if (!isExist)
                {
                    if (app.Bc.isAdressValid(tbxAddress.Text))
                    {
                        app.Settings.Contacts.Add(new Contact(tbxName.Text, tbxAddress.Text));
                        MessageBox.Show("Contact créé");
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Adresse publique invalide");
                    }
                }
                else
                {
                    MessageBox.Show("Nom de contact ou adresse publique déjà existante");
                }

            }

        }
    }
}
