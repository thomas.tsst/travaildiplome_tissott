﻿namespace TravailDiplome_TISSOTT
{
    partial class frmAdresse
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPublicAddress = new MetroSet_UI.Controls.MetroSetLabel();
            this.metroSetEllipse1 = new MetroSet_UI.Controls.MetroSetEllipse();
            this.pbxQrCode = new System.Windows.Forms.PictureBox();
            this.metroSetControlBox1 = new MetroSet_UI.Controls.MetroSetControlBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbxQrCode)).BeginInit();
            this.SuspendLayout();
            // 
            // lblPublicAddress
            // 
            this.lblPublicAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPublicAddress.IsDerivedStyle = true;
            this.lblPublicAddress.Location = new System.Drawing.Point(75, 304);
            this.lblPublicAddress.Name = "lblPublicAddress";
            this.lblPublicAddress.Size = new System.Drawing.Size(358, 23);
            this.lblPublicAddress.Style = MetroSet_UI.Enums.Style.Light;
            this.lblPublicAddress.StyleManager = null;
            this.lblPublicAddress.TabIndex = 1;
            this.lblPublicAddress.Text = "0x..";
            this.lblPublicAddress.ThemeAuthor = "Narwin";
            this.lblPublicAddress.ThemeName = "MetroLite";
            // 
            // metroSetEllipse1
            // 
            this.metroSetEllipse1.BorderThickness = 7;
            this.metroSetEllipse1.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.metroSetEllipse1.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.metroSetEllipse1.DisabledForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            this.metroSetEllipse1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.metroSetEllipse1.HoverBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.metroSetEllipse1.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.metroSetEllipse1.HoverTextColor = System.Drawing.Color.White;
            this.metroSetEllipse1.Image = global::TravailDiplome_TISSOTT.Properties.Resources.copy__1_;
            this.metroSetEllipse1.ImageSize = new System.Drawing.Size(50, 50);
            this.metroSetEllipse1.IsDerivedStyle = true;
            this.metroSetEllipse1.Location = new System.Drawing.Point(215, 330);
            this.metroSetEllipse1.Name = "metroSetEllipse1";
            this.metroSetEllipse1.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.metroSetEllipse1.NormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.metroSetEllipse1.NormalTextColor = System.Drawing.Color.Black;
            this.metroSetEllipse1.PressBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.metroSetEllipse1.PressColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.metroSetEllipse1.PressTextColor = System.Drawing.Color.White;
            this.metroSetEllipse1.Size = new System.Drawing.Size(78, 79);
            this.metroSetEllipse1.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetEllipse1.StyleManager = null;
            this.metroSetEllipse1.TabIndex = 2;
            this.metroSetEllipse1.ThemeAuthor = "Narwin";
            this.metroSetEllipse1.ThemeName = "MetroLite";
            this.metroSetEllipse1.Click += new System.EventHandler(this.metroSetEllipse1_Click);
            // 
            // pbxQrCode
            // 
            this.pbxQrCode.Location = new System.Drawing.Point(154, 102);
            this.pbxQrCode.Name = "pbxQrCode";
            this.pbxQrCode.Size = new System.Drawing.Size(200, 200);
            this.pbxQrCode.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxQrCode.TabIndex = 0;
            this.pbxQrCode.TabStop = false;
            // 
            // metroSetControlBox1
            // 
            this.metroSetControlBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.metroSetControlBox1.CloseHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.metroSetControlBox1.CloseHoverForeColor = System.Drawing.Color.White;
            this.metroSetControlBox1.CloseNormalForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.DisabledForeColor = System.Drawing.Color.DimGray;
            this.metroSetControlBox1.IsDerivedStyle = true;
            this.metroSetControlBox1.Location = new System.Drawing.Point(394, 5);
            this.metroSetControlBox1.MaximizeBox = true;
            this.metroSetControlBox1.MaximizeHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.metroSetControlBox1.MaximizeHoverForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.MaximizeNormalForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.MinimizeBox = true;
            this.metroSetControlBox1.MinimizeHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.metroSetControlBox1.MinimizeHoverForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.MinimizeNormalForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.Name = "metroSetControlBox1";
            this.metroSetControlBox1.Size = new System.Drawing.Size(100, 25);
            this.metroSetControlBox1.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetControlBox1.StyleManager = null;
            this.metroSetControlBox1.TabIndex = 3;
            this.metroSetControlBox1.Text = "metroSetControlBox1";
            this.metroSetControlBox1.ThemeAuthor = "Narwin";
            this.metroSetControlBox1.ThemeName = "MetroLite";
            // 
            // frmAdresse
            // 
            this.AllowResize = false;
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 26F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(509, 454);
            this.Controls.Add(this.metroSetControlBox1);
            this.Controls.Add(this.metroSetEllipse1);
            this.Controls.Add(this.lblPublicAddress);
            this.Controls.Add(this.pbxQrCode);
            this.Name = "frmAdresse";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Adresse publique";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmAdresse_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pbxQrCode)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pbxQrCode;
        private MetroSet_UI.Controls.MetroSetLabel lblPublicAddress;
        private MetroSet_UI.Controls.MetroSetEllipse metroSetEllipse1;
        private MetroSet_UI.Controls.MetroSetControlBox metroSetControlBox1;
    }
}