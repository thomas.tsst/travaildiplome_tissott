﻿/* 
 * App : SecuWallet
 * Auteur : Thomas Tissot
 * Org : CFPT
 * Date : 07.06.2022
 * brief : Vue permettant à l'utilisateur de visualiser les différents contacts enregistrés sur son compte.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroSet_UI.Forms;
namespace TravailDiplome_TISSOTT
{
    public partial class frmContact : MetroSetForm
    {
        AppController app;
        public frmContact(AppController app)
        {
            InitializeComponent();
            this.app = app;
            UpdateView();
        }
        /// <summary>
        /// Affiche la vue Ajout Contact.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNewContact_Click(object sender, EventArgs e)
        {
            frmAddModifyContact frm = new frmAddModifyContact(app, "Ajouter contact");
            this.Hide();
            frm.Closed += (s, args) => this.Show();
            frm.Closed += (s, args) => this.UpdateView();
            frm.Show();
            UpdateView();
        }
        /// <summary>
        /// Affiche la vue Modifier Contact.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnModify_Click(object sender, EventArgs e)
        {
            int index = GetListBoxSelectedIndex();
            //Vérification si un contact a bien été selectionné.
            if (index != -1)
            {
                frmAddModifyContact frm = new frmAddModifyContact(app, "Modifier contact", GetListBoxSelectedIndex());
                this.Hide();
                frm.Closed += (s, args) => this.Show();
                frm.Closed += (s, args) => this.UpdateView();
                frm.Show();
                UpdateView();
            }
            else
            {
                MessageBox.Show("Veuillez selectionner un contact pour le modifier");
            }
        }
        /// <summary>
        /// Suppression du contact sélectionné.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDelete_Click(object sender, EventArgs e)
        {
            int index = GetListBoxSelectedIndex();
            //Vérification si un contact a bien été selectionné.
            if (index != -1)
            {
                app.Settings.Contacts.RemoveAt(index);
                UpdateView();
            }
            else
            {
                MessageBox.Show("Veuillez selectionner un contact pour le supprimer");
            }
            
        }
        /// <summary>
        /// Retourne l'index sélectionné dans la listBox.
        /// </summary>
        /// <returns></returns>
        public int GetListBoxSelectedIndex()
        {
            return lbxContact.SelectedIndex;
        }
        /// <summary>
        /// Met à jour la vue.
        /// </summary>
        private void UpdateView()
        {
            lbxContact.Items.Clear();
            foreach (Contact contact in app.Settings.Contacts)
            {
                lbxContact.Items.Add(contact.Name + ": " + contact.PublicKey);
            }
        }
    }
}
