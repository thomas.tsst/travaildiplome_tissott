﻿namespace TravailDiplome_TISSOTT
{
    partial class frmLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbxEmail = new MetroSet_UI.Controls.MetroSetTextBox();
            this.tbxPassword = new MetroSet_UI.Controls.MetroSetTextBox();
            this.lblPassword = new MetroSet_UI.Controls.MetroSetLabel();
            this.lblEmail = new MetroSet_UI.Controls.MetroSetLabel();
            this.btnLogin = new MetroSet_UI.Controls.MetroSetButton();
            this.metroSetControlBox1 = new MetroSet_UI.Controls.MetroSetControlBox();
            this.SuspendLayout();
            // 
            // tbxEmail
            // 
            this.tbxEmail.AutoCompleteCustomSource = null;
            this.tbxEmail.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.tbxEmail.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.tbxEmail.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.tbxEmail.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.tbxEmail.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.tbxEmail.DisabledForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            this.tbxEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.tbxEmail.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.tbxEmail.Image = null;
            this.tbxEmail.IsDerivedStyle = true;
            this.tbxEmail.Lines = null;
            this.tbxEmail.Location = new System.Drawing.Point(53, 136);
            this.tbxEmail.MaxLength = 32767;
            this.tbxEmail.Multiline = false;
            this.tbxEmail.Name = "tbxEmail";
            this.tbxEmail.ReadOnly = false;
            this.tbxEmail.Size = new System.Drawing.Size(210, 30);
            this.tbxEmail.Style = MetroSet_UI.Enums.Style.Light;
            this.tbxEmail.StyleManager = null;
            this.tbxEmail.TabIndex = 0;
            this.tbxEmail.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbxEmail.ThemeAuthor = "Narwin";
            this.tbxEmail.ThemeName = "MetroLite";
            this.tbxEmail.UseSystemPasswordChar = false;
            this.tbxEmail.WatermarkText = "Email";
            // 
            // tbxPassword
            // 
            this.tbxPassword.AutoCompleteCustomSource = null;
            this.tbxPassword.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.tbxPassword.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.tbxPassword.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.tbxPassword.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.tbxPassword.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.tbxPassword.DisabledForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            this.tbxPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.tbxPassword.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.tbxPassword.Image = null;
            this.tbxPassword.IsDerivedStyle = true;
            this.tbxPassword.Lines = null;
            this.tbxPassword.Location = new System.Drawing.Point(53, 215);
            this.tbxPassword.MaxLength = 32767;
            this.tbxPassword.Multiline = false;
            this.tbxPassword.Name = "tbxPassword";
            this.tbxPassword.ReadOnly = false;
            this.tbxPassword.Size = new System.Drawing.Size(210, 30);
            this.tbxPassword.Style = MetroSet_UI.Enums.Style.Light;
            this.tbxPassword.StyleManager = null;
            this.tbxPassword.TabIndex = 1;
            this.tbxPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbxPassword.ThemeAuthor = "Narwin";
            this.tbxPassword.ThemeName = "MetroLite";
            this.tbxPassword.UseSystemPasswordChar = true;
            this.tbxPassword.WatermarkText = "Mot de passe";
            // 
            // lblPassword
            // 
            this.lblPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblPassword.IsDerivedStyle = true;
            this.lblPassword.Location = new System.Drawing.Point(53, 179);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(121, 23);
            this.lblPassword.Style = MetroSet_UI.Enums.Style.Light;
            this.lblPassword.StyleManager = null;
            this.lblPassword.TabIndex = 2;
            this.lblPassword.Text = "Mot de passe :";
            this.lblPassword.ThemeAuthor = "Narwin";
            this.lblPassword.ThemeName = "MetroLite";
            // 
            // lblEmail
            // 
            this.lblEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblEmail.IsDerivedStyle = true;
            this.lblEmail.Location = new System.Drawing.Point(53, 100);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(100, 23);
            this.lblEmail.Style = MetroSet_UI.Enums.Style.Light;
            this.lblEmail.StyleManager = null;
            this.lblEmail.TabIndex = 3;
            this.lblEmail.Text = "Email :";
            this.lblEmail.ThemeAuthor = "Narwin";
            this.lblEmail.ThemeName = "MetroLite";
            // 
            // btnLogin
            // 
            this.btnLogin.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnLogin.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnLogin.DisabledForeColor = System.Drawing.Color.Gray;
            this.btnLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnLogin.HoverBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btnLogin.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btnLogin.HoverTextColor = System.Drawing.Color.White;
            this.btnLogin.IsDerivedStyle = true;
            this.btnLogin.Location = new System.Drawing.Point(53, 258);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnLogin.NormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnLogin.NormalTextColor = System.Drawing.Color.White;
            this.btnLogin.PressBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btnLogin.PressColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btnLogin.PressTextColor = System.Drawing.Color.White;
            this.btnLogin.Size = new System.Drawing.Size(210, 48);
            this.btnLogin.Style = MetroSet_UI.Enums.Style.Light;
            this.btnLogin.StyleManager = null;
            this.btnLogin.TabIndex = 4;
            this.btnLogin.Text = "Connexion";
            this.btnLogin.ThemeAuthor = "Narwin";
            this.btnLogin.ThemeName = "MetroLite";
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // metroSetControlBox1
            // 
            this.metroSetControlBox1.CloseHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.metroSetControlBox1.CloseHoverForeColor = System.Drawing.Color.White;
            this.metroSetControlBox1.CloseNormalForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.DisabledForeColor = System.Drawing.Color.DimGray;
            this.metroSetControlBox1.IsDerivedStyle = true;
            this.metroSetControlBox1.Location = new System.Drawing.Point(202, 15);
            this.metroSetControlBox1.MaximizeBox = false;
            this.metroSetControlBox1.MaximizeHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.metroSetControlBox1.MaximizeHoverForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.MaximizeNormalForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.MinimizeBox = true;
            this.metroSetControlBox1.MinimizeHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.metroSetControlBox1.MinimizeHoverForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.MinimizeNormalForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.Name = "metroSetControlBox1";
            this.metroSetControlBox1.Size = new System.Drawing.Size(100, 25);
            this.metroSetControlBox1.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetControlBox1.StyleManager = null;
            this.metroSetControlBox1.TabIndex = 5;
            this.metroSetControlBox1.Text = "metroSetControlBox1";
            this.metroSetControlBox1.ThemeAuthor = "Narwin";
            this.metroSetControlBox1.ThemeName = "MetroLite";
            // 
            // frmLogin
            // 
            this.AllowResize = false;
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 26F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(317, 333);
            this.Controls.Add(this.metroSetControlBox1);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.lblEmail);
            this.Controls.Add(this.lblPassword);
            this.Controls.Add(this.tbxPassword);
            this.Controls.Add(this.tbxEmail);
            this.Name = "frmLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Connexion";
            this.ResumeLayout(false);

        }

        #endregion

        private MetroSet_UI.Controls.MetroSetTextBox tbxEmail;
        private MetroSet_UI.Controls.MetroSetTextBox tbxPassword;
        private MetroSet_UI.Controls.MetroSetLabel lblPassword;
        private MetroSet_UI.Controls.MetroSetLabel lblEmail;
        private MetroSet_UI.Controls.MetroSetButton btnLogin;
        private MetroSet_UI.Controls.MetroSetControlBox metroSetControlBox1;
    }
}