﻿namespace TravailDiplome_TISSOTT
{
    partial class frmEnvoi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroSetLabel1 = new MetroSet_UI.Controls.MetroSetLabel();
            this.tbxAdresse = new MetroSet_UI.Controls.MetroSetTextBox();
            this.btnEnvoyer = new MetroSet_UI.Controls.MetroSetButton();
            this.lbxWallets = new MetroSet_UI.Controls.MetroSetListBox();
            this.metroSetLabel2 = new MetroSet_UI.Controls.MetroSetLabel();
            this.tbxMontant = new MetroSet_UI.Controls.MetroSetTextBox();
            this.metroSetLabel3 = new MetroSet_UI.Controls.MetroSetLabel();
            this.metroSetControlBox1 = new MetroSet_UI.Controls.MetroSetControlBox();
            this.tbxPassword = new MetroSet_UI.Controls.MetroSetTextBox();
            this.metroSetLabel4 = new MetroSet_UI.Controls.MetroSetLabel();
            this.metroSetLabel5 = new MetroSet_UI.Controls.MetroSetLabel();
            this.lbxContact = new MetroSet_UI.Controls.MetroSetListBox();
            this.SuspendLayout();
            // 
            // metroSetLabel1
            // 
            this.metroSetLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.metroSetLabel1.IsDerivedStyle = true;
            this.metroSetLabel1.Location = new System.Drawing.Point(218, 82);
            this.metroSetLabel1.Name = "metroSetLabel1";
            this.metroSetLabel1.Size = new System.Drawing.Size(192, 23);
            this.metroSetLabel1.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetLabel1.StyleManager = null;
            this.metroSetLabel1.TabIndex = 0;
            this.metroSetLabel1.Text = "Envoyer à :";
            this.metroSetLabel1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.metroSetLabel1.ThemeAuthor = "Narwin";
            this.metroSetLabel1.ThemeName = "MetroLite";
            // 
            // tbxAdresse
            // 
            this.tbxAdresse.AutoCompleteCustomSource = null;
            this.tbxAdresse.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.tbxAdresse.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.tbxAdresse.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.tbxAdresse.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.tbxAdresse.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.tbxAdresse.DisabledForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            this.tbxAdresse.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.tbxAdresse.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.tbxAdresse.Image = null;
            this.tbxAdresse.IsDerivedStyle = true;
            this.tbxAdresse.Lines = null;
            this.tbxAdresse.Location = new System.Drawing.Point(218, 115);
            this.tbxAdresse.MaxLength = 32767;
            this.tbxAdresse.Multiline = false;
            this.tbxAdresse.Name = "tbxAdresse";
            this.tbxAdresse.ReadOnly = false;
            this.tbxAdresse.Size = new System.Drawing.Size(192, 30);
            this.tbxAdresse.Style = MetroSet_UI.Enums.Style.Light;
            this.tbxAdresse.StyleManager = null;
            this.tbxAdresse.TabIndex = 2;
            this.tbxAdresse.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbxAdresse.ThemeAuthor = "Narwin";
            this.tbxAdresse.ThemeName = "MetroLite";
            this.tbxAdresse.UseSystemPasswordChar = false;
            this.tbxAdresse.WatermarkText = "Adresse (0x...)";
            // 
            // btnEnvoyer
            // 
            this.btnEnvoyer.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnEnvoyer.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnEnvoyer.DisabledForeColor = System.Drawing.Color.Gray;
            this.btnEnvoyer.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnEnvoyer.HoverBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btnEnvoyer.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btnEnvoyer.HoverTextColor = System.Drawing.Color.White;
            this.btnEnvoyer.IsDerivedStyle = true;
            this.btnEnvoyer.Location = new System.Drawing.Point(189, 472);
            this.btnEnvoyer.Name = "btnEnvoyer";
            this.btnEnvoyer.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnEnvoyer.NormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnEnvoyer.NormalTextColor = System.Drawing.Color.White;
            this.btnEnvoyer.PressBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btnEnvoyer.PressColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btnEnvoyer.PressTextColor = System.Drawing.Color.White;
            this.btnEnvoyer.Size = new System.Drawing.Size(250, 46);
            this.btnEnvoyer.Style = MetroSet_UI.Enums.Style.Light;
            this.btnEnvoyer.StyleManager = null;
            this.btnEnvoyer.TabIndex = 3;
            this.btnEnvoyer.Text = "Envoyer";
            this.btnEnvoyer.ThemeAuthor = "Narwin";
            this.btnEnvoyer.ThemeName = "MetroLite";
            this.btnEnvoyer.Click += new System.EventHandler(this.btnEnvoyer_Click);
            // 
            // lbxWallets
            // 
            this.lbxWallets.BackColor = System.Drawing.Color.White;
            this.lbxWallets.BorderColor = System.Drawing.Color.LightGray;
            this.lbxWallets.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.lbxWallets.DisabledForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            this.lbxWallets.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lbxWallets.HoveredItemBackColor = System.Drawing.Color.LightGray;
            this.lbxWallets.HoveredItemColor = System.Drawing.Color.DimGray;
            this.lbxWallets.IsDerivedStyle = true;
            this.lbxWallets.ItemHeight = 30;
            this.lbxWallets.Location = new System.Drawing.Point(56, 334);
            this.lbxWallets.MultiSelect = false;
            this.lbxWallets.Name = "lbxWallets";
            this.lbxWallets.SelectedIndex = -1;
            this.lbxWallets.SelectedItem = null;
            this.lbxWallets.SelectedItemBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.lbxWallets.SelectedItemColor = System.Drawing.Color.White;
            this.lbxWallets.SelectedText = null;
            this.lbxWallets.SelectedValue = null;
            this.lbxWallets.ShowBorder = false;
            this.lbxWallets.ShowScrollBar = false;
            this.lbxWallets.Size = new System.Drawing.Size(250, 132);
            this.lbxWallets.Style = MetroSet_UI.Enums.Style.Light;
            this.lbxWallets.StyleManager = null;
            this.lbxWallets.TabIndex = 4;
            this.lbxWallets.ThemeAuthor = "Narwin";
            this.lbxWallets.ThemeName = "MetroLite";
            this.lbxWallets.SelectedIndexChanged += new MetroSet_UI.Controls.MetroSetListBox.SelectedIndexChangedEventHandler(this.lbxWallets_SelectedIndexChanged);
            // 
            // metroSetLabel2
            // 
            this.metroSetLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.metroSetLabel2.IsDerivedStyle = true;
            this.metroSetLabel2.Location = new System.Drawing.Point(218, 155);
            this.metroSetLabel2.Name = "metroSetLabel2";
            this.metroSetLabel2.Size = new System.Drawing.Size(192, 23);
            this.metroSetLabel2.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetLabel2.StyleManager = null;
            this.metroSetLabel2.TabIndex = 5;
            this.metroSetLabel2.Text = "Montant :";
            this.metroSetLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroSetLabel2.ThemeAuthor = "Narwin";
            this.metroSetLabel2.ThemeName = "MetroLite";
            // 
            // tbxMontant
            // 
            this.tbxMontant.AutoCompleteCustomSource = null;
            this.tbxMontant.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.tbxMontant.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.tbxMontant.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.tbxMontant.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.tbxMontant.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.tbxMontant.DisabledForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            this.tbxMontant.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.tbxMontant.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.tbxMontant.Image = null;
            this.tbxMontant.IsDerivedStyle = true;
            this.tbxMontant.Lines = null;
            this.tbxMontant.Location = new System.Drawing.Point(218, 188);
            this.tbxMontant.MaxLength = 32767;
            this.tbxMontant.Multiline = false;
            this.tbxMontant.Name = "tbxMontant";
            this.tbxMontant.ReadOnly = false;
            this.tbxMontant.Size = new System.Drawing.Size(192, 30);
            this.tbxMontant.Style = MetroSet_UI.Enums.Style.Light;
            this.tbxMontant.StyleManager = null;
            this.tbxMontant.TabIndex = 6;
            this.tbxMontant.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbxMontant.ThemeAuthor = "Narwin";
            this.tbxMontant.ThemeName = "MetroLite";
            this.tbxMontant.UseSystemPasswordChar = false;
            this.tbxMontant.WatermarkText = "Montant";
            // 
            // metroSetLabel3
            // 
            this.metroSetLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.metroSetLabel3.IsDerivedStyle = true;
            this.metroSetLabel3.Location = new System.Drawing.Point(56, 308);
            this.metroSetLabel3.Name = "metroSetLabel3";
            this.metroSetLabel3.Size = new System.Drawing.Size(250, 23);
            this.metroSetLabel3.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetLabel3.StyleManager = null;
            this.metroSetLabel3.TabIndex = 7;
            this.metroSetLabel3.Text = "Comptes personnels :";
            this.metroSetLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroSetLabel3.ThemeAuthor = "Narwin";
            this.metroSetLabel3.ThemeName = "MetroLite";
            // 
            // metroSetControlBox1
            // 
            this.metroSetControlBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.metroSetControlBox1.CloseHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.metroSetControlBox1.CloseHoverForeColor = System.Drawing.Color.White;
            this.metroSetControlBox1.CloseNormalForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.DisabledForeColor = System.Drawing.Color.DimGray;
            this.metroSetControlBox1.IsDerivedStyle = true;
            this.metroSetControlBox1.Location = new System.Drawing.Point(513, 13);
            this.metroSetControlBox1.MaximizeBox = true;
            this.metroSetControlBox1.MaximizeHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.metroSetControlBox1.MaximizeHoverForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.MaximizeNormalForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.MinimizeBox = true;
            this.metroSetControlBox1.MinimizeHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.metroSetControlBox1.MinimizeHoverForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.MinimizeNormalForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.Name = "metroSetControlBox1";
            this.metroSetControlBox1.Size = new System.Drawing.Size(100, 25);
            this.metroSetControlBox1.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetControlBox1.StyleManager = null;
            this.metroSetControlBox1.TabIndex = 8;
            this.metroSetControlBox1.Text = "metroSetControlBox1";
            this.metroSetControlBox1.ThemeAuthor = "Narwin";
            this.metroSetControlBox1.ThemeName = "MetroLite";
            // 
            // tbxPassword
            // 
            this.tbxPassword.AutoCompleteCustomSource = null;
            this.tbxPassword.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.tbxPassword.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.tbxPassword.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.tbxPassword.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.tbxPassword.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.tbxPassword.DisabledForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            this.tbxPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.tbxPassword.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.tbxPassword.Image = null;
            this.tbxPassword.IsDerivedStyle = true;
            this.tbxPassword.Lines = null;
            this.tbxPassword.Location = new System.Drawing.Point(218, 261);
            this.tbxPassword.MaxLength = 32767;
            this.tbxPassword.Multiline = false;
            this.tbxPassword.Name = "tbxPassword";
            this.tbxPassword.ReadOnly = false;
            this.tbxPassword.Size = new System.Drawing.Size(192, 30);
            this.tbxPassword.Style = MetroSet_UI.Enums.Style.Light;
            this.tbxPassword.StyleManager = null;
            this.tbxPassword.TabIndex = 10;
            this.tbxPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbxPassword.ThemeAuthor = "Narwin";
            this.tbxPassword.ThemeName = "MetroLite";
            this.tbxPassword.UseSystemPasswordChar = true;
            this.tbxPassword.WatermarkText = "Mot de passe";
            // 
            // metroSetLabel4
            // 
            this.metroSetLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.metroSetLabel4.IsDerivedStyle = true;
            this.metroSetLabel4.Location = new System.Drawing.Point(218, 228);
            this.metroSetLabel4.Name = "metroSetLabel4";
            this.metroSetLabel4.Size = new System.Drawing.Size(192, 23);
            this.metroSetLabel4.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetLabel4.StyleManager = null;
            this.metroSetLabel4.TabIndex = 9;
            this.metroSetLabel4.Text = "Mot de passe :";
            this.metroSetLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroSetLabel4.ThemeAuthor = "Narwin";
            this.metroSetLabel4.ThemeName = "MetroLite";
            // 
            // metroSetLabel5
            // 
            this.metroSetLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.metroSetLabel5.IsDerivedStyle = true;
            this.metroSetLabel5.Location = new System.Drawing.Point(342, 308);
            this.metroSetLabel5.Name = "metroSetLabel5";
            this.metroSetLabel5.Size = new System.Drawing.Size(250, 23);
            this.metroSetLabel5.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetLabel5.StyleManager = null;
            this.metroSetLabel5.TabIndex = 12;
            this.metroSetLabel5.Text = "Contacts :";
            this.metroSetLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroSetLabel5.ThemeAuthor = "Narwin";
            this.metroSetLabel5.ThemeName = "MetroLite";
            // 
            // lbxContact
            // 
            this.lbxContact.BackColor = System.Drawing.Color.White;
            this.lbxContact.BorderColor = System.Drawing.Color.LightGray;
            this.lbxContact.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.lbxContact.DisabledForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            this.lbxContact.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lbxContact.HoveredItemBackColor = System.Drawing.Color.LightGray;
            this.lbxContact.HoveredItemColor = System.Drawing.Color.DimGray;
            this.lbxContact.IsDerivedStyle = true;
            this.lbxContact.ItemHeight = 30;
            this.lbxContact.Location = new System.Drawing.Point(342, 334);
            this.lbxContact.MultiSelect = false;
            this.lbxContact.Name = "lbxContact";
            this.lbxContact.SelectedIndex = -1;
            this.lbxContact.SelectedItem = null;
            this.lbxContact.SelectedItemBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.lbxContact.SelectedItemColor = System.Drawing.Color.White;
            this.lbxContact.SelectedText = null;
            this.lbxContact.SelectedValue = null;
            this.lbxContact.ShowBorder = false;
            this.lbxContact.ShowScrollBar = false;
            this.lbxContact.Size = new System.Drawing.Size(250, 132);
            this.lbxContact.Style = MetroSet_UI.Enums.Style.Light;
            this.lbxContact.StyleManager = null;
            this.lbxContact.TabIndex = 11;
            this.lbxContact.ThemeAuthor = "Narwin";
            this.lbxContact.ThemeName = "MetroLite";
            this.lbxContact.SelectedIndexChanged += new MetroSet_UI.Controls.MetroSetListBox.SelectedIndexChangedEventHandler(this.lbxContact_SelectedIndexChanged);
            // 
            // frmEnvoi
            // 
            this.AllowResize = false;
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 26F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(628, 563);
            this.Controls.Add(this.metroSetLabel5);
            this.Controls.Add(this.lbxContact);
            this.Controls.Add(this.tbxPassword);
            this.Controls.Add(this.metroSetLabel4);
            this.Controls.Add(this.metroSetControlBox1);
            this.Controls.Add(this.metroSetLabel3);
            this.Controls.Add(this.tbxMontant);
            this.Controls.Add(this.metroSetLabel2);
            this.Controls.Add(this.lbxWallets);
            this.Controls.Add(this.btnEnvoyer);
            this.Controls.Add(this.tbxAdresse);
            this.Controls.Add(this.metroSetLabel1);
            this.Name = "frmEnvoi";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Envoi";
            this.Load += new System.EventHandler(this.frmEnvoi_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroSet_UI.Controls.MetroSetLabel metroSetLabel1;
        private MetroSet_UI.Controls.MetroSetTextBox tbxAdresse;
        private MetroSet_UI.Controls.MetroSetButton btnEnvoyer;
        private MetroSet_UI.Controls.MetroSetListBox lbxWallets;
        private MetroSet_UI.Controls.MetroSetLabel metroSetLabel2;
        private MetroSet_UI.Controls.MetroSetTextBox tbxMontant;
        private MetroSet_UI.Controls.MetroSetLabel metroSetLabel3;
        private MetroSet_UI.Controls.MetroSetControlBox metroSetControlBox1;
        private MetroSet_UI.Controls.MetroSetTextBox tbxPassword;
        private MetroSet_UI.Controls.MetroSetLabel metroSetLabel4;
        private MetroSet_UI.Controls.MetroSetLabel metroSetLabel5;
        private MetroSet_UI.Controls.MetroSetListBox lbxContact;
    }
}