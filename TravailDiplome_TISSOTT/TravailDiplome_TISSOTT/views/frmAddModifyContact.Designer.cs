﻿namespace TravailDiplome_TISSOTT
{
    partial class frmAddModifyContact
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbxAddress = new MetroSet_UI.Controls.MetroSetTextBox();
            this.metroSetLabel2 = new MetroSet_UI.Controls.MetroSetLabel();
            this.tbxName = new MetroSet_UI.Controls.MetroSetTextBox();
            this.metroSetLabel1 = new MetroSet_UI.Controls.MetroSetLabel();
            this.btnValidate = new MetroSet_UI.Controls.MetroSetButton();
            this.metroSetControlBox1 = new MetroSet_UI.Controls.MetroSetControlBox();
            this.SuspendLayout();
            // 
            // tbxAddress
            // 
            this.tbxAddress.AutoCompleteCustomSource = null;
            this.tbxAddress.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.tbxAddress.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.tbxAddress.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.tbxAddress.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.tbxAddress.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.tbxAddress.DisabledForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            this.tbxAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.tbxAddress.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.tbxAddress.Image = null;
            this.tbxAddress.IsDerivedStyle = true;
            this.tbxAddress.Lines = null;
            this.tbxAddress.Location = new System.Drawing.Point(72, 247);
            this.tbxAddress.MaxLength = 32767;
            this.tbxAddress.Multiline = false;
            this.tbxAddress.Name = "tbxAddress";
            this.tbxAddress.ReadOnly = false;
            this.tbxAddress.Size = new System.Drawing.Size(192, 30);
            this.tbxAddress.Style = MetroSet_UI.Enums.Style.Light;
            this.tbxAddress.StyleManager = null;
            this.tbxAddress.TabIndex = 10;
            this.tbxAddress.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbxAddress.ThemeAuthor = "Narwin";
            this.tbxAddress.ThemeName = "MetroLite";
            this.tbxAddress.UseSystemPasswordChar = false;
            this.tbxAddress.WatermarkText = "Adresse";
            // 
            // metroSetLabel2
            // 
            this.metroSetLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.metroSetLabel2.IsDerivedStyle = true;
            this.metroSetLabel2.Location = new System.Drawing.Point(118, 212);
            this.metroSetLabel2.Name = "metroSetLabel2";
            this.metroSetLabel2.Size = new System.Drawing.Size(100, 23);
            this.metroSetLabel2.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetLabel2.StyleManager = null;
            this.metroSetLabel2.TabIndex = 9;
            this.metroSetLabel2.Text = "Adresse";
            this.metroSetLabel2.ThemeAuthor = "Narwin";
            this.metroSetLabel2.ThemeName = "MetroLite";
            // 
            // tbxName
            // 
            this.tbxName.AutoCompleteCustomSource = null;
            this.tbxName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.tbxName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.tbxName.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.tbxName.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.tbxName.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.tbxName.DisabledForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            this.tbxName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.tbxName.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.tbxName.Image = null;
            this.tbxName.IsDerivedStyle = true;
            this.tbxName.Lines = null;
            this.tbxName.Location = new System.Drawing.Point(72, 170);
            this.tbxName.MaxLength = 32767;
            this.tbxName.Multiline = false;
            this.tbxName.Name = "tbxName";
            this.tbxName.ReadOnly = false;
            this.tbxName.Size = new System.Drawing.Size(192, 30);
            this.tbxName.Style = MetroSet_UI.Enums.Style.Light;
            this.tbxName.StyleManager = null;
            this.tbxName.TabIndex = 8;
            this.tbxName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbxName.ThemeAuthor = "Narwin";
            this.tbxName.ThemeName = "MetroLite";
            this.tbxName.UseSystemPasswordChar = false;
            this.tbxName.WatermarkText = "Nom";
            // 
            // metroSetLabel1
            // 
            this.metroSetLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.metroSetLabel1.IsDerivedStyle = true;
            this.metroSetLabel1.Location = new System.Drawing.Point(118, 135);
            this.metroSetLabel1.Name = "metroSetLabel1";
            this.metroSetLabel1.Size = new System.Drawing.Size(100, 23);
            this.metroSetLabel1.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetLabel1.StyleManager = null;
            this.metroSetLabel1.TabIndex = 7;
            this.metroSetLabel1.Text = "Nom :";
            this.metroSetLabel1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.metroSetLabel1.ThemeAuthor = "Narwin";
            this.metroSetLabel1.ThemeName = "MetroLite";
            // 
            // btnValidate
            // 
            this.btnValidate.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnValidate.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnValidate.DisabledForeColor = System.Drawing.Color.Gray;
            this.btnValidate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnValidate.HoverBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btnValidate.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btnValidate.HoverTextColor = System.Drawing.Color.White;
            this.btnValidate.IsDerivedStyle = true;
            this.btnValidate.Location = new System.Drawing.Point(76, 289);
            this.btnValidate.Name = "btnValidate";
            this.btnValidate.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnValidate.NormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnValidate.NormalTextColor = System.Drawing.Color.White;
            this.btnValidate.PressBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btnValidate.PressColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btnValidate.PressTextColor = System.Drawing.Color.White;
            this.btnValidate.Size = new System.Drawing.Size(185, 59);
            this.btnValidate.Style = MetroSet_UI.Enums.Style.Light;
            this.btnValidate.StyleManager = null;
            this.btnValidate.TabIndex = 11;
            this.btnValidate.Text = "Valider";
            this.btnValidate.ThemeAuthor = "Narwin";
            this.btnValidate.ThemeName = "MetroLite";
            this.btnValidate.Click += new System.EventHandler(this.btnValidate_Click);
            // 
            // metroSetControlBox1
            // 
            this.metroSetControlBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.metroSetControlBox1.CloseHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.metroSetControlBox1.CloseHoverForeColor = System.Drawing.Color.White;
            this.metroSetControlBox1.CloseNormalForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.DisabledForeColor = System.Drawing.Color.DimGray;
            this.metroSetControlBox1.IsDerivedStyle = true;
            this.metroSetControlBox1.Location = new System.Drawing.Point(221, 17);
            this.metroSetControlBox1.MaximizeBox = true;
            this.metroSetControlBox1.MaximizeHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.metroSetControlBox1.MaximizeHoverForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.MaximizeNormalForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.MinimizeBox = true;
            this.metroSetControlBox1.MinimizeHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.metroSetControlBox1.MinimizeHoverForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.MinimizeNormalForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.Name = "metroSetControlBox1";
            this.metroSetControlBox1.Size = new System.Drawing.Size(100, 25);
            this.metroSetControlBox1.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetControlBox1.StyleManager = null;
            this.metroSetControlBox1.TabIndex = 12;
            this.metroSetControlBox1.Text = "metroSetControlBox1";
            this.metroSetControlBox1.ThemeAuthor = "Narwin";
            this.metroSetControlBox1.ThemeName = "MetroLite";
            // 
            // frmAddModifyContact
            // 
            this.AllowResize = false;
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 26F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(336, 406);
            this.Controls.Add(this.metroSetControlBox1);
            this.Controls.Add(this.btnValidate);
            this.Controls.Add(this.tbxAddress);
            this.Controls.Add(this.metroSetLabel2);
            this.Controls.Add(this.tbxName);
            this.Controls.Add(this.metroSetLabel1);
            this.Name = "frmAddModifyContact";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmAddModifyContact";
            this.ResumeLayout(false);

        }

        #endregion

        private MetroSet_UI.Controls.MetroSetTextBox tbxAddress;
        private MetroSet_UI.Controls.MetroSetLabel metroSetLabel2;
        private MetroSet_UI.Controls.MetroSetTextBox tbxName;
        private MetroSet_UI.Controls.MetroSetLabel metroSetLabel1;
        private MetroSet_UI.Controls.MetroSetButton btnValidate;
        private MetroSet_UI.Controls.MetroSetControlBox metroSetControlBox1;
    }
}