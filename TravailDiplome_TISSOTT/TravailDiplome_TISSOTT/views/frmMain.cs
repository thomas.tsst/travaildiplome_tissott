﻿/* 
 * App : SecuWallet
 * Auteur : Thomas Tissot
 * Org : CFPT
 * Date : 07.06.2022
 * brief : Vue principale permettant à l'utilisateur de visualiser ces différents portefeuilles, ainsi que l’historique de ses transactions et évolutions de ceux-ci.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroSet_UI.Forms;
using Nethereum.Web3;
using Nethereum.RPC.Eth.DTOs;
using System.Net.Http;
using Newtonsoft.Json;
using System.Numerics;
using System.Windows.Forms.DataVisualization.Charting;

namespace TravailDiplome_TISSOTT
{
    public partial class frmMain : MetroSetForm
    {
        AppController app;
        public frmMain(AppController app)
        {
            InitializeComponent();
            this.app = app;

            object response = JwtController.DeserializeResponse(JwtController.GetToken(app.User.Email));
            app.User.Token = response.ToString();


        }
        /// <summary>
        /// Récupère les portefeuilles appartenant à l'utilisateur lors du chargement de cette vue,  et met à jour la vue.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_Load(object sender, EventArgs e)
        {
            app.Bc.KeystoreDirectoryCheck(app.User.Email);
            UpdateView(GetListBoxSelectedIndex());
            UpdateViewAsync();
        }
        /// <summary>
        /// Mise à jour de la vue.
        /// </summary>
        /// <param name="indexListBox"></param>
        private void UpdateView(int indexListBox)
        {
            //Ajoute les portefeuilles dans la listBox.
            if (app.Bc.Wallets.Count > 0)
            {
                lbxWallets.Clear();
                foreach (Wallet wallet in app.Bc.Wallets)
                {
                    lbxWallets.AddItem(wallet.Name);
                }
                lbxWallets.SelectedIndex = indexListBox;
                if (indexListBox != -1)
                {
                    lblNomPortefeuille.Text = app.Bc.Wallets[GetListBoxSelectedIndex()].Name;
                }

            }
            else
            {
                lbxWallets.Clear();
                lbxWallets.AddItem("Aucun portefeuille dispo.");
            }

        }
        /// <summary>
        /// Mise à jours de la vue pour les éléments qui nécéssitent du code asyncrone.
        /// </summary>
        /// <returns></returns>
        private async Task<string> UpdateViewAsync()
        {
            //Récupère le nombre d'Ethereum présent sur le portefeuille sélectionné et l'affiche dans le label.
            var balanceHex = await app.Bc.GetBalanceInETH(app.Bc.Wallets[GetListBoxSelectedIndex()].PublicKey);
            decimal balance = Web3.Convert.FromWei(balanceHex);
            lblMontant.Text = balance.ToString() + " ETH";
            //Récupère les transactions envoyées ou reçues par le portefeuille sélectionné.
            Dictionary<Transaction, string> transactions = await app.Bc.GetAllTransacFromPublicAddress(app.Bc.Wallets[GetListBoxSelectedIndex()].PublicKey);
            dgvTransac.Rows.Clear();

            //Ajout des transactions dans le DataGridView.
            foreach (Transaction item in transactions.Keys)
            {
                string type = "";
                lbxTransactions.Items.Add(item.From + " -> " + item.To + "   Montant :" + Web3.Convert.FromWei(item.Value));
                if (app.Bc.Wallets[GetListBoxSelectedIndex()].PublicKey.ToLower() == item.From)
                {
                    type = "Envoi";
                }
                else
                {
                    type = "Recu";
                }
                DataGridViewRow row = (DataGridViewRow)dgvTransac.Rows[0].Clone();
                row.Cells[0].Value = type;
                row.Cells[1].Value = UnixTimeStampToDateTime(transactions[item]).ToString();
                row.Cells[2].Value = item.From;
                row.Cells[3].Value = item.To;
                row.Cells[4].Value = Web3.Convert.FromWei(item.Value).ToString() + " ETH";
                row.Cells[5].Value = item.TransactionHash;
                dgvTransac.Rows.Add(row);         
            }

            //Récupère le prix actuel de l'Ethereum.
            Price ethValue = await GetEthValue();
            double walletValue = 0;
            //Affichage selon les paramètres de l'utilisateur.
            switch (app.Settings.Currency)
            {
                case "USD":
                    walletValue = ethValue.USD * (double)balance;
                    break;
                case "CHF":
                    walletValue = ethValue.CHF * (double)balance;
                    break;
                case "EUR":
                    walletValue = ethValue.EUR * (double)balance;
                    break;
                default:
                    break;
            }


            //Affichage de la valeur du portefeuille.
            lblMontantFiat.Text = walletValue.ToString("0.##")+ " " + app.Settings.Currency;

            
            Dictionary<string,decimal> balanceValueByTime = new Dictionary<string, decimal>();
            List<decimal> balanceList = new List<decimal>();
            decimal balanceEth;
            //Récupère la quantité d'Ethereum présent sur le portefeuille a chaque bloques.
            var balanceAtBlocks = await app.Bc.GetBalanceAtAllBlocks(app.Bc.Wallets[GetListBoxSelectedIndex()].PublicKey);
            //Récupère l'hisotrique du cours de l'Ethereum.
            Rootobject history = await GetEthValueEveryDay();
            //int dailyTimestamp = 86400;
            int dailyTimestamp = 60;
            //Comparaison de "BalanceAtBlock" et "history" pour récupérer le prix de l'Ethereum à chaque block.
            foreach (string timestamp in balanceAtBlocks.Keys)
            {
                int time = Convert.ToInt32(timestamp);
                for (int i = history.Data.Data.Length-1; i > 0; i--)
                {
                    //Vérification si la date et l'heure du bloc se trouvent dans la journée récupérée sur CryptoCompare API.
                    if (time > history.Data.Data[i].time && time < history.Data.Data[i].time + dailyTimestamp)
                    {
                        BigInteger bigIntFromString = new BigInteger(balanceAtBlocks[timestamp]);
                        balanceEth = Web3.Convert.FromWei(bigIntFromString);
                        balanceList.Add(balanceEth);
                        balanceValueByTime.Add(timestamp, history.Data.Data[i].open * balanceEth);
                        Console.WriteLine("Prix trouvé : " + history.Data.Data[i].open);
                        break;
                    }
                }
            }
            //Modification des propriétés des DataGridView.
            SetUpChart(chartEvolutionValeur);
            SetUpChart(chartEvolutionQuantite);
            //Ajout des points dans le graphique évolution de la quantité d'ethereum disponible sur le portefeuille.
            foreach (string timestamp in balanceValueByTime.Keys)
            {
                DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                dtDateTime = dtDateTime.AddSeconds(Convert.ToDouble(timestamp)).ToLocalTime();
                chartEvolutionValeur.Series[0].Points.AddXY(/*Convert.ToDouble(timestamp)*/dtDateTime, Convert.ToDouble(balanceValueByTime[timestamp]));
                
                chartEvolutionQuantite.Series[0].Points.AddXY(dtDateTime, balanceList[balanceValueByTime.Keys.ToList().IndexOf(timestamp)]);
            }

            //Ajout des points dans le graphique évolution de la valeur.
            /*foreach (var item in history.Data.Data)
            {
                decimal balansadasdce = 0;
                DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                dtDateTime = dtDateTime.AddSeconds(Convert.ToDouble(item.time)).ToLocalTime();
                if (item.time < Convert.ToInt32(balanceAtBlocks.ElementAt(0).Key)) 
                {
                    chartEvolutionValeur.Series[0].Points.AddXY(dtDateTime,0);
                }
                else 
                {
                    foreach (string timestamp in balanceAtBlocks.Keys)
                    {
                        if (Convert.ToInt32(timestamp) > item.time && Convert.ToInt32(timestamp) < item.time + dailyTimestamp)
                        {
                            chartEvolutionValeur.Series[0].Points.AddXY(dtDateTime, Convert.ToDouble(balanceValueByTime[timestamp]));
                            break;
                        }
                        else
                        {
                            chartEvolutionValeur.Series[0].Points.AddXY(dtDateTime, Convert.ToDouble(balanceValueByTime[timestamp]));
                        }
                    }
                }
                
            }*/
                return "ok";
        }
        private void btnSettings_Click(object sender, EventArgs e)
        {
            frmMonCompte frm = new frmMonCompte(app);
            this.Hide();
            frm.Closed += (s, args) => this.Show();
            frm.Show();
        }

        private void btnRecevoir_Click(object sender, EventArgs e)
        {
            frmAdresse frm = new frmAdresse(app.Bc.Wallets[GetListBoxSelectedIndex()].PublicKey);
            this.Hide();
            frm.ShowDialog();
            this.Show();
        }
        /// <summary>
        /// Retourne l'index sélectionné dans la listBox portefeuilles.
        /// </summary>
        /// <returns></returns>
        public int GetListBoxSelectedIndex()
        {
            return lbxWallets.SelectedIndex;
        }

        private void btmEnvoyer_Click(object sender, EventArgs e)
        {
            frmEnvoi frm = new frmEnvoi(app, app.Bc.Wallets[GetListBoxSelectedIndex()]);
            this.Hide();
            frm.Closed += (s, args) => this.Show();
            frm.Show();
        }
        /// <summary>
        /// Met à jour la vue lorsque l'utilisateur sélectionne un portefeuille.
        /// </summary>
        /// <param name="sender"></param>
        private void lbxWallets_SelectedIndexChanged(object sender)
        {
            UpdateView(GetListBoxSelectedIndex());
            UpdateViewAsync();
        }

        /// <summary>
        /// Convertit un timestamp Unix en type DateTime.
        /// </summary>
        /// <param name="unixTimeStamp"></param>
        /// <returns></returns>
        public DateTime UnixTimeStampToDateTime(string unixTimeStamp)
        {
            // Le timestamp Unix est en secondes après l'époque.
            double temp = Convert.ToDouble(unixTimeStamp);
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(temp).ToLocalTime();
            return dtDateTime;
        }
        /// <summary>
        /// Retourne la valeur de l'Ethereum actuel (Récupéré sur CryptoCompare API).
        /// </summary>
        /// <returns></returns>
        public async Task<Price> GetEthValue()
        {
            HttpClient client = new HttpClient();
            var response = await client.GetStringAsync("https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=" + app.Settings.Currency + "&api_key=c87927ed2ee344d1965ccf7e8d2fb4cd88f3def737e46c9e5f91eed27afa5d1b").ConfigureAwait(false);

            Price result = JsonConvert.DeserializeObject<Price>(response);
            

            return result;
        }
        /// <summary>
        /// Retourne l'historique de valeur de l'Ethereum actuel (Récupéré sur CryptoCompare API).
        /// </summary>
        /// <returns></returns>
        public async Task<Rootobject> GetEthValueEveryDay()
        {
            HttpClient client = new HttpClient();
            //var response = await client.GetStringAsync("https://min-api.cryptocompare.com/data/v2/histoday?fsym=ETH&tsym=" + app.Settings.Currency + "&limit=10&api_key=c87927ed2ee344d1965ccf7e8d2fb4cd88f3def737e46c9e5f91eed27afa5d1b").ConfigureAwait(false);
            
                var response = await client.GetStringAsync("https://min-api.cryptocompare.com/data/v2/histominute?fsym=ETH&tsym=" + app.Settings.Currency + "&limit=360&api_key=c87927ed2ee344d1965ccf7e8d2fb4cd88f3def737e46c9e5f91eed27afa5d1b").ConfigureAwait(false);
            Rootobject result = JsonConvert.DeserializeObject<Rootobject>(response);

            return result;
        }
        /// <summary>
        /// Gère les propriétés à modifier dans le DataGridView passé en paramètre.
        /// </summary>
        /// <param name="chart"></param>
        private void SetUpChart(Chart chart)
        {
            chart.Series.Clear();
            chart.ChartAreas.Clear();
            chart.ChartAreas.Add("Area");
            chart.Series.Add("Evolution");
            chart.Series[0].ChartType = SeriesChartType.Line;
            chart.ChartAreas[0].AxisY.IsStartedFromZero = false;
            chart.Series[0].XValueType = ChartValueType.DateTime;
            chart.ChartAreas[0].AxisX.LabelStyle.Format = "yyyy-MM-dd HH:mm";
            chart.ChartAreas[0].AxisX.Interval = 1;
            chart.ChartAreas[0].AxisX.IntervalType = DateTimeIntervalType.Minutes;
            chart.ChartAreas[0].AxisX.ScrollBar.Enabled = true;
            chart.ChartAreas[0].AxisY.Minimum = 0;
            chart.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
            chart.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
            chart.ChartAreas[0].AxisX.LabelStyle.Interval = 10;
        }
        /// <summary>
        /// Structure nécessaire au reçu du prix de l'Ethereum de l'API CryptoCompare.
        /// </summary>
        public class Price
        {
            public double USD { get; set; }
            public double CHF { get; set; }
            public double EUR { get; set; }
        }
        /// <summary>
        /// Sauvegarde les paramètres de l'utilisateur.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            app.SaveSettings();
        }


        public class Rootobject
        {
            public string Response { get; set; }
            public string Message { get; set; }
            public bool HasWarning { get; set; }
            public int Type { get; set; }
            public Ratelimit RateLimit { get; set; }
            public DataInfo Data { get; set; }
        }

        public class Ratelimit
        {
        }

        public class DataInfo
        {
            public bool Aggregated { get; set; }
            public int TimeFrom { get; set; }
            public int TimeTo { get; set; }
            public Datum[] Data { get; set; }
        }

        public class Datum
        {
            public int time { get; set; }
            public float high { get; set; }
            public float low { get; set; }
            public decimal open { get; set; }
            public float volumefrom { get; set; }
            public float volumeto { get; set; }
            public float close { get; set; }
            public string conversionType { get; set; }
            public string conversionSymbol { get; set; }
        }

    }
}
