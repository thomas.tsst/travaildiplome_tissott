﻿namespace TravailDiplome_TISSOTT
{
    partial class frmImportWalletFromFile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnImportFichier = new MetroSet_UI.Controls.MetroSetButton();
            this.btnImportWallet = new MetroSet_UI.Controls.MetroSetButton();
            this.tbxPassword = new MetroSet_UI.Controls.MetroSetTextBox();
            this.metroSetLabel1 = new MetroSet_UI.Controls.MetroSetLabel();
            this.lblNomFichier = new MetroSet_UI.Controls.MetroSetLabel();
            this.metroSetLabel2 = new MetroSet_UI.Controls.MetroSetLabel();
            this.tbxName = new MetroSet_UI.Controls.MetroSetTextBox();
            this.metroSetControlBox1 = new MetroSet_UI.Controls.MetroSetControlBox();
            this.SuspendLayout();
            // 
            // btnImportFichier
            // 
            this.btnImportFichier.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnImportFichier.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnImportFichier.DisabledForeColor = System.Drawing.Color.Gray;
            this.btnImportFichier.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnImportFichier.HoverBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btnImportFichier.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btnImportFichier.HoverTextColor = System.Drawing.Color.White;
            this.btnImportFichier.IsDerivedStyle = true;
            this.btnImportFichier.Location = new System.Drawing.Point(68, 111);
            this.btnImportFichier.Name = "btnImportFichier";
            this.btnImportFichier.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnImportFichier.NormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnImportFichier.NormalTextColor = System.Drawing.Color.White;
            this.btnImportFichier.PressBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btnImportFichier.PressColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btnImportFichier.PressTextColor = System.Drawing.Color.White;
            this.btnImportFichier.Size = new System.Drawing.Size(228, 55);
            this.btnImportFichier.Style = MetroSet_UI.Enums.Style.Light;
            this.btnImportFichier.StyleManager = null;
            this.btnImportFichier.TabIndex = 4;
            this.btnImportFichier.Text = "Importer fichier";
            this.btnImportFichier.ThemeAuthor = "Narwin";
            this.btnImportFichier.ThemeName = "MetroLite";
            this.btnImportFichier.Click += new System.EventHandler(this.btnImportFichier_Click);
            // 
            // btnImportWallet
            // 
            this.btnImportWallet.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnImportWallet.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnImportWallet.DisabledForeColor = System.Drawing.Color.Gray;
            this.btnImportWallet.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnImportWallet.HoverBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btnImportWallet.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btnImportWallet.HoverTextColor = System.Drawing.Color.White;
            this.btnImportWallet.IsDerivedStyle = true;
            this.btnImportWallet.Location = new System.Drawing.Point(68, 337);
            this.btnImportWallet.Name = "btnImportWallet";
            this.btnImportWallet.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnImportWallet.NormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnImportWallet.NormalTextColor = System.Drawing.Color.White;
            this.btnImportWallet.PressBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btnImportWallet.PressColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btnImportWallet.PressTextColor = System.Drawing.Color.White;
            this.btnImportWallet.Size = new System.Drawing.Size(228, 55);
            this.btnImportWallet.Style = MetroSet_UI.Enums.Style.Light;
            this.btnImportWallet.StyleManager = null;
            this.btnImportWallet.TabIndex = 5;
            this.btnImportWallet.Text = "Importer";
            this.btnImportWallet.ThemeAuthor = "Narwin";
            this.btnImportWallet.ThemeName = "MetroLite";
            this.btnImportWallet.Click += new System.EventHandler(this.btnImportWallet_Click);
            // 
            // tbxPassword
            // 
            this.tbxPassword.AutoCompleteCustomSource = null;
            this.tbxPassword.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.tbxPassword.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.tbxPassword.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.tbxPassword.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.tbxPassword.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.tbxPassword.DisabledForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            this.tbxPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.tbxPassword.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.tbxPassword.Image = null;
            this.tbxPassword.IsDerivedStyle = true;
            this.tbxPassword.Lines = null;
            this.tbxPassword.Location = new System.Drawing.Point(68, 233);
            this.tbxPassword.MaxLength = 32767;
            this.tbxPassword.Multiline = false;
            this.tbxPassword.Name = "tbxPassword";
            this.tbxPassword.ReadOnly = false;
            this.tbxPassword.Size = new System.Drawing.Size(228, 30);
            this.tbxPassword.Style = MetroSet_UI.Enums.Style.Light;
            this.tbxPassword.StyleManager = null;
            this.tbxPassword.TabIndex = 6;
            this.tbxPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbxPassword.ThemeAuthor = "Narwin";
            this.tbxPassword.ThemeName = "MetroLite";
            this.tbxPassword.UseSystemPasswordChar = false;
            this.tbxPassword.WatermarkText = "Mot de passe";
            // 
            // metroSetLabel1
            // 
            this.metroSetLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.metroSetLabel1.IsDerivedStyle = true;
            this.metroSetLabel1.Location = new System.Drawing.Point(116, 203);
            this.metroSetLabel1.Name = "metroSetLabel1";
            this.metroSetLabel1.Size = new System.Drawing.Size(132, 23);
            this.metroSetLabel1.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetLabel1.StyleManager = null;
            this.metroSetLabel1.TabIndex = 7;
            this.metroSetLabel1.Text = "Mot de passe :";
            this.metroSetLabel1.ThemeAuthor = "Narwin";
            this.metroSetLabel1.ThemeName = "MetroLite";
            // 
            // lblNomFichier
            // 
            this.lblNomFichier.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblNomFichier.IsDerivedStyle = true;
            this.lblNomFichier.Location = new System.Drawing.Point(40, 173);
            this.lblNomFichier.Name = "lblNomFichier";
            this.lblNomFichier.Size = new System.Drawing.Size(285, 23);
            this.lblNomFichier.Style = MetroSet_UI.Enums.Style.Light;
            this.lblNomFichier.StyleManager = null;
            this.lblNomFichier.TabIndex = 8;
            this.lblNomFichier.ThemeAuthor = "Narwin";
            this.lblNomFichier.ThemeName = "MetroLite";
            // 
            // metroSetLabel2
            // 
            this.metroSetLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.metroSetLabel2.IsDerivedStyle = true;
            this.metroSetLabel2.Location = new System.Drawing.Point(88, 270);
            this.metroSetLabel2.Name = "metroSetLabel2";
            this.metroSetLabel2.Size = new System.Drawing.Size(189, 23);
            this.metroSetLabel2.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetLabel2.StyleManager = null;
            this.metroSetLabel2.TabIndex = 10;
            this.metroSetLabel2.Text = "Nom du portefeuille :";
            this.metroSetLabel2.ThemeAuthor = "Narwin";
            this.metroSetLabel2.ThemeName = "MetroLite";
            // 
            // tbxName
            // 
            this.tbxName.AutoCompleteCustomSource = null;
            this.tbxName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.tbxName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.tbxName.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.tbxName.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.tbxName.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.tbxName.DisabledForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            this.tbxName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.tbxName.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.tbxName.Image = null;
            this.tbxName.IsDerivedStyle = true;
            this.tbxName.Lines = null;
            this.tbxName.Location = new System.Drawing.Point(68, 300);
            this.tbxName.MaxLength = 32767;
            this.tbxName.Multiline = false;
            this.tbxName.Name = "tbxName";
            this.tbxName.ReadOnly = false;
            this.tbxName.Size = new System.Drawing.Size(228, 30);
            this.tbxName.Style = MetroSet_UI.Enums.Style.Light;
            this.tbxName.StyleManager = null;
            this.tbxName.TabIndex = 9;
            this.tbxName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbxName.ThemeAuthor = "Narwin";
            this.tbxName.ThemeName = "MetroLite";
            this.tbxName.UseSystemPasswordChar = false;
            this.tbxName.WatermarkText = "Nom du portefeuille ";
            // 
            // metroSetControlBox1
            // 
            this.metroSetControlBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.metroSetControlBox1.CloseHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.metroSetControlBox1.CloseHoverForeColor = System.Drawing.Color.White;
            this.metroSetControlBox1.CloseNormalForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.DisabledForeColor = System.Drawing.Color.DimGray;
            this.metroSetControlBox1.IsDerivedStyle = true;
            this.metroSetControlBox1.Location = new System.Drawing.Point(250, 25);
            this.metroSetControlBox1.MaximizeBox = true;
            this.metroSetControlBox1.MaximizeHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.metroSetControlBox1.MaximizeHoverForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.MaximizeNormalForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.MinimizeBox = true;
            this.metroSetControlBox1.MinimizeHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.metroSetControlBox1.MinimizeHoverForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.MinimizeNormalForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.Name = "metroSetControlBox1";
            this.metroSetControlBox1.Size = new System.Drawing.Size(100, 25);
            this.metroSetControlBox1.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetControlBox1.StyleManager = null;
            this.metroSetControlBox1.TabIndex = 11;
            this.metroSetControlBox1.Text = "metroSetControlBox1";
            this.metroSetControlBox1.ThemeAuthor = "Narwin";
            this.metroSetControlBox1.ThemeName = "MetroLite";
            // 
            // frmImportWalletFromFile
            // 
            this.AllowResize = false;
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 26F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(365, 435);
            this.Controls.Add(this.metroSetControlBox1);
            this.Controls.Add(this.metroSetLabel2);
            this.Controls.Add(this.tbxName);
            this.Controls.Add(this.lblNomFichier);
            this.Controls.Add(this.metroSetLabel1);
            this.Controls.Add(this.tbxPassword);
            this.Controls.Add(this.btnImportWallet);
            this.Controls.Add(this.btnImportFichier);
            this.Name = "frmImportWalletFromFile";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Import (Fichier Json)";
            this.ResumeLayout(false);

        }

        #endregion

        private MetroSet_UI.Controls.MetroSetButton btnImportFichier;
        private MetroSet_UI.Controls.MetroSetButton btnImportWallet;
        private MetroSet_UI.Controls.MetroSetTextBox tbxPassword;
        private MetroSet_UI.Controls.MetroSetLabel metroSetLabel1;
        private MetroSet_UI.Controls.MetroSetLabel lblNomFichier;
        private MetroSet_UI.Controls.MetroSetLabel metroSetLabel2;
        private MetroSet_UI.Controls.MetroSetTextBox tbxName;
        private MetroSet_UI.Controls.MetroSetControlBox metroSetControlBox1;
    }
}