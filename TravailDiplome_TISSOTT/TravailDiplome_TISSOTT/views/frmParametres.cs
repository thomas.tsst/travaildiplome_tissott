﻿/* 
 * App : SecuWallet
 * Auteur : Thomas Tissot
 * Org : CFPT
 * Date : 07.06.2022
 * brief : Vue qui permet à l'utilisateur de changer de devise, d'accéder à la vue frmContact et frmAbout.
 */
using MetroSet_UI.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TravailDiplome_TISSOTT
{
    public partial class frmParametres : MetroSetForm
    {
        AppController app;
        public frmParametres(AppController app)
        {
            InitializeComponent();
            this.app = app;
        }
        /// <summary>
        /// Code appelé lors du changement de l'index de la listBox Devise.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbxDevise_SelectedIndexChanged(object sender, EventArgs e)
        {
            app.Settings.Currency = cbxDevise.SelectedItem.ToString();
        }
        /// <summary>
        /// Redirige sur la vue frmContact.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnContacts_Click(object sender, EventArgs e)
        {
            frmContact frm = new frmContact(app);
            this.Hide();
            frm.Closed += (s, args) => this.Close();
            frm.Show();
        }
    }
}
