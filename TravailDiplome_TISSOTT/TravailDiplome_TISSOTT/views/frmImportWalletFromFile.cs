﻿/* 
 * App : SecuWallet
 * Auteur : Thomas Tissot
 * Org : CFPT
 * Date : 07.06.2022
 * brief : Vue permettant à l'utilisateur d'importer un portefeuille à l'aide d'un fichier “JSON”.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroSet_UI.Forms;

namespace TravailDiplome_TISSOTT
{
    public partial class frmImportWalletFromFile : MetroSetForm
    {
        AppController app;
        private string path;
        public frmImportWalletFromFile(AppController app)
        {
            InitializeComponent();
            this.app = app;
        }
        /// <summary>
        /// Action de cliquer sur le bouton permettant d'importer le fichier "JSON".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnImportFichier_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog())
            {
                ofd.Filter = "Json files (*.json)|*.json";
                ofd.Multiselect = false;
                ofd.CheckFileExists = true;
                ofd.CheckPathExists = true;
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    path = ofd.FileName;
                    lblNomFichier.Text = path;

                }
            }
        }
        /// <summary>
        /// Action de clique sur le bouton "importer".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnImportWallet_Click(object sender, EventArgs e)
        {
            if (tbxPassword.Text != "" && path != "")
            {
                app.Bc.ImportAccountWithFile(tbxPassword.Text, path, tbxName.Text, app.User.Email);
            }
        }
    }
}
