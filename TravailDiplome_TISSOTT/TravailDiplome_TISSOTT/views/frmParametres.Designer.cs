﻿namespace TravailDiplome_TISSOTT
{
    partial class frmParametres
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroSetLabel1 = new MetroSet_UI.Controls.MetroSetLabel();
            this.lbxDevise = new MetroSet_UI.Controls.MetroSetListBox();
            this.cbxDevise = new MetroSet_UI.Controls.MetroSetComboBox();
            this.btnContacts = new MetroSet_UI.Controls.MetroSetButton();
            this.btnAPropos = new MetroSet_UI.Controls.MetroSetButton();
            this.metroSetControlBox1 = new MetroSet_UI.Controls.MetroSetControlBox();
            this.SuspendLayout();
            // 
            // metroSetLabel1
            // 
            this.metroSetLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.metroSetLabel1.IsDerivedStyle = true;
            this.metroSetLabel1.Location = new System.Drawing.Point(16, 104);
            this.metroSetLabel1.Name = "metroSetLabel1";
            this.metroSetLabel1.Size = new System.Drawing.Size(100, 23);
            this.metroSetLabel1.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetLabel1.StyleManager = null;
            this.metroSetLabel1.TabIndex = 0;
            this.metroSetLabel1.Text = "Devise";
            this.metroSetLabel1.ThemeAuthor = "Narwin";
            this.metroSetLabel1.ThemeName = "MetroLite";
            // 
            // lbxDevise
            // 
            this.lbxDevise.BackColor = System.Drawing.Color.White;
            this.lbxDevise.BorderColor = System.Drawing.Color.LightGray;
            this.lbxDevise.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.lbxDevise.DisabledForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            this.lbxDevise.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lbxDevise.HoveredItemBackColor = System.Drawing.Color.LightGray;
            this.lbxDevise.HoveredItemColor = System.Drawing.Color.DimGray;
            this.lbxDevise.IsDerivedStyle = true;
            this.lbxDevise.ItemHeight = 30;
            this.lbxDevise.Location = new System.Drawing.Point(106, 104);
            this.lbxDevise.MultiSelect = false;
            this.lbxDevise.Name = "lbxDevise";
            this.lbxDevise.SelectedIndex = -1;
            this.lbxDevise.SelectedItem = null;
            this.lbxDevise.SelectedItemBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.lbxDevise.SelectedItemColor = System.Drawing.Color.White;
            this.lbxDevise.SelectedText = null;
            this.lbxDevise.SelectedValue = null;
            this.lbxDevise.ShowBorder = false;
            this.lbxDevise.ShowScrollBar = false;
            this.lbxDevise.Size = new System.Drawing.Size(170, 23);
            this.lbxDevise.Style = MetroSet_UI.Enums.Style.Light;
            this.lbxDevise.StyleManager = null;
            this.lbxDevise.TabIndex = 1;
            this.lbxDevise.ThemeAuthor = "Narwin";
            this.lbxDevise.ThemeName = "MetroLite";
            // 
            // cbxDevise
            // 
            this.cbxDevise.AllowDrop = true;
            this.cbxDevise.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(150)))), ((int)(((byte)(150)))));
            this.cbxDevise.BackColor = System.Drawing.Color.Transparent;
            this.cbxDevise.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.cbxDevise.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(150)))), ((int)(((byte)(150)))));
            this.cbxDevise.CausesValidation = false;
            this.cbxDevise.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.cbxDevise.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.cbxDevise.DisabledForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            this.cbxDevise.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbxDevise.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxDevise.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.cbxDevise.FormattingEnabled = true;
            this.cbxDevise.IsDerivedStyle = true;
            this.cbxDevise.ItemHeight = 20;
            this.cbxDevise.Items.AddRange(new object[] {
            "USD",
            "CHF",
            "EUR"});
            this.cbxDevise.Location = new System.Drawing.Point(91, 101);
            this.cbxDevise.Name = "cbxDevise";
            this.cbxDevise.SelectedItemBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.cbxDevise.SelectedItemForeColor = System.Drawing.Color.White;
            this.cbxDevise.Size = new System.Drawing.Size(169, 26);
            this.cbxDevise.Style = MetroSet_UI.Enums.Style.Light;
            this.cbxDevise.StyleManager = null;
            this.cbxDevise.TabIndex = 2;
            this.cbxDevise.ThemeAuthor = "Narwin";
            this.cbxDevise.ThemeName = "MetroLite";
            this.cbxDevise.SelectedIndexChanged += new System.EventHandler(this.cbxDevise_SelectedIndexChanged);
            // 
            // btnContacts
            // 
            this.btnContacts.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnContacts.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnContacts.DisabledForeColor = System.Drawing.Color.Gray;
            this.btnContacts.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnContacts.HoverBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btnContacts.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btnContacts.HoverTextColor = System.Drawing.Color.White;
            this.btnContacts.IsDerivedStyle = true;
            this.btnContacts.Location = new System.Drawing.Point(68, 148);
            this.btnContacts.Name = "btnContacts";
            this.btnContacts.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnContacts.NormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnContacts.NormalTextColor = System.Drawing.Color.White;
            this.btnContacts.PressBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btnContacts.PressColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btnContacts.PressTextColor = System.Drawing.Color.White;
            this.btnContacts.Size = new System.Drawing.Size(194, 65);
            this.btnContacts.Style = MetroSet_UI.Enums.Style.Light;
            this.btnContacts.StyleManager = null;
            this.btnContacts.TabIndex = 3;
            this.btnContacts.Text = "Contacts";
            this.btnContacts.ThemeAuthor = "Narwin";
            this.btnContacts.ThemeName = "MetroLite";
            this.btnContacts.Click += new System.EventHandler(this.btnContacts_Click);
            // 
            // btnAPropos
            // 
            this.btnAPropos.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnAPropos.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnAPropos.DisabledForeColor = System.Drawing.Color.Gray;
            this.btnAPropos.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnAPropos.HoverBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btnAPropos.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btnAPropos.HoverTextColor = System.Drawing.Color.White;
            this.btnAPropos.IsDerivedStyle = true;
            this.btnAPropos.Location = new System.Drawing.Point(68, 234);
            this.btnAPropos.Name = "btnAPropos";
            this.btnAPropos.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnAPropos.NormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnAPropos.NormalTextColor = System.Drawing.Color.White;
            this.btnAPropos.PressBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btnAPropos.PressColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btnAPropos.PressTextColor = System.Drawing.Color.White;
            this.btnAPropos.Size = new System.Drawing.Size(194, 65);
            this.btnAPropos.Style = MetroSet_UI.Enums.Style.Light;
            this.btnAPropos.StyleManager = null;
            this.btnAPropos.TabIndex = 4;
            this.btnAPropos.Text = " A propos";
            this.btnAPropos.ThemeAuthor = "Narwin";
            this.btnAPropos.ThemeName = "MetroLite";
            // 
            // metroSetControlBox1
            // 
            this.metroSetControlBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.metroSetControlBox1.CloseHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.metroSetControlBox1.CloseHoverForeColor = System.Drawing.Color.White;
            this.metroSetControlBox1.CloseNormalForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.DisabledForeColor = System.Drawing.Color.DimGray;
            this.metroSetControlBox1.IsDerivedStyle = true;
            this.metroSetControlBox1.Location = new System.Drawing.Point(216, 11);
            this.metroSetControlBox1.MaximizeBox = true;
            this.metroSetControlBox1.MaximizeHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.metroSetControlBox1.MaximizeHoverForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.MaximizeNormalForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.MinimizeBox = true;
            this.metroSetControlBox1.MinimizeHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.metroSetControlBox1.MinimizeHoverForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.MinimizeNormalForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.Name = "metroSetControlBox1";
            this.metroSetControlBox1.Size = new System.Drawing.Size(100, 25);
            this.metroSetControlBox1.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetControlBox1.StyleManager = null;
            this.metroSetControlBox1.TabIndex = 5;
            this.metroSetControlBox1.Text = "metroSetControlBox1";
            this.metroSetControlBox1.ThemeAuthor = "Narwin";
            this.metroSetControlBox1.ThemeName = "MetroLite";
            // 
            // frmParametres
            // 
            this.AllowResize = false;
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 26F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(331, 354);
            this.Controls.Add(this.metroSetControlBox1);
            this.Controls.Add(this.btnAPropos);
            this.Controls.Add(this.btnContacts);
            this.Controls.Add(this.cbxDevise);
            this.Controls.Add(this.lbxDevise);
            this.Controls.Add(this.metroSetLabel1);
            this.Name = "frmParametres";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Paramètres";
            this.ResumeLayout(false);

        }

        #endregion

        private MetroSet_UI.Controls.MetroSetLabel metroSetLabel1;
        private MetroSet_UI.Controls.MetroSetListBox lbxDevise;
        private MetroSet_UI.Controls.MetroSetComboBox cbxDevise;
        private MetroSet_UI.Controls.MetroSetButton btnContacts;
        private MetroSet_UI.Controls.MetroSetButton btnAPropos;
        private MetroSet_UI.Controls.MetroSetControlBox metroSetControlBox1;
    }
}