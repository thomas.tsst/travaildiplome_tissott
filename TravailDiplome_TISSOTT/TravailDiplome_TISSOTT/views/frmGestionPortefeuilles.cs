﻿/* 
 * App : SecuWallet
 * Auteur : Thomas Tissot
 * Org : CFPT
 * Date : 07.06.2022
 * brief : Vue permettant à l'utilisateur de créer, importer et supprimer des portefeuilles.
 */
using MetroSet_UI.Forms;
using Nethereum.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TravailDiplome_TISSOTT
{
    public partial class frmGestionPortefeuilles : MetroSetForm
    {
        AppController app;
        public frmGestionPortefeuilles(AppController app)
        {
            InitializeComponent();
            this.app = app;
        }
        /// <summary>
        /// Génère un nouveau portefeuille.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCreerPortefeuille_Click(object sender, EventArgs e)
        {
            if (tbxNewWalletName.Text != "" && tbxPassword.Text.Length >= 6)
            {
                app.Bc.GenerateJSONFile(tbxPassword.Text, tbxNewWalletName.Text, app.User.Email);
            }
            else
            {
                MessageBox.Show("Nom du portefeuille vide et/ou mot de passe inférieur a 6 caractères.");
            }
        }
        /// <summary>
        /// Mise à jour de la vue.
        /// </summary>
        private void UpdateView()
        {
            if (app.Bc.Wallets.Count > 0)
            {
                lbxWallets.Clear();
                foreach (Wallet wallet in app.Bc.Wallets)
                {
                    lbxWallets.AddItem(wallet.Name);
                }
            }
            else
            {
                lbxWallets.Clear();
                lbxWallets.AddItem("Aucun portefeuille dispo.");
            }

        }

        private void btnImporterCle_Click(object sender, EventArgs e)
        {


        }
        /// <summary>
        /// Importation d'un portefeuille par clé privée.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnImportPrivateKey_Click(object sender, EventArgs e)
        {
            if (tbxNewWalletName.Text != "" && tbxPassword.Text.Length >= 6)
            {
                app.Bc.GenerateJSONFile(tbxPassword.Text, tbxNewWalletName.Text, app.User.Email);
            }
            else
            {
                MessageBox.Show("Nom du portefeuille vide et/ou mot de passe inférieur a 6 caractères.");
            }
        }
        /// <summary>
        /// Action de cliquer sur le bouton importer avec clé privée.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnImportPrivateKey_Click_1(object sender, EventArgs e)
        {
            frmImportWalletFromKey frm = new frmImportWalletFromKey(app);
            this.Hide();
            frm.Closed += (s, args) => this.Close();
            frm.Show();
        }
        /// <summary>
        /// Action de cliquer sur le bouton importer avec fichier "JSON".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnImportJson_Click(object sender, EventArgs e)
        {
            frmImportWalletFromFile frm = new frmImportWalletFromFile(app);
            this.Hide();
            frm.Closed += (s, args) => this.Close();
            frm.Show();
        }
        /// <summary>
        /// Mise à jour de la vue lors du chargement de ce formulaire.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmGestionPortefeuilles_Load(object sender, EventArgs e)
        {
            UpdateView();
        }
        /// <summary>
        /// Suppression du portefeuille selectionné dans la liste des portefeuilles.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSupprimer_Click(object sender, EventArgs e)
        {
            string AuthenticatorResult = app.ShowAuthenticatorDialogBox(this);
            
            if (AuthenticatorResult == "ok")
            {
                int index = GetListBoxSelectedIndex();
                //Vérification si un portefeuille est bien selectionné.
                if (index != -1)
                {
                    app.Bc.DeleteWallet(index, app.User.Email);
                    UpdateView();
                }
                else
                {
                    MessageBox.Show("Aucun portefeuille sélectionné");
                }
            }
        }
        /// <summary>
        /// Retourne l'index sélectionné dans la liste des portefeuilles.
        /// </summary>
        /// <returns></returns>
        public int GetListBoxSelectedIndex()
        {
            return lbxWallets.SelectedIndex;
        }

    }
}
