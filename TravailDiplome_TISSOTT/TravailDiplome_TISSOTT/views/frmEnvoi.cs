﻿/* 
 * App : SecuWallet
 * Auteur : Thomas Tissot
 * Org : CFPT
 * Date : 07.06.2022
 * brief : Vue permettant à l'utilisateur de créer des transactions sur la blockchain à  l’adresse publique de son choix, ses portefeuilles personnels ou les contacts préalablement enregistrés.
 */
using MetroSet_UI.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;
namespace TravailDiplome_TISSOTT
{
    public partial class frmEnvoi : MetroSetForm
    {
        AppController app;
        Wallet wallet;
        public frmEnvoi(AppController app, Wallet wallet)
        {
            InitializeComponent();
            this.Text = "Envoi depuis : " + wallet.Name;
            this.app = app;
            this.wallet = wallet;
         }

        private void btnEnvoyer_Click(object sender, EventArgs e)
        {
            if (tbxAdresse.Text != "" && tbxMontant.Text != "")
            {
                //Vérification du Google Authenticator.
                string AuthenticatorResult = app.ShowAuthenticatorDialogBox(this);
                if (AuthenticatorResult == "ok")
                {
                    string key = app.Bc.DecryptJSONFile(tbxPassword.Text, wallet.Path);
                    app.Bc.CreateTransaction(key, tbxAdresse.Text, Convert.ToDecimal(tbxMontant.Text));
                    MessageBox.Show("Transaction effectuée");
                    //MetroSetMessageBox.Show(this, "Transaction effectuée");
                }
            }
            else
            {
                MessageBox.Show("Veuillez remplir tout les champs du formulaire");
            }
        }
        /// <summary>
        /// Mise à jour de la vue lors de son ouverture.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmEnvoi_Load(object sender, EventArgs e)
        {
            UpdateView();
        }

        /// <summary>
        /// Met à jour la vue.
        /// </summary>
        public void UpdateView()
        {
            if (app.Bc.Wallets.Count > 0)
            {
                lbxWallets.Clear();
                foreach (Wallet wallet in app.Bc.Wallets)
                {
                    lbxWallets.AddItem(wallet.Name);
                }
            }
            else
            {
                lbxWallets.Clear();
                lbxWallets.AddItem("Aucun portefeuille dispo.");
            }

            if (app.Settings.Contacts.Count > 0)
            {
                lbxContact.Clear();
                foreach (Contact contact in app.Settings.Contacts)
                {
                    lbxContact.AddItem(contact.Name);
                }
            }
            else
            {
                lbxContact.Clear();
                lbxContact.AddItem("Aucun contact dispo.");
            }
        }
        /// <summary>
        /// Ajout automatique de l'adresse publique lorsque l'utilisateur clique sur un de ces portefeuilles.
        /// </summary>
        /// <param name="sender"></param>
        private void lbxWallets_SelectedIndexChanged(object sender)
        {
            tbxAdresse.Text = app.Bc.GetPublicAddressWithWalletIndex(GetListBoxWalletSelectedIndex());

        }

        /// <summary>
        /// Retourne l'index sélectionné dans la listBox portefeuilles.
        /// </summary>
        /// <returns></returns>
        public int GetListBoxWalletSelectedIndex()
        {
            return lbxWallets.SelectedIndex;
        }
        /// <summary>
        /// Retourne l'index sélectionné dans la listBox contacts.
        /// </summary>
        /// <returns></returns>
        public int GetListBoxContactSelectedIndex()
        {
            return lbxContact.SelectedIndex;
        }
        /// <summary>
        /// Ajout automatique de l'adresse publique lorsque l'utilisateur clique sur un de ces contacts.
        /// </summary>
        /// <param name="sender"></param>
        private void lbxContact_SelectedIndexChanged(object sender)
        {
            tbxAdresse.Text = app.Settings.GetContactPublicKeyWithIndex(GetListBoxContactSelectedIndex());
        }

    }
}
