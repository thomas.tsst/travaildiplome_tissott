﻿/* 
 * App : SecuWallet
 * Auteur : Thomas Tissot
 * Org : CFPT
 * Date : 07.06.2022
 * brief : Vue permettant à l'utilisateur d'importer un portefeuille à l'aide d'une clé privée.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroSet_UI.Forms;

namespace TravailDiplome_TISSOTT
{
    public partial class frmImportWalletFromKey : MetroSetForm
    {
        AppController app;
        public frmImportWalletFromKey(AppController app)
        {
            InitializeComponent();
            this.app = app;
        }
        /// <summary>
        /// Action de cliquer sur le bouton "Importer"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnImport_Click(object sender, EventArgs e)
        {
            if(tbxPrivateKey.Text != "" && tbxPassword.Text != "" && tbxNom.Text != "")
            {
                app.Bc.GenerateJSONFile(tbxPassword.Text, tbxNom.Text, app.User.Email, app.Bc.GetAccountFromPrivateKey(tbxPrivateKey.Text));
            }
        }
    }
}
