﻿namespace TravailDiplome_TISSOTT
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea9 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea10 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend5 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea11 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea12 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend6 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            this.lbxWallets = new MetroSet_UI.Controls.MetroSetListBox();
            this.lblNomPortefeuille = new MetroSet_UI.Controls.MetroSetLabel();
            this.lblMontant = new MetroSet_UI.Controls.MetroSetLabel();
            this.lblMontantFiat = new MetroSet_UI.Controls.MetroSetLabel();
            this.btnRecevoir = new MetroSet_UI.Controls.MetroSetButton();
            this.btmEnvoyer = new MetroSet_UI.Controls.MetroSetButton();
            this.metroSetControlBox1 = new MetroSet_UI.Controls.MetroSetControlBox();
            this.Historique = new MetroSet_UI.Controls.MetroSetTabControl();
            this.metroSetSetTabPage1 = new MetroSet_UI.Child.MetroSetSetTabPage();
            this.dgvTransac = new System.Windows.Forms.DataGridView();
            this.Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.De = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.A = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Montant = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TransactionHash = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lbxTransactions = new MetroSet_UI.Controls.MetroSetListBox();
            this.metroSetSetTabPage2 = new MetroSet_UI.Child.MetroSetSetTabPage();
            this.chartEvolutionValeur = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.metroSetSetTabPage3 = new MetroSet_UI.Child.MetroSetSetTabPage();
            this.chartEvolutionQuantite = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.btnSettings = new MetroSet_UI.Controls.MetroSetEllipse();
            this.transactionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.Historique.SuspendLayout();
            this.metroSetSetTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTransac)).BeginInit();
            this.metroSetSetTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartEvolutionValeur)).BeginInit();
            this.metroSetSetTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartEvolutionQuantite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.transactionBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // lbxWallets
            // 
            this.lbxWallets.BackColor = System.Drawing.Color.White;
            this.lbxWallets.BorderColor = System.Drawing.Color.LightGray;
            this.lbxWallets.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.lbxWallets.DisabledForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            this.lbxWallets.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lbxWallets.HoveredItemBackColor = System.Drawing.Color.LightGray;
            this.lbxWallets.HoveredItemColor = System.Drawing.Color.DimGray;
            this.lbxWallets.IsDerivedStyle = true;
            this.lbxWallets.ItemHeight = 30;
            this.lbxWallets.Items.Add("Aucun portefeuille dispo.");
            this.lbxWallets.Location = new System.Drawing.Point(15, 99);
            this.lbxWallets.MultiSelect = false;
            this.lbxWallets.Name = "lbxWallets";
            this.lbxWallets.SelectedIndex = -1;
            this.lbxWallets.SelectedItem = null;
            this.lbxWallets.SelectedItemBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.lbxWallets.SelectedItemColor = System.Drawing.Color.White;
            this.lbxWallets.SelectedText = null;
            this.lbxWallets.SelectedValue = null;
            this.lbxWallets.ShowBorder = true;
            this.lbxWallets.ShowScrollBar = true;
            this.lbxWallets.Size = new System.Drawing.Size(204, 225);
            this.lbxWallets.Style = MetroSet_UI.Enums.Style.Light;
            this.lbxWallets.StyleManager = null;
            this.lbxWallets.TabIndex = 0;
            this.lbxWallets.ThemeAuthor = "Narwin";
            this.lbxWallets.ThemeName = "MetroLite";
            this.lbxWallets.SelectedIndexChanged += new MetroSet_UI.Controls.MetroSetListBox.SelectedIndexChangedEventHandler(this.lbxWallets_SelectedIndexChanged);
            // 
            // lblNomPortefeuille
            // 
            this.lblNomPortefeuille.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblNomPortefeuille.IsDerivedStyle = true;
            this.lblNomPortefeuille.Location = new System.Drawing.Point(296, 99);
            this.lblNomPortefeuille.Name = "lblNomPortefeuille";
            this.lblNomPortefeuille.Size = new System.Drawing.Size(210, 23);
            this.lblNomPortefeuille.Style = MetroSet_UI.Enums.Style.Light;
            this.lblNomPortefeuille.StyleManager = null;
            this.lblNomPortefeuille.TabIndex = 1;
            this.lblNomPortefeuille.Text = "Nom portefeuille";
            this.lblNomPortefeuille.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblNomPortefeuille.ThemeAuthor = "Narwin";
            this.lblNomPortefeuille.ThemeName = "MetroLite";
            // 
            // lblMontant
            // 
            this.lblMontant.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblMontant.IsDerivedStyle = true;
            this.lblMontant.Location = new System.Drawing.Point(296, 190);
            this.lblMontant.Name = "lblMontant";
            this.lblMontant.Size = new System.Drawing.Size(210, 23);
            this.lblMontant.Style = MetroSet_UI.Enums.Style.Light;
            this.lblMontant.StyleManager = null;
            this.lblMontant.TabIndex = 2;
            this.lblMontant.Text = "0.1 ETH";
            this.lblMontant.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblMontant.ThemeAuthor = "Narwin";
            this.lblMontant.ThemeName = "MetroLite";
            // 
            // lblMontantFiat
            // 
            this.lblMontantFiat.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblMontantFiat.IsDerivedStyle = true;
            this.lblMontantFiat.Location = new System.Drawing.Point(296, 226);
            this.lblMontantFiat.Name = "lblMontantFiat";
            this.lblMontantFiat.Size = new System.Drawing.Size(210, 23);
            this.lblMontantFiat.Style = MetroSet_UI.Enums.Style.Light;
            this.lblMontantFiat.StyleManager = null;
            this.lblMontantFiat.TabIndex = 3;
            this.lblMontantFiat.Text = "300$";
            this.lblMontantFiat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblMontantFiat.ThemeAuthor = "Narwin";
            this.lblMontantFiat.ThemeName = "MetroLite";
            // 
            // btnRecevoir
            // 
            this.btnRecevoir.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnRecevoir.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnRecevoir.DisabledForeColor = System.Drawing.Color.Gray;
            this.btnRecevoir.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnRecevoir.HoverBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btnRecevoir.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btnRecevoir.HoverTextColor = System.Drawing.Color.White;
            this.btnRecevoir.IsDerivedStyle = true;
            this.btnRecevoir.Location = new System.Drawing.Point(239, 285);
            this.btnRecevoir.Name = "btnRecevoir";
            this.btnRecevoir.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnRecevoir.NormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnRecevoir.NormalTextColor = System.Drawing.Color.White;
            this.btnRecevoir.PressBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btnRecevoir.PressColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btnRecevoir.PressTextColor = System.Drawing.Color.White;
            this.btnRecevoir.Size = new System.Drawing.Size(156, 39);
            this.btnRecevoir.Style = MetroSet_UI.Enums.Style.Light;
            this.btnRecevoir.StyleManager = null;
            this.btnRecevoir.TabIndex = 4;
            this.btnRecevoir.Text = "Recevoir";
            this.btnRecevoir.ThemeAuthor = "Narwin";
            this.btnRecevoir.ThemeName = "MetroLite";
            this.btnRecevoir.Click += new System.EventHandler(this.btnRecevoir_Click);
            // 
            // btmEnvoyer
            // 
            this.btmEnvoyer.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btmEnvoyer.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btmEnvoyer.DisabledForeColor = System.Drawing.Color.Gray;
            this.btmEnvoyer.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btmEnvoyer.HoverBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btmEnvoyer.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btmEnvoyer.HoverTextColor = System.Drawing.Color.White;
            this.btmEnvoyer.IsDerivedStyle = true;
            this.btmEnvoyer.Location = new System.Drawing.Point(421, 285);
            this.btmEnvoyer.Name = "btmEnvoyer";
            this.btmEnvoyer.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btmEnvoyer.NormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btmEnvoyer.NormalTextColor = System.Drawing.Color.White;
            this.btmEnvoyer.PressBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btmEnvoyer.PressColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btmEnvoyer.PressTextColor = System.Drawing.Color.White;
            this.btmEnvoyer.Size = new System.Drawing.Size(143, 39);
            this.btmEnvoyer.Style = MetroSet_UI.Enums.Style.Light;
            this.btmEnvoyer.StyleManager = null;
            this.btmEnvoyer.TabIndex = 5;
            this.btmEnvoyer.Text = "Envoyer";
            this.btmEnvoyer.ThemeAuthor = "Narwin";
            this.btmEnvoyer.ThemeName = "MetroLite";
            this.btmEnvoyer.Click += new System.EventHandler(this.btmEnvoyer_Click);
            // 
            // metroSetControlBox1
            // 
            this.metroSetControlBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.metroSetControlBox1.CloseHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.metroSetControlBox1.CloseHoverForeColor = System.Drawing.Color.White;
            this.metroSetControlBox1.CloseNormalForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.DisabledForeColor = System.Drawing.Color.DimGray;
            this.metroSetControlBox1.IsDerivedStyle = true;
            this.metroSetControlBox1.Location = new System.Drawing.Point(683, 12);
            this.metroSetControlBox1.MaximizeBox = true;
            this.metroSetControlBox1.MaximizeHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.metroSetControlBox1.MaximizeHoverForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.MaximizeNormalForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.MinimizeBox = true;
            this.metroSetControlBox1.MinimizeHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.metroSetControlBox1.MinimizeHoverForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.MinimizeNormalForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.Name = "metroSetControlBox1";
            this.metroSetControlBox1.Size = new System.Drawing.Size(100, 25);
            this.metroSetControlBox1.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetControlBox1.StyleManager = null;
            this.metroSetControlBox1.TabIndex = 6;
            this.metroSetControlBox1.Text = "metroSetControlBox1";
            this.metroSetControlBox1.ThemeAuthor = "Narwin";
            this.metroSetControlBox1.ThemeName = "MetroLite";
            // 
            // Historique
            // 
            this.Historique.AnimateEasingType = MetroSet_UI.Enums.EasingType.CubeOut;
            this.Historique.AnimateTime = 200;
            this.Historique.BackgroundColor = System.Drawing.Color.White;
            this.Historique.Controls.Add(this.metroSetSetTabPage1);
            this.Historique.Controls.Add(this.metroSetSetTabPage2);
            this.Historique.Controls.Add(this.metroSetSetTabPage3);
            this.Historique.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Historique.IsDerivedStyle = true;
            this.Historique.ItemSize = new System.Drawing.Size(100, 38);
            this.Historique.Location = new System.Drawing.Point(15, 330);
            this.Historique.Name = "Historique";
            this.Historique.SelectedIndex = 2;
            this.Historique.SelectedTextColor = System.Drawing.Color.White;
            this.Historique.Size = new System.Drawing.Size(772, 396);
            this.Historique.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.Historique.Speed = 100;
            this.Historique.Style = MetroSet_UI.Enums.Style.Light;
            this.Historique.StyleManager = null;
            this.Historique.TabIndex = 7;
            this.Historique.ThemeAuthor = "Narwin";
            this.Historique.ThemeName = "MetroLite";
            this.Historique.UnselectedTextColor = System.Drawing.Color.Gray;
            this.Historique.UseAnimation = false;
            // 
            // metroSetSetTabPage1
            // 
            this.metroSetSetTabPage1.BaseColor = System.Drawing.Color.White;
            this.metroSetSetTabPage1.Controls.Add(this.dgvTransac);
            this.metroSetSetTabPage1.Controls.Add(this.lbxTransactions);
            this.metroSetSetTabPage1.Font = null;
            this.metroSetSetTabPage1.ImageIndex = 0;
            this.metroSetSetTabPage1.ImageKey = null;
            this.metroSetSetTabPage1.IsDerivedStyle = true;
            this.metroSetSetTabPage1.Location = new System.Drawing.Point(4, 42);
            this.metroSetSetTabPage1.Name = "metroSetSetTabPage1";
            this.metroSetSetTabPage1.Size = new System.Drawing.Size(764, 350);
            this.metroSetSetTabPage1.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetSetTabPage1.StyleManager = null;
            this.metroSetSetTabPage1.TabIndex = 0;
            this.metroSetSetTabPage1.Text = "Historique";
            this.metroSetSetTabPage1.ThemeAuthor = "Narwin";
            this.metroSetSetTabPage1.ThemeName = "MetroLite";
            this.metroSetSetTabPage1.ToolTipText = null;
            // 
            // dgvTransac
            // 
            this.dgvTransac.AllowUserToResizeRows = false;
            this.dgvTransac.BackgroundColor = System.Drawing.Color.White;
            this.dgvTransac.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvTransac.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvTransac.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTransac.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Type,
            this.Date,
            this.De,
            this.A,
            this.Montant,
            this.TransactionHash});
            this.dgvTransac.Location = new System.Drawing.Point(-4, 3);
            this.dgvTransac.Name = "dgvTransac";
            this.dgvTransac.RowHeadersWidth = 51;
            this.dgvTransac.RowTemplate.Height = 24;
            this.dgvTransac.Size = new System.Drawing.Size(772, 344);
            this.dgvTransac.TabIndex = 1;
            // 
            // Type
            // 
            this.Type.HeaderText = "Type";
            this.Type.MinimumWidth = 6;
            this.Type.Name = "Type";
            this.Type.ReadOnly = true;
            this.Type.Width = 125;
            // 
            // Date
            // 
            this.Date.HeaderText = "Date";
            this.Date.MinimumWidth = 6;
            this.Date.Name = "Date";
            this.Date.Width = 125;
            // 
            // De
            // 
            this.De.HeaderText = "De";
            this.De.MinimumWidth = 6;
            this.De.Name = "De";
            this.De.ReadOnly = true;
            this.De.Width = 125;
            // 
            // A
            // 
            this.A.HeaderText = "A";
            this.A.MinimumWidth = 6;
            this.A.Name = "A";
            this.A.ReadOnly = true;
            this.A.Width = 125;
            // 
            // Montant
            // 
            this.Montant.HeaderText = "Montant";
            this.Montant.MinimumWidth = 6;
            this.Montant.Name = "Montant";
            this.Montant.ReadOnly = true;
            this.Montant.Width = 125;
            // 
            // TransactionHash
            // 
            this.TransactionHash.HeaderText = "Transaction Hash";
            this.TransactionHash.MinimumWidth = 6;
            this.TransactionHash.Name = "TransactionHash";
            this.TransactionHash.ReadOnly = true;
            this.TransactionHash.Width = 125;
            // 
            // lbxTransactions
            // 
            this.lbxTransactions.BackColor = System.Drawing.Color.White;
            this.lbxTransactions.BorderColor = System.Drawing.Color.LightGray;
            this.lbxTransactions.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.lbxTransactions.DisabledForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            this.lbxTransactions.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lbxTransactions.HoveredItemBackColor = System.Drawing.Color.LightGray;
            this.lbxTransactions.HoveredItemColor = System.Drawing.Color.DimGray;
            this.lbxTransactions.IsDerivedStyle = true;
            this.lbxTransactions.ItemHeight = 30;
            this.lbxTransactions.Location = new System.Drawing.Point(4, 17);
            this.lbxTransactions.MultiSelect = false;
            this.lbxTransactions.Name = "lbxTransactions";
            this.lbxTransactions.SelectedIndex = -1;
            this.lbxTransactions.SelectedItem = null;
            this.lbxTransactions.SelectedItemBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.lbxTransactions.SelectedItemColor = System.Drawing.Color.White;
            this.lbxTransactions.SelectedText = null;
            this.lbxTransactions.SelectedValue = null;
            this.lbxTransactions.ShowBorder = false;
            this.lbxTransactions.ShowScrollBar = false;
            this.lbxTransactions.Size = new System.Drawing.Size(764, 190);
            this.lbxTransactions.Style = MetroSet_UI.Enums.Style.Light;
            this.lbxTransactions.StyleManager = null;
            this.lbxTransactions.TabIndex = 0;
            this.lbxTransactions.ThemeAuthor = "Narwin";
            this.lbxTransactions.ThemeName = "MetroLite";
            // 
            // metroSetSetTabPage2
            // 
            this.metroSetSetTabPage2.BaseColor = System.Drawing.Color.White;
            this.metroSetSetTabPage2.Controls.Add(this.chartEvolutionValeur);
            this.metroSetSetTabPage2.Font = null;
            this.metroSetSetTabPage2.ImageIndex = 0;
            this.metroSetSetTabPage2.ImageKey = null;
            this.metroSetSetTabPage2.IsDerivedStyle = true;
            this.metroSetSetTabPage2.Location = new System.Drawing.Point(4, 42);
            this.metroSetSetTabPage2.Name = "metroSetSetTabPage2";
            this.metroSetSetTabPage2.Size = new System.Drawing.Size(764, 350);
            this.metroSetSetTabPage2.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetSetTabPage2.StyleManager = null;
            this.metroSetSetTabPage2.TabIndex = 1;
            this.metroSetSetTabPage2.Text = "Evolution valeur";
            this.metroSetSetTabPage2.ThemeAuthor = "Narwin";
            this.metroSetSetTabPage2.ThemeName = "MetroLite";
            this.metroSetSetTabPage2.ToolTipText = null;
            // 
            // chartEvolutionValeur
            // 
            chartArea9.Name = "ChartArea1";
            chartArea10.Name = "ChartArea2";
            this.chartEvolutionValeur.ChartAreas.Add(chartArea9);
            this.chartEvolutionValeur.ChartAreas.Add(chartArea10);
            legend5.Name = "Legend1";
            this.chartEvolutionValeur.Legends.Add(legend5);
            this.chartEvolutionValeur.Location = new System.Drawing.Point(0, 3);
            this.chartEvolutionValeur.Name = "chartEvolutionValeur";
            this.chartEvolutionValeur.Size = new System.Drawing.Size(761, 344);
            this.chartEvolutionValeur.TabIndex = 0;
            this.chartEvolutionValeur.Text = "chart1";
            // 
            // metroSetSetTabPage3
            // 
            this.metroSetSetTabPage3.BaseColor = System.Drawing.Color.White;
            this.metroSetSetTabPage3.Controls.Add(this.chartEvolutionQuantite);
            this.metroSetSetTabPage3.Font = null;
            this.metroSetSetTabPage3.ImageIndex = 0;
            this.metroSetSetTabPage3.ImageKey = null;
            this.metroSetSetTabPage3.IsDerivedStyle = true;
            this.metroSetSetTabPage3.Location = new System.Drawing.Point(4, 42);
            this.metroSetSetTabPage3.Name = "metroSetSetTabPage3";
            this.metroSetSetTabPage3.Size = new System.Drawing.Size(764, 350);
            this.metroSetSetTabPage3.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetSetTabPage3.StyleManager = null;
            this.metroSetSetTabPage3.TabIndex = 2;
            this.metroSetSetTabPage3.Text = "Evolution quantitée";
            this.metroSetSetTabPage3.ThemeAuthor = "Narwin";
            this.metroSetSetTabPage3.ThemeName = "MetroLite";
            this.metroSetSetTabPage3.ToolTipText = null;
            // 
            // chartEvolutionQuantite
            // 
            chartArea11.Name = "ChartArea1";
            chartArea12.Name = "ChartArea2";
            this.chartEvolutionQuantite.ChartAreas.Add(chartArea11);
            this.chartEvolutionQuantite.ChartAreas.Add(chartArea12);
            legend6.Name = "Legend1";
            this.chartEvolutionQuantite.Legends.Add(legend6);
            this.chartEvolutionQuantite.Location = new System.Drawing.Point(2, 3);
            this.chartEvolutionQuantite.Name = "chartEvolutionQuantite";
            this.chartEvolutionQuantite.Size = new System.Drawing.Size(761, 344);
            this.chartEvolutionQuantite.TabIndex = 1;
            this.chartEvolutionQuantite.Text = "chart1";
            // 
            // btnSettings
            // 
            this.btnSettings.BorderThickness = 7;
            this.btnSettings.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.btnSettings.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.btnSettings.DisabledForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            this.btnSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnSettings.HoverBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.btnSettings.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.btnSettings.HoverTextColor = System.Drawing.Color.White;
            this.btnSettings.Image = global::TravailDiplome_TISSOTT.Properties.Resources.icons8_gear_60;
            this.btnSettings.ImageSize = new System.Drawing.Size(64, 64);
            this.btnSettings.IsDerivedStyle = true;
            this.btnSettings.Location = new System.Drawing.Point(667, 52);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.btnSettings.NormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.btnSettings.NormalTextColor = System.Drawing.Color.Black;
            this.btnSettings.PressBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.btnSettings.PressColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.btnSettings.PressTextColor = System.Drawing.Color.White;
            this.btnSettings.Size = new System.Drawing.Size(116, 117);
            this.btnSettings.Style = MetroSet_UI.Enums.Style.Light;
            this.btnSettings.StyleManager = null;
            this.btnSettings.TabIndex = 8;
            this.btnSettings.ThemeAuthor = "Narwin";
            this.btnSettings.ThemeName = "MetroLite";
            this.btnSettings.Click += new System.EventHandler(this.btnSettings_Click);
            // 
            // transactionBindingSource
            // 
            this.transactionBindingSource.DataSource = typeof(Nethereum.RPC.Eth.DTOs.Transaction);
            // 
            // frmMain
            // 
            this.AllowResize = false;
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 26F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(802, 741);
            this.Controls.Add(this.btnSettings);
            this.Controls.Add(this.Historique);
            this.Controls.Add(this.metroSetControlBox1);
            this.Controls.Add(this.btmEnvoyer);
            this.Controls.Add(this.btnRecevoir);
            this.Controls.Add(this.lblMontantFiat);
            this.Controls.Add(this.lblMontant);
            this.Controls.Add(this.lblNomPortefeuille);
            this.Controls.Add(this.lbxWallets);
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Portefeuille";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.Historique.ResumeLayout(false);
            this.metroSetSetTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTransac)).EndInit();
            this.metroSetSetTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartEvolutionValeur)).EndInit();
            this.metroSetSetTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartEvolutionQuantite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.transactionBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroSet_UI.Controls.MetroSetListBox lbxWallets;
        private MetroSet_UI.Controls.MetroSetLabel lblNomPortefeuille;
        private MetroSet_UI.Controls.MetroSetLabel lblMontant;
        private MetroSet_UI.Controls.MetroSetLabel lblMontantFiat;
        private MetroSet_UI.Controls.MetroSetButton btnRecevoir;
        private MetroSet_UI.Controls.MetroSetButton btmEnvoyer;
        private MetroSet_UI.Controls.MetroSetControlBox metroSetControlBox1;
        private MetroSet_UI.Controls.MetroSetTabControl Historique;
        private MetroSet_UI.Child.MetroSetSetTabPage metroSetSetTabPage1;
        private MetroSet_UI.Child.MetroSetSetTabPage metroSetSetTabPage2;
        private MetroSet_UI.Controls.MetroSetEllipse btnSettings;
        private MetroSet_UI.Controls.MetroSetListBox lbxTransactions;
        private System.Windows.Forms.DataGridView dgvTransac;
        private System.Windows.Forms.DataGridViewTextBoxColumn Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date;
        private System.Windows.Forms.DataGridViewTextBoxColumn De;
        private System.Windows.Forms.DataGridViewTextBoxColumn A;
        private System.Windows.Forms.DataGridViewTextBoxColumn Montant;
        private System.Windows.Forms.BindingSource transactionBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn TransactionHash;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartEvolutionValeur;
        private MetroSet_UI.Child.MetroSetSetTabPage metroSetSetTabPage3;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartEvolutionQuantite;
    }
}