﻿namespace TravailDiplome_TISSOTT
{
    partial class frmGestionPortefeuilles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroSetTabControl1 = new MetroSet_UI.Controls.MetroSetTabControl();
            this.metroSetSetTabPage1 = new MetroSet_UI.Child.MetroSetSetTabPage();
            this.tbxPassword = new MetroSet_UI.Controls.MetroSetTextBox();
            this.metroSetLabel7 = new MetroSet_UI.Controls.MetroSetLabel();
            this.btnCreerPortefeuille = new MetroSet_UI.Controls.MetroSetButton();
            this.tbxNewWalletName = new MetroSet_UI.Controls.MetroSetTextBox();
            this.metroSetLabel6 = new MetroSet_UI.Controls.MetroSetLabel();
            this.metroSetSetTabPage2 = new MetroSet_UI.Child.MetroSetSetTabPage();
            this.btnImportJson = new MetroSet_UI.Controls.MetroSetButton();
            this.btnImportPrivateKey = new MetroSet_UI.Controls.MetroSetButton();
            this.metroSetSetTabPage3 = new MetroSet_UI.Child.MetroSetSetTabPage();
            this.btnSupprimer = new MetroSet_UI.Controls.MetroSetButton();
            this.lbxWallets = new MetroSet_UI.Controls.MetroSetListBox();
            this.metroSetControlBox1 = new MetroSet_UI.Controls.MetroSetControlBox();
            this.metroSetTabControl1.SuspendLayout();
            this.metroSetSetTabPage1.SuspendLayout();
            this.metroSetSetTabPage2.SuspendLayout();
            this.metroSetSetTabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroSetTabControl1
            // 
            this.metroSetTabControl1.AnimateEasingType = MetroSet_UI.Enums.EasingType.QuartIn;
            this.metroSetTabControl1.AnimateTime = 200;
            this.metroSetTabControl1.BackgroundColor = System.Drawing.Color.White;
            this.metroSetTabControl1.Controls.Add(this.metroSetSetTabPage1);
            this.metroSetTabControl1.Controls.Add(this.metroSetSetTabPage2);
            this.metroSetTabControl1.Controls.Add(this.metroSetSetTabPage3);
            this.metroSetTabControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.metroSetTabControl1.IsDerivedStyle = true;
            this.metroSetTabControl1.ItemSize = new System.Drawing.Size(100, 38);
            this.metroSetTabControl1.Location = new System.Drawing.Point(15, 86);
            this.metroSetTabControl1.Name = "metroSetTabControl1";
            this.metroSetTabControl1.SelectedIndex = 2;
            this.metroSetTabControl1.SelectedTextColor = System.Drawing.Color.White;
            this.metroSetTabControl1.Size = new System.Drawing.Size(733, 445);
            this.metroSetTabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.metroSetTabControl1.Speed = 100;
            this.metroSetTabControl1.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetTabControl1.StyleManager = null;
            this.metroSetTabControl1.TabIndex = 0;
            this.metroSetTabControl1.ThemeAuthor = "Narwin";
            this.metroSetTabControl1.ThemeName = "MetroLite";
            this.metroSetTabControl1.UnselectedTextColor = System.Drawing.Color.Gray;
            this.metroSetTabControl1.UseAnimation = false;
            // 
            // metroSetSetTabPage1
            // 
            this.metroSetSetTabPage1.BaseColor = System.Drawing.Color.White;
            this.metroSetSetTabPage1.Controls.Add(this.tbxPassword);
            this.metroSetSetTabPage1.Controls.Add(this.metroSetLabel7);
            this.metroSetSetTabPage1.Controls.Add(this.btnCreerPortefeuille);
            this.metroSetSetTabPage1.Controls.Add(this.tbxNewWalletName);
            this.metroSetSetTabPage1.Controls.Add(this.metroSetLabel6);
            this.metroSetSetTabPage1.Font = null;
            this.metroSetSetTabPage1.ImageIndex = 0;
            this.metroSetSetTabPage1.ImageKey = null;
            this.metroSetSetTabPage1.IsDerivedStyle = true;
            this.metroSetSetTabPage1.Location = new System.Drawing.Point(4, 42);
            this.metroSetSetTabPage1.Name = "metroSetSetTabPage1";
            this.metroSetSetTabPage1.Size = new System.Drawing.Size(725, 399);
            this.metroSetSetTabPage1.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetSetTabPage1.StyleManager = null;
            this.metroSetSetTabPage1.TabIndex = 0;
            this.metroSetSetTabPage1.Text = "Nouveau";
            this.metroSetSetTabPage1.ThemeAuthor = "Narwin";
            this.metroSetSetTabPage1.ThemeName = "MetroLite";
            this.metroSetSetTabPage1.ToolTipText = null;
            // 
            // tbxPassword
            // 
            this.tbxPassword.AutoCompleteCustomSource = null;
            this.tbxPassword.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.tbxPassword.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.tbxPassword.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.tbxPassword.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.tbxPassword.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.tbxPassword.DisabledForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            this.tbxPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.tbxPassword.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.tbxPassword.Image = null;
            this.tbxPassword.IsDerivedStyle = true;
            this.tbxPassword.Lines = null;
            this.tbxPassword.Location = new System.Drawing.Point(220, 183);
            this.tbxPassword.MaxLength = 32767;
            this.tbxPassword.Multiline = false;
            this.tbxPassword.Name = "tbxPassword";
            this.tbxPassword.ReadOnly = false;
            this.tbxPassword.Size = new System.Drawing.Size(288, 30);
            this.tbxPassword.Style = MetroSet_UI.Enums.Style.Light;
            this.tbxPassword.StyleManager = null;
            this.tbxPassword.TabIndex = 4;
            this.tbxPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbxPassword.ThemeAuthor = "Narwin";
            this.tbxPassword.ThemeName = "MetroLite";
            this.tbxPassword.UseSystemPasswordChar = false;
            this.tbxPassword.WatermarkText = "Mot de passe";
            // 
            // metroSetLabel7
            // 
            this.metroSetLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.metroSetLabel7.IsDerivedStyle = true;
            this.metroSetLabel7.Location = new System.Drawing.Point(284, 143);
            this.metroSetLabel7.Name = "metroSetLabel7";
            this.metroSetLabel7.Size = new System.Drawing.Size(161, 23);
            this.metroSetLabel7.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetLabel7.StyleManager = null;
            this.metroSetLabel7.TabIndex = 3;
            this.metroSetLabel7.Text = "Mot de passe";
            this.metroSetLabel7.ThemeAuthor = "Narwin";
            this.metroSetLabel7.ThemeName = "MetroLite";
            // 
            // btnCreerPortefeuille
            // 
            this.btnCreerPortefeuille.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnCreerPortefeuille.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnCreerPortefeuille.DisabledForeColor = System.Drawing.Color.Gray;
            this.btnCreerPortefeuille.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnCreerPortefeuille.HoverBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btnCreerPortefeuille.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btnCreerPortefeuille.HoverTextColor = System.Drawing.Color.White;
            this.btnCreerPortefeuille.IsDerivedStyle = true;
            this.btnCreerPortefeuille.Location = new System.Drawing.Point(220, 230);
            this.btnCreerPortefeuille.Name = "btnCreerPortefeuille";
            this.btnCreerPortefeuille.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnCreerPortefeuille.NormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnCreerPortefeuille.NormalTextColor = System.Drawing.Color.White;
            this.btnCreerPortefeuille.PressBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btnCreerPortefeuille.PressColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btnCreerPortefeuille.PressTextColor = System.Drawing.Color.White;
            this.btnCreerPortefeuille.Size = new System.Drawing.Size(288, 50);
            this.btnCreerPortefeuille.Style = MetroSet_UI.Enums.Style.Light;
            this.btnCreerPortefeuille.StyleManager = null;
            this.btnCreerPortefeuille.TabIndex = 2;
            this.btnCreerPortefeuille.Text = "Créer";
            this.btnCreerPortefeuille.ThemeAuthor = "Narwin";
            this.btnCreerPortefeuille.ThemeName = "MetroLite";
            this.btnCreerPortefeuille.Click += new System.EventHandler(this.btnCreerPortefeuille_Click);
            // 
            // tbxNewWalletName
            // 
            this.tbxNewWalletName.AutoCompleteCustomSource = null;
            this.tbxNewWalletName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.tbxNewWalletName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.tbxNewWalletName.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.tbxNewWalletName.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.tbxNewWalletName.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.tbxNewWalletName.DisabledForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            this.tbxNewWalletName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.tbxNewWalletName.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.tbxNewWalletName.Image = null;
            this.tbxNewWalletName.IsDerivedStyle = true;
            this.tbxNewWalletName.Lines = null;
            this.tbxNewWalletName.Location = new System.Drawing.Point(220, 96);
            this.tbxNewWalletName.MaxLength = 32767;
            this.tbxNewWalletName.Multiline = false;
            this.tbxNewWalletName.Name = "tbxNewWalletName";
            this.tbxNewWalletName.ReadOnly = false;
            this.tbxNewWalletName.Size = new System.Drawing.Size(288, 30);
            this.tbxNewWalletName.Style = MetroSet_UI.Enums.Style.Light;
            this.tbxNewWalletName.StyleManager = null;
            this.tbxNewWalletName.TabIndex = 1;
            this.tbxNewWalletName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbxNewWalletName.ThemeAuthor = "Narwin";
            this.tbxNewWalletName.ThemeName = "MetroLite";
            this.tbxNewWalletName.UseSystemPasswordChar = false;
            this.tbxNewWalletName.WatermarkText = "Nom";
            // 
            // metroSetLabel6
            // 
            this.metroSetLabel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.metroSetLabel6.IsDerivedStyle = true;
            this.metroSetLabel6.Location = new System.Drawing.Point(284, 56);
            this.metroSetLabel6.Name = "metroSetLabel6";
            this.metroSetLabel6.Size = new System.Drawing.Size(161, 23);
            this.metroSetLabel6.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetLabel6.StyleManager = null;
            this.metroSetLabel6.TabIndex = 0;
            this.metroSetLabel6.Text = "Nom du portefeuille";
            this.metroSetLabel6.ThemeAuthor = "Narwin";
            this.metroSetLabel6.ThemeName = "MetroLite";
            // 
            // metroSetSetTabPage2
            // 
            this.metroSetSetTabPage2.BaseColor = System.Drawing.Color.White;
            this.metroSetSetTabPage2.Controls.Add(this.btnImportJson);
            this.metroSetSetTabPage2.Controls.Add(this.btnImportPrivateKey);
            this.metroSetSetTabPage2.Font = null;
            this.metroSetSetTabPage2.ImageIndex = 0;
            this.metroSetSetTabPage2.ImageKey = null;
            this.metroSetSetTabPage2.IsDerivedStyle = true;
            this.metroSetSetTabPage2.Location = new System.Drawing.Point(4, 42);
            this.metroSetSetTabPage2.Name = "metroSetSetTabPage2";
            this.metroSetSetTabPage2.Size = new System.Drawing.Size(725, 399);
            this.metroSetSetTabPage2.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetSetTabPage2.StyleManager = null;
            this.metroSetSetTabPage2.TabIndex = 1;
            this.metroSetSetTabPage2.Text = "Importer";
            this.metroSetSetTabPage2.ThemeAuthor = "Narwin";
            this.metroSetSetTabPage2.ThemeName = "MetroLite";
            this.metroSetSetTabPage2.ToolTipText = null;
            // 
            // btnImportJson
            // 
            this.btnImportJson.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnImportJson.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnImportJson.DisabledForeColor = System.Drawing.Color.Gray;
            this.btnImportJson.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold);
            this.btnImportJson.HoverBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btnImportJson.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btnImportJson.HoverTextColor = System.Drawing.Color.White;
            this.btnImportJson.IsDerivedStyle = true;
            this.btnImportJson.Location = new System.Drawing.Point(215, 3);
            this.btnImportJson.Name = "btnImportJson";
            this.btnImportJson.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnImportJson.NormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnImportJson.NormalTextColor = System.Drawing.Color.White;
            this.btnImportJson.PressBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btnImportJson.PressColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btnImportJson.PressTextColor = System.Drawing.Color.White;
            this.btnImportJson.Size = new System.Drawing.Size(206, 130);
            this.btnImportJson.Style = MetroSet_UI.Enums.Style.Light;
            this.btnImportJson.StyleManager = null;
            this.btnImportJson.TabIndex = 4;
            this.btnImportJson.Text = "Fichier Json";
            this.btnImportJson.ThemeAuthor = "Narwin";
            this.btnImportJson.ThemeName = "MetroLite";
            this.btnImportJson.Click += new System.EventHandler(this.btnImportJson_Click);
            // 
            // btnImportPrivateKey
            // 
            this.btnImportPrivateKey.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnImportPrivateKey.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnImportPrivateKey.DisabledForeColor = System.Drawing.Color.Gray;
            this.btnImportPrivateKey.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImportPrivateKey.HoverBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btnImportPrivateKey.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btnImportPrivateKey.HoverTextColor = System.Drawing.Color.White;
            this.btnImportPrivateKey.IsDerivedStyle = true;
            this.btnImportPrivateKey.Location = new System.Drawing.Point(0, 3);
            this.btnImportPrivateKey.Name = "btnImportPrivateKey";
            this.btnImportPrivateKey.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnImportPrivateKey.NormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnImportPrivateKey.NormalTextColor = System.Drawing.Color.White;
            this.btnImportPrivateKey.PressBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btnImportPrivateKey.PressColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btnImportPrivateKey.PressTextColor = System.Drawing.Color.White;
            this.btnImportPrivateKey.Size = new System.Drawing.Size(206, 130);
            this.btnImportPrivateKey.Style = MetroSet_UI.Enums.Style.Light;
            this.btnImportPrivateKey.StyleManager = null;
            this.btnImportPrivateKey.TabIndex = 3;
            this.btnImportPrivateKey.Text = "Clé privée";
            this.btnImportPrivateKey.ThemeAuthor = "Narwin";
            this.btnImportPrivateKey.ThemeName = "MetroLite";
            this.btnImportPrivateKey.Click += new System.EventHandler(this.btnImportPrivateKey_Click_1);
            // 
            // metroSetSetTabPage3
            // 
            this.metroSetSetTabPage3.BaseColor = System.Drawing.Color.White;
            this.metroSetSetTabPage3.Controls.Add(this.btnSupprimer);
            this.metroSetSetTabPage3.Controls.Add(this.lbxWallets);
            this.metroSetSetTabPage3.Font = null;
            this.metroSetSetTabPage3.ImageIndex = 0;
            this.metroSetSetTabPage3.ImageKey = null;
            this.metroSetSetTabPage3.IsDerivedStyle = true;
            this.metroSetSetTabPage3.Location = new System.Drawing.Point(4, 42);
            this.metroSetSetTabPage3.Name = "metroSetSetTabPage3";
            this.metroSetSetTabPage3.Size = new System.Drawing.Size(725, 399);
            this.metroSetSetTabPage3.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetSetTabPage3.StyleManager = null;
            this.metroSetSetTabPage3.TabIndex = 2;
            this.metroSetSetTabPage3.Text = "Supprimer";
            this.metroSetSetTabPage3.ThemeAuthor = "Narwin";
            this.metroSetSetTabPage3.ThemeName = "MetroLite";
            this.metroSetSetTabPage3.ToolTipText = null;
            // 
            // btnSupprimer
            // 
            this.btnSupprimer.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnSupprimer.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnSupprimer.DisabledForeColor = System.Drawing.Color.Gray;
            this.btnSupprimer.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnSupprimer.HoverBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btnSupprimer.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btnSupprimer.HoverTextColor = System.Drawing.Color.White;
            this.btnSupprimer.IsDerivedStyle = true;
            this.btnSupprimer.Location = new System.Drawing.Point(222, 287);
            this.btnSupprimer.Name = "btnSupprimer";
            this.btnSupprimer.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnSupprimer.NormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnSupprimer.NormalTextColor = System.Drawing.Color.White;
            this.btnSupprimer.PressBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btnSupprimer.PressColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btnSupprimer.PressTextColor = System.Drawing.Color.White;
            this.btnSupprimer.Size = new System.Drawing.Size(215, 52);
            this.btnSupprimer.Style = MetroSet_UI.Enums.Style.Light;
            this.btnSupprimer.StyleManager = null;
            this.btnSupprimer.TabIndex = 1;
            this.btnSupprimer.Text = "Supprimer";
            this.btnSupprimer.ThemeAuthor = "Narwin";
            this.btnSupprimer.ThemeName = "MetroLite";
            this.btnSupprimer.Click += new System.EventHandler(this.btnSupprimer_Click);
            // 
            // lbxWallets
            // 
            this.lbxWallets.BackColor = System.Drawing.Color.White;
            this.lbxWallets.BorderColor = System.Drawing.Color.LightGray;
            this.lbxWallets.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.lbxWallets.DisabledForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            this.lbxWallets.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lbxWallets.HoveredItemBackColor = System.Drawing.Color.LightGray;
            this.lbxWallets.HoveredItemColor = System.Drawing.Color.DimGray;
            this.lbxWallets.IsDerivedStyle = true;
            this.lbxWallets.ItemHeight = 30;
            this.lbxWallets.Location = new System.Drawing.Point(0, 3);
            this.lbxWallets.MultiSelect = false;
            this.lbxWallets.Name = "lbxWallets";
            this.lbxWallets.SelectedIndex = -1;
            this.lbxWallets.SelectedItem = null;
            this.lbxWallets.SelectedItemBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.lbxWallets.SelectedItemColor = System.Drawing.Color.White;
            this.lbxWallets.SelectedText = null;
            this.lbxWallets.SelectedValue = null;
            this.lbxWallets.ShowBorder = false;
            this.lbxWallets.ShowScrollBar = false;
            this.lbxWallets.Size = new System.Drawing.Size(725, 278);
            this.lbxWallets.Style = MetroSet_UI.Enums.Style.Light;
            this.lbxWallets.StyleManager = null;
            this.lbxWallets.TabIndex = 0;
            this.lbxWallets.ThemeAuthor = "Narwin";
            this.lbxWallets.ThemeName = "MetroLite";
            // 
            // metroSetControlBox1
            // 
            this.metroSetControlBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.metroSetControlBox1.CloseHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.metroSetControlBox1.CloseHoverForeColor = System.Drawing.Color.White;
            this.metroSetControlBox1.CloseNormalForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.DisabledForeColor = System.Drawing.Color.DimGray;
            this.metroSetControlBox1.IsDerivedStyle = true;
            this.metroSetControlBox1.Location = new System.Drawing.Point(644, 20);
            this.metroSetControlBox1.MaximizeBox = true;
            this.metroSetControlBox1.MaximizeHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.metroSetControlBox1.MaximizeHoverForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.MaximizeNormalForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.MinimizeBox = true;
            this.metroSetControlBox1.MinimizeHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.metroSetControlBox1.MinimizeHoverForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.MinimizeNormalForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.Name = "metroSetControlBox1";
            this.metroSetControlBox1.Size = new System.Drawing.Size(100, 25);
            this.metroSetControlBox1.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetControlBox1.StyleManager = null;
            this.metroSetControlBox1.TabIndex = 1;
            this.metroSetControlBox1.Text = "metroSetControlBox1";
            this.metroSetControlBox1.ThemeAuthor = "Narwin";
            this.metroSetControlBox1.ThemeName = "MetroLite";
            // 
            // frmGestionPortefeuilles
            // 
            this.AllowResize = false;
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 26F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(763, 546);
            this.Controls.Add(this.metroSetControlBox1);
            this.Controls.Add(this.metroSetTabControl1);
            this.Name = "frmGestionPortefeuilles";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gestion des portefeuilles";
            this.Load += new System.EventHandler(this.frmGestionPortefeuilles_Load);
            this.metroSetTabControl1.ResumeLayout(false);
            this.metroSetSetTabPage1.ResumeLayout(false);
            this.metroSetSetTabPage2.ResumeLayout(false);
            this.metroSetSetTabPage3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroSet_UI.Controls.MetroSetTabControl metroSetTabControl1;
        private MetroSet_UI.Child.MetroSetSetTabPage metroSetSetTabPage1;
        private MetroSet_UI.Child.MetroSetSetTabPage metroSetSetTabPage2;
        private MetroSet_UI.Child.MetroSetSetTabPage metroSetSetTabPage3;
        private MetroSet_UI.Controls.MetroSetButton btnSupprimer;
        private MetroSet_UI.Controls.MetroSetListBox lbxWallets;
        private MetroSet_UI.Controls.MetroSetButton btnCreerPortefeuille;
        private MetroSet_UI.Controls.MetroSetTextBox tbxNewWalletName;
        private MetroSet_UI.Controls.MetroSetLabel metroSetLabel6;
        private MetroSet_UI.Controls.MetroSetTextBox tbxPassword;
        private MetroSet_UI.Controls.MetroSetLabel metroSetLabel7;
        private MetroSet_UI.Controls.MetroSetButton btnImportJson;
        private MetroSet_UI.Controls.MetroSetButton btnImportPrivateKey;
        private MetroSet_UI.Controls.MetroSetControlBox metroSetControlBox1;
    }
}