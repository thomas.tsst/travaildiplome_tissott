﻿/* 
 * App : SecuWallet
 * Auteur : Thomas Tissot
 * Org : CFPT
 * Date : 07.06.2022
 * brief : Pop-Up permettant à l'utilisateur d'entrer son code Google Authenticator.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroSet_UI.Forms;
namespace TravailDiplome_TISSOTT
{
    public partial class frmAuthenticatorPopUp : MetroSetForm
    {
        AppController app;
        public frmAuthenticatorPopUp(AppController app)
        {
            InitializeComponent();
            this.app = app;
        }

        private void btnCode_Click(object sender, EventArgs e)
        {
            if (tbxCode.Text.Length == 6)
            {
                //Vérification du token par L'API
                object tokenReponse = JwtController.DeserializeResponse(JwtController.CheckToken(app.User.Token, app.User.Email));
                if (tokenReponse.ToString() == "ok")
                {
                    //Vérification du Google Authenticator par L'API
                    object authenticatorReponse = JwtController.DeserializeResponse(JwtController.CheckUserAuthenticator(app.User.Email, tbxCode.Text));
                    if (authenticatorReponse.ToString() == "ok")
                    {
                        this.DialogResult = DialogResult.OK;
                        this.Hide();
                    }
                    else
                    {
                        MessageBox.Show("Code erroné.");
                    }
                }
            }
        }
    }
}
