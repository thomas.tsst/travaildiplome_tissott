﻿/* 
 * App : SecuWallet
 * Auteur : Thomas Tissot
 * Org : CFPT
 * Date : 07.06.2022
 * brief : Vue permettant à l'utilisateur de se connecter à l'aide de son email et de son mot de passe.
 */
using MetroSet_UI.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Web.UI.Design.WebControls;
using System.Net;
using System.IO;
using System.Security.Cryptography;
using System.Threading;

namespace TravailDiplome_TISSOTT
{
    public partial class frmLogin : MetroSetForm
    {
        JwtController jwtController = new JwtController();
        AppController app;
        public frmLogin()
        {
            InitializeComponent();
            BlockchainController bc = new BlockchainController();   
        }
        /// <summary>
        /// Evenement servant à vérifié les entrées de connexion de l'utilisateur.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (tbxEmail.Text != "" || tbxPassword.Text != "")
            {
                //Encryption du mot de passe en SHA1.
                string encryptedPassword;
                using (SHA1 sha1Hash = SHA1.Create())
                {
                    byte[] sourceBytes = Encoding.UTF8.GetBytes(tbxPassword.Text);
                    byte[] hashBytes = sha1Hash.ComputeHash(sourceBytes);
                    encryptedPassword = BitConverter.ToString(hashBytes).Replace("-", String.Empty);
                }
                //Vérification par l'API du login utilisateur.
                object reponse = JwtController.DeserializeResponse(JwtController.CheckUserLogin(tbxEmail.Text, encryptedPassword.ToLower()));
                if (reponse.ToString() == "ok")
                {
                    app = new AppController(tbxEmail.Text);
                    frmAuthenticator frmAuthenticator = new frmAuthenticator(app);
                    this.Hide();
                    frmAuthenticator.Closed += (s, args) => this.Close();
                    frmAuthenticator.Show();
                }
                else
                {
                    MessageBox.Show("Email ou mot de passe erroné.");
                }
            }

        }
    }
}
