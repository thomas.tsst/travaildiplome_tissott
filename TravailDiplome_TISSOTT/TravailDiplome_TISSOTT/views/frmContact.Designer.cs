﻿namespace TravailDiplome_TISSOTT
{
    partial class frmContact
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbxContact = new MetroSet_UI.Controls.MetroSetListBox();
            this.btnNewContact = new MetroSet_UI.Controls.MetroSetButton();
            this.btnModify = new MetroSet_UI.Controls.MetroSetButton();
            this.btnDelete = new MetroSet_UI.Controls.MetroSetButton();
            this.metroSetControlBox1 = new MetroSet_UI.Controls.MetroSetControlBox();
            this.SuspendLayout();
            // 
            // lbxContact
            // 
            this.lbxContact.BackColor = System.Drawing.Color.White;
            this.lbxContact.BorderColor = System.Drawing.Color.LightGray;
            this.lbxContact.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.lbxContact.DisabledForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            this.lbxContact.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lbxContact.HoveredItemBackColor = System.Drawing.Color.LightGray;
            this.lbxContact.HoveredItemColor = System.Drawing.Color.DimGray;
            this.lbxContact.IsDerivedStyle = true;
            this.lbxContact.ItemHeight = 30;
            this.lbxContact.Location = new System.Drawing.Point(16, 92);
            this.lbxContact.MultiSelect = false;
            this.lbxContact.Name = "lbxContact";
            this.lbxContact.SelectedIndex = -1;
            this.lbxContact.SelectedItem = null;
            this.lbxContact.SelectedItemBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.lbxContact.SelectedItemColor = System.Drawing.Color.White;
            this.lbxContact.SelectedText = null;
            this.lbxContact.SelectedValue = null;
            this.lbxContact.ShowBorder = false;
            this.lbxContact.ShowScrollBar = false;
            this.lbxContact.Size = new System.Drawing.Size(566, 273);
            this.lbxContact.Style = MetroSet_UI.Enums.Style.Light;
            this.lbxContact.StyleManager = null;
            this.lbxContact.TabIndex = 0;
            this.lbxContact.ThemeAuthor = "Narwin";
            this.lbxContact.ThemeName = "MetroLite";
            // 
            // btnNewContact
            // 
            this.btnNewContact.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnNewContact.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnNewContact.DisabledForeColor = System.Drawing.Color.Gray;
            this.btnNewContact.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnNewContact.HoverBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btnNewContact.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btnNewContact.HoverTextColor = System.Drawing.Color.White;
            this.btnNewContact.IsDerivedStyle = true;
            this.btnNewContact.Location = new System.Drawing.Point(15, 371);
            this.btnNewContact.Name = "btnNewContact";
            this.btnNewContact.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnNewContact.NormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnNewContact.NormalTextColor = System.Drawing.Color.White;
            this.btnNewContact.PressBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btnNewContact.PressColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btnNewContact.PressTextColor = System.Drawing.Color.White;
            this.btnNewContact.Size = new System.Drawing.Size(185, 59);
            this.btnNewContact.Style = MetroSet_UI.Enums.Style.Light;
            this.btnNewContact.StyleManager = null;
            this.btnNewContact.TabIndex = 1;
            this.btnNewContact.Text = "Nouveau";
            this.btnNewContact.ThemeAuthor = "Narwin";
            this.btnNewContact.ThemeName = "MetroLite";
            this.btnNewContact.Click += new System.EventHandler(this.btnNewContact_Click);
            // 
            // btnModify
            // 
            this.btnModify.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnModify.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnModify.DisabledForeColor = System.Drawing.Color.Gray;
            this.btnModify.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnModify.HoverBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btnModify.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btnModify.HoverTextColor = System.Drawing.Color.White;
            this.btnModify.IsDerivedStyle = true;
            this.btnModify.Location = new System.Drawing.Point(206, 371);
            this.btnModify.Name = "btnModify";
            this.btnModify.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnModify.NormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnModify.NormalTextColor = System.Drawing.Color.White;
            this.btnModify.PressBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btnModify.PressColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btnModify.PressTextColor = System.Drawing.Color.White;
            this.btnModify.Size = new System.Drawing.Size(185, 59);
            this.btnModify.Style = MetroSet_UI.Enums.Style.Light;
            this.btnModify.StyleManager = null;
            this.btnModify.TabIndex = 2;
            this.btnModify.Text = "Modifier";
            this.btnModify.ThemeAuthor = "Narwin";
            this.btnModify.ThemeName = "MetroLite";
            this.btnModify.Click += new System.EventHandler(this.btnModify_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnDelete.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnDelete.DisabledForeColor = System.Drawing.Color.Gray;
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnDelete.HoverBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btnDelete.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btnDelete.HoverTextColor = System.Drawing.Color.White;
            this.btnDelete.IsDerivedStyle = true;
            this.btnDelete.Location = new System.Drawing.Point(397, 371);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnDelete.NormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnDelete.NormalTextColor = System.Drawing.Color.White;
            this.btnDelete.PressBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btnDelete.PressColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btnDelete.PressTextColor = System.Drawing.Color.White;
            this.btnDelete.Size = new System.Drawing.Size(185, 59);
            this.btnDelete.Style = MetroSet_UI.Enums.Style.Light;
            this.btnDelete.StyleManager = null;
            this.btnDelete.TabIndex = 3;
            this.btnDelete.Text = "Supprimer";
            this.btnDelete.ThemeAuthor = "Narwin";
            this.btnDelete.ThemeName = "MetroLite";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // metroSetControlBox1
            // 
            this.metroSetControlBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.metroSetControlBox1.CloseHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.metroSetControlBox1.CloseHoverForeColor = System.Drawing.Color.White;
            this.metroSetControlBox1.CloseNormalForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.DisabledForeColor = System.Drawing.Color.DimGray;
            this.metroSetControlBox1.IsDerivedStyle = true;
            this.metroSetControlBox1.Location = new System.Drawing.Point(481, 18);
            this.metroSetControlBox1.MaximizeBox = true;
            this.metroSetControlBox1.MaximizeHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.metroSetControlBox1.MaximizeHoverForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.MaximizeNormalForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.MinimizeBox = true;
            this.metroSetControlBox1.MinimizeHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.metroSetControlBox1.MinimizeHoverForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.MinimizeNormalForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.Name = "metroSetControlBox1";
            this.metroSetControlBox1.Size = new System.Drawing.Size(100, 25);
            this.metroSetControlBox1.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetControlBox1.StyleManager = null;
            this.metroSetControlBox1.TabIndex = 4;
            this.metroSetControlBox1.Text = "metroSetControlBox1";
            this.metroSetControlBox1.ThemeAuthor = "Narwin";
            this.metroSetControlBox1.ThemeName = "MetroLite";
            // 
            // frmContact
            // 
            this.AllowResize = false;
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 26F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(596, 445);
            this.Controls.Add(this.metroSetControlBox1);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnModify);
            this.Controls.Add(this.btnNewContact);
            this.Controls.Add(this.lbxContact);
            this.Name = "frmContact";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Contacts";
            this.ResumeLayout(false);

        }

        #endregion

        private MetroSet_UI.Controls.MetroSetListBox lbxContact;
        private MetroSet_UI.Controls.MetroSetButton btnNewContact;
        private MetroSet_UI.Controls.MetroSetButton btnModify;
        private MetroSet_UI.Controls.MetroSetButton btnDelete;
        private MetroSet_UI.Controls.MetroSetControlBox metroSetControlBox1;
    }
}