﻿namespace TravailDiplome_TISSOTT
{
    partial class frmImportWalletFromKey
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroSetLabel1 = new MetroSet_UI.Controls.MetroSetLabel();
            this.metroSetLabel2 = new MetroSet_UI.Controls.MetroSetLabel();
            this.btnImport = new MetroSet_UI.Controls.MetroSetButton();
            this.tbxPrivateKey = new MetroSet_UI.Controls.MetroSetTextBox();
            this.tbxPassword = new MetroSet_UI.Controls.MetroSetTextBox();
            this.tbxNom = new MetroSet_UI.Controls.MetroSetTextBox();
            this.lblNom = new MetroSet_UI.Controls.MetroSetLabel();
            this.metroSetControlBox1 = new MetroSet_UI.Controls.MetroSetControlBox();
            this.SuspendLayout();
            // 
            // metroSetLabel1
            // 
            this.metroSetLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.metroSetLabel1.IsDerivedStyle = true;
            this.metroSetLabel1.Location = new System.Drawing.Point(19, 83);
            this.metroSetLabel1.Name = "metroSetLabel1";
            this.metroSetLabel1.Size = new System.Drawing.Size(100, 23);
            this.metroSetLabel1.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetLabel1.StyleManager = null;
            this.metroSetLabel1.TabIndex = 0;
            this.metroSetLabel1.Text = "Clé privée :";
            this.metroSetLabel1.ThemeAuthor = "Narwin";
            this.metroSetLabel1.ThemeName = "MetroLite";
            // 
            // metroSetLabel2
            // 
            this.metroSetLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.metroSetLabel2.IsDerivedStyle = true;
            this.metroSetLabel2.Location = new System.Drawing.Point(19, 213);
            this.metroSetLabel2.Name = "metroSetLabel2";
            this.metroSetLabel2.Size = new System.Drawing.Size(177, 23);
            this.metroSetLabel2.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetLabel2.StyleManager = null;
            this.metroSetLabel2.TabIndex = 1;
            this.metroSetLabel2.Text = "Mot de passe :";
            this.metroSetLabel2.ThemeAuthor = "Narwin";
            this.metroSetLabel2.ThemeName = "MetroLite";
            // 
            // btnImport
            // 
            this.btnImport.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnImport.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnImport.DisabledForeColor = System.Drawing.Color.Gray;
            this.btnImport.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnImport.HoverBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btnImport.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btnImport.HoverTextColor = System.Drawing.Color.White;
            this.btnImport.IsDerivedStyle = true;
            this.btnImport.Location = new System.Drawing.Point(19, 278);
            this.btnImport.Name = "btnImport";
            this.btnImport.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnImport.NormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnImport.NormalTextColor = System.Drawing.Color.White;
            this.btnImport.PressBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btnImport.PressColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btnImport.PressTextColor = System.Drawing.Color.White;
            this.btnImport.Size = new System.Drawing.Size(228, 55);
            this.btnImport.Style = MetroSet_UI.Enums.Style.Light;
            this.btnImport.StyleManager = null;
            this.btnImport.TabIndex = 3;
            this.btnImport.Text = "Importer";
            this.btnImport.ThemeAuthor = "Narwin";
            this.btnImport.ThemeName = "MetroLite";
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // tbxPrivateKey
            // 
            this.tbxPrivateKey.AutoCompleteCustomSource = null;
            this.tbxPrivateKey.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.tbxPrivateKey.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.tbxPrivateKey.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.tbxPrivateKey.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.tbxPrivateKey.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.tbxPrivateKey.DisabledForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            this.tbxPrivateKey.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.tbxPrivateKey.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.tbxPrivateKey.Image = null;
            this.tbxPrivateKey.IsDerivedStyle = true;
            this.tbxPrivateKey.Lines = null;
            this.tbxPrivateKey.Location = new System.Drawing.Point(19, 112);
            this.tbxPrivateKey.MaxLength = 32767;
            this.tbxPrivateKey.Multiline = false;
            this.tbxPrivateKey.Name = "tbxPrivateKey";
            this.tbxPrivateKey.ReadOnly = false;
            this.tbxPrivateKey.Size = new System.Drawing.Size(228, 30);
            this.tbxPrivateKey.Style = MetroSet_UI.Enums.Style.Light;
            this.tbxPrivateKey.StyleManager = null;
            this.tbxPrivateKey.TabIndex = 4;
            this.tbxPrivateKey.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbxPrivateKey.ThemeAuthor = "Narwin";
            this.tbxPrivateKey.ThemeName = "MetroLite";
            this.tbxPrivateKey.UseSystemPasswordChar = false;
            this.tbxPrivateKey.WatermarkText = "Clé privée";
            // 
            // tbxPassword
            // 
            this.tbxPassword.AutoCompleteCustomSource = null;
            this.tbxPassword.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.tbxPassword.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.tbxPassword.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.tbxPassword.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.tbxPassword.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.tbxPassword.DisabledForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            this.tbxPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.tbxPassword.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.tbxPassword.Image = null;
            this.tbxPassword.IsDerivedStyle = true;
            this.tbxPassword.Lines = null;
            this.tbxPassword.Location = new System.Drawing.Point(19, 242);
            this.tbxPassword.MaxLength = 32767;
            this.tbxPassword.Multiline = false;
            this.tbxPassword.Name = "tbxPassword";
            this.tbxPassword.ReadOnly = false;
            this.tbxPassword.Size = new System.Drawing.Size(228, 30);
            this.tbxPassword.Style = MetroSet_UI.Enums.Style.Light;
            this.tbxPassword.StyleManager = null;
            this.tbxPassword.TabIndex = 5;
            this.tbxPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbxPassword.ThemeAuthor = "Narwin";
            this.tbxPassword.ThemeName = "MetroLite";
            this.tbxPassword.UseSystemPasswordChar = false;
            this.tbxPassword.WatermarkText = "Mot de passe";
            // 
            // tbxNom
            // 
            this.tbxNom.AutoCompleteCustomSource = null;
            this.tbxNom.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.tbxNom.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.tbxNom.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.tbxNom.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.tbxNom.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.tbxNom.DisabledForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            this.tbxNom.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.tbxNom.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.tbxNom.Image = null;
            this.tbxNom.IsDerivedStyle = true;
            this.tbxNom.Lines = null;
            this.tbxNom.Location = new System.Drawing.Point(19, 177);
            this.tbxNom.MaxLength = 32767;
            this.tbxNom.Multiline = false;
            this.tbxNom.Name = "tbxNom";
            this.tbxNom.ReadOnly = false;
            this.tbxNom.Size = new System.Drawing.Size(228, 30);
            this.tbxNom.Style = MetroSet_UI.Enums.Style.Light;
            this.tbxNom.StyleManager = null;
            this.tbxNom.TabIndex = 7;
            this.tbxNom.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbxNom.ThemeAuthor = "Narwin";
            this.tbxNom.ThemeName = "MetroLite";
            this.tbxNom.UseSystemPasswordChar = false;
            this.tbxNom.WatermarkText = "Nom";
            // 
            // lblNom
            // 
            this.lblNom.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblNom.IsDerivedStyle = true;
            this.lblNom.Location = new System.Drawing.Point(19, 148);
            this.lblNom.Name = "lblNom";
            this.lblNom.Size = new System.Drawing.Size(100, 23);
            this.lblNom.Style = MetroSet_UI.Enums.Style.Light;
            this.lblNom.StyleManager = null;
            this.lblNom.TabIndex = 6;
            this.lblNom.Text = "Nom :";
            this.lblNom.ThemeAuthor = "Narwin";
            this.lblNom.ThemeName = "MetroLite";
            // 
            // metroSetControlBox1
            // 
            this.metroSetControlBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.metroSetControlBox1.CloseHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.metroSetControlBox1.CloseHoverForeColor = System.Drawing.Color.White;
            this.metroSetControlBox1.CloseNormalForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.DisabledForeColor = System.Drawing.Color.DimGray;
            this.metroSetControlBox1.IsDerivedStyle = true;
            this.metroSetControlBox1.Location = new System.Drawing.Point(152, 16);
            this.metroSetControlBox1.MaximizeBox = true;
            this.metroSetControlBox1.MaximizeHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.metroSetControlBox1.MaximizeHoverForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.MaximizeNormalForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.MinimizeBox = true;
            this.metroSetControlBox1.MinimizeHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.metroSetControlBox1.MinimizeHoverForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.MinimizeNormalForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.Name = "metroSetControlBox1";
            this.metroSetControlBox1.Size = new System.Drawing.Size(100, 25);
            this.metroSetControlBox1.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetControlBox1.StyleManager = null;
            this.metroSetControlBox1.TabIndex = 8;
            this.metroSetControlBox1.Text = "metroSetControlBox1";
            this.metroSetControlBox1.ThemeAuthor = "Narwin";
            this.metroSetControlBox1.ThemeName = "MetroLite";
            // 
            // frmImportWalletFromKey
            // 
            this.AllowResize = false;
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 26F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(267, 365);
            this.Controls.Add(this.metroSetControlBox1);
            this.Controls.Add(this.tbxNom);
            this.Controls.Add(this.lblNom);
            this.Controls.Add(this.tbxPassword);
            this.Controls.Add(this.tbxPrivateKey);
            this.Controls.Add(this.btnImport);
            this.Controls.Add(this.metroSetLabel2);
            this.Controls.Add(this.metroSetLabel1);
            this.Name = "frmImportWalletFromKey";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Import (Clé privée)";
            this.ResumeLayout(false);

        }

        #endregion

        private MetroSet_UI.Controls.MetroSetLabel metroSetLabel1;
        private MetroSet_UI.Controls.MetroSetLabel metroSetLabel2;
        private MetroSet_UI.Controls.MetroSetButton btnImport;
        private MetroSet_UI.Controls.MetroSetTextBox tbxPrivateKey;
        private MetroSet_UI.Controls.MetroSetTextBox tbxPassword;
        private MetroSet_UI.Controls.MetroSetTextBox tbxNom;
        private MetroSet_UI.Controls.MetroSetLabel lblNom;
        private MetroSet_UI.Controls.MetroSetControlBox metroSetControlBox1;
    }
}