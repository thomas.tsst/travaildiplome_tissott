﻿namespace TravailDiplome_TISSOTT
{
    partial class frmMonCompte
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGestionPortefeuille = new MetroSet_UI.Controls.MetroSetButton();
            this.btnParametres = new MetroSet_UI.Controls.MetroSetButton();
            this.btnDeconnexion = new MetroSet_UI.Controls.MetroSetButton();
            this.metroSetControlBox1 = new MetroSet_UI.Controls.MetroSetControlBox();
            this.SuspendLayout();
            // 
            // btnGestionPortefeuille
            // 
            this.btnGestionPortefeuille.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnGestionPortefeuille.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnGestionPortefeuille.DisabledForeColor = System.Drawing.Color.Gray;
            this.btnGestionPortefeuille.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnGestionPortefeuille.HoverBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btnGestionPortefeuille.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btnGestionPortefeuille.HoverTextColor = System.Drawing.Color.White;
            this.btnGestionPortefeuille.IsDerivedStyle = true;
            this.btnGestionPortefeuille.Location = new System.Drawing.Point(15, 105);
            this.btnGestionPortefeuille.Name = "btnGestionPortefeuille";
            this.btnGestionPortefeuille.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnGestionPortefeuille.NormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnGestionPortefeuille.NormalTextColor = System.Drawing.Color.White;
            this.btnGestionPortefeuille.PressBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btnGestionPortefeuille.PressColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btnGestionPortefeuille.PressTextColor = System.Drawing.Color.White;
            this.btnGestionPortefeuille.Size = new System.Drawing.Size(318, 62);
            this.btnGestionPortefeuille.Style = MetroSet_UI.Enums.Style.Light;
            this.btnGestionPortefeuille.StyleManager = null;
            this.btnGestionPortefeuille.TabIndex = 0;
            this.btnGestionPortefeuille.Text = "Gérer les portefeuilles";
            this.btnGestionPortefeuille.ThemeAuthor = "Narwin";
            this.btnGestionPortefeuille.ThemeName = "MetroLite";
            this.btnGestionPortefeuille.Click += new System.EventHandler(this.btnGestionPortefeuille_Click);
            // 
            // btnParametres
            // 
            this.btnParametres.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnParametres.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnParametres.DisabledForeColor = System.Drawing.Color.Gray;
            this.btnParametres.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnParametres.HoverBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btnParametres.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btnParametres.HoverTextColor = System.Drawing.Color.White;
            this.btnParametres.IsDerivedStyle = true;
            this.btnParametres.Location = new System.Drawing.Point(15, 173);
            this.btnParametres.Name = "btnParametres";
            this.btnParametres.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnParametres.NormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnParametres.NormalTextColor = System.Drawing.Color.White;
            this.btnParametres.PressBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btnParametres.PressColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btnParametres.PressTextColor = System.Drawing.Color.White;
            this.btnParametres.Size = new System.Drawing.Size(318, 62);
            this.btnParametres.Style = MetroSet_UI.Enums.Style.Light;
            this.btnParametres.StyleManager = null;
            this.btnParametres.TabIndex = 1;
            this.btnParametres.Text = "Paramètres";
            this.btnParametres.ThemeAuthor = "Narwin";
            this.btnParametres.ThemeName = "MetroLite";
            this.btnParametres.Click += new System.EventHandler(this.btnParametres_Click);
            // 
            // btnDeconnexion
            // 
            this.btnDeconnexion.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnDeconnexion.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnDeconnexion.DisabledForeColor = System.Drawing.Color.Gray;
            this.btnDeconnexion.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnDeconnexion.HoverBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btnDeconnexion.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btnDeconnexion.HoverTextColor = System.Drawing.Color.White;
            this.btnDeconnexion.IsDerivedStyle = true;
            this.btnDeconnexion.Location = new System.Drawing.Point(15, 241);
            this.btnDeconnexion.Name = "btnDeconnexion";
            this.btnDeconnexion.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnDeconnexion.NormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnDeconnexion.NormalTextColor = System.Drawing.Color.White;
            this.btnDeconnexion.PressBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btnDeconnexion.PressColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btnDeconnexion.PressTextColor = System.Drawing.Color.White;
            this.btnDeconnexion.Size = new System.Drawing.Size(318, 62);
            this.btnDeconnexion.Style = MetroSet_UI.Enums.Style.Light;
            this.btnDeconnexion.StyleManager = null;
            this.btnDeconnexion.TabIndex = 2;
            this.btnDeconnexion.Text = "Déconnexion";
            this.btnDeconnexion.ThemeAuthor = "Narwin";
            this.btnDeconnexion.ThemeName = "MetroLite";
            this.btnDeconnexion.Click += new System.EventHandler(this.btnDeconnexion_Click);
            // 
            // metroSetControlBox1
            // 
            this.metroSetControlBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.metroSetControlBox1.CloseHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.metroSetControlBox1.CloseHoverForeColor = System.Drawing.Color.White;
            this.metroSetControlBox1.CloseNormalForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.DisabledForeColor = System.Drawing.Color.DimGray;
            this.metroSetControlBox1.IsDerivedStyle = true;
            this.metroSetControlBox1.Location = new System.Drawing.Point(233, 10);
            this.metroSetControlBox1.MaximizeBox = true;
            this.metroSetControlBox1.MaximizeHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.metroSetControlBox1.MaximizeHoverForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.MaximizeNormalForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.MinimizeBox = true;
            this.metroSetControlBox1.MinimizeHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.metroSetControlBox1.MinimizeHoverForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.MinimizeNormalForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.Name = "metroSetControlBox1";
            this.metroSetControlBox1.Size = new System.Drawing.Size(100, 25);
            this.metroSetControlBox1.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetControlBox1.StyleManager = null;
            this.metroSetControlBox1.TabIndex = 3;
            this.metroSetControlBox1.Text = "metroSetControlBox1";
            this.metroSetControlBox1.ThemeAuthor = "Narwin";
            this.metroSetControlBox1.ThemeName = "MetroLite";
            // 
            // frmMonCompte
            // 
            this.AllowResize = false;
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 26F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(348, 317);
            this.Controls.Add(this.metroSetControlBox1);
            this.Controls.Add(this.btnDeconnexion);
            this.Controls.Add(this.btnParametres);
            this.Controls.Add(this.btnGestionPortefeuille);
            this.Name = "frmMonCompte";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mon compte";
            this.ResumeLayout(false);

        }

        #endregion

        private MetroSet_UI.Controls.MetroSetButton btnGestionPortefeuille;
        private MetroSet_UI.Controls.MetroSetButton btnParametres;
        private MetroSet_UI.Controls.MetroSetButton btnDeconnexion;
        private MetroSet_UI.Controls.MetroSetControlBox metroSetControlBox1;
    }
}