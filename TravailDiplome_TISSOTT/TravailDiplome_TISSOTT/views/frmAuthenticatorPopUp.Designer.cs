﻿namespace TravailDiplome_TISSOTT
{
    partial class frmAuthenticatorPopUp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCode = new MetroSet_UI.Controls.MetroSetButton();
            this.tbxCode = new MetroSet_UI.Controls.MetroSetTextBox();
            this.metroSetLabel1 = new MetroSet_UI.Controls.MetroSetLabel();
            this.SuspendLayout();
            // 
            // btnCode
            // 
            this.btnCode.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnCode.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnCode.DisabledForeColor = System.Drawing.Color.Gray;
            this.btnCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnCode.HoverBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btnCode.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.btnCode.HoverTextColor = System.Drawing.Color.White;
            this.btnCode.IsDerivedStyle = true;
            this.btnCode.Location = new System.Drawing.Point(85, 243);
            this.btnCode.Name = "btnCode";
            this.btnCode.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnCode.NormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnCode.NormalTextColor = System.Drawing.Color.White;
            this.btnCode.PressBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btnCode.PressColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.btnCode.PressTextColor = System.Drawing.Color.White;
            this.btnCode.Size = new System.Drawing.Size(210, 48);
            this.btnCode.Style = MetroSet_UI.Enums.Style.Light;
            this.btnCode.StyleManager = null;
            this.btnCode.TabIndex = 8;
            this.btnCode.Text = "Vérification";
            this.btnCode.ThemeAuthor = "Narwin";
            this.btnCode.ThemeName = "MetroLite";
            this.btnCode.Click += new System.EventHandler(this.btnCode_Click);
            // 
            // tbxCode
            // 
            this.tbxCode.AutoCompleteCustomSource = null;
            this.tbxCode.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.tbxCode.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.tbxCode.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.tbxCode.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.tbxCode.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.tbxCode.DisabledForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            this.tbxCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.tbxCode.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.tbxCode.Image = null;
            this.tbxCode.IsDerivedStyle = true;
            this.tbxCode.Lines = null;
            this.tbxCode.Location = new System.Drawing.Point(85, 184);
            this.tbxCode.MaxLength = 32767;
            this.tbxCode.Multiline = false;
            this.tbxCode.Name = "tbxCode";
            this.tbxCode.ReadOnly = false;
            this.tbxCode.Size = new System.Drawing.Size(210, 30);
            this.tbxCode.Style = MetroSet_UI.Enums.Style.Light;
            this.tbxCode.StyleManager = null;
            this.tbxCode.TabIndex = 7;
            this.tbxCode.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbxCode.ThemeAuthor = "Narwin";
            this.tbxCode.ThemeName = "MetroLite";
            this.tbxCode.UseSystemPasswordChar = false;
            this.tbxCode.WatermarkText = "Code";
            // 
            // metroSetLabel1
            // 
            this.metroSetLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.metroSetLabel1.IsDerivedStyle = true;
            this.metroSetLabel1.Location = new System.Drawing.Point(96, 132);
            this.metroSetLabel1.Name = "metroSetLabel1";
            this.metroSetLabel1.Size = new System.Drawing.Size(189, 23);
            this.metroSetLabel1.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetLabel1.StyleManager = null;
            this.metroSetLabel1.TabIndex = 6;
            this.metroSetLabel1.Text = "Code d\'authentification";
            this.metroSetLabel1.ThemeAuthor = "Narwin";
            this.metroSetLabel1.ThemeName = "MetroLite";
            // 
            // frmAuthenticatorPopUp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 26F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(380, 353);
            this.Controls.Add(this.btnCode);
            this.Controls.Add(this.tbxCode);
            this.Controls.Add(this.metroSetLabel1);
            this.Moveable = false;
            this.Name = "frmAuthenticatorPopUp";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Code d\'authentification";
            this.ResumeLayout(false);

        }

        #endregion

        private MetroSet_UI.Controls.MetroSetButton btnCode;
        private MetroSet_UI.Controls.MetroSetLabel metroSetLabel1;
        internal MetroSet_UI.Controls.MetroSetTextBox tbxCode;
    }
}