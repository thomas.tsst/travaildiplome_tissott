﻿/* 
 * App : SecuWallet
 * Auteur : Thomas Tissot
 * Org : CFPT
 * Date : 07.06.2022
 * brief : Classe permettant de stocker les paramètres de l'utilisateur.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravailDiplome_TISSOTT
{
    internal class Settings
    {
        List<Contact> contacts;
        string currency;

        public List<Contact> Contacts { get => contacts; set => contacts = value; }
        public string Currency { get => currency; set => currency = value; }

        public Settings()
        {
            Currency = "USD";
            Contacts = new List<Contact>();
        }

        public string GetContactPublicKeyWithIndex(int index)
        {
            return Contacts[index].PublicKey;
        }
  
    }
}
