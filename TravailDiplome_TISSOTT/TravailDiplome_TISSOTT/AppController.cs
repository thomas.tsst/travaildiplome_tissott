﻿/* 
 * App : SecuWallet
 * Auteur : Thomas Tissot
 * Org : CFPT
 * Date : 07.06.2022
 * brief : Classe nécessaire aux recueille d'informations nécessaire au bon fonctionnement de l'application
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
namespace TravailDiplome_TISSOTT
{
    public class AppController
    {
        private User user;
        BlockchainController bc;
        Settings settings;
        public User User { get => user; set => user = value; }
        internal BlockchainController Bc { get => bc; set => bc = value; }
        internal Settings Settings { get => settings; set => settings = value; }

        public AppController(string userEmail)
        {
            User = new User(userEmail);
            Bc = new BlockchainController();
            CheckIfSettingsAreSaved();
        }
        /// <summary>
        /// Vérifie si des paramètre on déjà été sauvegardés en lien avec l'utilisateur actuellement connecté
        /// </summary>
        private void CheckIfSettingsAreSaved()
        {
            if (File.Exists(Environment.CurrentDirectory +"\\" + User.Email + "/settings/settings.json"))
            {
                string content = File.ReadAllText(Environment.CurrentDirectory + "\\" + User.Email + "/settings/settings.json");
                Settings = JsonConvert.DeserializeObject<Settings>(content);
            }
            else
            {
                Settings = new Settings();
                DirectoryInfo di = Directory.CreateDirectory(Environment.CurrentDirectory+ "\\" + User.Email + "\\settings");
                di.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
            }
        }
        /// <summary>
        /// Sérialise et sauvegarde l'objet settings présent dans cette classe dans le répertoire propre à l'utilisateur actuellement connecté
        /// </summary>
        public void SaveSettings()
        {
            JsonSerializer serializer = new JsonSerializer();
            using (StreamWriter sw = new StreamWriter(Environment.CurrentDirectory + "\\" + User.Email + "/settings/settings.json"))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                serializer.Serialize(writer, Settings);
            }

        }
        /// <summary>
        /// Affiche la vue Authenticator et attend une réponse de celle-ci 
        /// </summary>
        /// <param name="frmOwner"></param>
        /// <returns></returns>
        public string ShowAuthenticatorDialogBox(Form frmOwner)
        {
            frmAuthenticatorPopUp frm = new frmAuthenticatorPopUp(this);
            string result;
            if (frm.ShowDialog(frmOwner) == DialogResult.OK)
            {
                result = "ok";
            }
            else
            {
                result = "Cancelled";
            }
            frm.Dispose();
            return result;
        }
    }
}
