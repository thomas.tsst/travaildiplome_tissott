﻿/* 
 * App : SecuWallet
 * Auteur : Thomas Tissot
 * Org : CFPT
 * Date : 07.06.2022
 * brief : Classe permettant de stocker les informations de l'utilisateur.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravailDiplome_TISSOTT
{
    public class User
    {
        private string email;
        private string token;
        public string Email { get => email; set => email = value; }
        public string Token { get => token; set => token = value; }


        public User(string email)
        {
            Email = email;
        }
    }
}
