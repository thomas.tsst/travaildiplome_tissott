﻿/* 
 * App : SecuWallet
 * Auteur : Thomas Tissot
 * Org : CFPT
 * Date : 07.06.2022
 * brief : Classe représentant un contact dans l'application.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravailDiplome_TISSOTT
{
    public class Contact
    {
        string name;
        string publicKey;

        public string Name { get => name; set => name = value; }
        public string PublicKey { get => publicKey; set => publicKey = value; }

        public Contact(string name, string publicKey)
        {
            Name = name;
            PublicKey = publicKey;
        }
    }
}
