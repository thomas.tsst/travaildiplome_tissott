﻿/* 
 * App : SecuWallet
 * Auteur : Thomas Tissot
 * Org : CFPT
 * Date : 07.06.2022
 * brief : classe nécessaire à la communication avec la blockchain et de la gestion des portefeuilles.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nethereum.Hex.HexConvertors.Extensions;
using Nethereum.Web3.Accounts;
using Nethereum.Web3.Accounts.Managed;
using Nethereum.KeyStore.Model;
using Nethereum.Web3;
using Nethereum.Util;
using Nethereum.Hex.HexTypes;
using System.IO;
using Nethereum.Contracts.Standards.ENS.Registrar.ContractDefinition;
using System.IO.IsolatedStorage;
using Nethereum.RPC.Eth.DTOs;
using Newtonsoft.Json;

namespace TravailDiplome_TISSOTT
{
    internal class BlockchainController
    {
        private List<Wallet> wallets = new List<Wallet>();
        const string BLOCKCHAIN_URL = "http://127.0.0.1:8545/";
        Web3 web3;

        public List<Wallet> Wallets { get => wallets; }

        public BlockchainController()
        {
            web3 = new Web3(BLOCKCHAIN_URL);
        }


        /// <summary>
        /// Prend le dernier id du bloc stocké de la blockchain.
        /// </summary>
        /// <returns></returns>
        public async Task<HexBigInteger> GetLastBlock()
        {
            return await web3.Eth.Blocks.GetBlockNumber.SendRequestAsync();
            
        }

        /// <summary>
        /// Prend le solde d'une publique adresse en "wei" et la converti en "Eth".
        /// </summary>
        /// <param name="adresse"></param>
        /// <returns>Retourne la solde converti en "Eth"</returns>
        public async Task<HexBigInteger> GetBalanceInETH(string adresse)
        {
            return await web3.Eth.GetBalance.SendRequestAsync(adresse); 
        }

        /*public void CreateWallet()
        {
            var ecKey = Nethereum.Signer.EthECKey.GenerateKey();
            var privateKey = ecKey.GetPrivateKeyAsBytes().ToHex();
            var account = new Account(privateKey);
        }*/

        /// <summary>
        /// Génère un fichier "JSON" destiné à contenir les informations de portefeuille à partir d'une clé privée aléatoire.
        /// </summary>
        /// <param name="password"></param>
        /// <param name="name"></param>
        /// <param name="accountName"></param>
        public void GenerateJSONFile(string password, string name, string accountName)
        {
            string path = Environment.CurrentDirectory + "/" + accountName + "/" + name;
            var keyStoreService = new Nethereum.KeyStore.KeyStoreScryptService();
            //Paramètre d'encryption.
            var scryptParams = new ScryptParams { Dklen = 32, N = 262144, R = 1, P = 8 };

            var ecKey = Nethereum.Signer.EthECKey.GenerateKey();
            //Génération de clé privée.
            string key = ecKey.GetPrivateKey();

            var keyStore = keyStoreService.EncryptAndGenerateKeyStore(password, ecKey.GetPrivateKeyAsBytes(), ecKey.GetPublicAddress(), scryptParams);
            var json = keyStoreService.SerializeKeyStoreToJson(keyStore);
            //Vérification si le portefeuille est existant.
            if (File.Exists(path))
            {
                MessageBox.Show("Un portefeuille portant ce nom est déjà existant.");
            }
            else
            {
                File.WriteAllText(path + ".json", json);
                File.WriteAllText(path + ".txt", ecKey.GetPublicAddress());
                MessageBox.Show("Portefeuille créé !");
            }

        }
        /// <summary>
        /// Génère un fichier "JSON" destiné à contenir les informations de portefeuille à partir d'une clé privée spécifié par l'utilisateur.
        /// </summary>
        /// <param name="password"></param>
        /// <param name="name"></param>
        /// <param name="accountName"></param>
        /// <param name="account"></param>
        public void GenerateJSONFile(string password, string name, string accountName, Account account)
        {
            string path = Environment.CurrentDirectory + "/" + accountName + "/" + name;
            var keyStoreService = new Nethereum.KeyStore.KeyStoreScryptService();
            //Paramètre d'encryption.
            var scryptParams = new ScryptParams { Dklen = 32, N = 262144, R = 1, P = 8 };

            var keyStore = keyStoreService.EncryptAndGenerateKeyStore(password, account.PrivateKey.HexToByteArray(), account.PublicKey.ToHexUTF8(), scryptParams);
            var json = keyStoreService.SerializeKeyStoreToJson(keyStore);
            //Vérification si le portefeuille est existant.
            if (File.Exists(path))
            {
                MessageBox.Show("Un portefeuille portant ce nom est déjà existant.");
            }
            else
            {
                File.WriteAllText(path + ".json", json);
                File.WriteAllText(path + ".txt", account.Address);
            }

        }
        /// <summary>
        /// Décrypte le fichier "JSON" à l'aide du mot de passe à l'emplacement préciser par jsonPath.
        /// </summary>
        /// <param name="password">Mot de passe créé à la création ou l’importation du portefeuille.</param>
        /// <param name="jsonPath">Chemin d'accès au fichier.</param>
        /// <returns></returns>
        public string DecryptJSONFile(string password, string jsonPath)
        {
            byte[] key = new byte[1000];
            var keyStoreService = new Nethereum.KeyStore.KeyStoreScryptService();
            var json = File.ReadAllText(jsonPath);
            try
            {
                key = keyStoreService.DecryptKeyStoreFromJson(password, json);
            }
            catch (Exception)
            {

                MessageBox.Show("Mot de passe du portefeuille érroné");
            }
            
            return key.ToHex();
        }
        /// <summary>
        /// Créer une transaction sur la blockchain.
        /// </summary>
        /// <param name="privateKey"></param>
        /// <param name="publicAddress"></param>
        /// <param name="amount"></param>
        public async void CreateTransaction(string privateKey, string publicAddress, decimal amount)
        {
            if (isAdressValid(publicAddress))
            {
                Account account = GetAccountFromPrivateKey(privateKey);
                Web3 web3Transac = new Web3(account);
                //var transac = await web3Transac.TransactionManager.SendTransactionAsync(account.Address, publicAddress, new HexBigInteger(20));
                var transaction = await web3Transac.Eth.GetEtherTransferService().TransferEtherAndWaitForReceiptAsync(publicAddress, amount, 2);
            }
            else
            {
                MessageBox.Show("Adresse publique invalide");
            }
             
        }
        /// <summary>
        /// Vérifie si le répertoire propre à l'utilisateur connecté a déjà été créé.
        /// </summary>
        /// <param name="email"></param>
        public void KeystoreDirectoryCheck(string email)
        {
            string path = Environment.CurrentDirectory + "\\" + email;
            if (Directory.Exists(path))
            {
                wallets = GetUserWallets(path);
            }
            else
            {
                DirectoryInfo di = Directory.CreateDirectory(path);
                di.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
            }
        }
        /// <summary>
        /// Vérification des différents portefeuilles disponible de l'utilisateur connecté.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public List<Wallet> GetUserWallets(string path)
        {
            List<Wallet> result = new List<Wallet>();
            string[] filesJson = Directory.GetFiles(path, "*.json");
            string[] filesTxt = Directory.GetFiles(path, "*.txt");

            foreach (string filePath in filesJson)
            {
                result.Add(new Wallet(Path.GetFileNameWithoutExtension(filePath), filePath));
            }
            for (int i = 0; i < filesTxt.Length; i++)
            {
                result[i].PublicKey = File.ReadAllText(filesTxt[i]);
            }

            return result;
        }
        /// <summary>
        /// Créer un objet “Account” à partir d'une clé privé et appelle la méthode nécessaire à la génération du fichier "JSON".
        /// </summary>
        /// <param name="privateKey"></param>
        /// <param name="password"></param>
        /// <param name="email"></param>
        /// <param name="walletName"></param>
        public void ImportAccountWithPrivateKey(string privateKey, string password, string email, string walletName)
        {
            Account account = new Account(privateKey);

            GenerateJSONFile(password, walletName, email, account);
        }
        /// <summary>
        /// Créer un objet “Account” à partir d'un fichier “JSON” décrypté pour atteindre la clé privée et appelle la méthode nécessaire à la génération du fichier “JSON”.
        /// </summary>
        /// <param name="password"></param>
        /// <param name="path"></param>
        /// <param name="name"></param>
        /// <param name="accountName"></param>
        public void ImportAccountWithFile(string password, string path, string name, string accountName)
        {
            string privateKey = DecryptJSONFile(password, path);
            Account account = GetAccountFromPrivateKey(privateKey);

            GenerateJSONFile(password, name, accountName, account);
        }
        /// <summary>
        /// Supprime le fichier d'un portefeuille.
        /// </summary>
        /// <param name="walletIndex"></param>
        /// <param name="emailUser"></param>
        public void DeleteWallet(int walletIndex, string emailUser)
        {
            string walletName = Wallets[walletIndex].Name;
            string rootFolderPath = Environment.CurrentDirectory + "/" + emailUser + "/";
            string filesToDelete = walletName + "*";
            string[] fileList = System.IO.Directory.GetFiles(rootFolderPath, filesToDelete);
            foreach (string file in fileList)
            {
                System.IO.File.Delete(file);
            }
            Wallets.RemoveAt(walletIndex);
        }
        /// <summary>
        /// Génère un "Account" à partir d'une clé privée.
        /// </summary>
        /// <param name="privateKey"></param>
        /// <returns></returns>
        public Account GetAccountFromPrivateKey(string privateKey)
        {
            return new Account(privateKey);
        }
        /// <summary>
        /// Vérifie si une adresse publique est valide.
        /// </summary>
        /// <param name="publicKey"></param>
        /// <returns></returns>
        public bool isAdressValid(string publicKey)
        {
            if (publicKey.Length == 42 && publicKey[0] == '0' && publicKey[1] == 'x')
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// Retourne l'adresse publique d'un portefeuille grâce à son index dans la liste Wallets.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public string GetPublicAddressWithWalletIndex(int index)
        {
            return Wallets[index].PublicKey;
        }

        /// <summary>
        /// Récupère toutes les transactions où l'adresse publique est présente dans la blockchain.
        /// </summary>
        /// <param name="publicAddress"></param>
        /// <returns></returns>
        public async Task<Dictionary<Transaction, string>> GetAllTransacFromPublicAddress(string publicAddress)
        {
            var response = await GetLastBlock();
            //int lastBlockNumber = Convert.ToInt32(response);
            string blockNumber = response.ToString();
            Dictionary<Transaction, string> myTransac = new Dictionary<Transaction, string>();
            for (int i = 0; i < Convert.ToInt32(blockNumber); i++)
            {
                var blockWithTransac = await web3.Eth.Blocks.GetBlockWithTransactionsByNumber.SendRequestAsync(new BlockParameter(i.ToHexBigInteger()));
                Transaction[] lstTransac = blockWithTransac.Transactions;
                foreach (Transaction t in lstTransac)
                {
                    if (t.From == publicAddress.ToLower() || t.To == publicAddress.ToLower())
                    {
                        myTransac.Add(t, blockWithTransac.Timestamp.ToString());
                    }
                }
            }

            return myTransac;
        }

        /// <summary>
        /// Récupère tous les blocs avec leurs transactions de la blockchain.
        /// </summary>
        /// <returns></returns>
        private async Task<List<BlockWithTransactions>> GetAllBlocks()
        {
            List<BlockWithTransactions> blocks = new List<BlockWithTransactions>();
            var response = await GetLastBlock();
            string blockNumber = response.ToString();
            for (int i = 0; i < Convert.ToInt32(blockNumber); i++)
            {
                blocks.Add(await web3.Eth.Blocks.GetBlockWithTransactionsByNumber.SendRequestAsync(new BlockParameter(i.ToHexBigInteger())));
            }


            return blocks;
        }
        /// <summary>
        /// Récupère la quantité d’Ethereum présente sur une adresse publique au fil du temps.
        /// </summary>
        /// <param name="adresse"></param>
        /// <returns></returns>
        public async Task<Dictionary<string, double>> GetBalanceAtAllBlocks(string adresse)
        {
            Dictionary<string, double> balanceAtTimestamp = new Dictionary<string, double>();
            List<BlockWithTransactions> blocks = await GetAllBlocks();

            for (int i = 0; i < blocks.Count; i++)
            {
                var result = await web3.Eth.GetBalance.SendRequestAsync(adresse, new BlockParameter(i.ToHexBigInteger()));
                Console.WriteLine(result.ToString());
                Console.WriteLine(blocks[i].Timestamp.ToString());
                balanceAtTimestamp.Add(blocks[i].Timestamp.ToString(), Convert.ToDouble(result.ToString()));
            }

            return balanceAtTimestamp;
        }

    }
}
