﻿/* 
 * App : SecuWallet
 * Auteur : Thomas Tissot
 * Org : CFPT
 * Date : 07.06.2022
 * brief : Classe nécessaire à la communication avec l'API PHP.
 */
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace TravailDiplome_TISSOTT
{
    public class JwtController
    {
        private const string LOGIN_URL = "login.php";
        private const string AUTHENTICATOR_URL = "authenticator.php";
        private const string GET_TOKEN_URL = "tokenCreator.php";
        private const string CHECK_TOKEN_URL = "tokenAuthenticator.php";
        private static readonly HttpClient client = new HttpClient();
        public JwtController()
        {
            client.BaseAddress = new Uri("http://127.0.0.1/site/api/");
            /*string usersUrl = "login.php";
            string authUrl = usersUrl + "authenticate.php";
            Task<string> checkLogin = CheckUserLogin(usersUrl, "");
            checkLogin.Wait();
            object login = JsonConvert.DeserializeObject(checkLogin.Result);
            var tokenTask = GetToken("tokenCreator.php", "tt@gmail.com");
            tokenTask.Wait();
            string token = JsonConvert.DeserializeObject<string>(tokenTask.Result);

            var checkToken = CheckToken("tokenAuthenticator.php", token, "tt@gmail.com");
            checkToken.Wait();
            string checkedToken = JsonConvert.DeserializeObject<string>(checkToken.Result);

            Task<string> checkAuthenticator = CheckUserAuthenticator("authenticator.php", "123456");
            checkAuthenticator.Wait();
            object authenticator = JsonConvert.DeserializeObject(checkAuthenticator.Result);*/
        }
        /// <summary>
        /// Envoie l'email et le mot de passe de l'utilisateur à l'API pour vérifier les informations.
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public async static Task<string> CheckUserLogin(string email, string password)
        {
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var stringContent = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("email", email),
                new KeyValuePair<string, string>("password", password),
            });

            var response = await client.PostAsync(LOGIN_URL, stringContent).ConfigureAwait(false);

            return await response.Content.ReadAsStringAsync();
        }

        /// <summary>
        /// Envoie le code Google Authenticator à l'API pour vérifier les informations.
        /// </summary>
        /// <param name="email"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        public async static Task<string> CheckUserAuthenticator(string email,string code)
        {
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var stringContent = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("email", email),
                new KeyValuePair<string, string>("code", code),
            });

            var response = await client.PostAsync(AUTHENTICATOR_URL, stringContent).ConfigureAwait(false);

            return await response.Content.ReadAsStringAsync();
        }
        /// <summary>
        /// Récupère un nouveau token JWT à l'API.
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public async static Task<string> GetToken(string email)
        {
            var values = new Dictionary<string, string>
            {
                { "email", email },
            };
            FormUrlEncodedContent formUrlEncodedContent = new FormUrlEncodedContent(values);
            var response = await client.PostAsync(GET_TOKEN_URL, formUrlEncodedContent).ConfigureAwait(false);

            return await response.Content.ReadAsStringAsync();
        }
        /// <summary>
        /// Vérification du token de l'utilisateur par l'API.
        /// </summary>
        /// <param name="token"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        public async static Task<string> CheckToken(string token, string email)
        {
            var stringContent = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("token", token),
                new KeyValuePair<string, string>("email", email),
            });

            var response = await client.PostAsync(CHECK_TOKEN_URL, stringContent).ConfigureAwait(false);

            return await response.Content.ReadAsStringAsync();
        }
        /// <summary>
        /// Désérialise les réponses de l'API.
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        public static object DeserializeResponse(Task<string> response)
        {
            response.Wait();
            return JsonConvert.DeserializeObject(response.Result);
        }

    }
}
