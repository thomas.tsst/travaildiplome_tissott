# Code source
## C#

### AppController.cs
```C#
/* 
 * App : SecuWallet
 * Auteur : Thomas Tissot
 * Org : CFPT
 * Date : 07.06.2022
 * brief : Classe nécessaire aux recueille d'informations nécessaire au bon fonctionnement de l'application
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
namespace TravailDiplome_TISSOTT
{
    public class AppController
    {
        private User user;
        BlockchainController bc;
        Settings settings;
        public User User { get => user; set => user = value; }
        internal BlockchainController Bc { get => bc; set => bc = value; }
        internal Settings Settings { get => settings; set => settings = value; }

        public AppController(string userEmail)
        {
            User = new User(userEmail);
            Bc = new BlockchainController();
            CheckIfSettingsAreSaved();
        }
        /// <summary>
        /// Vérifie si des paramètre on déjà été sauvegardés en lien avec l'utilisateur actuellement connecté
        /// </summary>
        private void CheckIfSettingsAreSaved()
        {
            if (File.Exists(Environment.CurrentDirectory +"\\" + User.Email + "/settings/settings.json"))
            {
                string content = File.ReadAllText(Environment.CurrentDirectory + "\\" + User.Email + "/settings/settings.json");
                Settings = JsonConvert.DeserializeObject<Settings>(content);
            }
            else
            {
                Settings = new Settings();
                DirectoryInfo di = Directory.CreateDirectory(Environment.CurrentDirectory+ "\\" + User.Email + "\\settings");
                di.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
            }
        }
        /// <summary>
        /// Sérialise et sauvegarde l'objet settings présent dans cette classe dans le répertoire propre à l'utilisateur actuellement connecté
        /// </summary>
        public void SaveSettings()
        {
            JsonSerializer serializer = new JsonSerializer();
            using (StreamWriter sw = new StreamWriter(Environment.CurrentDirectory + "\\" + User.Email + "/settings/settings.json"))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                serializer.Serialize(writer, Settings);
            }

        }
        /// <summary>
        /// Affiche la vue Authenticator et attend une réponse de celle-ci 
        /// </summary>
        /// <param name="frmOwner"></param>
        /// <returns></returns>
        public string ShowAuthenticatorDialogBox(Form frmOwner)
        {
            frmAuthenticatorPopUp frm = new frmAuthenticatorPopUp(this);
            string result;
            if (frm.ShowDialog(frmOwner) == DialogResult.OK)
            {
                result = "ok";
            }
            else
            {
                result = "Cancelled";
            }
            frm.Dispose();
            return result;
        }
    }
}

```
### BlockchainController.cs
```C#
/* 
 * App : SecuWallet
 * Auteur : Thomas Tissot
 * Org : CFPT
 * Date : 07.06.2022
 * brief : classe nécessaire à la communication avec la blockchain et de la gestion des portefeuilles.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nethereum.Hex.HexConvertors.Extensions;
using Nethereum.Web3.Accounts;
using Nethereum.Web3.Accounts.Managed;
using Nethereum.KeyStore.Model;
using Nethereum.Web3;
using Nethereum.Util;
using Nethereum.Hex.HexTypes;
using System.IO;
using Nethereum.Contracts.Standards.ENS.Registrar.ContractDefinition;
using System.IO.IsolatedStorage;
using Nethereum.RPC.Eth.DTOs;
using Newtonsoft.Json;

namespace TravailDiplome_TISSOTT
{
    internal class BlockchainController
    {
        private List<Wallet> wallets = new List<Wallet>();
        const string BLOCKCHAIN_URL = "http://127.0.0.1:8545/";
        Web3 web3;

        public List<Wallet> Wallets { get => wallets; }

        public BlockchainController()
        {
            web3 = new Web3(BLOCKCHAIN_URL);
        }


        /// <summary>
        /// Prend le dernier id du bloc stocké de la blockchain.
        /// </summary>
        /// <returns></returns>
        public async Task<HexBigInteger> GetLastBlock()
        {
            return await web3.Eth.Blocks.GetBlockNumber.SendRequestAsync();
            
        }

        /// <summary>
        /// Prend le solde d'une publique adresse en "wei" et la converti en "Eth".
        /// </summary>
        /// <param name="adresse"></param>
        /// <returns>Retourne la solde converti en "Eth"</returns>
        public async Task<HexBigInteger> GetBalanceInETH(string adresse)
        {
            return await web3.Eth.GetBalance.SendRequestAsync(adresse); 
        }

        /*public void CreateWallet()
        {
            var ecKey = Nethereum.Signer.EthECKey.GenerateKey();
            var privateKey = ecKey.GetPrivateKeyAsBytes().ToHex();
            var account = new Account(privateKey);
        }*/

        /// <summary>
        /// Génère un fichier "JSON" destiné à contenir les informations de portefeuille à partir d'une clé privée aléatoire.
        /// </summary>
        /// <param name="password"></param>
        /// <param name="name"></param>
        /// <param name="accountName"></param>
        public void GenerateJSONFile(string password, string name, string accountName)
        {
            string path = Environment.CurrentDirectory + "/" + accountName + "/" + name;
            var keyStoreService = new Nethereum.KeyStore.KeyStoreScryptService();
            //Paramètre d'encryption.
            var scryptParams = new ScryptParams { Dklen = 32, N = 262144, R = 1, P = 8 };

            var ecKey = Nethereum.Signer.EthECKey.GenerateKey();
            //Génération de clé privée.
            string key = ecKey.GetPrivateKey();

            var keyStore = keyStoreService.EncryptAndGenerateKeyStore(password, ecKey.GetPrivateKeyAsBytes(), ecKey.GetPublicAddress(), scryptParams);
            var json = keyStoreService.SerializeKeyStoreToJson(keyStore);
            //Vérification si le portefeuille est existant.
            if (File.Exists(path))
            {
                MessageBox.Show("Un portefeuille portant ce nom est déjà existant.");
            }
            else
            {
                File.WriteAllText(path + ".json", json);
                File.WriteAllText(path + ".txt", ecKey.GetPublicAddress());
                MessageBox.Show("Portefeuille créé !");
            }

        }
        /// <summary>
        /// Génère un fichier "JSON" destiné à contenir les informations de portefeuille à partir d'une clé privée spécifié par l'utilisateur.
        /// </summary>
        /// <param name="password"></param>
        /// <param name="name"></param>
        /// <param name="accountName"></param>
        /// <param name="account"></param>
        public void GenerateJSONFile(string password, string name, string accountName, Account account)
        {
            string path = Environment.CurrentDirectory + "/" + accountName + "/" + name;
            var keyStoreService = new Nethereum.KeyStore.KeyStoreScryptService();
            //Paramètre d'encryption.
            var scryptParams = new ScryptParams { Dklen = 32, N = 262144, R = 1, P = 8 };

            var keyStore = keyStoreService.EncryptAndGenerateKeyStore(password, account.PrivateKey.HexToByteArray(), account.PublicKey.ToHexUTF8(), scryptParams);
            var json = keyStoreService.SerializeKeyStoreToJson(keyStore);
            //Vérification si le portefeuille est existant.
            if (File.Exists(path))
            {
                MessageBox.Show("Un portefeuille portant ce nom est déjà existant.");
            }
            else
            {
                File.WriteAllText(path + ".json", json);
                File.WriteAllText(path + ".txt", account.Address);
            }

        }
        /// <summary>
        /// Décrypte le fichier "JSON" à l'aide du mot de passe à l'emplacement préciser par jsonPath.
        /// </summary>
        /// <param name="password">Mot de passe créé à la création ou l’importation du portefeuille.</param>
        /// <param name="jsonPath">Chemin d'accès au fichier.</param>
        /// <returns></returns>
        public string DecryptJSONFile(string password, string jsonPath)
        {
            byte[] key = new byte[1000];
            var keyStoreService = new Nethereum.KeyStore.KeyStoreScryptService();
            var json = File.ReadAllText(jsonPath);
            try
            {
                key = keyStoreService.DecryptKeyStoreFromJson(password, json);
            }
            catch (Exception)
            {

                MessageBox.Show("Mot de passe du portefeuille érroné");
            }
            
            return key.ToHex();
        }
        /// <summary>
        /// Créer une transaction sur la blockchain.
        /// </summary>
        /// <param name="privateKey"></param>
        /// <param name="publicAddress"></param>
        /// <param name="amount"></param>
        public async void CreateTransaction(string privateKey, string publicAddress, decimal amount)
        {
            if (isAdressValid(publicAddress))
            {
                Account account = GetAccountFromPrivateKey(privateKey);
                Web3 web3Transac = new Web3(account);
                //var transac = await web3Transac.TransactionManager.SendTransactionAsync(account.Address, publicAddress, new HexBigInteger(20));
                var transaction = await web3Transac.Eth.GetEtherTransferService().TransferEtherAndWaitForReceiptAsync(publicAddress, amount, 2);
            }
            else
            {
                MessageBox.Show("Adresse publique invalide");
            }
             
        }
        /// <summary>
        /// Vérifie si le répertoire propre à l'utilisateur connecté a déjà été créé.
        /// </summary>
        /// <param name="email"></param>
        public void KeystoreDirectoryCheck(string email)
        {
            string path = Environment.CurrentDirectory + "\\" + email;
            if (Directory.Exists(path))
            {
                wallets = GetUserWallets(path);
            }
            else
            {
                DirectoryInfo di = Directory.CreateDirectory(path);
                di.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
            }
        }
        /// <summary>
        /// Vérification des différents portefeuilles disponible de l'utilisateur connecté.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public List<Wallet> GetUserWallets(string path)
        {
            List<Wallet> result = new List<Wallet>();
            string[] filesJson = Directory.GetFiles(path, "*.json");
            string[] filesTxt = Directory.GetFiles(path, "*.txt");

            foreach (string filePath in filesJson)
            {
                result.Add(new Wallet(Path.GetFileNameWithoutExtension(filePath), filePath));
            }
            for (int i = 0; i < filesTxt.Length; i++)
            {
                result[i].PublicKey = File.ReadAllText(filesTxt[i]);
            }

            return result;
        }
        /// <summary>
        /// Créer un objet “Account” à partir d'une clé privé et appelle la méthode nécessaire à la génération du fichier "JSON".
        /// </summary>
        /// <param name="privateKey"></param>
        /// <param name="password"></param>
        /// <param name="email"></param>
        /// <param name="walletName"></param>
        public void ImportAccountWithPrivateKey(string privateKey, string password, string email, string walletName)
        {
            Account account = new Account(privateKey);

            GenerateJSONFile(password, walletName, email, account);
        }
        /// <summary>
        /// Créer un objet “Account” à partir d'un fichier “JSON” décrypté pour atteindre la clé privée et appelle la méthode nécessaire à la génération du fichier “JSON”.
        /// </summary>
        /// <param name="password"></param>
        /// <param name="path"></param>
        /// <param name="name"></param>
        /// <param name="accountName"></param>
        public void ImportAccountWithFile(string password, string path, string name, string accountName)
        {
            string privateKey = DecryptJSONFile(password, path);
            Account account = GetAccountFromPrivateKey(privateKey);

            GenerateJSONFile(password, name, accountName, account);
        }
        /// <summary>
        /// Supprime le fichier d'un portefeuille.
        /// </summary>
        /// <param name="walletIndex"></param>
        /// <param name="emailUser"></param>
        public void DeleteWallet(int walletIndex, string emailUser)
        {
            string walletName = Wallets[walletIndex].Name;
            string rootFolderPath = Environment.CurrentDirectory + "/" + emailUser + "/";
            string filesToDelete = walletName + "*";
            string[] fileList = System.IO.Directory.GetFiles(rootFolderPath, filesToDelete);
            foreach (string file in fileList)
            {
                System.IO.File.Delete(file);
            }
            Wallets.RemoveAt(walletIndex);
        }
        /// <summary>
        /// Génère un "Account" à partir d'une clé privée.
        /// </summary>
        /// <param name="privateKey"></param>
        /// <returns></returns>
        public Account GetAccountFromPrivateKey(string privateKey)
        {
            return new Account(privateKey);
        }
        /// <summary>
        /// Vérifie si une adresse publique est valide.
        /// </summary>
        /// <param name="publicKey"></param>
        /// <returns></returns>
        public bool isAdressValid(string publicKey)
        {
            if (publicKey.Length == 42 && publicKey[0] == '0' && publicKey[1] == 'x')
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// Retourne l'adresse publique d'un portefeuille grâce à son index dans la liste Wallets.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public string GetPublicAddressWithWalletIndex(int index)
        {
            return Wallets[index].PublicKey;
        }

        /// <summary>
        /// Récupère toutes les transactions où l'adresse publique est présente dans la blockchain.
        /// </summary>
        /// <param name="publicAddress"></param>
        /// <returns></returns>
        public async Task<Dictionary<Transaction, string>> GetAllTransacFromPublicAddress(string publicAddress)
        {
            var response = await GetLastBlock();
            //int lastBlockNumber = Convert.ToInt32(response);
            string blockNumber = response.ToString();
            Dictionary<Transaction, string> myTransac = new Dictionary<Transaction, string>();
            for (int i = 0; i < Convert.ToInt32(blockNumber); i++)
            {
                var blockWithTransac = await web3.Eth.Blocks.GetBlockWithTransactionsByNumber.SendRequestAsync(new BlockParameter(i.ToHexBigInteger()));
                Transaction[] lstTransac = blockWithTransac.Transactions;
                foreach (Transaction t in lstTransac)
                {
                    if (t.From == publicAddress.ToLower() || t.To == publicAddress.ToLower())
                    {
                        myTransac.Add(t, blockWithTransac.Timestamp.ToString());
                    }
                }
            }

            return myTransac;
        }

        /// <summary>
        /// Récupère tous les blocs avec leurs transactions de la blockchain.
        /// </summary>
        /// <returns></returns>
        private async Task<List<BlockWithTransactions>> GetAllBlocks()
        {
            List<BlockWithTransactions> blocks = new List<BlockWithTransactions>();
            var response = await GetLastBlock();
            string blockNumber = response.ToString();
            for (int i = 0; i < Convert.ToInt32(blockNumber); i++)
            {
                blocks.Add(await web3.Eth.Blocks.GetBlockWithTransactionsByNumber.SendRequestAsync(new BlockParameter(i.ToHexBigInteger())));
            }


            return blocks;
        }
        /// <summary>
        /// Récupère la quantité d’Ethereum présente sur une adresse publique au fil du temps.
        /// </summary>
        /// <param name="adresse"></param>
        /// <returns></returns>
        public async Task<Dictionary<string, double>> GetBalanceAtAllBlocks(string adresse)
        {
            Dictionary<string, double> balanceAtTimestamp = new Dictionary<string, double>();
            List<BlockWithTransactions> blocks = await GetAllBlocks();

            for (int i = 0; i < blocks.Count; i++)
            {
                var result = await web3.Eth.GetBalance.SendRequestAsync(adresse, new BlockParameter(i.ToHexBigInteger()));
                Console.WriteLine(result.ToString());
                Console.WriteLine(blocks[i].Timestamp.ToString());
                balanceAtTimestamp.Add(blocks[i].Timestamp.ToString(), Convert.ToDouble(result.ToString()));
            }

            return balanceAtTimestamp;
        }

    }
}

```
### JwtController.cs
```C#
/* 
 * App : SecuWallet
 * Auteur : Thomas Tissot
 * Org : CFPT
 * Date : 07.06.2022
 * brief : Classe nécessaire à la communication avec l'API PHP.
 */
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace TravailDiplome_TISSOTT
{
    public class JwtController
    {
        private const string LOGIN_URL = "login.php";
        private const string AUTHENTICATOR_URL = "authenticator.php";
        private const string GET_TOKEN_URL = "tokenCreator.php";
        private const string CHECK_TOKEN_URL = "tokenAuthenticator.php";
        private static readonly HttpClient client = new HttpClient();
        public JwtController()
        {
            client.BaseAddress = new Uri("http://127.0.0.1/site/api/");
            /*string usersUrl = "login.php";
            string authUrl = usersUrl + "authenticate.php";
            Task<string> checkLogin = CheckUserLogin(usersUrl, "");
            checkLogin.Wait();
            object login = JsonConvert.DeserializeObject(checkLogin.Result);
            var tokenTask = GetToken("tokenCreator.php", "tt@gmail.com");
            tokenTask.Wait();
            string token = JsonConvert.DeserializeObject<string>(tokenTask.Result);

            var checkToken = CheckToken("tokenAuthenticator.php", token, "tt@gmail.com");
            checkToken.Wait();
            string checkedToken = JsonConvert.DeserializeObject<string>(checkToken.Result);

            Task<string> checkAuthenticator = CheckUserAuthenticator("authenticator.php", "123456");
            checkAuthenticator.Wait();
            object authenticator = JsonConvert.DeserializeObject(checkAuthenticator.Result);*/
        }
        /// <summary>
        /// Envoie l'email et le mot de passe de l'utilisateur à l'API pour vérifier les informations.
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public async static Task<string> CheckUserLogin(string email, string password)
        {
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var stringContent = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("email", email),
                new KeyValuePair<string, string>("password", password),
            });

            var response = await client.PostAsync(LOGIN_URL, stringContent).ConfigureAwait(false);

            return await response.Content.ReadAsStringAsync();
        }

        /// <summary>
        /// Envoie le code Google Authenticator à l'API pour vérifier les informations.
        /// </summary>
        /// <param name="email"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        public async static Task<string> CheckUserAuthenticator(string email,string code)
        {
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var stringContent = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("email", email),
                new KeyValuePair<string, string>("code", code),
            });

            var response = await client.PostAsync(AUTHENTICATOR_URL, stringContent).ConfigureAwait(false);

            return await response.Content.ReadAsStringAsync();
        }
        /// <summary>
        /// Récupère un nouveau token JWT à l'API.
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public async static Task<string> GetToken(string email)
        {
            var values = new Dictionary<string, string>
            {
                { "email", email },
            };
            FormUrlEncodedContent formUrlEncodedContent = new FormUrlEncodedContent(values);
            var response = await client.PostAsync(GET_TOKEN_URL, formUrlEncodedContent).ConfigureAwait(false);

            return await response.Content.ReadAsStringAsync();
        }
        /// <summary>
        /// Vérification du token de l'utilisateur par l'API.
        /// </summary>
        /// <param name="token"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        public async static Task<string> CheckToken(string token, string email)
        {
            var stringContent = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("token", token),
                new KeyValuePair<string, string>("email", email),
            });

            var response = await client.PostAsync(CHECK_TOKEN_URL, stringContent).ConfigureAwait(false);

            return await response.Content.ReadAsStringAsync();
        }
        /// <summary>
        /// Désérialise les réponses de l'API.
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        public static object DeserializeResponse(Task<string> response)
        {
            response.Wait();
            return JsonConvert.DeserializeObject(response.Result);
        }

    }
}

```
### Contact.cs
```C#
/* 
 * App : SecuWallet
 * Auteur : Thomas Tissot
 * Org : CFPT
 * Date : 07.06.2022
 * brief : Classe représentant un contact dans l'application.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravailDiplome_TISSOTT
{
    public class Contact
    {
        string name;
        string publicKey;

        public string Name { get => name; set => name = value; }
        public string PublicKey { get => publicKey; set => publicKey = value; }

        public Contact(string name, string publicKey)
        {
            Name = name;
            PublicKey = publicKey;
        }
    }
}

```
### Users.cs
```C#
/* 
 * App : SecuWallet
 * Auteur : Thomas Tissot
 * Org : CFPT
 * Date : 07.06.2022
 * brief : Classe permettant de stocker les informations de l'utilisateur.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravailDiplome_TISSOTT
{
    public class User
    {
        private string email;
        private string token;
        public string Email { get => email; set => email = value; }
        public string Token { get => token; set => token = value; }


        public User(string email)
        {
            Email = email;
        }
    }
}

```
### Wallet.cs
```C#
/* 
 * App : SecuWallet
 * Auteur : Thomas Tissot
 * Org : CFPT
 * Date : 07.06.2022
 * brief : Classe permettant de stocker un portefeuille.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravailDiplome_TISSOTT
{
    public class Wallet
    {
        string name;
        string publicKey;
        string path;

        public string Name { get => name; set => name = value; }
        public string PublicKey { get => publicKey; set => publicKey = value; }
        public string Path { get => path; set => path = value; }

        public Wallet(string name, string path)
        {
            Name = name;
            Path = path;
        }
    }
}

```
### Settings.cs
```C#
/* 
 * App : SecuWallet
 * Auteur : Thomas Tissot
 * Org : CFPT
 * Date : 07.06.2022
 * brief : Classe permettant de stocker les paramètres de l'utilisateur.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravailDiplome_TISSOTT
{
    internal class Settings
    {
        List<Contact> contacts;
        string currency;

        public List<Contact> Contacts { get => contacts; set => contacts = value; }
        public string Currency { get => currency; set => currency = value; }

        public Settings()
        {
            Currency = "USD";
            Contacts = new List<Contact>();
        }

        public string GetContactPublicKeyWithIndex(int index)
        {
            return Contacts[index].PublicKey;
        }
  
    }
}

```
### frmLogin.cs
```C#
/* 
 * App : SecuWallet
 * Auteur : Thomas Tissot
 * Org : CFPT
 * Date : 07.06.2022
 * brief : Vue permettant à l'utilisateur de se connecter à l'aide de son email et de son mot de passe.
 */
using MetroSet_UI.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Web.UI.Design.WebControls;
using System.Net;
using System.IO;
using System.Security.Cryptography;
using System.Threading;

namespace TravailDiplome_TISSOTT
{
    public partial class frmLogin : MetroSetForm
    {
        JwtController jwtController = new JwtController();
        AppController app;
        public frmLogin()
        {
            InitializeComponent();
            BlockchainController bc = new BlockchainController();   
        }
        /// <summary>
        /// Evenement servant à vérifié les entrées de connexion de l'utilisateur.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (tbxEmail.Text != "" || tbxPassword.Text != "")
            {
                //Encryption du mot de passe en SHA1.
                string encryptedPassword;
                using (SHA1 sha1Hash = SHA1.Create())
                {
                    byte[] sourceBytes = Encoding.UTF8.GetBytes(tbxPassword.Text);
                    byte[] hashBytes = sha1Hash.ComputeHash(sourceBytes);
                    encryptedPassword = BitConverter.ToString(hashBytes).Replace("-", String.Empty);
                }
                //Vérification par l'API du login utilisateur.
                object reponse = JwtController.DeserializeResponse(JwtController.CheckUserLogin(tbxEmail.Text, encryptedPassword.ToLower()));
                if (reponse.ToString() == "ok")
                {
                    app = new AppController(tbxEmail.Text);
                    frmAuthenticator frmAuthenticator = new frmAuthenticator(app);
                    this.Hide();
                    frmAuthenticator.Closed += (s, args) => this.Close();
                    frmAuthenticator.Show();
                }
                else
                {
                    MessageBox.Show("Email ou mot de passe erroné.");
                }
            }

        }
    }
}

```
### frmAuthenticator.cs
```C#
/* 
 * App : SecuWallet
 * Auteur : Thomas Tissot
 * Org : CFPT
 * Date : 07.06.2022
 * brief : Vue permettant à l'utilisateur d'entrer son code Google Authenticator.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroSet_UI.Forms;

namespace TravailDiplome_TISSOTT
{
    public partial class frmAuthenticator : MetroSetForm
    {
        AppController app;
        public frmAuthenticator(AppController app)
        {
            InitializeComponent();
            this.app = app;
        }

        private void btnCode_Click(object sender, EventArgs e)
        {
            //Le code Google Authenticator doit toujours être égale à 6 chiffres.
            if (tbxCode.Text.Length == 6)
            {
                //Demande à l'API si le code entré par l'utilisateur est valide
                object reponse = JwtController.DeserializeResponse(JwtController.CheckUserAuthenticator(app.User.Email, tbxCode.Text));
                if (reponse.ToString() == "ok")
                {
                    frmMain frmGestionPortefeuilles = new frmMain(app);
                    this.Hide();
                    frmGestionPortefeuilles.Closed += (s, args) => this.Close();
                    frmGestionPortefeuilles.Show();
                }
                else
                {
                    MessageBox.Show("Code erroné.");
                }
            }
        }
    }
}

```
### frmAuthenticatorPopUp.cs
```C#
/* 
 * App : SecuWallet
 * Auteur : Thomas Tissot
 * Org : CFPT
 * Date : 07.06.2022
 * brief : Pop-Up permettant à l'utilisateur d'entrer son code Google Authenticator.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroSet_UI.Forms;
namespace TravailDiplome_TISSOTT
{
    public partial class frmAuthenticatorPopUp : MetroSetForm
    {
        AppController app;
        public frmAuthenticatorPopUp(AppController app)
        {
            InitializeComponent();
            this.app = app;
        }

        private void btnCode_Click(object sender, EventArgs e)
        {
            if (tbxCode.Text.Length == 6)
            {
                //Vérification du token par L'API
                object tokenReponse = JwtController.DeserializeResponse(JwtController.CheckToken(app.User.Token, app.User.Email));
                if (tokenReponse.ToString() == "ok")
                {
                    //Vérification du Google Authenticator par L'API
                    object authenticatorReponse = JwtController.DeserializeResponse(JwtController.CheckUserAuthenticator(app.User.Email, tbxCode.Text));
                    if (authenticatorReponse.ToString() == "ok")
                    {
                        this.DialogResult = DialogResult.OK;
                        this.Hide();
                    }
                    else
                    {
                        MessageBox.Show("Code erroné.");
                    }
                }
            }
        }
    }
}

```
### frmMain.cs
```C#
/* 
 * App : SecuWallet
 * Auteur : Thomas Tissot
 * Org : CFPT
 * Date : 07.06.2022
 * brief : Vue principale permettant à l'utilisateur de visualiser ces différents portefeuilles, ainsi que l’historique de ses transactions et évolutions de ceux-ci.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroSet_UI.Forms;
using Nethereum.Web3;
using Nethereum.RPC.Eth.DTOs;
using System.Net.Http;
using Newtonsoft.Json;
using System.Numerics;
using System.Windows.Forms.DataVisualization.Charting;

namespace TravailDiplome_TISSOTT
{
    public partial class frmMain : MetroSetForm
    {
        AppController app;
        public frmMain(AppController app)
        {
            InitializeComponent();
            this.app = app;

            object response = JwtController.DeserializeResponse(JwtController.GetToken(app.User.Email));
            app.User.Token = response.ToString();


        }
        /// <summary>
        /// Récupère les portefeuilles appartenant à l'utilisateur lors du chargement de cette vue,  et met à jour la vue.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_Load(object sender, EventArgs e)
        {
            app.Bc.KeystoreDirectoryCheck(app.User.Email);
            UpdateView(GetListBoxSelectedIndex());
            UpdateViewAsync();
        }
        /// <summary>
        /// Mise à jour de la vue.
        /// </summary>
        /// <param name="indexListBox"></param>
        private void UpdateView(int indexListBox)
        {
            //Ajoute les portefeuilles dans la listBox.
            if (app.Bc.Wallets.Count > 0)
            {
                lbxWallets.Clear();
                foreach (Wallet wallet in app.Bc.Wallets)
                {
                    lbxWallets.AddItem(wallet.Name);
                }
                lbxWallets.SelectedIndex = indexListBox;
                if (indexListBox != -1)
                {
                    lblNomPortefeuille.Text = app.Bc.Wallets[GetListBoxSelectedIndex()].Name;
                }

            }
            else
            {
                lbxWallets.Clear();
                lbxWallets.AddItem("Aucun portefeuille dispo.");
            }

        }
        /// <summary>
        /// Mise à jours de la vue pour les éléments qui nécéssitent du code asyncrone.
        /// </summary>
        /// <returns></returns>
        private async Task<string> UpdateViewAsync()
        {
            //Récupère le nombre d'Ethereum présent sur le portefeuille sélectionné et l'affiche dans le label.
            var balanceHex = await app.Bc.GetBalanceInETH(app.Bc.Wallets[GetListBoxSelectedIndex()].PublicKey);
            decimal balance = Web3.Convert.FromWei(balanceHex);
            lblMontant.Text = balance.ToString() + " ETH";
            //Récupère les transactions envoyées ou reçues par le portefeuille sélectionné.
            Dictionary<Transaction, string> transactions = await app.Bc.GetAllTransacFromPublicAddress(app.Bc.Wallets[GetListBoxSelectedIndex()].PublicKey);
            dgvTransac.Rows.Clear();

            //Ajout des transactions dans le DataGridView.
            foreach (Transaction item in transactions.Keys)
            {
                string type = "";
                lbxTransactions.Items.Add(item.From + " -> " + item.To + "   Montant :" + Web3.Convert.FromWei(item.Value));
                if (app.Bc.Wallets[GetListBoxSelectedIndex()].PublicKey.ToLower() == item.From)
                {
                    type = "Envoi";
                }
                else
                {
                    type = "Recu";
                }
                DataGridViewRow row = (DataGridViewRow)dgvTransac.Rows[0].Clone();
                row.Cells[0].Value = type;
                row.Cells[1].Value = UnixTimeStampToDateTime(transactions[item]).ToString();
                row.Cells[2].Value = item.From;
                row.Cells[3].Value = item.To;
                row.Cells[4].Value = Web3.Convert.FromWei(item.Value).ToString() + " ETH";
                row.Cells[5].Value = item.TransactionHash;
                dgvTransac.Rows.Add(row);         
            }

            //Récupère le prix actuel de l'Ethereum.
            Price ethValue = await GetEthValue();
            double walletValue = 0;
            //Affichage selon les paramètres de l'utilisateur.
            switch (app.Settings.Currency)
            {
                case "USD":
                    walletValue = ethValue.USD * (double)balance;
                    break;
                case "CHF":
                    walletValue = ethValue.CHF * (double)balance;
                    break;
                case "EUR":
                    walletValue = ethValue.EUR * (double)balance;
                    break;
                default:
                    break;
            }


            //Affichage de la valeur du portefeuille.
            lblMontantFiat.Text = walletValue.ToString("0.##")+ " " + app.Settings.Currency;

            
            Dictionary<string,decimal> balanceValueByTime = new Dictionary<string, decimal>();
            List<decimal> balanceList = new List<decimal>();
            decimal balanceEth;
            //Récupère la quantité d'Ethereum présent sur le portefeuille a chaque bloques.
            var balanceAtBlocks = await app.Bc.GetBalanceAtAllBlocks(app.Bc.Wallets[GetListBoxSelectedIndex()].PublicKey);
            //Récupère l'hisotrique du cours de l'Ethereum.
            Rootobject history = await GetEthValueEveryDay();
            //int dailyTimestamp = 86400;
            int dailyTimestamp = 60;
            //Comparaison de "BalanceAtBlock" et "history" pour récupérer le prix de l'Ethereum à chaque block.
            foreach (string timestamp in balanceAtBlocks.Keys)
            {
                int time = Convert.ToInt32(timestamp);
                for (int i = history.Data.Data.Length-1; i > 0; i--)
                {
                    //Vérification si la date et l'heure du bloc se trouvent dans la journée récupérée sur CryptoCompare API.
                    if (time > history.Data.Data[i].time && time < history.Data.Data[i].time + dailyTimestamp)
                    {
                        BigInteger bigIntFromString = new BigInteger(balanceAtBlocks[timestamp]);
                        balanceEth = Web3.Convert.FromWei(bigIntFromString);
                        balanceList.Add(balanceEth);
                        balanceValueByTime.Add(timestamp, history.Data.Data[i].open * balanceEth);
                        Console.WriteLine("Prix trouvé : " + history.Data.Data[i].open);
                        break;
                    }
                }
            }
            //Modification des propriétés des DataGridView.
            SetUpChart(chartEvolutionValeur);
            SetUpChart(chartEvolutionQuantite);
            //Ajout des points dans le graphique évolution de la quantité d'ethereum disponible sur le portefeuille.
            foreach (string timestamp in balanceValueByTime.Keys)
            {
                DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                dtDateTime = dtDateTime.AddSeconds(Convert.ToDouble(timestamp)).ToLocalTime();
                chartEvolutionValeur.Series[0].Points.AddXY(/*Convert.ToDouble(timestamp)*/dtDateTime, Convert.ToDouble(balanceValueByTime[timestamp]));
                
                chartEvolutionQuantite.Series[0].Points.AddXY(dtDateTime, balanceList[balanceValueByTime.Keys.ToList().IndexOf(timestamp)]);
            }

            //Ajout des points dans le graphique évolution de la valeur.
            /*foreach (var item in history.Data.Data)
            {
                decimal balansadasdce = 0;
                DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                dtDateTime = dtDateTime.AddSeconds(Convert.ToDouble(item.time)).ToLocalTime();
                if (item.time < Convert.ToInt32(balanceAtBlocks.ElementAt(0).Key)) 
                {
                    chartEvolutionValeur.Series[0].Points.AddXY(dtDateTime,0);
                }
                else 
                {
                    foreach (string timestamp in balanceAtBlocks.Keys)
                    {
                        if (Convert.ToInt32(timestamp) > item.time && Convert.ToInt32(timestamp) < item.time + dailyTimestamp)
                        {
                            chartEvolutionValeur.Series[0].Points.AddXY(dtDateTime, Convert.ToDouble(balanceValueByTime[timestamp]));
                            break;
                        }
                        else
                        {
                            chartEvolutionValeur.Series[0].Points.AddXY(dtDateTime, Convert.ToDouble(balanceValueByTime[timestamp]));
                        }
                    }
                }
                
            }*/
                return "ok";
        }
        private void btnSettings_Click(object sender, EventArgs e)
        {
            frmMonCompte frm = new frmMonCompte(app);
            this.Hide();
            frm.Closed += (s, args) => this.Show();
            frm.Show();
        }

        private void btnRecevoir_Click(object sender, EventArgs e)
        {
            frmAdresse frm = new frmAdresse(app.Bc.Wallets[GetListBoxSelectedIndex()].PublicKey);
            this.Hide();
            frm.ShowDialog();
            this.Show();
        }
        /// <summary>
        /// Retourne l'index sélectionné dans la listBox portefeuilles.
        /// </summary>
        /// <returns></returns>
        public int GetListBoxSelectedIndex()
        {
            return lbxWallets.SelectedIndex;
        }

        private void btmEnvoyer_Click(object sender, EventArgs e)
        {
            frmEnvoi frm = new frmEnvoi(app, app.Bc.Wallets[GetListBoxSelectedIndex()]);
            this.Hide();
            frm.Closed += (s, args) => this.Show();
            frm.Show();
        }
        /// <summary>
        /// Met à jour la vue lorsque l'utilisateur sélectionne un portefeuille.
        /// </summary>
        /// <param name="sender"></param>
        private void lbxWallets_SelectedIndexChanged(object sender)
        {
            UpdateView(GetListBoxSelectedIndex());
            UpdateViewAsync();
        }

        /// <summary>
        /// Convertit un timestamp Unix en type DateTime.
        /// </summary>
        /// <param name="unixTimeStamp"></param>
        /// <returns></returns>
        public DateTime UnixTimeStampToDateTime(string unixTimeStamp)
        {
            // Le timestamp Unix est en secondes après l'époque.
            double temp = Convert.ToDouble(unixTimeStamp);
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(temp).ToLocalTime();
            return dtDateTime;
        }
        /// <summary>
        /// Retourne la valeur de l'Ethereum actuel (Récupéré sur CryptoCompare API).
        /// </summary>
        /// <returns></returns>
        public async Task<Price> GetEthValue()
        {
            HttpClient client = new HttpClient();
            var response = await client.GetStringAsync("https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=" + app.Settings.Currency + "&api_key=c87927ed2ee344d1965ccf7e8d2fb4cd88f3def737e46c9e5f91eed27afa5d1b").ConfigureAwait(false);

            Price result = JsonConvert.DeserializeObject<Price>(response);
            

            return result;
        }
        /// <summary>
        /// Retourne l'historique de valeur de l'Ethereum actuel (Récupéré sur CryptoCompare API).
        /// </summary>
        /// <returns></returns>
        public async Task<Rootobject> GetEthValueEveryDay()
        {
            HttpClient client = new HttpClient();
            //var response = await client.GetStringAsync("https://min-api.cryptocompare.com/data/v2/histoday?fsym=ETH&tsym=" + app.Settings.Currency + "&limit=10&api_key=c87927ed2ee344d1965ccf7e8d2fb4cd88f3def737e46c9e5f91eed27afa5d1b").ConfigureAwait(false);
            
                var response = await client.GetStringAsync("https://min-api.cryptocompare.com/data/v2/histominute?fsym=ETH&tsym=" + app.Settings.Currency + "&limit=360&api_key=c87927ed2ee344d1965ccf7e8d2fb4cd88f3def737e46c9e5f91eed27afa5d1b").ConfigureAwait(false);
            Rootobject result = JsonConvert.DeserializeObject<Rootobject>(response);

            return result;
        }
        /// <summary>
        /// Gère les propriétés à modifier dans le DataGridView passé en paramètre.
        /// </summary>
        /// <param name="chart"></param>
        private void SetUpChart(Chart chart)
        {
            chart.Series.Clear();
            chart.ChartAreas.Clear();
            chart.ChartAreas.Add("Area");
            chart.Series.Add("Evolution");
            chart.Series[0].ChartType = SeriesChartType.Line;
            chart.ChartAreas[0].AxisY.IsStartedFromZero = false;
            chart.Series[0].XValueType = ChartValueType.DateTime;
            chart.ChartAreas[0].AxisX.LabelStyle.Format = "yyyy-MM-dd HH:mm";
            chart.ChartAreas[0].AxisX.Interval = 1;
            chart.ChartAreas[0].AxisX.IntervalType = DateTimeIntervalType.Minutes;
            chart.ChartAreas[0].AxisX.ScrollBar.Enabled = true;
            chart.ChartAreas[0].AxisY.Minimum = 0;
            chart.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
            chart.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
            chart.ChartAreas[0].AxisX.LabelStyle.Interval = 10;
        }
        /// <summary>
        /// Structure nécessaire au reçu du prix de l'Ethereum de l'API CryptoCompare.
        /// </summary>
        public class Price
        {
            public double USD { get; set; }
            public double CHF { get; set; }
            public double EUR { get; set; }
        }
        /// <summary>
        /// Sauvegarde les paramètres de l'utilisateur.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            app.SaveSettings();
        }


        public class Rootobject
        {
            public string Response { get; set; }
            public string Message { get; set; }
            public bool HasWarning { get; set; }
            public int Type { get; set; }
            public Ratelimit RateLimit { get; set; }
            public DataInfo Data { get; set; }
        }

        public class Ratelimit
        {
        }

        public class DataInfo
        {
            public bool Aggregated { get; set; }
            public int TimeFrom { get; set; }
            public int TimeTo { get; set; }
            public Datum[] Data { get; set; }
        }

        public class Datum
        {
            public int time { get; set; }
            public float high { get; set; }
            public float low { get; set; }
            public decimal open { get; set; }
            public float volumefrom { get; set; }
            public float volumeto { get; set; }
            public float close { get; set; }
            public string conversionType { get; set; }
            public string conversionSymbol { get; set; }
        }

    }
}

```
### frmAddress.cs
```C#
/* 
 * App : SecuWallet
 * Auteur : Thomas Tissot
 * Org : CFPT
 * Date : 07.06.2022
 * brief : Vue permettant l'affichage et la copie de l'adresse publique d'un portefeuille.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroSet_UI.Forms;
using QRCoder;
namespace TravailDiplome_TISSOTT
{
    public partial class frmAdresse : MetroSetForm
    {
        public frmAdresse(string publicKey)
        {
            InitializeComponent();
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(publicKey, QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);
            Bitmap qrCodeImage = qrCode.GetGraphic(20);
            pbxQrCode.Image = qrCodeImage;
            lblPublicAddress.Text = publicKey;
        }
        /// <summary>
        /// Copie de l'adresse publique dans le presse-papiers.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void metroSetEllipse1_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(lblPublicAddress.Text);
        }

        private void frmAdresse_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }
    }
}

```
### frmEnvoi.cs
```C#
/* 
 * App : SecuWallet
 * Auteur : Thomas Tissot
 * Org : CFPT
 * Date : 07.06.2022
 * brief : Vue permettant à l'utilisateur de créer des transactions sur la blockchain à  l’adresse publique de son choix, ses portefeuilles personnels ou les contacts préalablement enregistrés.
 */
using MetroSet_UI.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;
namespace TravailDiplome_TISSOTT
{
    public partial class frmEnvoi : MetroSetForm
    {
        AppController app;
        Wallet wallet;
        public frmEnvoi(AppController app, Wallet wallet)
        {
            InitializeComponent();
            this.Text = "Envoi depuis : " + wallet.Name;
            this.app = app;
            this.wallet = wallet;
         }

        private void btnEnvoyer_Click(object sender, EventArgs e)
        {
            if (tbxAdresse.Text != "" && tbxMontant.Text != "")
            {
                //Vérification du Google Authenticator.
                string AuthenticatorResult = app.ShowAuthenticatorDialogBox(this);
                if (AuthenticatorResult == "ok")
                {
                    string key = app.Bc.DecryptJSONFile(tbxPassword.Text, wallet.Path);
                    app.Bc.CreateTransaction(key, tbxAdresse.Text, Convert.ToDecimal(tbxMontant.Text));
                    MessageBox.Show("Transaction effectuée");
                    //MetroSetMessageBox.Show(this, "Transaction effectuée");
                }
            }
            else
            {
                MessageBox.Show("Veuillez remplir tout les champs du formulaire");
            }
        }
        /// <summary>
        /// Mise à jour de la vue lors de son ouverture.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmEnvoi_Load(object sender, EventArgs e)
        {
            UpdateView();
        }

        /// <summary>
        /// Met à jour la vue.
        /// </summary>
        public void UpdateView()
        {
            if (app.Bc.Wallets.Count > 0)
            {
                lbxWallets.Clear();
                foreach (Wallet wallet in app.Bc.Wallets)
                {
                    lbxWallets.AddItem(wallet.Name);
                }
            }
            else
            {
                lbxWallets.Clear();
                lbxWallets.AddItem("Aucun portefeuille dispo.");
            }

            if (app.Settings.Contacts.Count > 0)
            {
                lbxContact.Clear();
                foreach (Contact contact in app.Settings.Contacts)
                {
                    lbxContact.AddItem(contact.Name);
                }
            }
            else
            {
                lbxContact.Clear();
                lbxContact.AddItem("Aucun contact dispo.");
            }
        }
        /// <summary>
        /// Ajout automatique de l'adresse publique lorsque l'utilisateur clique sur un de ces portefeuilles.
        /// </summary>
        /// <param name="sender"></param>
        private void lbxWallets_SelectedIndexChanged(object sender)
        {
            tbxAdresse.Text = app.Bc.GetPublicAddressWithWalletIndex(GetListBoxWalletSelectedIndex());

        }

        /// <summary>
        /// Retourne l'index sélectionné dans la listBox portefeuilles.
        /// </summary>
        /// <returns></returns>
        public int GetListBoxWalletSelectedIndex()
        {
            return lbxWallets.SelectedIndex;
        }
        /// <summary>
        /// Retourne l'index sélectionné dans la listBox contacts.
        /// </summary>
        /// <returns></returns>
        public int GetListBoxContactSelectedIndex()
        {
            return lbxContact.SelectedIndex;
        }
        /// <summary>
        /// Ajout automatique de l'adresse publique lorsque l'utilisateur clique sur un de ces contacts.
        /// </summary>
        /// <param name="sender"></param>
        private void lbxContact_SelectedIndexChanged(object sender)
        {
            tbxAdresse.Text = app.Settings.GetContactPublicKeyWithIndex(GetListBoxContactSelectedIndex());
        }

    }
}

```
### frmContact.cs
```C#
/* 
 * App : SecuWallet
 * Auteur : Thomas Tissot
 * Org : CFPT
 * Date : 07.06.2022
 * brief : Vue permettant à l'utilisateur de visualiser les différents contacts enregistrés sur son compte.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroSet_UI.Forms;
namespace TravailDiplome_TISSOTT
{
    public partial class frmContact : MetroSetForm
    {
        AppController app;
        public frmContact(AppController app)
        {
            InitializeComponent();
            this.app = app;
            UpdateView();
        }
        /// <summary>
        /// Affiche la vue Ajout Contact.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNewContact_Click(object sender, EventArgs e)
        {
            frmAddModifyContact frm = new frmAddModifyContact(app, "Ajouter contact");
            this.Hide();
            frm.Closed += (s, args) => this.Show();
            frm.Closed += (s, args) => this.UpdateView();
            frm.Show();
            UpdateView();
        }
        /// <summary>
        /// Affiche la vue Modifier Contact.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnModify_Click(object sender, EventArgs e)
        {
            int index = GetListBoxSelectedIndex();
            //Vérification si un contact a bien été selectionné.
            if (index != -1)
            {
                frmAddModifyContact frm = new frmAddModifyContact(app, "Modifier contact", GetListBoxSelectedIndex());
                this.Hide();
                frm.Closed += (s, args) => this.Show();
                frm.Closed += (s, args) => this.UpdateView();
                frm.Show();
                UpdateView();
            }
            else
            {
                MessageBox.Show("Veuillez selectionner un contact pour le modifier");
            }
        }
        /// <summary>
        /// Suppression du contact sélectionné.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDelete_Click(object sender, EventArgs e)
        {
            int index = GetListBoxSelectedIndex();
            //Vérification si un contact a bien été selectionné.
            if (index != -1)
            {
                app.Settings.Contacts.RemoveAt(index);
                UpdateView();
            }
            else
            {
                MessageBox.Show("Veuillez selectionner un contact pour le supprimer");
            }
            
        }
        /// <summary>
        /// Retourne l'index sélectionné dans la listBox.
        /// </summary>
        /// <returns></returns>
        public int GetListBoxSelectedIndex()
        {
            return lbxContact.SelectedIndex;
        }
        /// <summary>
        /// Met à jour la vue.
        /// </summary>
        private void UpdateView()
        {
            lbxContact.Items.Clear();
            foreach (Contact contact in app.Settings.Contacts)
            {
                lbxContact.Items.Add(contact.Name + ": " + contact.PublicKey);
            }
        }
    }
}

```
### frmGestionPortefeuilles.cs
```C#
/* 
 * App : SecuWallet
 * Auteur : Thomas Tissot
 * Org : CFPT
 * Date : 07.06.2022
 * brief : Vue permettant à l'utilisateur de créer, importer et supprimer des portefeuilles.
 */
using MetroSet_UI.Forms;
using Nethereum.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TravailDiplome_TISSOTT
{
    public partial class frmGestionPortefeuilles : MetroSetForm
    {
        AppController app;
        public frmGestionPortefeuilles(AppController app)
        {
            InitializeComponent();
            this.app = app;
        }
        /// <summary>
        /// Génère un nouveau portefeuille.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCreerPortefeuille_Click(object sender, EventArgs e)
        {
            if (tbxNewWalletName.Text != "" && tbxPassword.Text.Length >= 6)
            {
                app.Bc.GenerateJSONFile(tbxPassword.Text, tbxNewWalletName.Text, app.User.Email);
            }
            else
            {
                MessageBox.Show("Nom du portefeuille vide et/ou mot de passe inférieur a 6 caractères.");
            }
        }
        /// <summary>
        /// Mise à jour de la vue.
        /// </summary>
        private void UpdateView()
        {
            if (app.Bc.Wallets.Count > 0)
            {
                lbxWallets.Clear();
                foreach (Wallet wallet in app.Bc.Wallets)
                {
                    lbxWallets.AddItem(wallet.Name);
                }
            }
            else
            {
                lbxWallets.Clear();
                lbxWallets.AddItem("Aucun portefeuille dispo.");
            }

        }

        private void btnImporterCle_Click(object sender, EventArgs e)
        {


        }
        /// <summary>
        /// Importation d'un portefeuille par clé privée.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnImportPrivateKey_Click(object sender, EventArgs e)
        {
            if (tbxNewWalletName.Text != "" && tbxPassword.Text.Length >= 6)
            {
                app.Bc.GenerateJSONFile(tbxPassword.Text, tbxNewWalletName.Text, app.User.Email);
            }
            else
            {
                MessageBox.Show("Nom du portefeuille vide et/ou mot de passe inférieur a 6 caractères.");
            }
        }
        /// <summary>
        /// Action de cliquer sur le bouton importer avec clé privée.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnImportPrivateKey_Click_1(object sender, EventArgs e)
        {
            frmImportWalletFromKey frm = new frmImportWalletFromKey(app);
            this.Hide();
            frm.Closed += (s, args) => this.Close();
            frm.Show();
        }
        /// <summary>
        /// Action de cliquer sur le bouton importer avec fichier "JSON".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnImportJson_Click(object sender, EventArgs e)
        {
            frmImportWalletFromFile frm = new frmImportWalletFromFile(app);
            this.Hide();
            frm.Closed += (s, args) => this.Close();
            frm.Show();
        }
        /// <summary>
        /// Mise à jour de la vue lors du chargement de ce formulaire.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmGestionPortefeuilles_Load(object sender, EventArgs e)
        {
            UpdateView();
        }
        /// <summary>
        /// Suppression du portefeuille selectionné dans la liste des portefeuilles.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSupprimer_Click(object sender, EventArgs e)
        {
            string AuthenticatorResult = app.ShowAuthenticatorDialogBox(this);
            
            if (AuthenticatorResult == "ok")
            {
                int index = GetListBoxSelectedIndex();
                //Vérification si un portefeuille est bien selectionné.
                if (index != -1)
                {
                    app.Bc.DeleteWallet(index, app.User.Email);
                    UpdateView();
                }
                else
                {
                    MessageBox.Show("Aucun portefeuille sélectionné");
                }
            }
        }
        /// <summary>
        /// Retourne l'index sélectionné dans la liste des portefeuilles.
        /// </summary>
        /// <returns></returns>
        public int GetListBoxSelectedIndex()
        {
            return lbxWallets.SelectedIndex;
        }

    }
}

```
### frmAddModifyContact.cs
```C#
/* 
 * App : SecuWallet
 * Auteur : Thomas Tissot
 * Org : CFPT
 * Date : 07.06.2022
 * brief : Vue permettant de modifier ou d'ajouter un contact.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroSet_UI.Forms;
namespace TravailDiplome_TISSOTT
{
    public partial class frmAddModifyContact : MetroSetForm
    {
        AppController app;
        private bool modify;
        private int id;
        /// <summary>
        /// Constructeur nécessaire pour modifier un contact.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="frmTitle"></param>
        /// <param name="contactIndex"></param>
        public frmAddModifyContact(AppController app, string frmTitle, int contactIndex)
        {
            InitializeComponent();
            this.app = app;
            this.Text = frmTitle;
            this.id = contactIndex;
            modify = true;
            tbxName.Text = app.Settings.Contacts[contactIndex].Name;
            tbxAddress.Text = app.Settings.Contacts[contactIndex].PublicKey;
        }

        /// <summary>
        /// Constructeur nécessaire pour la création d'un nouveau contact.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="frmTitle"></param>
        public frmAddModifyContact(AppController app, string frmTitle)
        {
            InitializeComponent();
            this.app = app;
            this.Text = frmTitle;
            modify = false;
        }

        private void btnValidate_Click(object sender, EventArgs e)
        {
            bool isExist = false;
            //Vérification du type d'action de la page et vérifie si le nom ou l'adresse du contact est déjà existante.
            if (modify)
            {
                foreach (Contact contact in app.Settings.Contacts)
                {
                    if (app.Settings.Contacts.IndexOf(contact) != id)
                    {
                        if (tbxName.Text == contact.Name || tbxAddress.Text == contact.PublicKey)
                        {
                            isExist = true;
                        }
                    }
                }
                if (!isExist)
                {
                    if (app.Bc.isAdressValid(tbxAddress.Text))
                    {
                        app.Settings.Contacts[id].Name = tbxName.Text;
                        app.Settings.Contacts[id].PublicKey = tbxAddress.Text;
                        MessageBox.Show("Contact modifié");
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Adresse publique invalide");
                    }
                    
                }
                else
                {
                    MessageBox.Show("Nom de contact ou adresse publique déjà existante");
                }
            }
            else
            {
                foreach (Contact contact in app.Settings.Contacts)
                {
                    if (tbxName.Text == contact.Name || tbxAddress.Text == contact.PublicKey)
                    {
                        isExist = true;
                    }
                }
                if (!isExist)
                {
                    if (app.Bc.isAdressValid(tbxAddress.Text))
                    {
                        app.Settings.Contacts.Add(new Contact(tbxName.Text, tbxAddress.Text));
                        MessageBox.Show("Contact créé");
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Adresse publique invalide");
                    }
                }
                else
                {
                    MessageBox.Show("Nom de contact ou adresse publique déjà existante");
                }

            }

        }
    }
}

```
### frmMonCompte.cs
```C#
/* 
 * App : SecuWallet
 * Auteur : Thomas Tissot
 * Org : CFPT
 * Date : 07.06.2022
 * brief : Vue permettant l'accès au vue Gestion des portefeuilles, Paramètres ainsi qu'à la déconnexion de l'utilisateur.
 */
using MetroSet_UI.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TravailDiplome_TISSOTT
{
    public partial class frmMonCompte : MetroSetForm
    {
        AppController app;
        public frmMonCompte(AppController app)
        {
            this.app = app;
            InitializeComponent();
        }
        /// <summary>
        /// Redirige sur la vue Gestion des portefeuilles.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGestionPortefeuille_Click(object sender, EventArgs e)
        {
            frmGestionPortefeuilles frmGestionPortefeuilles = new frmGestionPortefeuilles(app);
            this.Hide();
            frmGestionPortefeuilles.Closed += (s, args) => this.Close();
            frmGestionPortefeuilles.Show();
        }
        /// <summary>
        /// Redirige sur la vue frmParametres.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnParametres_Click(object sender, EventArgs e)
        {
            frmParametres frm = new frmParametres(app);
            this.Hide();
            frm.Closed += (s, args) => this.Close();
            frm.Show();
        }
        /// <summary>
        /// Déconnecte l'utilisateur et redirection sur la vue Login.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDeconnexion_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }
    }
}

```
### frmParametres.cs
```C#
/* 
 * App : SecuWallet
 * Auteur : Thomas Tissot
 * Org : CFPT
 * Date : 07.06.2022
 * brief : Vue qui permet à l'utilisateur de changer de devise, d'accéder à la vue frmContact et frmAbout.
 */
using MetroSet_UI.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TravailDiplome_TISSOTT
{
    public partial class frmParametres : MetroSetForm
    {
        AppController app;
        public frmParametres(AppController app)
        {
            InitializeComponent();
            this.app = app;
        }
        /// <summary>
        /// Code appelé lors du changement de l'index de la listBox Devise.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbxDevise_SelectedIndexChanged(object sender, EventArgs e)
        {
            app.Settings.Currency = cbxDevise.SelectedItem.ToString();
        }
        /// <summary>
        /// Redirige sur la vue frmContact.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnContacts_Click(object sender, EventArgs e)
        {
            frmContact frm = new frmContact(app);
            this.Hide();
            frm.Closed += (s, args) => this.Close();
            frm.Show();
        }
    }
}

```
### frmImportWalletFromKey.cs
```C#
/* 
 * App : SecuWallet
 * Auteur : Thomas Tissot
 * Org : CFPT
 * Date : 07.06.2022
 * brief : Vue permettant à l'utilisateur d'importer un portefeuille à l'aide d'une clé privée.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroSet_UI.Forms;

namespace TravailDiplome_TISSOTT
{
    public partial class frmImportWalletFromKey : MetroSetForm
    {
        AppController app;
        public frmImportWalletFromKey(AppController app)
        {
            InitializeComponent();
            this.app = app;
        }
        /// <summary>
        /// Action de cliquer sur le bouton "Importer"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnImport_Click(object sender, EventArgs e)
        {
            if(tbxPrivateKey.Text != "" && tbxPassword.Text != "" && tbxNom.Text != "")
            {
                app.Bc.GenerateJSONFile(tbxPassword.Text, tbxNom.Text, app.User.Email, app.Bc.GetAccountFromPrivateKey(tbxPrivateKey.Text));
            }
        }
    }
}

```
### frmImportWalletFromFile.cs
```C#
/* 
 * App : SecuWallet
 * Auteur : Thomas Tissot
 * Org : CFPT
 * Date : 07.06.2022
 * brief : Vue permettant à l'utilisateur d'importer un portefeuille à l'aide d'un fichier “JSON”.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroSet_UI.Forms;

namespace TravailDiplome_TISSOTT
{
    public partial class frmImportWalletFromFile : MetroSetForm
    {
        AppController app;
        private string path;
        public frmImportWalletFromFile(AppController app)
        {
            InitializeComponent();
            this.app = app;
        }
        /// <summary>
        /// Action de cliquer sur le bouton permettant d'importer le fichier "JSON".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnImportFichier_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog())
            {
                ofd.Filter = "Json files (*.json)|*.json";
                ofd.Multiselect = false;
                ofd.CheckFileExists = true;
                ofd.CheckPathExists = true;
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    path = ofd.FileName;
                    lblNomFichier.Text = path;

                }
            }
        }
        /// <summary>
        /// Action de clique sur le bouton "importer".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnImportWallet_Click(object sender, EventArgs e)
        {
            if (tbxPassword.Text != "" && path != "")
            {
                app.Bc.ImportAccountWithFile(tbxPassword.Text, path, tbxName.Text, app.User.Email);
            }
        }
    }
}

```
## Site web

### fonctions.php
```php
<?php
  require_once ("../functionConnexionDB.php");
  require_once ("../functions.php");
  include_once "../lib/google-authenticator-main/vendor/autoload.php";
/**
 * Génère une clé unique nécessaire à L'API Google Authenticator.
 *
 * @param string  $account  Email de l'utilisateur.
 * 
 * @author Thomas Tissot
 * @return $secret Clé générée grâce à la librairie \Dolondro\GoogleAuthenticator\.
 */ 
function createSecretKey($account){
    $secretFactory = new \Dolondro\GoogleAuthenticator\SecretFactory();
    $secret = $secretFactory->create("SecuWallet", $account);
    $secretKey = $secret->getSecretKey();
    return $secret;
}

/**
 * Génère une clé unique nécessaire à L'API Google Authenticator.
 *
 * @param string  $secretKey  Clé secrète de l'utilisateur.
 * @param string  $code  Code entré par l'utilisateur.
 * 
 * @author Thomas Tissot
 * @return bool TRUE Si le code est validé. FALSE si le code est éronné.
 */ 
function checkAuthenticator($secretKey, $code){
    $googleAuthenticator = new \Dolondro\GoogleAuthenticator\GoogleAuthenticator();
    if ($googleAuthenticator->authenticate($secretKey, $code)) {
            return TRUE;
      }else {
            return FALSE;
      }
      
}
?>
```
### functionConnexionDB.php
```php
<?php
define('DB_HOST', "localhost");
define('DB_NAME', "DBTravailDiplome");
define('DB_USER', "tissott");
define('DB_PASS', "Super");

/**
 * Génère un object PDO connecté à la base de données.
 *
 * 
 * @author Thomas Tissot
 * @return $dbb Object PDO connecté à la base de données
 */ 
function getConnexion() {
    static $dbb = NULL;
    if ($dbb === NULL) {
        try {
            $connectionString = 'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME;
            $dbb = new PDO($connectionString, DB_USER, DB_PASS);
            $dbb->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            die('Erreur : ' . $e->getMessage());
        }
    }
    return $dbb;
}

/**
 * Insert un utilisateur dans la base de données.
 *
 * @param string  $email  Email de l'utilisateur.
 * @param string  $password  mot de passe en SHA1 de l'utilisateur.
 * @param string  $secretKey  Clé secrète associée à l'utilisateur.
 * 
 * @author Thomas Tissot
 */ 
function createUser($email, $password, $secretKey) {
    $connexion = getConnexion();
    $requete = $connexion->prepare("INSERT INTO `DBTravailDiplome`.`Users` (mail, password, secretKey) VALUES (?,?,?);");
    $requete->bindParam(1, $email);
    $requete->bindParam(2, $password);
    $requete->bindParam(3, $secretKey);
    var_dump($requete);
    $requete->execute();
}

/**
 * Vérification les login de l'utilisateur
 *
 * @param string  $email  Email de l'utilisateur.
 * 
 * @author Thomas Tissot
 * @return string Clé secret assosié a l'email utilisateur.
 */ 
function GetUserSecretKey($email){
    $connexion = getConnexion();
    $requete = $connexion->prepare("SELECT `Users`.`secretKey`
    FROM `DBTravailDiplome`.`Users` 
    WHERE mail=\"$email\";");
    $requete->execute();
    return $requete->fetch(PDO::FETCH_ASSOC);
}

/**
 * Vérification les login de l'utilisateur
 *
 * @param string  $email  Email de l'utilisateur
 * @param string  $password  mot de passe SHA1 de l'utilisateur
 * 
 * @author Thomas Tissot
 * @return string Clé secret assosié a l'email et le mot de passe de l'utilisateur.
 */ 
function CheckUserLogin($email, $password){
    $connexion = getConnexion();
    $requete = $connexion->prepare("SELECT `Users`.`secretKey`
    FROM `DBTravailDiplome`.`Users` 
    WHERE mail=\"$email\" AND password=\"$password\";");
    $requete->execute();
    return $requete->fetch(PDO::FETCH_ASSOC);
}

/**
 * Retourne l'idUser associé à l'email de l'utilisateur.
 *
 * @param string  $email  Email de l'utilisateur.
 * 
 * @author Thomas Tissot
 * @return string Id de l'utilisateur.
 */ 
function GetUserId($email){
    $connexion = getConnexion();
    $requete = $connexion->prepare("SELECT idUsers from Users WHERE mail=\"$email\"");
    $requete->execute();
    return $requete->fetch(PDO::FETCH_ASSOC);
}
?>
```

### Index.php
```php
<?php 
session_start();
if($_SESSION["logged"] == NULL)
{
$_SESSION["logged"] = FALSE;
}
?>
<!doctype html>
<html lang="en" class="no-js">
  <head>
    
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width,initial-scale=1">
      
      
      
      
      <link rel="icon" href="assets/images/favicon.png">
      <meta name="generator" content="mkdocs-1.2.3, mkdocs-material-7.3.6">
    
    
      
        <title>SecuWallet</title>
      
    
    
      <link rel="stylesheet" href="assets/stylesheets/main.a57b2b03.min.css">
      
        
        <link rel="stylesheet" href="assets/stylesheets/palette.3f5d1f46.min.css">
        
      
    
    
    
      
        
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,700%7CRoboto+Mono&display=fallback">
        <style>:root{--md-text-font-family:"Roboto";--md-code-font-family:"Roboto Mono"}</style>
      
    
    
    
      <link rel="stylesheet" href="stylesheets/extra.css">
    
    
      


    
    
  </head>
  
  
    
    
    
    
    
    <body dir="ltr" data-md-color-scheme="" data-md-color-primary="none" data-md-color-accent="none">
  
    
    <script>function __prefix(e){return new URL(".",location).pathname+"."+e}function __get(e,t=localStorage){return JSON.parse(t.getItem(__prefix(e)))}</script>
    
    <input class="md-toggle" data-md-toggle="drawer" type="checkbox" id="__drawer" autocomplete="off">
    <input class="md-toggle" data-md-toggle="search" type="checkbox" id="__search" autocomplete="off">
    <label class="md-overlay" for="__drawer"></label>
    <div data-md-component="skip">
      
        
        <a href="#quel-but" class="md-skip">
          Skip to content
        </a>
      
    </div>
    <div data-md-component="announce">
      
    </div>
    
      

<header class="md-header" data-md-component="header">
  <nav class="md-header__inner md-grid" aria-label="Header">
    <a href="." title="SecuWallet" class="md-header__button md-logo" aria-label="SecuWallet" data-md-component="logo">
      
  
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 8a3 3 0 0 0 3-3 3 3 0 0 0-3-3 3 3 0 0 0-3 3 3 3 0 0 0 3 3m0 3.54C9.64 9.35 6.5 8 3 8v11c3.5 0 6.64 1.35 9 3.54 2.36-2.19 5.5-3.54 9-3.54V8c-3.5 0-6.64 1.35-9 3.54z"/></svg>

    </a>
    <label class="md-header__button md-icon" for="__drawer">
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M3 6h18v2H3V6m0 5h18v2H3v-2m0 5h18v2H3v-2z"/></svg>
    </label>
    <div class="md-header__title" data-md-component="header-title">
      <div class="md-header__ellipsis">
        <div class="md-header__topic">
          <span class="md-ellipsis">
            SecuWallet
          </span>
        </div>
        <div class="md-header__topic" data-md-component="header-topic">
          <span class="md-ellipsis">
            
              Quel but ?
            
          </span>
        </div>
      </div>
    </div>
    
    
    
    
  </nav>
  
</header>
    
    <div class="md-container" data-md-component="container">
      
      
        
          
        
      
      <main class="md-main" data-md-component="main">
        <div class="md-main__inner md-grid">
          
            
              
              <div class="md-sidebar md-sidebar--primary" data-md-component="sidebar" data-md-type="navigation" >
                <div class="md-sidebar__scrollwrap">
                  <div class="md-sidebar__inner">
                    


<nav class="md-nav md-nav--primary" aria-label="Navigation" data-md-level="0">
  <label class="md-nav__title" for="__drawer">
    <a href="." title="SecuWallet" class="md-nav__button md-logo" aria-label="SecuWallet" data-md-component="logo">
      
  
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 8a3 3 0 0 0 3-3 3 3 0 0 0-3-3 3 3 0 0 0-3 3 3 3 0 0 0 3 3m0 3.54C9.64 9.35 6.5 8 3 8v11c3.5 0 6.64 1.35 9 3.54 2.36-2.19 5.5-3.54 9-3.54V8c-3.5 0-6.64 1.35-9 3.54z"/></svg>

    </a>
    SecuWallet
  </label>
  
  <ul class="md-nav__list" data-md-scrollfix>
    
      
      
      

  
  
    
  
  
    <li class="md-nav__item md-nav__item--active">
      
      <input class="md-nav__toggle md-toggle" data-md-toggle="toc" type="checkbox" id="__toc">
      
      
        
      
      
        <label class="md-nav__link md-nav__link--active" for="__toc">
          Quel but ?
          <span class="md-nav__icon md-icon"></span>
        </label>
      
      <a href="." class="md-nav__link md-nav__link--active">
        Quel but ?
      </a>
      
        


<nav class="md-nav md-nav--secondary" aria-label="Table of contents">
  
  
  
    
  
  
    <label class="md-nav__title" for="__toc">
      <span class="md-nav__icon md-icon"></span>
      Table of contents
    </label>
    <ul class="md-nav__list" data-md-component="toc" data-md-scrollfix>
      
        <li class="md-nav__item">
  <a href="#installation" class="md-nav__link">
    Installation
  </a>
  
</li>
      
    </ul>
  
</nav>
      
    </li>
  

    
      
      
      

  
  
  
    <li class="md-nav__item">
      <a href="connexion/" class="md-nav__link">
        Connexion
      </a>
    </li>
  

    
      
      
      

  
  
  
    <li class="md-nav__item">
      <a href="createAccount/" class="md-nav__link">
        Créer un compte
      </a>
    </li>
  

    
      
      
      

  
  
  
    <li class="md-nav__item">
      <a href="download/" class="md-nav__link">
        Télécharger
      </a>
    </li>
  

    
  </ul>
</nav>
                  </div>
                </div>
              </div>
            
            
              
              <div class="md-sidebar md-sidebar--secondary" data-md-component="sidebar" data-md-type="toc" >
                <div class="md-sidebar__scrollwrap">
                  <div class="md-sidebar__inner">
                    


<nav class="md-nav md-nav--secondary" aria-label="Table of contents">
  
  
  
    
  
  
    <label class="md-nav__title" for="__toc">
      <span class="md-nav__icon md-icon"></span>
      Table of contents
    </label>
    <ul class="md-nav__list" data-md-component="toc" data-md-scrollfix>
      
        <li class="md-nav__item">
  <a href="#installation" class="md-nav__link">
    Installation
  </a>
  
</li>
      
    </ul>
  
</nav>
                  </div>
                </div>
              </div>
            
          
          <div class="md-content" data-md-component="content">
            <article class="md-content__inner md-typeset">
              
                
                
                <h1 id="quel-but">Quel but ?</h1>
<p>SecuWallet est une application qui permet de gérer et de stocker des portefeuilles cryptomonnaies sur la blockchain Ethereum. Celle-ci offre la possibilité de créer des portefeuilles, importer des portefeuilles déjà existant. L'importation peut se faire de deux manières différentes, par clé privée ou à l'aide d'un fichier "JSON" comportant un portefeuille. La puissance de ce projet réside dans la sécurité apportée lors du stockage des portefeuilles sur la machine. L'utilisateur aura également un suivi de l'évolution des différents portefeuilles importer dans l'application. Il pourra également créer des transactions sur la blockchain.</p>
<h2 id="installation">Installation</h2>
<p>Pour avoir accès au logiciel d'installation, il est nécessaire de se <a href="login/">créer un compte</a>, pour cela il est nécessaire de lier un compte Google Authenticator. Enfin, il restera plus qu'a se connecter a ce dernier et télécharger le logiciel.</p>
                
              
              
                


              
            </article>
          </div>
        </div>
        
      </main>
      
        
<footer class="md-footer">
  
    <nav class="md-footer__inner md-grid" aria-label="Footer">
      
      
        
        <a href="connexion/" class="md-footer__link md-footer__link--next" aria-label="Next: Connexion" rel="next">
          <div class="md-footer__title">
            <div class="md-ellipsis">
              <span class="md-footer__direction">
                Next
              </span>
              Connexion
            </div>
          </div>
          <div class="md-footer__button md-icon">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M4 11v2h12l-5.5 5.5 1.42 1.42L19.84 12l-7.92-7.92L10.5 5.5 16 11H4z"/></svg>
          </div>
        </a>
      
    </nav>
  
  <div class="md-footer-meta md-typeset">
    <div class="md-footer-meta__inner md-grid">
      <div class="md-footer-copyright">
        
        
          Made with
          <a href="https://squidfunk.github.io/mkdocs-material/" target="_blank" rel="noopener">
            Material for MkDocs
          </a>
        
        
      </div>
      
    </div>
  </div>
</footer>
      
    </div>
    <div class="md-dialog" data-md-component="dialog">
      <div class="md-dialog__inner md-typeset"></div>
    </div>
    <script id="__config" type="application/json">{"base": ".", "features": [], "translations": {"clipboard.copy": "Copy to clipboard", "clipboard.copied": "Copied to clipboard", "search.config.lang": "en", "search.config.pipeline": "trimmer, stopWordFilter", "search.config.separator": "[\\s\\-]+", "search.placeholder": "Search", "search.result.placeholder": "Type to start searching", "search.result.none": "No matching documents", "search.result.one": "1 matching document", "search.result.other": "# matching documents", "search.result.more.one": "1 more on this page", "search.result.more.other": "# more on this page", "search.result.term.missing": "Missing", "select.version.title": "Select version"}, "search": "assets/javascripts/workers/search.fcfe8b6d.min.js", "version": null}</script>
    
    
      <script src="assets/javascripts/bundle.b1047164.min.js"></script>
      
    
  </body>
</html>
```
### connexion.php
```php
<?php
session_start();
  require_once ("../functionConnexionDB.php");
  require_once ("../functions.php");
  
  $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
  $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
  $submit = filter_has_var(INPUT_POST, 'submit');
  
  $authenticator = filter_has_var(INPUT_POST, 'authenticator');
  $code = filter_input(INPUT_POST, 'code', FILTER_SANITIZE_STRING);
  //Si le formulaire est validé récupération de la clé secrète de l'utilisateur
  if ($submit){
    $result = GetUserSecretKey($email);
    if(isset($result['secretKey'])){
        $_SESSION['secretKey'] = $result['secretKey'];
    }else{
      echo '<script>alert("Email ou mot de passe éronné")</script>';
    }
  }
  //Contrôle du formulaire Google Authenticator
  if($authenticator){
    $googleAuthenticator = new \Dolondro\GoogleAuthenticator\GoogleAuthenticator();
    if ($googleAuthenticator->authenticate($_SESSION['secretKey'], $code)) {
      $_SESSION['logged'] = TRUE;
      $_SESSION['secretKey'] = "";
      $_SESSION['email'] = $email;
      header("Location: ../download");
    }
    else {
      echo "Code non valide !!!";
    }
  }
?>
<!doctype html>
<html lang="en" class="no-js">
  <head>
    
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width,initial-scale=1">
      
      
      
      
      <link rel="icon" href="../assets/images/favicon.png">
      <meta name="generator" content="mkdocs-1.2.3, mkdocs-material-7.3.6">
    
    
      
        <title>Connexion - SecuWallet</title>
      
    
    
      <link rel="stylesheet" href="../assets/stylesheets/main.a57b2b03.min.css">
      
        
        <link rel="stylesheet" href="../assets/stylesheets/palette.3f5d1f46.min.css">
        
      
    
    
    
      
        
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,700%7CRoboto+Mono&display=fallback">
        <style>:root{--md-text-font-family:"Roboto";--md-code-font-family:"Roboto Mono"}</style>
      
    
    
    
      <link rel="stylesheet" href="../stylesheets/extra.css">
    
    
      


    
    
  </head>
  
  
    
    
    
    
    
    <body dir="ltr" data-md-color-scheme="" data-md-color-primary="none" data-md-color-accent="none">
  
    
    <script>function __prefix(e){return new URL("..",location).pathname+"."+e}function __get(e,t=localStorage){return JSON.parse(t.getItem(__prefix(e)))}</script>
    
    <input class="md-toggle" data-md-toggle="drawer" type="checkbox" id="__drawer" autocomplete="off">
    <input class="md-toggle" data-md-toggle="search" type="checkbox" id="__search" autocomplete="off">
    <label class="md-overlay" for="__drawer"></label>
    <div data-md-component="skip">
      
        
        <a href="#connexion" class="md-skip">
          Skip to content
        </a>
      
    </div>
    <div data-md-component="announce">
      
    </div>
    
      

<header class="md-header" data-md-component="header">
  <nav class="md-header__inner md-grid" aria-label="Header">
    <a href=".." title="SecuWallet" class="md-header__button md-logo" aria-label="SecuWallet" data-md-component="logo">
      
  
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 8a3 3 0 0 0 3-3 3 3 0 0 0-3-3 3 3 0 0 0-3 3 3 3 0 0 0 3 3m0 3.54C9.64 9.35 6.5 8 3 8v11c3.5 0 6.64 1.35 9 3.54 2.36-2.19 5.5-3.54 9-3.54V8c-3.5 0-6.64 1.35-9 3.54z"/></svg>

    </a>
    <label class="md-header__button md-icon" for="__drawer">
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M3 6h18v2H3V6m0 5h18v2H3v-2m0 5h18v2H3v-2z"/></svg>
    </label>
    <div class="md-header__title" data-md-component="header-title">
      <div class="md-header__ellipsis">
        <div class="md-header__topic">
          <span class="md-ellipsis">
            SecuWallet
          </span>
        </div>
        <div class="md-header__topic" data-md-component="header-topic">
          <span class="md-ellipsis">
            
              Connexion
            
          </span>
        </div>
      </div>
    </div>
    
    
    
    
  </nav>
  
</header>
    
    <div class="md-container" data-md-component="container">
      
      
        
          
        
      
      <main class="md-main" data-md-component="main">
        <div class="md-main__inner md-grid">
          
            
              
              <div class="md-sidebar md-sidebar--primary" data-md-component="sidebar" data-md-type="navigation" >
                <div class="md-sidebar__scrollwrap">
                  <div class="md-sidebar__inner">
                    


<nav class="md-nav md-nav--primary" aria-label="Navigation" data-md-level="0">
  <label class="md-nav__title" for="__drawer">
    <a href=".." title="SecuWallet" class="md-nav__button md-logo" aria-label="SecuWallet" data-md-component="logo">
      
  
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 8a3 3 0 0 0 3-3 3 3 0 0 0-3-3 3 3 0 0 0-3 3 3 3 0 0 0 3 3m0 3.54C9.64 9.35 6.5 8 3 8v11c3.5 0 6.64 1.35 9 3.54 2.36-2.19 5.5-3.54 9-3.54V8c-3.5 0-6.64 1.35-9 3.54z"/></svg>

    </a>
    SecuWallet
  </label>
  
  <ul class="md-nav__list" data-md-scrollfix>
    
      
      
      

  
  
  
    <li class="md-nav__item">
      <a href=".." class="md-nav__link">
        Quel but ?
      </a>
    </li>
  

    
      
      
      

  
  
    
  
  
    <li class="md-nav__item md-nav__item--active">
      
      <input class="md-nav__toggle md-toggle" data-md-toggle="toc" type="checkbox" id="__toc">
      
      
        
      
      
      <a href="./" class="md-nav__link md-nav__link--active">
        Connexion
      </a>
      
    </li>
  

    
      
      
      

  
  
  
    <li class="md-nav__item">
      <a href="../createAccount/" class="md-nav__link">
        Créer un compte
      </a>
    </li>
  

    
      
      
      

  
  
  
    <li class="md-nav__item">
      <a href="../download/" class="md-nav__link">
        Télécharger
      </a>
    </li>
  

    
  </ul>
</nav>
                  </div>
                </div>
              </div>
            
            
              
              <div class="md-sidebar md-sidebar--secondary" data-md-component="sidebar" data-md-type="toc" >
                <div class="md-sidebar__scrollwrap">
                  <div class="md-sidebar__inner">
                    


<nav class="md-nav md-nav--secondary" aria-label="Table of contents">
  
  
  
    
  
  
</nav>
                  </div>
                </div>
              </div>
            
          
          <div class="md-content" data-md-component="content">
            <article class="md-content__inner md-typeset">
              
                
                
                <h1 id="connexion">Connexion</h1>
<form method="POST">
    Email:<br> <input  placeholder="Email" type="text" name="email"><br>
    Mot de passe:<br> <input  placeholder="Mot de passe" type="password" name="password"><br>
    <br><input type="submit" name="submit">
</form>
                
<?php 
//Si la clé secret a été généré cela signifie que l'utilisateur a entrer un email et un mot de passe valide. Affichage de l'imput text pour que l'utilsiateur entre le code Google Authenticator
if(strlen($_SESSION['secretKey']) == 16){
  echo "<form method='POST' action='index.php'>";
  echo "<label>Google Authenticator</label><br>";
  echo "<input  placeholder='Entré le code' type='text' name='code' >";
  echo "<input type='submit' name='authenticator'>";
  echo "</form>";
}


?>             
              
                


              
            </article>
          </div>
        </div>
        
      </main>
      
        
<footer class="md-footer">
  
    <nav class="md-footer__inner md-grid" aria-label="Footer">
      
        
        <a href=".." class="md-footer__link md-footer__link--prev" aria-label="Previous: Quel but ?" rel="prev">
          <div class="md-footer__button md-icon">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M20 11v2H8l5.5 5.5-1.42 1.42L4.16 12l7.92-7.92L13.5 5.5 8 11h12z"/></svg>
          </div>
          <div class="md-footer__title">
            <div class="md-ellipsis">
              <span class="md-footer__direction">
                Previous
              </span>
              Quel but ?
            </div>
          </div>
        </a>
      
      
        
        <a href="../createAccount/" class="md-footer__link md-footer__link--next" aria-label="Next: Créer un compte" rel="next">
          <div class="md-footer__title">
            <div class="md-ellipsis">
              <span class="md-footer__direction">
                Next
              </span>
              Créer un compte
            </div>
          </div>
          <div class="md-footer__button md-icon">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M4 11v2h12l-5.5 5.5 1.42 1.42L19.84 12l-7.92-7.92L10.5 5.5 16 11H4z"/></svg>
          </div>
        </a>
      
    </nav>
  
  <div class="md-footer-meta md-typeset">
    <div class="md-footer-meta__inner md-grid">
      <div class="md-footer-copyright">
        
        
          Made with
          <a href="https://squidfunk.github.io/mkdocs-material/" target="_blank" rel="noopener">
            Material for MkDocs
          </a>
        
        
      </div>
      
    </div>
  </div>
</footer>
      
    </div>
    <div class="md-dialog" data-md-component="dialog">
      <div class="md-dialog__inner md-typeset"></div>
    </div>
    <script id="__config" type="application/json">{"base": "..", "features": [], "translations": {"clipboard.copy": "Copy to clipboard", "clipboard.copied": "Copied to clipboard", "search.config.lang": "en", "search.config.pipeline": "trimmer, stopWordFilter", "search.config.separator": "[\\s\\-]+", "search.placeholder": "Search", "search.result.placeholder": "Type to start searching", "search.result.none": "No matching documents", "search.result.one": "1 matching document", "search.result.other": "# matching documents", "search.result.more.one": "1 more on this page", "search.result.more.other": "# more on this page", "search.result.term.missing": "Missing", "select.version.title": "Select version"}, "search": "../assets/javascripts/workers/search.fcfe8b6d.min.js", "version": null}</script>
    
    
      <script src="../assets/javascripts/bundle.b1047164.min.js"></script>
      
    
  </body>
</html>
```
### createAccount.php
```php

<?php
session_start();
  require_once ("../functionConnexionDB.php");
  require_once ("../functions.php");
  include "../lib/google-authenticator-main/vendor/autoload.php";

  $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
  $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
  $submit = filter_has_var(INPUT_POST, 'submit');
  
  $authenticator = filter_has_var(INPUT_POST, 'authenticator');
  $code = filter_input(INPUT_POST, 'code', FILTER_SANITIZE_STRING);
  //Création d'un nouveau compte utilisateur.
  if ($submit){
    if(filter_var($email, FILTER_VALIDATE_EMAIL) && strlen($password) >= 6){
      $secret = createSecretKey($email);
      
      $secretKey = $secret->getSecretKey();
      $_SESSION['secretKey'] = $secretKey;

      $_SESSION['email'] = $email;
      $_SESSION['password'] = sha1($password);

      $qrImageGenerator = new \Dolondro\GoogleAuthenticator\QrImageGenerator\EndroidQrImageGenerator();
    }else{
      echo '<script>alert("Email non valide et/ou mot de passe trop court. (6 caractères minimum)")</script>';
    }
  }
  //Assosciation de Google Authenticator à l'utilisateur.
  if($authenticator){
    $googleAuthenticator = new \Dolondro\GoogleAuthenticator\GoogleAuthenticator();
    if ($googleAuthenticator->authenticate($_SESSION['secretKey'], $code)) {
      createUser($_SESSION['email'], $_SESSION['password'], $_SESSION['secretKey']);
      
      $_SESSION['secretKey'] = "";
      header("Location:../connexion/index.php");
    }
    else {
      echo "Code non valide !!!";
    }
  }
?>
<!doctype html>
<html lang="en" class="no-js">
  <head>
    
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width,initial-scale=1">
      
      
      
      
      <link rel="icon" href="../assets/images/favicon.png">
      <meta name="generator" content="mkdocs-1.2.3, mkdocs-material-7.3.6">
    
    
      
        <title>Créer un compte - SecuWallet</title>
      
    
    
      <link rel="stylesheet" href="../assets/stylesheets/main.a57b2b03.min.css">
      
        
        <link rel="stylesheet" href="../assets/stylesheets/palette.3f5d1f46.min.css">
        
      
    
    
    
      
        
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,700%7CRoboto+Mono&display=fallback">
        <style>:root{--md-text-font-family:"Roboto";--md-code-font-family:"Roboto Mono"}</style>
      
    
    
    
      <link rel="stylesheet" href="../stylesheets/extra.css">
    
    
      


    
    
  </head>
  
  
    
    
    
    
    
    <body dir="ltr" data-md-color-scheme="" data-md-color-primary="none" data-md-color-accent="none">
  
    
    <script>function __prefix(e){return new URL("..",location).pathname+"."+e}function __get(e,t=localStorage){return JSON.parse(t.getItem(__prefix(e)))}</script>
    
    <input class="md-toggle" data-md-toggle="drawer" type="checkbox" id="__drawer" autocomplete="off">
    <input class="md-toggle" data-md-toggle="search" type="checkbox" id="__search" autocomplete="off">
    <label class="md-overlay" for="__drawer"></label>
    <div data-md-component="skip">
      
        
        <a href="#creer-un-compte" class="md-skip">
          Skip to content
        </a>
      
    </div>
    <div data-md-component="announce">
      
    </div>
    
      

<header class="md-header" data-md-component="header">
  <nav class="md-header__inner md-grid" aria-label="Header">
    <a href=".." title="SecuWallet" class="md-header__button md-logo" aria-label="SecuWallet" data-md-component="logo">
      
  
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 8a3 3 0 0 0 3-3 3 3 0 0 0-3-3 3 3 0 0 0-3 3 3 3 0 0 0 3 3m0 3.54C9.64 9.35 6.5 8 3 8v11c3.5 0 6.64 1.35 9 3.54 2.36-2.19 5.5-3.54 9-3.54V8c-3.5 0-6.64 1.35-9 3.54z"/></svg>

    </a>
    <label class="md-header__button md-icon" for="__drawer">
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M3 6h18v2H3V6m0 5h18v2H3v-2m0 5h18v2H3v-2z"/></svg>
    </label>
    <div class="md-header__title" data-md-component="header-title">
      <div class="md-header__ellipsis">
        <div class="md-header__topic">
          <span class="md-ellipsis">
            SecuWallet
          </span>
        </div>
        <div class="md-header__topic" data-md-component="header-topic">
          <span class="md-ellipsis">
            
              Créer un compte
            
          </span>
        </div>
      </div>
    </div>
    
    
    
    
  </nav>
  
</header>
    
    <div class="md-container" data-md-component="container">
      
      
        
          
        
      
      <main class="md-main" data-md-component="main">
        <div class="md-main__inner md-grid">
          
            
              
              <div class="md-sidebar md-sidebar--primary" data-md-component="sidebar" data-md-type="navigation" >
                <div class="md-sidebar__scrollwrap">
                  <div class="md-sidebar__inner">
                    


<nav class="md-nav md-nav--primary" aria-label="Navigation" data-md-level="0">
  <label class="md-nav__title" for="__drawer">
    <a href=".." title="SecuWallet" class="md-nav__button md-logo" aria-label="SecuWallet" data-md-component="logo">
      
  
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 8a3 3 0 0 0 3-3 3 3 0 0 0-3-3 3 3 0 0 0-3 3 3 3 0 0 0 3 3m0 3.54C9.64 9.35 6.5 8 3 8v11c3.5 0 6.64 1.35 9 3.54 2.36-2.19 5.5-3.54 9-3.54V8c-3.5 0-6.64 1.35-9 3.54z"/></svg>

    </a>
    SecuWallet
  </label>
  
  <ul class="md-nav__list" data-md-scrollfix>
    
      
      
      

  
  
  
    <li class="md-nav__item">
      <a href=".." class="md-nav__link">
        Quel but ?
      </a>
    </li>
  

    
      
      
      

  
  
  
    <li class="md-nav__item">
      <a href="../connexion/" class="md-nav__link">
        Connexion
      </a>
    </li>
  

    
      
      
      

  
  
    
  
  
    <li class="md-nav__item md-nav__item--active">
      
      <input class="md-nav__toggle md-toggle" data-md-toggle="toc" type="checkbox" id="__toc">
      
      
        
      
      
      <a href="./" class="md-nav__link md-nav__link--active">
        Créer un compte
      </a>
      
    </li>
  

    
      
      
      

  
  
  
    <li class="md-nav__item">
      <a href="../download/" class="md-nav__link">
        Télécharger
      </a>
    </li>
  

    
  </ul>
</nav>
                  </div>
                </div>
              </div>
            
            
              
              <div class="md-sidebar md-sidebar--secondary" data-md-component="sidebar" data-md-type="toc" >
                <div class="md-sidebar__scrollwrap">
                  <div class="md-sidebar__inner">
                    


<nav class="md-nav md-nav--secondary" aria-label="Table of contents">
  
  
  
    
  
  
</nav>
                  </div>
                </div>
              </div>
            
          
          <div class="md-content" data-md-component="content">
            <article class="md-content__inner md-typeset">
              
                
                
                <h1 id="creer-un-compte">Créer un compte</h1>
<form method="POST">
    Email:<br> <input  placeholder="Email" type="text" name="email"><br>
    Mot de passe:<br> <input  placeholder="Mot de passe" type="password" name="password"><br>
    <br><input type="submit" name="submit">
  </form>
                
  <?php 
  if(strlen($secretKey) == 16){
    echo "<label>Veullez scanner le QR code avec l'application Google Authenticator</label><br>";
    echo "<img id='qrCode' src='".$qrImageGenerator->generateUri($secret)."'>'<br>";
    echo "Voici la clé secrète a sauvegarder pour récupérer votre compte : ".$secretKey."\n";
    echo "<form method='POST' action='index.php'>";
    echo "<input  placeholder='Entré le code' type='text' name='code' >";
    echo "<input type='submit' name='authenticator'>";
    echo "</form>";
  }
  
  
  ?>              
              
                


              
            </article>
          </div>
        </div>
        
      </main>
      
        
<footer class="md-footer">
  
    <nav class="md-footer__inner md-grid" aria-label="Footer">
      
        
        <a href="../connexion/" class="md-footer__link md-footer__link--prev" aria-label="Previous: Connexion" rel="prev">
          <div class="md-footer__button md-icon">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M20 11v2H8l5.5 5.5-1.42 1.42L4.16 12l7.92-7.92L13.5 5.5 8 11h12z"/></svg>
          </div>
          <div class="md-footer__title">
            <div class="md-ellipsis">
              <span class="md-footer__direction">
                Previous
              </span>
              Connexion
            </div>
          </div>
        </a>
      
      
        
        <a href="../download/" class="md-footer__link md-footer__link--next" aria-label="Next: Télécharger" rel="next">
          <div class="md-footer__title">
            <div class="md-ellipsis">
              <span class="md-footer__direction">
                Next
              </span>
              Télécharger
            </div>
          </div>
          <div class="md-footer__button md-icon">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M4 11v2h12l-5.5 5.5 1.42 1.42L19.84 12l-7.92-7.92L10.5 5.5 16 11H4z"/></svg>
          </div>
        </a>
      
    </nav>
  
  <div class="md-footer-meta md-typeset">
    <div class="md-footer-meta__inner md-grid">
      <div class="md-footer-copyright">
        
        
          Made with
          <a href="https://squidfunk.github.io/mkdocs-material/" target="_blank" rel="noopener">
            Material for MkDocs
          </a>
        
        
      </div>
      
    </div>
  </div>
</footer>
      
    </div>
    <div class="md-dialog" data-md-component="dialog">
      <div class="md-dialog__inner md-typeset"></div>
    </div>
    <script id="__config" type="application/json">{"base": "..", "features": [], "translations": {"clipboard.copy": "Copy to clipboard", "clipboard.copied": "Copied to clipboard", "search.config.lang": "en", "search.config.pipeline": "trimmer, stopWordFilter", "search.config.separator": "[\\s\\-]+", "search.placeholder": "Search", "search.result.placeholder": "Type to start searching", "search.result.none": "No matching documents", "search.result.one": "1 matching document", "search.result.other": "# matching documents", "search.result.more.one": "1 more on this page", "search.result.more.other": "# more on this page", "search.result.term.missing": "Missing", "select.version.title": "Select version"}, "search": "../assets/javascripts/workers/search.fcfe8b6d.min.js", "version": null}</script>
    
    
      <script src="../assets/javascripts/bundle.b1047164.min.js"></script>
      
    
  </body>
</html>
```
### download.php
```php
<?php 
session_start();
if($_SESSION["logged"])
{

}else {
  $_SESSION["notConnected"] = TRUE;
  header("Location: http://127.0.0.1/siteSansModif/");
}

if (isset($_GET['destroy'])) {
  session_destroy();
}
?>
<!doctype html>
<html lang="en" class="no-js">
  <head>
    
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width,initial-scale=1">
      
      
      
      
      <link rel="icon" href="../assets/images/favicon.png">
      <meta name="generator" content="mkdocs-1.2.3, mkdocs-material-7.3.6">
    
    
      
        <title>Télécharger - SecuWallet</title>
      
    
    
      <link rel="stylesheet" href="../assets/stylesheets/main.a57b2b03.min.css">
      
        
        <link rel="stylesheet" href="../assets/stylesheets/palette.3f5d1f46.min.css">
        
      
    
    
    
      
        
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,700%7CRoboto+Mono&display=fallback">
        <style>:root{--md-text-font-family:"Roboto";--md-code-font-family:"Roboto Mono"}</style>
      
    
    
    
      <link rel="stylesheet" href="../stylesheets/extra.css">
    
    
      


    
    
  </head>
  
  
    
    
    
    
    
    <body dir="ltr" data-md-color-scheme="" data-md-color-primary="none" data-md-color-accent="none">
  
    
    <script>function __prefix(e){return new URL("..",location).pathname+"."+e}function __get(e,t=localStorage){return JSON.parse(t.getItem(__prefix(e)))}</script>
    
    <input class="md-toggle" data-md-toggle="drawer" type="checkbox" id="__drawer" autocomplete="off">
    <input class="md-toggle" data-md-toggle="search" type="checkbox" id="__search" autocomplete="off">
    <label class="md-overlay" for="__drawer"></label>
    <div data-md-component="skip">
      
        
        <a href="#telecharger" class="md-skip">
          Skip to content
        </a>
      
    </div>
    <div data-md-component="announce">
      
    </div>
    
      

<header class="md-header" data-md-component="header">
  <nav class="md-header__inner md-grid" aria-label="Header">
    <a href=".." title="SecuWallet" class="md-header__button md-logo" aria-label="SecuWallet" data-md-component="logo">
      
  
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 8a3 3 0 0 0 3-3 3 3 0 0 0-3-3 3 3 0 0 0-3 3 3 3 0 0 0 3 3m0 3.54C9.64 9.35 6.5 8 3 8v11c3.5 0 6.64 1.35 9 3.54 2.36-2.19 5.5-3.54 9-3.54V8c-3.5 0-6.64 1.35-9 3.54z"/></svg>

    </a>
    <label class="md-header__button md-icon" for="__drawer">
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M3 6h18v2H3V6m0 5h18v2H3v-2m0 5h18v2H3v-2z"/></svg>
    </label>
    <div class="md-header__title" data-md-component="header-title">
      <div class="md-header__ellipsis">
        <div class="md-header__topic">
          <span class="md-ellipsis">
            SecuWallet
          </span>
        </div>
        <div class="md-header__topic" data-md-component="header-topic">
          <span class="md-ellipsis">
            
              Télécharger
            
          </span>
        </div>
      </div>
    </div>
    
    
    
    
  </nav>
  
</header>
    
    <div class="md-container" data-md-component="container">
      
      
        
          
        
      
      <main class="md-main" data-md-component="main">
        <div class="md-main__inner md-grid">
          
            
              
              <div class="md-sidebar md-sidebar--primary" data-md-component="sidebar" data-md-type="navigation" >
                <div class="md-sidebar__scrollwrap">
                  <div class="md-sidebar__inner">
                    


<nav class="md-nav md-nav--primary" aria-label="Navigation" data-md-level="0">
  <label class="md-nav__title" for="__drawer">
    <a href=".." title="SecuWallet" class="md-nav__button md-logo" aria-label="SecuWallet" data-md-component="logo">
      
  
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 8a3 3 0 0 0 3-3 3 3 0 0 0-3-3 3 3 0 0 0-3 3 3 3 0 0 0 3 3m0 3.54C9.64 9.35 6.5 8 3 8v11c3.5 0 6.64 1.35 9 3.54 2.36-2.19 5.5-3.54 9-3.54V8c-3.5 0-6.64 1.35-9 3.54z"/></svg>

    </a>
    SecuWallet
  </label>
  
  <ul class="md-nav__list" data-md-scrollfix>
    
      
      
      

  
  
  
    <li class="md-nav__item">
      <a href=".." class="md-nav__link">
        Quel but ?
      </a>
    </li>
  

    
      
      
      

  
  
  
    <li class="md-nav__item">
      <a href="../connexion/" class="md-nav__link">
        Connexion
      </a>
    </li>
  

    
      
      
      

  
  
  
    <li class="md-nav__item">
      <a href="../createAccount/" class="md-nav__link">
        Créer un compte
      </a>
    </li>
  

    
      
      
      

  
  
    
  
  
    <li class="md-nav__item md-nav__item--active">
      
      <input class="md-nav__toggle md-toggle" data-md-toggle="toc" type="checkbox" id="__toc">
      
      
        
      
      
      <a href="./" class="md-nav__link md-nav__link--active">
        Télécharger
      </a>
      
    </li>
  

    
  </ul>
</nav>
                  </div>
                </div>
              </div>
            
            
              
              <div class="md-sidebar md-sidebar--secondary" data-md-component="sidebar" data-md-type="toc" >
                <div class="md-sidebar__scrollwrap">
                  <div class="md-sidebar__inner">
                    


<nav class="md-nav md-nav--secondary" aria-label="Table of contents">
  
  
  
    
  
  
</nav>
                  </div>
                </div>
              </div>
            
          
          <div class="md-content" data-md-component="content">
            <article class="md-content__inner md-typeset">
              
                
                
                <h1 id="telecharger">Télécharger</h1>
                
                <a href="./SecuWallet.msi?destroy=true">Télécharger le fichier d'installation</a>
              
                


              
            </article>
          </div>
        </div>
        
      </main>
      
        
<footer class="md-footer">
  
    <nav class="md-footer__inner md-grid" aria-label="Footer">
      
        
        <a href="../createAccount/" class="md-footer__link md-footer__link--prev" aria-label="Previous: Créer un compte" rel="prev">
          <div class="md-footer__button md-icon">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M20 11v2H8l5.5 5.5-1.42 1.42L4.16 12l7.92-7.92L13.5 5.5 8 11h12z"/></svg>
          </div>
          <div class="md-footer__title">
            <div class="md-ellipsis">
              <span class="md-footer__direction">
                Previous
              </span>
              Créer un compte
            </div>
          </div>
        </a>
      
      
    </nav>
  
  <div class="md-footer-meta md-typeset">
    <div class="md-footer-meta__inner md-grid">
      <div class="md-footer-copyright">
        
        
          Made with
          <a href="https://squidfunk.github.io/mkdocs-material/" target="_blank" rel="noopener">
            Material for MkDocs
          </a>
        
        
      </div>
      
    </div>
  </div>
</footer>
      
    </div>
    <div class="md-dialog" data-md-component="dialog">
      <div class="md-dialog__inner md-typeset"></div>
    </div>
    <script id="__config" type="application/json">{"base": "..", "features": [], "translations": {"clipboard.copy": "Copy to clipboard", "clipboard.copied": "Copied to clipboard", "search.config.lang": "en", "search.config.pipeline": "trimmer, stopWordFilter", "search.config.separator": "[\\s\\-]+", "search.placeholder": "Search", "search.result.placeholder": "Type to start searching", "search.result.none": "No matching documents", "search.result.one": "1 matching document", "search.result.other": "# matching documents", "search.result.more.one": "1 more on this page", "search.result.more.other": "# more on this page", "search.result.term.missing": "Missing", "select.version.title": "Select version"}, "search": "../assets/javascripts/workers/search.fcfe8b6d.min.js", "version": null}</script>
    
    
      <script src="../assets/javascripts/bundle.b1047164.min.js"></script>
      
    
  </body>
</html>
```
## API

### authenticator.php
```php
<?php
    include "../lib/google-authenticator-main/vendor/autoload.php";
    use Emarref\Jwt\Claim;
    require_once ("../functionConnexionDB.php");
    require_once ("../functions.php");

    header('Content-Type: application/json; charset=utf-8');
    //Contrôle que l'API a bien reçu l'email et le code de l'utilisateur
    if(isset($_POST['email']) && isset($_POST['code']))
    {
        //Récupération de la clé secrete
        $secretKey = GetUserSecretKey($_POST['email']);
        //Contrôle de Google Authenticator
        if (checkAuthenticator($secretKey['secretKey'], $_POST['code'])) {
            $value = "ok";
        }else {
            $value = "ko";
        }
        http_response_code(200);
    }else{
        $value = "Error";
    }
    echo json_encode($value);

    
?>
```
### login.php
```php
<?php
//include "../lib/google-authenticator-main/vendor/autoload.php";
//use Emarref\Jwt;
    require_once ("../functionConnexionDB.php");
    require_once ("../functions.php");

    header('Content-Type: application/json; charset=utf-8');
    //Check les login d'un utilisateur provenant du client C#
    if(isset($_POST['email']) && isset($_POST['password']))
    {
        $result = CheckUserLogin($_POST['email'],$_POST['password']);
        $secretKey = $result['secretKey'];
        if(strlen($secretKey) == 16){
            $value = "ok";
        }else {
            $value = "ko";
        }
        http_response_code(200);
    }else{
        $value = "Error";
    }
    echo json_encode($value);
?>
```
### tokenAuthenticator.php
```php
<?php 
require_once ("../functionConnexionDB.php");
include "../lib/google-authenticator-main/vendor/autoload.php";
use Emarref\Jwt\Claim;

if (isset($_POST['token']) && $_POST['token'] != "") {
    $verification = authenticate_token($_POST['token']);
    if ($verification['status'] == 'ok') {
        header('Content-Type: application/json; charset=utf-8');
        http_response_code(200);
        echo json_encode("ok");
    } else if ($verification['status'] == 'error') {
        header('Content-Type: application/json; charset=utf-8');
        http_response_code(403);
        echo json_encode(['data' => $verification['err']]);
    }
} else {
    header('Content-Type: application/json; charset=utf-8');
    http_response_code(401);
    
}

/**
 * Vérifie le token JWT
 *
 * @param string  $serializedToken  Token provenant du client C#
 * 
 * @author Thomas Tissot
 * @return string Ok si le token valise. Si le token est invalide retourne le code d'erreur
 */ 
function authenticate_token($serializedToken)
{
    $jwt = new Emarref\Jwt\Jwt();
    try {
        $token = $jwt->deserialize($serializedToken);
    } catch (Exception $e) {
        return ['status' => 'error', 'err' => $e->getMessage()];
    } catch (Error $e) {
        return ['status' => 'error', 'err' => $e->getMessage()];
    }
    $algorithm = new Emarref\Jwt\Algorithm\Hs256('verysecret');
    $encryption = Emarref\Jwt\Encryption\Factory::create($algorithm);
    $context = new Emarref\Jwt\Verification\Context($encryption);
    $context->setAudience('http://127.0.0.1/site/api/');
    $context->setIssuer('SecuWallet');
    $context->setSubject($_POST['email']);
    
    try {
        $jwt->verify($token, $context);
    } catch (Emarref\Jwt\Exception\VerificationException $e) {
        return ['status' => 'error', 'err' => $e->getMessage()];
    }
    return ['status' => 'ok'];
}
?>
```
### tokenCreator.php
```php
<?php 
    require_once ("../functionConnexionDB.php");
    include "../lib/google-authenticator-main/vendor/autoload.php";
    use Emarref\Jwt\Claim;

    header('Content-Type: application/json; charset=utf-8');
    http_response_code(200);
    echo json_encode(create_token());

/**
 * Génère un token en utilisant l'id et l'email de l'utilisateur
 * 
 * @author Thomas Tissot
 * @return $serializedToken Token JWT généré grace a la librairie Emarref\Jwt
 */     
function create_token(){
    $token = new Emarref\Jwt\Token();
    
    // Standard claims are supported
    $token->addClaim(new Claim\Audience(['http://127.0.0.1/site/api/', 'audience_2']));
    $token->addClaim(new Claim\Expiration(new \DateTime('30 minutes')));
    $token->addClaim(new Claim\IssuedAt(new \DateTime('now')));
    $token->addClaim(new Claim\Issuer('SecuWallet'));
    $token->addClaim(new Claim\JwtId(GetUserId($_POST['email'])));
    $token->addClaim(new Claim\NotBefore(new \DateTime('now')));
    $token->addClaim(new Claim\Subject($_POST['email']));
    
    $jwt = new Emarref\Jwt\Jwt();
    $algorithm = new Emarref\Jwt\Algorithm\Hs256('verysecret');
    $encryption = Emarref\Jwt\Encryption\Factory::create($algorithm);
    $serializedToken = $jwt->serialize($token, $encryption);
    return $serializedToken;
}
?>
```