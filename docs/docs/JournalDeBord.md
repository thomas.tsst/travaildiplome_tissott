# Journal de bord Travail de diplôme
### 04.04.2022
Tâches à réaliser aujourd'hui :

* Création du git ainsi que sa structure ✅
* Réalisation du planning prévisionnel (restent quelques modifications à faire)

Discussion avec M. Bonvin et M. Garcia au sujet de l'application que je vais réaliser durant ce travail de diplôme. La première option qui était de stocker les portefeuilles sur un serveur tombe à l'eau, car rares seront les personnes qui oseraient donner les clés privées de leurs portefeuilles. Donc l'application au moment de sont installation sera capable de créer un dossier ou une partition encryptée pour y stocker les portefeuilles de l'utilisateur directement sur ça machine. (Il faut encore que je me documente sur le sujet de l'encryption pour y sortir la meilleure solution)

### 05.04.2022

Modification de mon template de planning suite à une discussion avec M. Garcia.

Ce matin j'ai reçu un Raspberry Pi 4 ainsi qu'un boitier Argon One pour intégrer un disque dur M2 ce qui va me permettre d'héberger mon serveur HTTPS.
Durant cette journée j'ai terminé la planification prévisionnelle et commencé à déployer le serveur Apache2 en HTTPS. J'ai également installé un serveur Samba pour permettre à ma machine d'accéder au fichier du Raspberry plus facilement en y ajoutant un disque réseau.

<img src="./img/Raspberry.jpg">

### 06.04.2022
### 07.04.2022
J'ai pu discuter avec un collège qui avait déjà fait une architecture très similaire à mon projet en ce qui concerne le serveur HTTPS qui reçoit des demandes sur la base du client. Ça m’a permis de mieux comprendre comment j'allais devoir m'y prendre pour réaliser ce travail.

Sinon j'ai effectué un travail de recherche pour sécuriser au maximum le Raspberry.  
### 08.04.2022
Aujourd'hui je vais exclusivement me pencher sur la documentation technique. (User stories / Documentation du travail réaliser au cours de cette première semaine)

### 11.04.2022
Aujourd'hui je me suis penché sur l'organisation de ma documentation comme discuter avec M. Garcia. (Plusieurs fichiers séparés)

### 12.04.2022
Aujourd'hui j'ai installé le serveur Hardhat sur le Raspberry. Aucun problème rencontré lors de l'installation.
### 13.04.2022
User stories Site Web

### 25.04.2022
Documentation de l'encryption des fichiers des wallet (.json)
Mon choix après différente recherche s'est porté sur une encryption AES avec une clé différente pour chaque fichier.

### 26.04.2022
J'ai essayer de me connecter a la blockchain présente sur le Raspberry au début la connexion était refusée depuis l'extérieur du Raspberry j'ai trouver sur internet que c'était un problème connu et qu'il fallait simplement changer l'adresse IP qui est par défaut 127.0.0.1 en 0.0.0.0.

Suite a un entretien avec M. Garcia il m'a demandé de refaire plus proprement un schéma avec toutes mes futures interfaces ainsi que les liens entre elles.

### 27.04.2022

### 02.05.2022
Création des différentes classes nécessaire à mon projet.
Appcontroller qui réunit les informations qui sont nécessaires de faire transiter entre les différentes forms. BlockchainController qui permet l'échange de données avec la blockchain, mais également l'enregistrement local des portefeuilles dans l'application
### 03.05.2022
Documentation guide d'installation de l'environnement sur le Raspberry

### 04.05.2022
Réalisation du poster ainsi que le résumer a rendre le lundi de la semaine prochaine. Problème rencontré l’utilisation pas simple de Photoshop.

### 05.05.2022
Ce matin j'ai réalisé le résumé de mon application ainsi que sa traduction en anglais (à rendre lundi de la semaine prochaine)

Revu du poster par M. Garcia suite a cela je dois mieux imager le fonctionnement des différentes technologies utilisées par mon projet. 

### 10.05.2022
Développement du login de l'application C#
### 11.05.2022
Ajout de l'enregistrement local des portefeuilles
### 12.05.2022
Ajout des requêtes sur la blockchain répétitive.
* GetBalance
* GetBlock
* ...
### 13.05.2022
Ajout des paramètres de l'application ainsi que l'enregistrement de ceux-ci a la fermeture du programme.
### 16.05.2022
Réalisation du site web en markdown
Problème rencontrer : Pas trouvé comment forcer un css sur les inputs du formulaire.
Création de la base de données pour stocker les utilisateurs

### 17.05.2022
Ajout de la création de comptes. Pour ajouter une double authentification, j'ai décidé d'utiliser cette libraire https://github.com/dochne/google-authenticator. Elle est encore tenue à jour contrairement à l'ancienne libraire la plus utilisée (https://github.com/sonata-project/GoogleAuthenticator). J'ai eu quelque problème avec composer pour ajouter automatiquement les packages nécessaires à cette librairie.

### 18.05.2022
Ajout du login ainsi que l'accès a la page téléchargement pour avoir accès aux futures installer du client C#
### 19.05.2022
Création de l'api PHP.
Aujourd'hui J'ai réussi à finir le script qui valide les informations de l'utilisateur lors de sa tentative de connexion
### 20.05.2022
Ajout du script authenticator.php a l'api pour valider les codes Google Authenticator qu'entre l'utilisateur
voici la librairie que j'ai décider d'utiliser : https://github.com/dochne/google-authenticator.git
### 23.05.2022
Ajout du script tokenCreator.php afin de pouvoir générer un token JWT lors de la connexion de l'utilisateur
Problème rencontrer documentation pauvre si les informations que je dois fournir au token.

En fin de journée j'ai commencé la vérification du token
### 24.05.2022
Finalisation du script tokenAuthenticator.php pour le moment l'API n'est pas encore connecté a l'application j'ai uniquement tester les différentes fonctionnalités grâce à l'application "Talend Api tester".
### 25.05.2022
Tentative de connecter mon application C# avec l'api. Le serveur a bien l'air de recevoir les paquets HTTP. J'ai pu vérifier ça grâce à l'application wireshark qui me permet de visualiser tous les paquets qui transitent sur le réseau. Le problème a l'air de venir de mon code C# comme s’il restait à l'écoute du serveur en bloquant l'application.
### 26.05.2022
Après plusieurs recherches sur internet, j'ai réussi à débuger les problèmes d'hier. Pour éviter les codes bloquants asynchrones il faut ajoute  à la fin de chacune de mes requêtes pour éviter ce qu'on appel les Deadlock .ConfigureAwait(false);. 
La ligne complète ressemble à ça : 
```c#
await client.PostAsync(LOGIN_URL, stringContent).ConfigureAwait(false);
```
### 27.05.2022
Ajout de la classe JWTController qui s'occupe de la communication avec l'api.
Ajout de la création d'un nouveau wallet.
### 30.05.2022
Ajout de l'import des wallet soit grâce à la clé privée sois par un fichier json extrait d'une autre application
### 31.05.2022
Développement des graphiques de l'évolution des portefeuilles. Graphique sur l'évolution de la quantité d'eth présents sur le portefeuille fonctionnel. Mais le graphique sur l'évolution de la valeur de la devise sélectionnée dans les paramètres par l'utilisateur a plusieurs bugs d'affichage.
### 01.06.2022
Ajout du listing des différentes transactions effectuer avec le wallet sélectionné.
### 02.06.2022
Ajout des transactions d'ethereum
Ajout des contacts enregistré sur l'application.
### 03.06.2022
Correction des différents bugs présents dans l'application (Affichage / Try catch)
### 06.06.2022
Documentation des différentes interfaces, JWT.
### 07.06.2022
Ajout de commentaires sur le codes

### 08.06.2022
Rédaction de la conclusion, Encryption AES, Google Authenticator.

Retour de M. Garcia sur ma documentation :
    * Conclusion trop courte
    * Aspect sécuriter a dévelloper
    * Revoir les tests
    * Mettre en avant les information qui transite entre la blockchain et le client


### 09.06.2022
Appronfondissement de la partie Sécurite de ma documentaion.
Ajout des code source.
### 10.06.2022
Correction orthographe.
Check si toutes les images sont présente.