# Interfaces

L'élaboration de l'application étant faite grâce à la [conceptualisation](#conceptualisation), il m'est tout de même arrivé que certaines des [user stories](#userStories), fonctionnalités ou encore [interfaces](#InterfacesMockUp) aient été modifiées.

Mon application est divisée en deux parties. Il s'agit d'un [site web](#site-web) où l'utilisateur doit se rendre afin de créer son compte et de créer sa clé secrète utile à la librairie Goolge authenticator. Ensuite, du moment où il se connecte, il a la possibilité de télécharger mon application en format ".msi".

Du moment où l'[application](#application-c) est installée, il peut se connecter avec les mêmes données, sans oublier d'utiliser son code Authenticator.

## Site Web
<!--
Le site est web est constitué de plusieurs sections qui sont l'accueil, la création de comptes, la connexion et le téléchargement. -->

### Acceuil

L'accueil est une page qui founit des informations importantes à l'utilisateur, puisqu'il s'agit de l'explication de mon application: "Qu'est-ce qu'elle fait cette application?". Informations prisent, il est donc spécifié à l'utilisateur de [créer un compte](#création-de-compte) pour permettre de télécharger le logiciel. Dans le cas où l'utilisateur serait déjà inscrit, alors il peut aller sur l'onglet ["Connexion"](#connexion).

{{fig("./img/acceuilWeb.png", "Page d'accueil", 70)}}

### Création de compte

Du moment où l'utilisateur est d'accord d'utiliser mon application, il arrive donc sur cette page qui va permettre de s'inscrire. Il est demandé de renseigner deux informations qui sont: le nom d'utilisateur (mail) et le code pour se connecter. 

Ensuite, les informations sont contrôlées afin de voir si les informations fournies répondent aux exigences. Il s'agit donc de regarder s’il s'agit d'un mail valide et si le mot de passe contient au minimum 6 caractères.

Enfin, si tout est valide, un QR code est alors affiché, afin que l'utilisateur puisse le scanner avec l'application "authenticator" de Google. Ce QR code fait référence à sa clé secrète qui est alors stockée dans ma base de données et qui est donc liée à cette application pour la protégée indépendamment pour chaque utilisateur.

Google Authenticator utilise cette clé secrète pour générer un code à six chiffres toutes les 30 secondes.

{{fig("./img/creerCompteWeb.png", "Page création compte", 70)}}

### Connexion

 Pour que mon application soit téléchargée, il faut que l'utilisateur se connecte avec les informations remplies au préalable. Du moment où la connexion est effectuée, un onglet téléchargé est alors affiché.

{{fig("./img/connexionWeb.png", "Page connexion", 70)}}

### Télécharger

Le téléchargement est faisable d'un simple clique sur le lien. Le logiciel est donc téléchargé sur son ordinateur personnel au format ".msi".

## Application C#
<!-- 
Ce logiciel est le cœur de mon projet, puisqu'il s'agit de l'application qui pourra gérer des portefeuilles soit en l'important soit en le créant. D'autant plus que cela fournira la possibilité de faire des transactions en envoyant ou en recevant  ainsi que de voir le cours l'Etherum sur la devise choisi ("USD" est la configuration initiale). Différents graphiques sont affichés afin de fournir un aspect visuel de la quantité de cryptomonnaie et de la valeur de celle-ci. -->

### Connexion

Cette interface est celle qui permettra à l'utilisateur de se connecter avec les informations fournies lors de la [création du compte](#création-de-compte) sur mon site web.

{{fig("./img/loginApp.png", "Interface Connexion", 25)}}

### Google authenticator

Après validation de la connexion, cette interface est alors affichée afin que l'utilisateur puisse fournir le code Authenticator affiché dans son application "Google authenticator".

{{fig("./img/authenticatorApp.png", "Interface Google Authenticator", 25)}}

### Acceuil

Une fois toutes les démarches de connexion réussies, cette interface est alors affichée. Dans celle-ci on voit que l'utilisateur peut sélectionner ses portefeuilles afin de voir les informations qui les caractérisent.

Les onglets en bas montrent l'historique des transactions ("Historique"), l'évolution de la valeur ("Évolution valeur") et l'évolution de la quantité d'Ethereum ("Évolution quantité").

L'historique permet de voir toutes les transactions effectuées avec le wallet sélectionné, que ce soit un envoi ou un reçu.

Pour la partie évolution, je dois souligner que des modifications ont été apportées par rapport à sa conceptualisation, ce qui fait que j'ai divisé en deux parties. Il s'agit donc de "l'évolution valeur" et de "l'évolution quantités".

L'évolution valeur, montre un graphique qui démontre de manière visuelle la variation de la valeur de ses ETH actuellement présents dans le portefeuille sur le temps.

L'évolution quantité, montre également un graphique, mais cette fois pour montrer la variation de la quantité de ses coins présents dans le portefeuille.

Il peut également appuyer sur le bouton ["Recevoir"](#recevoir) qui permet de recevoir un certain montant de cryptomonnaie.

L'utilisateur a la possibilité d'appuyer sur le bouton ["Envoyer"](#envoyer) qui permet d'envoyer une certaine somme de cryptomonnaie sur un portefeuille spécifique.

On voit sur cette interface un [rouage](#mon-compte) qui permet d'afficher les options possibles.

{{fig("./img/historiqueTransacApp.png", "Interface Acceuil", 40)}}

### Recevoir

Du moment où l'utilisateur veut récupérer sa clé privée, il doit cliquer sur le bouton "Recevoir". Il est alors redirigé sur cette interface qui permet de choisir la manière dont il voudrait procéder. C'est-à-dire soit par le biais d'un QR code soit en copiant sa clé publique.

Le QR code permet à la personne qui voudrait lui envoyer de la cryptomonnaie de le scanner afin d'avoir directement sa clé publique.

Le fait de copier la clé publique qui est affichée en bas permet de pouvoir coller celle-ci à l'endroit indiqué afin de pouvoir remplir le formulaire d'envoi.

{{fig("./img/recevoirApp.png", "Interface Recevoir", 30)}}

### Envoyer

Si par contre l'utilisateur décide d'envoyer de l'etherum alors cette interface est affichée. Il s'agit d'un formulaire qui demande plusieurs informations spécifiques pour permettre d'envoyer. Cela étant dit, plusieurs modifications ont été apportées. Puisque j'avais dans mon optique de fonctionner uniquement en utilisant l'adresse des wallets. 

{{fig("./img/envoieApp.png", "Interface Envoyer", 40)}}

Il doit donc choisir à qui il veut l'envoyer c'est pour cela qu'il doit remplir ce champ avec la clé publique du destinataire. Pour faciliter cette tâche, j'ai remanié mon idée de base en lui laissant la possibilité de choisir un de ses wallets s’il le souhaite. Il peut donc choisir d'envoyer avec un "Comptes personnels" ou de choisir un contact qu'il aurait enregistré directement sur l'interface. Par contre, s’il le désire, il a la possibilité d'indiquer lui-même la clé publique.

Ensuite, il doit fournir le montant qu'il voudrait envoyer. Dans le cas où il voudrait possiblement envoyer un montant à virgule, il est impératif d'utiliser les virgules (",").

{{fig("./img/confirmeEnvoie.png", "Interface Confirmation d'envoi", 30)}}

Enfin, il faut rentrer son mot de passe, appuyer sur "Envoyer" et renseigner l'Authenticator. Cette partie a une fois encore été modifiée, pour des raisons de sécurités.

Pour que l'utilisateur soit averti que la transaction s'est bien déroulée ou non, alors un petit message de confirmation est affiché.

{{fig("./img/confirmeAuthenticatorApp.png", "Interface Confirmation Authenticator", 30)}}

### Mon Compte

Du moment où l'utilisateur veut accéder à son compte, il peut appuyer sur ce rouage afin de pouvoir accéder à d'autres options. Il s'agit de pouvoir gérer ses [portefeuilles](#gestion-des-portefeuilles), [paramètres](#paramètre) ou [déconnexion](#déconnexion).

{{fig("./img/monCompteApp.png", "Interface mon compte", 25)}}

### Gestion des portefeuilles

{{fig("./img/supprimerApp.png", "Interface suppression portefeuille", 40)}}

On arrive sur cette interface qui a été modifiée par rapport à sa conceptualisation. Une fois que l'utilisateur a choisi de gérer ses portefeuilles. Celle-ci offre trois onglets, "Nouveau",  "Importer" ou "supprimer".

{{fig("./img/creerWalletApp.png", "Interface création portefeuille", 40)}}

Du moment où l'utilisateur veut créer un nouveau portefeuille il doit fournir le nom de ce dernier pour permettre la différenciation avec les autres. Il va de soi que le nom de celui-ci doit être différent de ceux présents dans son compte. En plus de mon idée de base et afin de sécuriser ce wallet je demande à l'utilisateur de remplir un mot de passe.

{{fig("./img/importerWalletApp.png", "Interface importer portefeuille", 40)}}

Maintenant l'utilisateur veut importer un portefeuille, parce qu'il en a un, mais qui n'est pas encore stocké dans mon application. Il a deux possibilités de le faire, soit de l'importer avec une clé privée directement soit de l'importer avec un fichier "JSON" qu'il aurait dans ses fichiers. Il convient de spécifier que lors de l'élaboration du concept j'avais dans l'optique de mettre deux formulaires distincts sur la même interface. Cela me paraissait tout de même trop flou pour l'utilisateur c'est pourquoi j'ai décidé de séparer ces deux fonctions en deux interfaces différentes.

{{fig("./img/importClePrivee.png", "Interface import par clé privée", 25)}}

L'importation par clé privée doit se faire en remplissant un formulaire. Il s'agit de mettre la clé privée du portefeuille ainsi que de fournir un nom pour ce portefeuille, encore une fois diffère par rapport aux autres portefeuilles présents dans son compte. Enfin, il doit confirmer cette importation avec un mot de passe spécifique à ce wallet.

{{fig("./img/importFicherJSON.png", "Interface import par fichier JSON", 25)}}

L'importation par fichier "JSON" doit s'effectuer en important le fichier en question en appuyant sur le bouton "Importer fichier". Ensuite, il est important de spécifier que le mot de passe qui doit être fourni doit être identique à ce portefeuille puisque le fichier "JSON" est encrypté par celui-ci. Enfin, pour confirmer l'importation, il faut appuyer sur le bouton "Importer".

{{fig("./img/supprimerApp.png", "Interface suppression portefeuille", 40)}}

Il arrive que parfois on ait plus besoin d'utiliser un portefeuille pour telle ou telle raison. C'est pourquoi l'option supprimer est mise à disposition. Pour ce faire, l'utilisateur doit sélectionner un portefeuille présent dans cette liste ensuite appuyer sur supprimer.

Une interface [authenticator] s'affiche pour confirmer l'opération.


### Paramètre

Cette interface permet à l'utilisateur de choisir la ["devise"](#devise) de l'application. Il convient de spécifier que la configuration par défaut est mise sur "USD". Un bouton ["Contact"](#contact) est affiché pour permettre d'accéder à son journal de contact. Enfin, un bouton "A propos" est mis à disposition afin de donner des informations sur l'application (version, auteur, etc.).

{{fig("./img/parametreApp.png", "Interface paramètres", 25)}}

#### Devise

Une liste de choix est mise à disposition afin que l'utilisateur puisse choisir la devise qui lui convienne. Les choix possibles sont "USD", "EUR" et "CHF". À la sélection, la devise est alors enregistrée. 

### Contact

Cette interface permet à l'utilisateur de gérer son journal de contact.

{{fig("./img/contactApp.png", "Interface contacts", 40)}}

Il a la possibilité d'en créer un nouveau en appuyant sur le bouton "Nouveau". Il doit fournir des informations qui sont le nom du contact et son adresse qui fait référence à sa clé publique. 

{{fig("./img/creerContact.png", "Interface créer contact", 25)}}

Il peut aussi modifier des contacts déjà existants. Pour cela, il doit sélectionner le contact en question et appuyer sur le bouton modifié. C'est alors qu'on est redirigé vers une interface qui reprend les informations du contact sélectionné. Cela laisse à l'utilisateur la possibilité de changer à sa guise les informations que ce soit le nom du contact ou la clé publique qui serait peut-être fausse. Pour valider ces modifications, il faut appuyer sur "Valider".

{{fig("./img/modifierContactApp.png", "Interface modifier contact", 25)}}

Enfin, il peut supprimer un contact et ici le processus est presque identique à la modification puisqu'il faut sélectionner l'utilisateur et ensuite appuyer sur le bouton "Supprimer".

### Déconnexion

Une fois que l'utilisateur veut quitter l'application, il peut se déconnecter en appuyant sur le bouton indiqué qui fait que la connexion est arrêtée et relance l'application sur l'interface de [connexion](#connexion).
