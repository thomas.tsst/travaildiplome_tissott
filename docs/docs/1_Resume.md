## Résumé (intermédiaire)
SecuWallet est une application qui permet de gérer et de stocker des portefeuilles cryptomonnaies sur la blockchain Ethereum. Il y a la possibilité de créer des portefeuilles, importer des portefeuilles déjà existants. La puissance de ce projet est la sécurité apportée lors du stockage des portefeuilles sur la machine. L'utilisateur aura également un suivi de l'évolution des différents portefeuilles importés dans l'application. Il pourra également créer des transactions sur la blockchain.

SecuWallet pourra être installé grâce à un fichier d'installation qui sera téléchargeable sur un petit site web. Cette application est codée en C# tout en utilisant la librairie Web3 pour communiquer avec la blockchain. 

## Abstract (intermediary)
SecuWallet is an application that allows you to manage and store cryptocurrency wallets on the Ethereum blockchain. There is the possibility to create wallets, import already existing wallets. The power of this project is the security provided when storing wallets on the machine. The user will also be able to track the progress of the various wallets imported into the application. He will also be able to create transactions on the blockchain.

SecuWallet can be installed thanks to an installation file that can be downloaded from a small website. This application is coded in C# while using the Web3 library to communicate with the blockchain.
