# Planning

## Prévisionnel

{{fig("./img/PlanningPrevisionnel.png", "Planning previsionel", 70)}}

## Effectif

{{fig("./img/PlanningEffectif.png", "Planning effectif", 70)}}

Lors de la réalisation du projet, plusieurs de tâches sont venues s’ajouter au planning, le développement m'a pris en peu plus de temps que prévu. Ce qui m'a retardé est la phase de recherche sur les différents fonctionnements entre mon code C# et mon API. J'ai préféré détailler les tâches dans le planning final afin que le déroulement des tâches soient plus claire.

Le planning initial n'est pas respecté, car au moment où je l'ai réalisé je n'avais pas pensé à l'entièreté de mes fonctionnalités et la structure de mon projet. D'autant plus qu'il faut souligner que le projet a à plusieurs reprise été remanié.