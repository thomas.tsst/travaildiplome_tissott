# Sécurité
 
Mon projet est une application qui va demander de manipuler des données sensibles. C'est pourquoi il était indispensable de bien penser à la sécurité de celle-ci. Pour cela, j'ai dû me renseigner sur les différentes manières de sécuriser ou d'accéder. Ainsi, j'ai dû choisir le moyen qui serait le plus judicieux pour mon projet.  
 
Pour renforcer cette sécurité, j'ai mis en place une API PHP qui avait comme but de permettre uniquement d'accéder à la base de données en local. De cette manière, je faisais en sorte de faire transiter le moins de données sensibles entre le client et le serveur.
 
## Google Authenticator
 
Dans mon projet, l'utilisation de Google Authenticator était pour moi la manière la plus indiquée de faire fonctionner la connexion ou la confirmation.
 
Ce logiciel est utilisé pour l'authentification à deux facteurs, ce qui signifie qu'en plus de son mail et de son mot de passe d'origine, l'utilisateur est dans l'obligation d'utiliser celle-ci.
 
### Comment ça fonctionne?
 
Google Authenticator est un logiciel développé par Google afin de fournir un code à 6 chiffres appelé "code éphémère". Nommé ainsi, puisque le code fourni se met à jour toutes les 30 secondes. Passé ce temps, le code est donc généré à nouveau pour en afficher un nouveau. Cela étant dit, il est important de souligner que chaque code fourni par Google Authenticator fait référence à une clé numérique propre à l'utilisateur. Dans notre cas il s'agit du QR code que l'utilisateur scanne pour lier son compte "SecuWallet" à Google Authenticator.
 
## JWT
Les Json web token ont été créé en 2015, permettent l'échange de token sécuriser entre différentes parties. Dans mon cas j'utilise cette technologie pour ajouter une nouvelle sécurité lors de l'échange de donné entre mon client et L'API PHP.

Sa structure se compose en trois parties :
 
- Un header
- un payload (data)
- La signature numérique
 
### Header
 
Le header détermine quel type d'algorithme devrait être utilisée pour générer la signature. À cela vient s'ajouter l'identification du type de Token utilisé.
 
Exemple d'un header :
 
```json
{
  "alg": "HS256",
  "typ": "JWT"
}
```
 
### Payload
 
Le payload fait référence à la partie du token qui contient les informations qu'on voudrait transmettre. Ces informations sont nommées "claims", ce qui fait que nous avons la possibilité de spécifier au token les "claims" voulus.
 
Exemple d'un payload :
 
```json
{
  "sub": "SecuWallet",
  "name": "John Doe",
  "iat": 1516239022
}
```

Il contient ce qu'on appelle le sujet qui est régulièrement le nom de l'application (sub) pour laquelle le token est créé. Un nom unique qui permet de différencier les utilisateurs au sein de mon application (name). Pour finir un [timestamp Unix](https://fr.wikipedia.org/wiki/Heure_Unix)(iat) qui représente la date et le temps à laquelle le token est considéré comme expiré.
 
### Signature
 
La signature numérique du token est un hash généré à partir de l'en-tête et des informations du jeton en utilisant l'algorithme présent dans ce dernier avec un mot de passe choisi à la génération du token. Ce hash représente toute la sécurité présente derrière cette technologie, car si une personne essaie de modifier n'importe quelle information présente dans celui-ci, le hash serait différent de celui de base. Ainsi, grâce a cette comparaison de hash on peut déterminer si le token est authentique ou non.
 

### Structure
Ces trois parties sont encodées en [bas64url](https://fr.wikipedia.org/wiki/Base64) ainsi que concaténées avec un (".").

Voici un exemple de token complet qui transite entre mon client et mon API:

```json
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c
```

Ceci étant fait, on peut mettre les parties ensemble comme suit :

token = encodeBase64(header) + '.' + encodeBase64(payload) + '.' + encodeBase64(signature)
 
 
## Encryption portefeuille
 
L'encryption est un pilier dans mon application puisqu'avec elle je garantis au mieux la sécurité des différents comptes et portefeuilles. Pour cela, j'utilise diverses manières afin de répondre à cette exigence.
 
### KeyStore
 
La KeyStore est un fichier JSON encodé, qui contient une clé privée. De cette manière, il m'était possible de stocker les clés privées localement tout en gardant un très bon niveau de sécurité qui sera utilisé par mon projet.
 
### AES Encryption
 
L'utilisation de AES Encryption, me permettait de garantir la sécurité des portefeuilles stockés sur le client. Il convient de spécifier que cette encryption fait référence à une norme de sécurité cryptographique. Je tiens à souligner que celle-ci est le seul chiffrement par blocs approuvé par "NSA". C'est pourquoi il est important de spécifier que pour pouvoir cracker une clé il faudrait des milliards d'années à un supercalculateur. Ce qui démontre à quel point il serait difficile pour quelqu'un de voler des données sans avoir le mot de passe.
 
Les algorithmes AES sont donc devenus un pilier pour le monde cryptographique grâce au fait qu'elles sont impénétrables.
 
Il convient donc de dire que dans ce projet j'ai utilisé l'algorithme AES-128-CTR qui est l'exigence minimale pour utiliser le AES Encryption.
 
Sa structure est constituée comme suit:
 
{
    "crypto" : {
        "cipher" : "aes-128-ctr",
        "cipherparams" : {
            "iv" : "83dbcc02d8ccb40e466191a123791e0e"
        },
        "ciphertext" : "d172bf743a674da9cdad04534d56926ef8358534d458fffccd4e6ad2fbde479c",
        "kdf" : "scrypt",
        "kdfparams" : {
            "dklen" : 32,
            "n" : 262144,
            "p" : 8,
            "r" : 1,
            "salt" : "ab0c7876052600dd703518d6fc3fe8984592145b591fc8fb5c6d43190334ba19"
        },
        "mac" : "2103ac29920d71da29f15d75b4a16dbe95cfd7ff8faea1056c33131d846e3097"
    },
    "id" : "3198bc9c-6672-5ab3-d995-4942343ae5b6",
    "version" : 3
}
 
- cipher : algorithme de chiffrement.
- cipherparams
  - iv: Vecteur d'initialisation 128 bits du chiffrement (vérifier qu'il soit bien aléatoire).
- ciphertext: Texte chiffré
- kdf : fonction de dérivation de clé
- kdfparams
  - dklen : taille de la clé utilisée
  - n : Paramètre de coût CPU/Mémoire, doit être supérieur à 1 et puissance de 2.
  - p : paramètre de parallélisme
  - r : paramètre de taille de bloc
  - salt : valeur sécurisée de 128 bits(16 octets) ou plus et générée de manière aléatoire.
- mac : Code utilisé pour vérifier le mot de passe (clé encodée en SHA3).
- id : Identifiant
- version : identifiant de la version en "dur"
 
### AES-128-CTR
 
Il s'agit d'un chiffrement par bloc, dans notre cas le "AES-128-CTR" utilise une longueur de clé de 128 bits afin de crypter ou de décrypter un bloc de message.

## Encryption de bases de données
Dans le cadre de mon projet de fin d'études, j'ai trouvé pertinent de me renseigner sur différente manière d'encrypter des données et ainsi trouver celle qui me conviendrait le mieux pour ce travail. Sachant que les données ne transitent plus hors du serveur, mais directement à l'aide d'un utilisateur possédant uniquement un accès local, j'ai mis en place une API PHP que j'ai créé. L'élaboration de celle-ci a permis d'agir comme un intermédiaire entre l'application, le client c# et la base de données.

## Cryptage possible
### Chiffrement en Transit
Cryptage des données entre le client et le serveur. Ce qui veut dire que ce chiffrement permet d'éviter que si les communications entre les différents intervenants sont interceptées, la personne mal veillante ne pourra rien faire de ces données collectées. J'utilise le protocole Transport Layer Security (TLS), dans l'optique de résoudre le problème des données en transit.

j'ai décidé de me renseigner sur des certificats autosignés et non pas des certificats qui proviennent de parties externes telles que Google, Comodo etc. Si la base de données venait à être déployée en production, il serait nécessaire d'utiliser en partie une autorité de certification externe.

TLS demande de manière impérative de vérifier les identités entre les différents acteurs, avant même de permettre l'échange de ces certificats et de leurs clés. 

Il existe un outil MySQL qui se nomme mysql_ssl_rsa_setup, qui s'occupe automatiquement de la génération des clés et des certificats. Malgré cela, j'ai pris la décision d'utiliser OpenSSl pour les créer personnellement.

Certificats et Clés nécessaires :

- Clé Certificate Autority(CA) : On génère une clé privée RSA stocké dans un fichier avec l'extension Privacy Enchanced Mail (.pem)
- Certificat CA : Il s'agit d'un certificat en format X.509, également stocké dans un fichier .pem. Il contient les métadonnées des certificats ainsi que les clés publiques.
- Serveur CSR et Client CSR: Message adressé à une autorité de certification afin d'obtenir un certificat d'identité numérique. Il est nécessaire que les noms communs demandés soient différents.
- Clé du serveur et Clé du client : Clé privée RSA 
- Certificat de serveur et Certificat Client : Certificat X.509 signé par la clé CA, celle-ci contient donc les métadonnées du certificat ainsi que leurs clés publiques. Il est nécessaire de spécifier que ces données sont propres à chacun.

Tous ces éléments ont été stockés après leurs créations dans le répertoire suivant:
```/etc/mysql/transit/```

{{fig("./img/GenerationCertificat.PNG", "Génération des certificats", 50)}}

Cette image représente les différentes entrées demandées afin de générer un certificat.

Il m'a été nécessaire de modifier les droits sur ce répertoire ci-dessus afin que les clés ne soient pas lisibles 

{{fig("./img/DroitDossierTransit.PNG", "Modification des droits sur le dossier transit", 40)}}

Afin d'activer la connexion au TLS, qui est le successeur de SSL, j'ai donc ajouté ces lignes dans le fichier de configuration de MariaDB, sous la [section](https://stackoverflow.com/a/15453913) \[mysqld\].

Ensuite, il nous faut créer un utilisateur relié à une base, en spécifiant dans la requête le require SSL.

{{fig("./img/CipherSSL.PNG", "Status de la connexion en utilisant TLS", 40)}}

À partir d'ici nous avons la confirmation que le transit de données entre le client et le serveur est bel et bien crypté. La connexion TLS utilise l'algorithme de chiffrement dans Cipher [DHE-RSA-AES256-GCM-SHA384](https://ciphersuite.info/cs/TLS_DHE_RSA_WITH_AES_256_GCM_SHA384/).

### Chiffrement au Repos (At-Rest) :

Il est important de noter que c'est ce cryptage que j'ai utilisé pour mon travail de diplôme.

Le cryptage At-Rest signifie que les fichiers de données ainsi que les logs sont encryptés dans le disque du serveur. Celle-ci rend impossible pour une personne mal intentionnée, qui arriverait à accéder aux données et a les volés, de pouvoir les lire ou d'en faire quoi que ce soit (en considérant que la clé est sécurisée et donc pas stockée localement).

Effectuer un cryptage At-Rest ralentit de 5 à 10% environ l'exécution des processus.

{{fig("./img/DifferenceBaseEncrypter.com.png", "Performance table claire vs cryptée", 50)}}

Voici les différents éléments qui peuvent être chiffrés sur un serveur MariaDB:

- Fichiers de données InnoDB [(Tablespace)](https://dev.mysql.com/doc/refman/8.0/en/general-tablespaces.html).
- Table Aria (uniquement supporté dans MariaDB).
- Les différents [log MySQL](https://dev.mysql.com/doc/refman/5.7/en/server-logs.html).

Le cryptage At-Rest de MariaDB requiert l'utilisation d'un plug-in de gestion de clé et de cryptage, dans notre cas on utilise [file_key_management](https://mariadb.com/kb/en/file-key-management-encryption-plugin/). Ce plug-in a malheureusement un grand défaut qui est la clé. Celle-ci peut être lue par le root donc l'utilisateur a tous les droits sur la base.

On génère des clés dans un répertoire ```/etc/mysql/rest/```. Dans ce dossier on y trouve un fichier keyFile qui s'apparente à un tableau associatif.[id->clé]. L'id doit être un int et la clé est généré grâce OpenSSL, dans le fichier ces deux valeurs sont séparées par un ";". Comme le démontre l'image ci-dessous.

{{fig("./img/CatKeyfile.PNG", "Contenu du fichier key", 40)}}

Ensuite on doit créer un mot de passe qu'on stock dans un ficher keyFile.passwd qui va nous permettre d'encrypter la clé mentionnée ci-dessus, avec un algorithme symétrique qui ce nomme [AES](https://fr.wikipedia.org/wiki/Advanced_Encryption_Standard).

Voici donc les fichiers présents dans ce répertoire 

{{fig("./img/ContenuRest.PNG", "Contenu du dossier rest", 40)}}

Pour des raisons de sécurités, il est grandement recommandé de supprimer le keyFile (sans extension).

Ensuite, il est nécessaire de changer les droits dans le but de rendre la lecture des fichiers impossible.

{{fig("./img/DroitRest.PNG", "Modification des droits sur le dossier rest", 40)}}

Il faut ajouter des lignes dans le fichier de configuration de MariaDB dans la section [MariaDB](https://fr.wikipedia.org/wiki/MariaDB).  

{{fig("./img/ConfigAtRest.PNG", "Configuration de l'encryption At-Rest", 50)}}

Ensuite j'ai créé une table encryptée et une table classique pour comparer le contenu des fichiers comme le montre l'image ci-dessous. Ces tables ont uniquement des entées qui contiennent "Test data".

{{fig("./img/TableClaire.png", "Table claire", 40)}}


{{fig("./img/TableEnc.png", "Table cryptée", 40)}}

## Encryption réalisée

Suite à mes recherches, j'ai décidé de m'orienter sur l'encryption [At-Rest](#chiffrement-au-repos-at-rest), puisque comme indiqué dans [l'introduction](#encryption-de-bases-de-données) les données ne transitent plus hors du serveur.