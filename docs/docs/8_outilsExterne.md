# Outils et librairies externes

## Raspberry

Afin d'avoir une machine permettant d'accueillir les différents serveurs, le choix le plus évident était un Raspberry que ce soit pour une question de coût, mais également la facilité d'installation sur un système Linux.

{{fig("./img/logoRaspberry.png", "Logo Raspberry", 10)}}

Voici les différents serveurs que contiendra le Raspberry :

{{fig("./img/ContenuRaspberyy.png", "Contenu du Raspberry", 50)}}

## Apache2 (HTTPS)

Afin d’avoir une sécurité maximum, je vais utiliser un serveur Apache2 pour authentifier les utilisateurs. Ainsi, il aura un accès à la base de données en local avec un utilisateur possédant les droits minimums possibles.

Pour sécuriser la communication entre le client et le serveur, il est nécessaire de forcer la connexion en HTTPS (HyperText Transfer Protocol Secure). Grâce à cela le transit de données entre le client et le serveur est crypté par le biais du protocole TLS.  

{{fig("./img/apache.png", "Logo Apache2", 15)}}

## MariaDB

Pour la gestion des différents utilisateurs, il m'est nécessaire d'avoir une base de données. Contrairement a un serveur Mysql, MariDB me permet d'encrypter la table contenant les utilisateurs bien plus facilement.

{{fig("./img/logoMariaDB.png", "Logo MariaDB", 30)}}

## Hardhat

Pour faciliter le développement, tout en restant au plus proche de la réalité, j'ai décidé d'utiliser un serveur Hardhat. Celui-ci me permet de faire tourner ma propre blockchain tout en ayant la possibilité de copier une existante. Un serveur Hardhat par défaut lance un noyau Ethereum vierge.

{{fig("./img/logoHardhat.png", "Logo Hardhat", 15)}}

## Samba

Pour faciliter le développement, j'ai installé Samba sur le Raspberry pour avoir un accès sur les fichiers de celui-ci depuis ma machine (Windows 10). Bien évidemment, ce service sera désactivé à la fin du développement pour éviter des soucis de sécurité.

{{fig("./img/logoSamba.png", "Logo Samba", 30)}}

## VNC

Afin d'éviter d'avoir à connecter le Raspberry à un écran ainsi qu'une souris et un clavier j'ai activé un accès distant VNC qui me permet de le contrôler à distance depuis ma machine. Ce service sera également désactivé quand il sera plus nécessaire.

{{fig("./img/logoVNC.png", "Logo VNC", 20)}}

## WSL 2

Windows Subsystem for Linux (WSL) permet l'exécution d'exécutable Linux sur Windows 10. Cette version a apporté des changements majeurs, notamment la présence d'un noyau Linux. WSL 2 m'a notamment permis de générer cette documentation à l'aide de [mkdocs](https://discordapp.com/channels/@me/543891120275325007/984515500694462476).

{{fig("./img/WSL2.png", "Logo WSL 2", 25)}}

## GitLab

GitLab est un logiciel libre basé sur git qui me permet d'assurer un backup de mon projet, mais également de pouvoir revenir a une version stable si une modification apporter a mon projet le rend instable. 

{{fig("./img/logoGitLab.svg", "Logo GitLab", 25)}}

## Visual Studio 2022

Pour le développement de l'application C# j'ai utilisé l'IDE Visual Studio 2022.

{{fig("./img/logoVisualStudio2022.png", "Logo Visual Studio 2022", 20)}}


## Visual Studio Code

Pour le développement Web (HTML / PHP), j'ai utilisé l'IDE Visual Studio Code.

{{fig("./img/logoVisualStudioCode.png", "Logo Visual Studio Code", 15)}}

## MySQL Workbench 8.0

MySQL Workbench est un logiciel qui offre la possibilité de gérer et d'administrer une base de données.

{{fig("./img/workbench.png", "Logo MySQL Workbench", 15)}}

## Wireshark

Wireshark est un logiciel qui permet d'analyser les paquets qui circulent sur une interface réseau.

{{fig("./img/wireshark.png", "Logo Wireshark", 30)}}

## Markdown

Markdown permet de rédiger des fichiers en utilisant un moyen codé (balisage) pour la mise en forme créés en le compilant dans MkDocs.

{{fig("./img/markdown.png", "Logo Markdown", 20)}}

## Nethereum

Nethereum est une librairie .Net pour Ethereum, simplifiant la gestion des contrats intelligents et l'interaction avec les nœuds Ethereum, qu'ils soient publics ou privés.

{{fig("./img/logoNethereum.png", "Logo Nethereum", 20)}}

## MetroSet_UI
J'ai utilisé cette librairie .Net pour appliquer un thème aux différentes vues de mon application C#.

## dochne/google-authenticator
Cette libraire me permet d'implémenter le service Google Authenticator au sein de mon API PHP.

## emarref/jwt
Cette librairie me permet de créer et vérifier des jetons Json Web Token.

## Api CryptoCompare

Cette API me permet d'extraire un historique de la valeur de l'ethereum contre une devise monétaire. À la base le site [CryptoCompare](https://www.cryptocompare.com/) permet la visualisation de l'évolution du cours de plusieurs milliers de cryptomonnaies différentes.

{{fig("./img/logoCryptoCompare.png", "Logo CryptoCompare", 15)}}