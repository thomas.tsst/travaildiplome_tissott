# Cahier des charges

## Sujet
Création d'un gestionnaire de portefeuilles crypto.

## But
créer une application C# poposant la création de différents portefeuilles crypto, ainsi que de rendre possible l'échange entre les différentes cryptomonnaies. Rendre plus simple le suivi pour les personnes détenant plusieurs portefeuilles. Visualiser simplement l'évolution des portefeuilles.

## Spécifications
Au démarrage de l'application il est demandé de se connnecter ou créer un compte (Double authentification Google). Quand l'utilisateur sera connecté il lui sera possible de créer ou d'importer un portefeuille (HD Wallet) de cryptomonnaie ERC-20 (Blockchain Ethereum). L'utilisateur aura également la possibilité de créer une transaction à l'adresse désiré. Il y aura une Form qui permet de visualiser la valeur total ou celle des différents portefeuilles. Une interface permettant de suivre les profits ou les pertes sur un lapse de temps que l'utilisateur peut modifier. 

Pour stocker les données j'aurais également a créer un base de données encrypté At-Rest qui signifie qu'aucune données ne sera écrite en clair sur le disque. Pour pousser plus loin la sécurité il sera nécessaire de forcer une connexion TLS/SLL ce qui me permetra d'encrypter les données qui voyage entre le client et le serveur MySQL.

Pour communiquer avec une blockchain je vais utiliser la librairie Web3.js plus précisement le package web3-eth. Toute les méthodes dont je vais avoir besoin sont disponible dans cette librairie.

Pour faciliter le développement je vais utiliser un serveur Hardhat qui me permet de faire tourner une blockchain localement. Cette blockchain en question sera une copie de la blockchain Ethereum pour me rapprocher le maximum possible de la réaliter.

### Interfaces
Login :

{{fig("./img/login.png", "Mockup connexion", 40)}}

Page d'accueil :

{{fig("./img/walletConnecte.png", "Mockup acceuil", 40)}}

Paramètres du comtpe :

{{fig("./img/parametres.png", "Mockup paramètres", 40)}}

Créer un compte :

{{fig("./img/creerCompte.png", "Mockup créer compte", 40)}}

Importer un compte :

{{fig("./img/importerCompte.png", "Mockup importer compte", 40)}}

## Restriction
Aucune
## Environnement
### Languague de programmation
- C#
- Mysql
###  Système d'exploitation
- Windows 10
- WSL2 (Serveur HTTP Apache 2)
- Harhat
### Versionning
- GitHub
## Livrable
- Fichier zip contenant le projet
- Documentation et poster au format PDF
- Journal de bord

