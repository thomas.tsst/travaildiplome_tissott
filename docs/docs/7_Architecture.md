# Architecture
## Arborescence de fichier
L'arborescence est une partie importante pour tout type de projet, puisqu'il fait référence à l'ensemble des différentes compositions de projets et de leurs liens. Il est important de spécifier ici que dans cette structure on voit trois racines. Il s'agit de la partie "docs", "TravailDiplome_TISSOTT" et de "website", qui sont des racines.

La répertoire "docs" contient la documentation.

La partie "TravailDiplome_TISSOTT" qui est la racine de mon application est divisée en deux sections. La section des classes ainsi que celle des vues. Il était important de séparer ces deux parties pour mettre en avant leur différence fonctionnelle. Mes classes ne sont pas au même niveau hiérarchique que mes vues puisqu'il s'agit de la partie qui va "être" utilisé dans mes vues (views).

La partie "website" qui est la racine de mon site web est divisée en plusieurs parties. Il était important de séparer les différentes structures pour optimiser au mieux l'accessibilité aux contenus. 
```
+-- docs
|	+-- main.py
|	+-- planningPervisionnelV2.xlsx
|   +-- docs
|       +-- img
|       +-- Fichiers MarkDown...
+-- TravailDiplome_TISSOTT
|    +-- packages
|        +-- QRCoder.1.4.3
|        +-- Nethereum.4.4.1
|        +-- MetroSet_UI.2.0.0
|        +-- Newtonsoft.Json.13.0.1
|   +-- TravailDiplome_TISSOTT
|       +-- AppController.cs
|       +-- BlockchainController.cs
|       +-- Contact.cs
|       +-- views
|           +-- frmAddModifyContact.cs
|           +-- frmAddModifyContact.Designer.cs
|           +-- frmAdresse.cs
|           +-- frmAdresse.Designer.cs
|           +-- frmMain.cs
|           +-- frmAuthenticator.cs
|           +-- frmAuthenticator.Designer.cs
|           +-- frmAuthenticatorPopUp.cs
|           +-- frmAuthenticatorPopUp.Designer.cs
|           +-- frmContact.cs
|           +-- frmContact.Designer.cs
|           +-- frmEnvoi.cs
|           +-- frmEnvoi.Designer.cs
|           +-- frmGestionPortefeuilles.cs
|           +-- frmGestionPortefeuilles.Designer.cs
|           +-- frmImportWalletFromFile.cs
|           +-- frmImportWalletFromFile.Designer.cs
|           +-- frmImportWalletFromKey.cs
|           +-- frmImportWalletFromKey.Designer.cs
|           +-- frmLogin.cs
|           +-- frmLogin.Designer.cs
|           +-- frmMain.cs
|           +-- frmMain.Designer.cs
|           +-- frmMonCompte.cs
|           +-- frmMonCompte.Designer.cs
|           +-- frmParametres.cs
|           +-- frmParametres.Designer.cs
|       +-- JwtController.cs
|       +-- Program.cs
|       +-- Settings.cs
|       +-- User.cs
|       +-- Wallet.cs
|   +-- TravailDiplome_TISSOTT.sln
+-- website
|   +-- site
|       +-- index.php
|       +-- Functions.php
|       +-- FunctionConnexionDB.php
|       +-- API
|           +-- authenticator.php
|           +-- login.php
|           +-- tokenAuthenticator.php
|           +-- tokenCreator.php
|    +-- docs
|       +-- connexion.md
|       +-- createAccount.md
|       +-- download.md
|       +-- inedx.md
|       +-- stylesheets
|          +-- extra.css

```

Ce qui suit fait référence à l'arborescence ci-dessus. J'explique en détail ce que représente chaque structure de mon arborescence afin que cela soit clair et détaillé. Puisque, comme on le voit sur l'arborescence, il y a trois racines structurées comme suit.

## Docs

Dans ce dossier on y trouve des fichiers qui sont utilisés afin d'uniformiser les PDF rendus par tous les élèves. Dans le dossier "docs" de celle-ci, on y trouve un dossier "img" qui est le dossier qui comporte la totalité des images que j'utilise dans mon rapport de travail de diplôme ainsi que des fichiers générés automatiquement par MarkDown.

## TravailDiplome_TISSOTT

### Packages

#### QRCoder.1.4.3

Librairie que j'utilise pour générer des QRcode en C#.

#### Nethereum.4.4.1

Wrapper c# de la libraire Web3 JS permet la communication avec la blockchaine, celle-ci a également un module permettant d'encrypter les portefeuilles en format "JSON".

#### MetroSet_UI.2.0.0

Librairie que j'utilise pour appliquer un thème aux différentes vues de mon application, ce qui rend le visuel plus actuel.

#### Newtonsoft.Json.13.0.1

Ce package permet la Sérialisation et désérialisation des objets présents de mon application. 

### TravailDiplome_TISSOTT

Répertoire qui comporte la totalité du code pour mon application client C#.

{{fig("./img/DiagrammeClasse.png", "Diagramme de classes", 75)}}

#### AppController.cs

Cette classe représente le modèle de données nécessaires au bon fonctionnement du programme, c'est cet objet qui est transmis entre les différentes vues lors de leurs instanciations. 
### Champs
* User : Objet de type User.
* Bc : Objet de type BlockchainController.
* Settings : Objet de type Settings.
### Méthodes

### Constructeur

Un "AppController" est caractérisé par trois valeurs. Il s'agit d'un "User" user, d'un "BlockchainController" bc et d'un "Settings" settings.

Un constructeur est mis en place avec un paramètre "string" userEmail. Dans celui-ci on indique que le "User" devient un nouvel utilisateur (new User) avec comme paramètre le userEmail. Enfin j'appelle la méthode [CheckIfSettingsAreSaved](#checkifsettingaresaved).

#### CheckIfSettingAreSaved

Cette méthode permet de vérifier si des paramètres ont déjà été enregistrés. Dans le cas où ça le serait, il instancie la classe "Settings" à l'aide de l'objet sérialisé présent dans le répertoire propre à chaque utilisateur.

#### SaveSettings

Cette méthode permet de sauvegarder les paramètres en sérialisant la classe "Settings" dans le répertoire de l'utilisateur connecté, lors de la sélection.

#### ShowAuthenticatorDialogBox

Cette méthode prend en paramètre une "From" frmOwner.

Cette méthode permet d'afficher la "pop-up" Authenticator et d'attendre un résultat de celle-ci.

Enfin je retourne le résultat de celle-ci.

#### BlockchainController.cs

### Constantes
* BLOCKCHAIN_URL : URL pointant sur la blockchain.
### Champs
* Wallets : Liste d'objets de type Wallet.
### Méthodes 

### Constructeur

Ce constructeur permet d'instancier l'objet Web3 qui communique avec la blockchain.

#### CreateTransaction

Cette méthode prend en paramètre un "string" privateKey, un "string" publiKey et une "decimal" amount.

Son but est de permettre la création d'une transaction par l'utilisateur à une tierce personne. C'est pourquoi il faut renseigner l'adresse publique ainsi que le montant. L'adresse privée permet de signer la transaction sur la blockchain.

Réponse de la blockchain  :
```json
{
    "transactionHash":"0x7cac6796d856fe83114ef61ad2372b8d13d91d476145bb57d46cdadd7e1f0655",
    "transactionIndex":"0x0","blockHash":"0x2e52ff60b3cb911a761bea8b7ea8fe9d334494dbceed997bbbeab1135f286392",
    "blockNumber":"0x2","from":"0xf39fd6e51aad88f6f4ce6ab8827279cfffb92266","to":"0x90f79bf6eb2c4f870365e785982e1f101e93b906",
    "cumulativeGasUsed":"0x5208",
    "gasUsed":"0x5208",
    "effectiveGasPrice":"0x77359400",
    "contractAddress":null,
    "status":"0x1",
    "logs":[],
    "type":"0x0",
    "logsBloom":"0x000000000....",
    "root":null}
```
#### DecryptJSONFile

Cette méthode prend en paramètre un "string" password et un "string" jsonPath.

Son but est de pouvoir prendre un portefeuille crypté sous forme de fichier "JSON" à un emplacement spécifique (jsonPath) et de pouvoir le décrypter à l'aide du code (password) afin d'y retirer les informations nécessaires. Il s'agit de préciser ici que le code doit être le même que celui utilisé pour son portefeuille à la création/importation.

#### DeleteWallet

Cette méthode prend en paramètre un "int" walletIndex et un "string" emailUser.

Son utilité est de pouvoir supprimer un portefeuille de sa liste "Wallets" de cette classe. 

Pour ce faire, on cherche le "Wallet" suivant son index, ensuite on y extrait son champ "path" ainsi que son "name".

Enfin, on parcourt le dossier pour trouver tout fichier qui comporte le nom demandé. Sans oublier de le supprimer de notre Liste de portefeuille à l'index spécifié en paramètre.

#### GenerateJSONFile

Cette méthode prend en paramètre un "string" password, un "string" name et un "string" accountName.

Le but est de générer un fichier "JSON" dans un dossier spécifié. Il est également important de regarder si le fichier existe déjà, dans ce cas on fait apparaître un message pour informer l'utilisateur. Dans le cas contraire, on crée le fichier "JSON" comportant les informations cryptées et un fichier "txt" qui contient la clé publique.

#### GenerateJSONFile 

Cette méthode prend en paramètre en plus de celle ci-dessus un "Account" account. Ce type est nécessaire lors de l'importation d'un fichier "JSON", puisqu’on est obligé de générer un "Account" pour récupérer la clé publique de ce portefeuille.

#### GetAccountFromPrivateKey

Cette méthode prend en paramètre un "string" privateKey.

Son utilité est de pouvoir créer et retourner un compte (Account) en utilisant sa clé privée.

#### GetAllBlocks

Retourne tous les blocs, avec leurs transactions, présents dans la blockchain.

Requête sur la blockchain :
```json
{
"id":1,
"jsonrpc":"2.0",
"method":"eth_getBlockByNumber",
"params":["0x1",true]
}
```
Réponse de la blockchain :
```json
{
    "jsonrpc":"2.0",
    "id":1,
    "result":
    {
        "number":"0x0",
        "hash":"0x8dc17285c0e2786a1bb77fe5bc5d20d35ecae62821574791504ca9fd51033209",
        "parentHash":"0x0000000000000000000000000000000000000000000000000000000000000000",
        "nonce":"0x0000000000000042",
        "mixHash":"0x0000000000000000000000000000000000000000000000000000000000000000",
        "sha3Uncles":"0x1dcc4de8dec75d7aab85b567b6ccd41ad312451b948a7413f0a142fd40d49347",
        "logsBloom":"0x000000000......",
        "transactionsRoot":"0x56e81f171bcc55a6ff8345e692c0f86e5b48e01b996cadc001622fb5e363b421",
        "stateRoot":"0x950cc3b1c26a9cd52a81793977d91eb11603c310375216ccacc4ffe18af72959",
        "receiptsRoot":"0x56e81f171bcc55a6ff8345e692c0f86e5b48e01b996cadc001622fb5e363b421",
        "miner":"0x0000000000000000000000000000000000000000",
        "difficulty":"0x1",
        "totalDifficulty":"0x1",
        "extraData":"0x1234",
        "size":"0x204",
        "gasLimit":"0x1c9c380",
        "gasUsed":"0x0",
        "timestamp":"0x62a19bd2",
        "transactions":[],
        "uncles":[],
        "baseFeePerGas":"0x3b9aca00"
    }
}
```
#### GetAllTransacFromPublicAddress

Cette méthode prend en paramètre un "string" publicAdress.

Son utilité est de pouvoir obtenir toutes les transactions effectuées avec un certain portefeuille. Pour y arriver, on parcourt la liste des blocs fournie par la méthode [GetAllBlocks](#getallblocks) pour y extraire toutes les transactions où l'adresse publique est présente. 
 
Enfin, on retourne le dictionnaire de transaction "myTransac".

#### GetBalanceAtAllBlocks

Cette méthode prend en paramètre un "string" adresse.

Pour chaque bloc présent dans la blockchain, on effectue une requête "GetBalance" ce qui nous permet d'extraire l'évolution de la quantité d'Etherum présente dans un portefeuille.

Elle retourne un dictionnaire avec comme clé le "TimeStamp" de chaque bloc et comme valeur associée la quantité d'Etherum disponible.

#### GetLastBlock
Cette méthode asynchrone prend en paramètre un "string" publicAddress.

Elle retourne le dernier id du bloc stocké dans la blockchain.

Réponse de la blockchain  :
```json
{
    "id":1,
    "jsonrpc":"2.0",
    "method":"eth_blockNumber",
    "params":[]
}
```

Réponse de la blockchain  :
```json
{
    "jsonrpc":"2.0",
    "id":1,
    "result":"0x2"
}
```
#### GetBalanceInETH

Cette méthode prend en paramètre un "string" adresse.

Son utilité est qu'elle prend la balance d'une clé publique en "wei" et la convertie en "Eth".

Requête sur la blockchain :
```json
{
    "id":1,
    "jsonrpc":"2.0",
    "method":"eth_getBalance",
    "params":["0xf39Fd6e51aad88F6F4ce6aB8827279cffFb92266","latest"]
}
```

Réponse de la blockchain :
```json
{
    "jsonrpc":"2.0",
    "id":1,
    "result":"0x21d0452374bd846c000"
}
```

#### GetPublicAddressWithWalletIndex

Cette méthode prend en paramètre un "int" index.

Son utilité est de pouvoir retourner la clé publique à un index spécifié (publicAddress) dans la Liste "Wallets".

#### GetUserWallets

Cette méthode prend en paramètre un "string" path.

Son utilité est de pouvoir instancier les différents portefeuilles appartenant à l'utilisateur connecté dans la Liste "Wallets".

Enfin, je retourne cette variable "result" qui contient une Liste de wallet.

#### ImportAccountWithFile

Cette méthode prend en paramètre un "string" password, un "string" path, un "string" name et un "string" accountName.

Son but est de pouvoir instancier une variable de type "Account" nécessaire à la méthode [GenerateJsonfile](#generatejsonfile). Mais avant de pouvoir faire cela, il est nécessaire d'utiliser la méthode [DecryptJSONFile](#decryptjsonfile).


#### ImportAccountWithPrivateKey

Cette méthode prend en paramètre un "string" privateKey, un "string" password, un "string" email et un "string" walletName.

Son but est de pouvoir instancier une variable de type "Account" nécessaire à la méthode [GenerateJsonfile](#generatejsonfile).

#### isAddressValid

Cette méthode prend en paramètre un "string" privateKey.

Son but est de vérifié si l'adresse publique on non.

Elle retourne "True" si valide sinon "False".

#### KeystoreDirectoryCheck

Cette méthode prend en paramètre un "string" email.

Son but est de vérifier si le répertoire devant contenir les différents portefeuilles de l'utilisateur a été préalablement créé.

#### Settings.cs

Cette classe permet de lister les différents contacts créés par l'utilisateur. Permets également d'enregistrer la devise utilisée par l'application.

### Champs
* Contacts : Liste d'objets de type Contact.
* currency : La devise utilisée par l'utilisateur.
### Méthodes

### Constructeur

Dans ce constructeur j'instancie la devise (currency) à "USD" par défaut. De plus, je créer une nouvelle liste d'objet "Contact" vide.

#### GetContactPublicKeyWithIndex

Cette méthode prend en paramètre un "int" index.

Elle retourne un "string" avec la publicKey d'un contact dans la Liste "Contacts" à l'index spécifié.

#### Contact.cs

Cette classe représente la structure de donnée d'un contact nécessaire à mon application.
### Champs
* name : Nom du contact.
* PublicKey : Clé publique du contact.

### Constructeur

Ce constructeur prend en paramètre un "string" name et un "string" publicKey, pour assigné directement les valeurs aux champs.

#### JwtController.cs

Classe statique permettant l'échange d'informations entre l'API PHP et le client.

#### User.cs

Cette classe représente la structure de donnée d'un utilisateur nécessaire à mon application.

### Constantes
### Champs
* Email : Email avec lequel l'utilisateur s'est connecté.
* Token : Token JWT que reçoit l'utilisateur une fois connecté.

### Constructeur

Ce constructeur prend en paramètre un "string" email, afin d'assigne directement la valeur au champ Email a l'instanciation de cette classe.

#### Wallet.cs

Cette classe est utilisée pour stocker les portefeuilles dans l'application. Celle-ci, est constituée d'un nom, du chemin d'accès qui pointe sur le fichier "JSON" dans lequel les données sont encryptées et stockées, ainsi que la clé publique ce qui permet d'éviter de devoir décrypter le portefeuille pour un simple accès à la clé publique.

### Constructeur

Ce constructeur a comme paramètres un string nom ("Name"), un string "publicKey" et d'un string "path".

Celui-ci permet d'instancier un Wallet et d'assigner les valeurs.


## website

Il s’agit du dossier qui contient les fichiers qui composent mon site web, pour permettre de se créer un compte ainsi que se connecter afin de pouvoir obtenir un code Authenticator ainsi que de permettre le téléchargement de mon application.

### Docs
Ce dossier contient les fichiers nécessaires pour générer le site à l'aide de "MkDocs".

### site
Il s'agit du dossier du site généré automatiquement par MarkDown

#### Functions.php

##### CreateSecretKey

Cette fonction prend en paramètre le compte ($account) qui fait référence à l'email de l'utilisateur.

Son but est de pouvoir créer une clé unique qui sera nécessaire à l'API de "Google Authenticator".

Elle retourne une clé ($secret) qui est générée par la librairie "\Dolondro\GoogleAuthenticator\".

##### CheckAuthenticator

Cette fonction prend en paramètre deux valeurs, une clé secrète ($secretKey) et un mot de passe ($code).

Son but est de vérifier si la clé secrète ($secretKey) est identique au mot de passe($code) en temps réel.

Elle retourne "True" si le code introduit par l'utilisateur est valide ou "False" dans le cas contraire.

#### FunctionConnexionDB.php

##### GetConnexion

Cette fonction sert à pouvoir se connecter à la base de données. Pour ce faire, elle génère un objet PDO qui est connecté à la base de données.

Elle retourne un objet PDO qui est connecté à la base de données.

##### CreateUser

Cette fonction prend en paramètre trois valeurs, un email($email), un mot de passe ($password) et une clé secrète ($secretKey).

Son but est de permettre d'insérer un utilisateur dans la base de données.

##### GetUserSecretKey

Cette fonction prend en paramètre l'email ($email) de l'utilisateur.

Son but est de pouvoir prendre sa clé privée dans la base de données qui fait référence à l'email de l'utilisateur qui est unique.

Elle retourne un tableau à une dimension qui fait référence à la clé secrète associée à l'utilisateur.

##### CheckUserLogin

Cette fonction prend en paramètre deux valeurs, le mail de l'utilisateur ($email) et son mot de passe ($password).

Son but est de pouvoir contrôler la connexion de l'utilisateur. Pour ce faire, on sélectionne l'utilisateur qui fait référence au mail et le mot de passe en question.

Elle retourne un tableau à une dimension qui détient la valeur en "string" de la clé secrète de l'utilisateur associé.

##### GetUserId

Cette fonction prend en paramètre le mail de l'utilisateur ($email).

Son but est de pouvoir sélectionner l'identifiant (idUser) de l'utilisateur par rapport à son mail ($email).

Elle retourne donc un tableau à une dimension avec comme valeur en "string" l'identifiant de l'utilisateur en question.

#### API

##### authenticator.php

Il s’agit d’un fichier PHP qui permet la vérification du code Google Authenticator fournie par l’utilisateur.

##### Login.php

Il s’agit d’un fichier PHP qui permet de savoir si l’utilisateur en question a bien mit les données qui lui correspond. Il s’agit donc de son nom d’utilisateur et de son mot de passe.

##### TokenAuthenticator.php

Il s’agit d’un fichier PHP qui permet de vérifier le token.

##### TokenCreator.php

Il s’agit d’un fichier PHP qui permet de créer le token.

#### Assets

Génération automatique de ces dossiers par MarkDown.

#### Connexion

Ce dossier contient un fichier PHP qui permet de se connecter au site grâce aux données enregistrées par l’utilisateur.

#### CreateAcount

Ce dossier contient un fichier PHP qui permet de créer un utilisateur.

#### Download

Ce dossier contient un fichier PHP qui permet à l’utilisateur connecter de pouvoir télécharger mon application en format ".msi".

#### stylesheets

Ce dossier contient le fichier CSS généré par MarkDown.

#### Index.php

Fichier PHP qui est la page d’accueil de mon site. Une explication de mon projet est mise en place pour informer l’utilisateur de quoi il s’agit. Dans le cas où la personne serait intéressée, j’avertis que pour cela il doit créer un compte, pour ensuite s'y connecter afin de pouvoir télécharger le fichier d'installation.