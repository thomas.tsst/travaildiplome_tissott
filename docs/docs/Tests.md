<style>table{
    border:1px solid #000000;
    
}
td{
    border:1px solid #000000;
}
thead{
    border:1px solid #000000;
}
</style>
# Tests
Pour cette partie du travail, j'ai mis en place plusieurs tests afin de les mettre en pratique et ainsi voir les résultats et apporter des modifications si nécessaires. Ils sont représentés par 3 tableaux un pour l'application C#, un pour le site web et le dernier pour l'API PHP.

## Client

| **N°** | **Interface** | **Description** | **Résultat** |
|---|---|---|:---:|
|1|frmLogin.cs|Lorsque l'utilisateur lance l'application, on voit apparaître une interface de connexion | OK|
|2|frmLogin.cs|Lorsque l'utilisateur entre son mail, mais qu'il n’est pas conforme ou n'est pas présent dans la base de données, alors un message s'affiche. | OK|
|3|frmLogin.cs|Lorsque l'utilisateur met son mot de passe, mais qu'il ne correspond pas à un minimum de 6 chiffres alors un message s'affiche. | OK|
|4|frmAuthenticatorPopUP.cs|Lorsque l'utilisateur rentre toutes ses données de connexion et qu'elles sont validées alors une interface "authenticator" s'affiche. |OK|
|5|frmAuthenticatorPopUp.cs|Lorsque l'utilisateur met le code de Google Authenticator, mais qu'il est expiré alors un message s'affiche. |OK|
|6|frmMain.cs|Lorsque l'utilisateur est conforme à la partie Authenticator alors une interface "acceuil" s'affiche avec ses wallet mis en évidence. |OK|
|7|frmMain.cs|Lorsque l'utilisateur choisit un de ses portefeuilles, le chargement des données de celle-ci s'affiche (Ex. quantité ETH, conversion avec la devise de base ou celle sélectionnée, historique des transactions). |OK|
|8|frmMain.cs|Lorsque l'utilisateur veut voir son graphique valeur en appuyant sur l'onglet en question, alors on voit apparaître un graphique qui correspond à l'évolution de la valeur de sa cryptomonnaie en heure. |KO|
|9|frmMain.cs|Lorsque l'utilisateur veut voir son graphique quantité en appuyant sur l'onglet en question, alors on voit apparaître un graphique qui correspond à l'évolution de la quantité de cryptomonnaie en heure. |OK|
|10|frmMain.cs|Lorsque l'utilisateur veut envoyer de l'Ethereum, il appuie sur le bouton "Envoyer", alors une interface s'affiche. |OK|
|11|frmEnvoi.cs|Lorsque l'utilisateur veut envoyer de l'Ethereum, il remplit le formulaire en question, mais la clé publique est erronée, alors un message s'affiche. | OK |
|12|frmEnvoi.cs|Lorsque l'utilisateur veut envoyer de l'Ethereum , il remplit le formulaire en question, mais il oublie de remplir une ou plusieurs informations, alors un message d'erreur s'affiche. | OK |
|13|frmEnvoi.cs|Lorsque l'utilisateur veut envoyer il remplit le formulaire en question, mais son mot de passe est erroné alors un message d'erreur s'affiche. | OK |
|14|frmEnvoi.cs|Lorsque l'utilisateur veut envoyer il remplie le formulaire en question il sélectionne un portefeuille dans la section "Compte personnel", alors la clé publique est copiée dans le formulaire. |OK|

| **N°** | **Interface** | **Description** | **Résultat** |
|---|---|---|:---:|
|15|frmEnvoi.cs|Lorsque l'utilisateur veut envoyer il remplie le formulaire en question il sélectionne un contact dans la section "Contacts", alors la clé publique est copiée dans le formulaire. |OK|
|16|frmEnvoi.cs|Lorsque l'utilisateur veut envoyer il remplie le formulaire en question et tout se passe bien alors une "pop-up" Authenticator s'affiche. |OK
|17|frmAuthenticatorPopUp.cs|Lorsque l'utilisateur veut envoyer il remplie le formulaire en question et tout se passe bien alors une "pop-up" Authenticator s'affiche. Mais il se trompe alors un message d'erreur s'affiche | OK |
|18|frmEnvoi.cs|Lorsque l'utilisateur veut envoyer il remplie le formulaire en question et tout se passe bien alors une "pop-up" Authenticator s'affiche. Mais le code est expiré alors un message d'erreur s'affiche | OK |
|19|frmAuthenticatorPopUp.cs|Lorsque l'utilisateur veut envoyer il remplie le formulaire en question et tout se passe bien alors une "pop-up" Authenticator s'affiche. Tout se passe bien alors un message de confirmation est affiché. |OK|
|20|frmAddress.cs|Lorsque l'utilisateur veut recevoir alors il appuie sur le bouton "Recevoir" de la page d'accueil. Une interface s'affiche. | OK|
|21|frmEnvoi.cs|Lorsque l'utilisateur veut recevoir alors un QR code s'affiche et fonctionne. |OK|
|22|frmEnvoi.cs|Lorsque l'utilisateur veut recevoir alors sa clé publique s'affiche et peut être copiée. |OK|
|23|frmMonCompte.cs|Lorsque l'utilisateur veut aller dans la partie "mon compte", alors il appuie sur le "rouage" et une interface s'affiche. |OK|
|24|frmGestionPorteFeuilles.cs|Lorsque l'utilisateur veut gérer ses portefeuilles alors il appuie sur le bouton "Gérer mes portefeuilles", alors une interface s'affiche avec les portefeuilles qu'il a déjà en sa possession. |OK|
|25|frmGestionPorteFeuilles.cs|Lorsque l'utilisateur veut ajouter un portefeuille, il appuie sur l'onglet "Nouveau", alors il s'affiche un petit formulaire où il doit rentrer le nom et le mot de passe. |OK|
|26|frmGestionPorteFeuilles.cs|Lorsque l'utilisateur veut confirmer l'ajout, mais que le nom du portefeuille existe déjà, alors un message s'affiche pour avertir comme quoi il existe déjà. |OK|
|27|frmGestionPorteFeuilles.cs|Lorsque l'utilisateur veut supprimer un portefeuille il appuie directement sur supprimer, alors un message s'affiche pour informer qu'il faut sélectionner un portefeuille présent dans la liste. | OK |
|28|frmGestionPorteFeuilles.cs|Lorsque l'utilisateur veut supprimer un portefeuille il en sélectionne un et appuie sur supprimer, alors un code Authenticator est demandé. | OK |

| **N°** | **Interface** | **Description** | **Résultat** |
|---|---|---|:---:|
|29|frmGestionPorteFeuilles.cs|Lorsque l'utilisateur veut importer un portefeuille et qu'il choisit de l'importer sous forme de clé privée, alors il appuie sur le bouton "Clé privée", alors cela ouvre une interface avec un formulaire. | OK|
|30|frmGestionPorteFeuilles.cs|Lorsque l'utilisateur veut importer un portefeuille et qu'il choisit de l'importer sous forme de clé privée, il remplit le formulaire. Mais la clé privée est erronée alors un message s'affiche| OK|
|31|frmGestionPorteFeuilles.cs|Lorsque l'utilisateur veut importer un portefeuille et qu'il choisit de l'importer sous forme de clé privée, il remplit le formulaire, mais que le nom du portefeuille est existant, alors on affiche un message d'erreur.  | OK|
|32|frmGestionPorteFeuilles.cs|Lorsque l'utilisateur veut importer un portefeuille et qu'il choisit de l'importer sous forme de clé privée, il remplit le formulaire, mais le mot de passe ne correspond pas à au moins 6 caractères, alors un message d'erreur s'affiche. |OK|
|33|frmGestionPorteFeuilles.cs|Lorsque l'utilisateur veut importer un portefeuille et qu'il choisit de l'importer sous forme de clé privée, il remplit le formulaire et appuie sur le bouton "Importer" alors un message s'affiche pour donner l'indication que le portefeuille est bel et bien créé. |OK|
|34|frmGestionPorteFeuilles.cs|Lorsque l'utilisateur veut importer un portefeuille et qu'il choisit de l'importer sous forme de fichier "JSON", alors il appuie sur le bouton "Fichier JSON", alors cela ouvre une interface avec un formulaire. |OK|
|35|frmGestionPorteFeuilles.cs|Lorsque l'utilisateur veut importer un portefeuille et qu'il choisit de l'importer sous forme de fichier "JSON", mais que le fichier invalide, alors on affiche une erreur| OK |
|36|frmGestionPorteFeuilles.cs|Lorsque l'utilisateur veut importer un portefeuille et qu'il choisit de l'importer sous forme de fichier "JSON", et que le mot de passe n'est pas le bon puisqu'il faut cela soit le même que son fichier "JSON", alors un message d'erreur s'affiche| OK |
|37|frmGestionPorteFeuilles.cs|Lorsque l'utilisateur veut importer un portefeuille et qu'il choisit de l'importer sous forme de fichier "JSON", mais que le nom est le même qu'un de ses portefeuilles, alors on affiche un message d'erreur. | OK |
|38|frmGestionPorteFeuilles.cs|Lorsque l'utilisateur veut importer un portefeuille et qu'il choisit de l'importer sous forme de fichier "JSON", en appuyant sur le bouton "Importer", alors un message de confirmation est affiché. | OK |
|39|frmMonCompte.cs|Lorsque l'utilisateur veut voir ses paramètres, il appuie sur le bouton "Paramètre" de l'interface "Mon compte", alors une interface s'affiche. | OK|
|40|frmParametres.cs|Lorsque l'utilisateur veut voir ses paramètres pour changer sa devise, il sélectionne la devise voulue et elle immédiatement enregistrée. |OK|
|41|frmContact.cs|Lorsque l'utilisateur veut gérer son journal de contact, alors il appuie sur le bouton "Contacts" et une interface s'affiche. |OK|
|42|frmContact.cs|Lorsque l'utilisateur veut gérer son journal de contact pour supprimer un contact, alors il appuie directement sur "Supprimer" sans sélectionner un contact un message d'erreur s'affiche. | OK |
|43|frmContact.cs|Lorsque l'utilisateur veut gérer son journal de contact pour supprimer un contact, alors il sélectionne un contact dans la liste et appuie sur le bouton "Supprimer". Le contact disparait. |OK|
|44|frmContact.cs|Lorsque l'utilisateur veut gérer son journal de contact pour modifier un contact, alors il appuie directement sur "Modifer" un message d'erreur s'affiche. | OK |
|45|frmContact.cs|Lorsque l'utilisateur veut gérer son journal de contact pour modifier un contact, il sélectionne un contact dans la liste et il appuie sur "Modifer" une interface s'affiche. |OK|
|46|frmContact.cs|Lorsque l'utilisateur veut gérer son journal de contact pour modifier un contact, il sélectionne un contact dans la liste et il appuie sur "Modifer" une interface s'affiche. |OK|
|47|frmContact.cs|Lorsque l'utilisateur veut gérer son journal de contact pour modifier un contact, il sélectionne un contact dans la liste et il appuie sur "Modifer" une interface s'affiche avec les informations du contact sélectionné. |OK|
|48|frmContact.cs|Lorsque l'utilisateur veut gérer son journal de contact pour modifier un contact, il sélectionne un contact dans la liste et il appuie sur "Modifer" une interface s'affiche avec les informations du contact sélectionné. Il les modifie et appuie sur le bouton "Valider", alors on est redirigé vers l'interface du journal de contacts. | OK |
|49|frmContact.cs|Lorsque l'utilisateur veut gérer son journal de contact pour ajouter un contact, alors il appuie sur le bouton "Nouveau" et une interface s'affiche. |OK|
|50|frmContact.cs|Lorsque l'utilisateur veut gérer son journal de contact pour ajouter un contact, il remplit le formulaire en question, mais se trompe sur la clé publique. Dans ce cas un message d'erreur s'affiche. | OK |
|51|frmContact.cs|Lorsque l'utilisateur veut gérer son journal de contact pour ajouter un contact, il remplit le formulaire en question, mais le nom du contact est déjà pris. Dans ce cas un message d'erreur s'affiche. | OK |
|52|frmContact.cs|Lorsque l'utilisateur veut gérer son journal de contact pour ajouter un contact, il remplit le formulaire et que tout est bon alors il appuie sur le bouton "Valider" et un message de confirmation s'affiche. | OK |
|53|frmMonCompte.cs|Lorsque l'utilisateur désire se déconnecter de l'application il appuie sur le bouton "Déconnexion" de l'interface "Mon compte", alors tout se faire et la page login apparaît. |OK|

Le test qui m'a donné le plus de difficulté et qui n'est toujours pas validé est le N° 8. Le comparatif entre le temps des blocs et le temps de l'API CryptoCompare afin de déterminer le prix de l'Ethereum à l'instant où l'utilisateur détient un certain nombre de coins. Je n'ai pas corrigé ce bug par souci de temps.

## Site internet

| **N°** | **Interface** | **Description** | **Résultat** |
|---|---|---|:---:|
| 1 | connexion.php | L'utilisateur peut entrer un mail et un mot de passe valide et voit apparaître une nouvelle TextBox. | Ok |
| 2 | connexion.php | L'utilisateur une fois après avoir réalisé le test N° 1 peut entrer son code Google Authenticator valide et est redirigé sur la page de téléchargement. | Ok |
| 3 | login.php | L'utilisateur peut entrer un mail et un mot de passe valide et voit apparaître un QR code accompagné d'une TextBox. | Ok |
| 4 | login.php | L'utilisateur une fois après avoir réaliser le test N° 3 peut scanner le QR code dans l'application Google Authenticator ce qui lui ajoute un compte. | Ok |
| 5 | login.php | L'utilisateur une fois après avoir réalisé le test N° 4 peut entrer le code Google Authenticator et se voit redirigé sur la page connexion.php. | Ok |
| 6 | download.php | L'utilisateur peut en cliquant sur un lien télécharger le fichier d'installation de mon client C# | Ok |

## API PHP

Afin d'effectuer les tests sur l'API que j'ai développé j'ai utilisé l'extension Google Chrome Talend API tester. Ce logiciel m'a permis d'envoyer des requêtes POST sur mes différents scripts en y ajoutant des valeurs valide et erronée.


| **Id** | **Description** | **Résultat** |
|---|---|:---:|
| 1 | Check login utilisateur accepté s’il est valide | OK |
| 2 | Check login utilisateur refusé s’il est erroné | OK |
| 3 | Check code Google Authenticator accepté s’il est valide | OK |
| 4 | Check code Google Authenticator refusé s’il est erroné | OK |
| 5 | Création du token JWT | OK |
| 6 | Vérification du token JWT accepté s’il est valide | OK |
| 7 | Vérification du token JWT refusé s’il est modifié | OK |
