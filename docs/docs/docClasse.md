# Classes
<img src="./img/DiagrammeClasse.png" width="500">
fig{"./img/DiagrammeClasse.png", "Diagramme de classes", 80}
## JwtController
### Constante
* LOGIN_URL : Route sur le script login.php de l'API.
* AUTHENTICATOR_URL : Route sur le script authenticator.php de l'API.
* GET_TOKEN_URL : Route sur le script tokenCreator.php de l'API.
* CHECK_TOKEN_URL : Route sur le script tokenAuthenticator.php de l'API.
### Champs
* client : HTTPClient connecté a l'API PHP.
  
### Constructeur

Ce constructeur a comme unique tâche d'initialiser la propriété "URL" du champ "client" afin qu'il pointe sur le répertoire où se situe l'API.

### Méthodes 

#### CheckToken

Cette méthode asynchrone prend en paramètre deux "string", un pour le token et un autre pour le mail. 

Elle fait une requête sur l'API, permettant de vérifier un token "JWT".

Retourne un "string" avec comme valeur "ok" si le token est valide, et dans le cas contraire retourne le code d'erreur émis par la librairie Emmaref/JWT.

#### CheckUserAuthenticator

Cette méthode asynchrone prend en paramètre deux "string", un pour le mail et un autre pour le code.

Celle-ci fait une requête à l'API, permettant de vérifier le code Google Authenticator que l'utilisateur a fourni.

Retourne un "string" avec comme valeur "ok" si le token est valide, et dans le cas contraire "ko".

#### CheckUserLogin

Cette méthode asynchrone prend en paramètre deux "string", un pour le mail et un autre pour le password (mot de passe).

Celle-ci fait une requête à l'API, permettant de vérifier si l'email et le mot de passe que l'utilisateur fournit coïncident avec un des éléments présents dans la base de données.

Retourne un "string" avec comme valeur "ok" si le token est valide, et dans le cas contraire "ko".

#### GetToken

Cette méthode asynchrone prend en paramètre un "string", qui est pour le mail.

Celle-ci fait une requête à l'API, permettant de recevoir un nouveau token "JWT". 

Retourne un "string" avec comme valeur un token "JWT".

#### DeserializeResponse

Cette méthode prend en paramètre un "task< string>" response, il s'agit d'une tâche qui s'exécute de manière asynchrone.

Elle permet d'attendre la réponse complète de l'API et convertir la task "JSON" en "string".

## Wallet

### Constructeur

Ce constructeur a comme paramètres un string nom ("Name"), un string "publicKey" et d'un string "path".

Celui-ci permet d'instancier un Wallet et d'assigner les valeurs.

### Méthodes

## BlockchainController
### Constantes
* BLOCKCHAIN_URL : Url pointant sur la blockchain.
### Champs
* Wallets : Liste d'objets de type Wallet.
### Méthodes 

### Constructeur

Ce constructeur permet d'instancier l'objet Web3 qui communique avec la blockchain.

#### CreateTransaction

Cette méthode prend en paramètre un "string" privateKey, un "string" publiKey et une "decimal" amount.

Son but est de permettre la création d'une transaction par l'utilisateur à une tierce personne. C'est pourquoi il faut renseigner l'adresse publique ainsi que le montant. L'adresse privée permet de signer la transaction sur la blockchain.

#### DecryptJSONFile

Cette méthode prend en paramètre un "string" password et un "string" jsonPath.

Son but est de pouvoir prendre un portefeuille crypté sous forme de fichier JSON à un emplacement spécifique (jsonPath) et de pouvoir le décrypter à l'aide du code (password) afin d'y retirer les informations nécessaires. Il s'agit de préciser ici que le code doit être le même que celui utiliser pour son portefeuille à la création/importation.

#### DeleteWallet

Cette méthode prend en paramètre un "int" walletIndex et un "string" emailUser.

Son utilité est de pouvoir supprimer un portefeuille de sa liste "Wallets" de cette classe. 

Pour ce faire, on cherche le "Wallet" suivant son index, ensuite on y extrait son champ "path" ainsi que son "name".

Enfin, on parcourt le dossier pour trouver tout fichier qui comporte le nom demandé. Sans oublier de le supprimer de notre Liste de portefeuille à l'index spécifié en paramètre.

#### GenerateJSONFile

Cette méthode prend en paramètre un "string" password, un "string" name et un "string" accountName.

Le but est de générer un fichier "JSON" dans un dossier spécifié. Il est également important de regarder si le fichier existe déjà, dans ce cas on fait apparaître un message pour informer l'utilisateur. Dans le cas contraire on créé le fichier "JSON" comportant les informations cryptées et un fichier "txt" qui contient la clé publique.

#### GenerateJSONFile 

Cette méthode prend en paramètre en plus de celle ci-dessus un "Account" account. Ce type est nécessaire lors de l'import d'un fichier "JSON" puisqu’on est obligé de générer un "Account" pour récupérer la clé publique de ce portefeuille.

#### GetAccountFromPrivateKey

Cette méthode prend en paramètre un "string" privateKey.

Son utilité est de pouvoir créer et retourner un compte (Account) en utilisant sa clé privée.

#### GetAllBlocks

Retourne tous les blocs, avec leurs transactions, présents dans la blockchain.

#### GetAllTransacFromPublicAddress

Cette méthode prend en paramètre un "string" publicAdress.

Son utilité est de pouvoir obtenir toutes les transactions effectuées avec un certain portefeuille. Pour y arriver, on parcourt la liste des blocs fournie par la méthode [GetAllBlocks](#getallblocks) pour y extraire toutes les transactions où l'adresse publique est présente. 
 
Enfin, on retourne le dictionnaire de transaction "myTransac".

#### GetBalanceAtAllBlocks

Cette méthode prend en paramètre un "string" adresse.

Pour chaque bloc présent dans la blockchain, on effectue une requête "GetBalance" ce qui nous permet d'extraire l'évolution de la quantité d'Etherum présente dans un portefeuille.

Elle retourne un dictionnaire avec comme clé le "TimeStamp" de chaque bloc et comme valeur associée la quantité d'Etherum disponible.

#### GetLastBlock
Cette méthode asynchrone prend en paramètre un "string" publicAddress.

Elle retourne le dernier id du bloc stocké dans la blockchain.

#### GetBalanceInETH

Cette méthode prend en paramètre un "string" adresse.

Son utilité est qu'elle prend la balance d'une clé publique en "wei" et la convertie en "Eth".

#### GetPublicAddressWithWalletIndex

Cette méthode prend en paramètre un "int" index.

Son utilité est de pouvoir retourner la clé publique à un index spécifié (publicAddress) dans la Liste "Wallets".

#### GetUserWallets

Cette méthode en paramètre un "string" path.

Son utilité est de pouvoir instancier les différents portefeuilles appartenant à l'utilisateur connecté, dans la Liste "Wallets".

Enfin, je retourne cette variable "result" qui contient une Liste de wallet.

#### ImportAccountWithFile

Cette méthode prend en paramètre un "string" password, un "string" path, un "string" name et un "string" accountName.

Son but est de pouvoir instancier une variable de type "Account" nécessaire à la méthode [GenerateJsonfile](#generatejsonfile). Mais avant de pouvoir de faire cela, il est nécessaire d'utiliser la méthode [DecryptJSONFile](#decryptjsonfile).


#### ImportAccountWithPrivateKey

Cette méthode prend en paramètre un "string" privateKey, un "string" password, un "string" email et un "string" walletName.

Son but est de pouvoir instancier une variable de type "Account" nécessaire à la méthode [GenerateJsonfile](#generatejsonfile).

#### isAddressValid

Cette méthode prend en paramètre un "string" privateKey.

??????

#### KeystoreDirectoryCheck

Cette méthode prend en paramètre un "string" email.

Son but est de vérifier si le répertoire devant contenir les différents portefeuilles de l'utilisateur a été préalablement créé.

## AppController
Cette classe représente le modèle de données nécessaire au bon fonctionnement du programme, c'est cet objet qui est transmis entre les différentes vues lors de leurs instanciations. 
### Champs
* User : Objet de type User.
* Bc : Objet de type BlockchainController.
* Settings : Objet de type Settings.
### Méthodes

### Constructeur

Un "AppController" est caractérisé par trois valeurs. Il s'agit d'un "User" user, d'un "BlockchainController" bc et d'un "Settings" settings.

Un constructeur est mis en place avec un paramètre "string" userEmail. Dans celle-ci on indique que le "User" devient un nouvel utilisateur (new User) avec comme paramètre le userEmail. Enfin j'appelle la méthode [CheckIfSettingsAreSaved](#checkifsettingaresaved).

#### CheckIfSettingAreSaved

Cette méthode permet de vérifier si des paramètres ont déjà été enregistrés. Dans le cas où ça le serait, il instancie la classe "Settings" à l'aide de l'objet sérialisé présent dans le répertoire propre à chaque utilisateur.

#### SaveSettings

Cette méthode permet de sauvegarder les paramètres en sérialisant la classe "Settings" dans le répertoire de l'utilisateur connecter, lors de la fermeture de l'application.

#### ShowAuthenticatorDialogBox

Cette méthode prend en paramètre une "From" frmOwner.

Cette méthode permet d'afficher la "pop-up" authenticator et d'attendre un résultat de celle-ci.

Enfin je retourne le résultat de celle-ci.

## Settings

Cette classe permet de lister les différents contacts créés par l'utilisateur. Permets également d'enregistrer la devise utilisée par l'application.

### Champs
* Contacts : Liste d'objets de type Contact.
* currency : La devise utilisée par l'utilisateur.
### Méthodes

### Constructeur

Dans ce constructeur j'instancie la devise (currency) à "USD" par défaut. De plus, je créer une nouvelle liste d'objet "Contact" vide.

#### GetContactPublicKeyWithIndex

Cette méthode prend en paramètre un "int" index.

Elle retourne un "string" avec la publicKey d'un contact dans la Liste "Contacts" à l'index spécifié.

## Contact
Cette classe représente la structure de donnée d'un contact nécessaire à mon application.
### Champs
* name : Nom du contact.
* publicKey : Clé publique du contact.

### Constructeur

Ce constructeur prend en paramètre un "string" name et un "string" publicKey, pour assigné directement les valeurs aux champs.

## User
Cette classe représente la structure de donnée d'un utilisateur nécessaire à mon application.
### Constantes
### Champs
* Email : Email avec laquelle l'utilisateur s'est connecté.
* Token : Token JWT que reçoit l'utilisateur une fois connecté.
* 
### Constructeur

Ce constructeur prend en paramètre un "string" email, afin d'assigne directement la valeur au champ Email a l'instanciation de cette classe.



