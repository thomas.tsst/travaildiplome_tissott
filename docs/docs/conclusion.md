# Conclusion
 
Ce projet était pour moi une bonne opportunité de mettre en pratique mes compétences apprises tout au long de ma formation, mais pas seulement. L'application m'a demandé de me renseigner sur des fonctionnalités que je ne connaissais pas, tel que le type de cryptage possible, ce qui était fort intéressant. De cette manière j'ai dû me questionner pour savoir quel type je voulais utiliser et qui serait le plus indiqué pour celle-ci. Sachant cela, le point fort de mon projet réside dans la sécurité, il était donc préférable de bien me renseigner avant de me lancer dans l'élaboration du programme. D'autant plus qu'il s'agissait de pouvoir obtenir le plus rapidement possible des informations de la blockchain, ce qui demandait également une grande connaissance sur la matière.

Il est vrai que l'utilisation de Google Authenticator était pour ma part une très bonne expérience, puisqu'avec cela j'ai pu obtenir une meilleure compréhension de cette technologie. D'autant plus que cela pourrait fortement m'aider dans des projets futurs.
 
Le fait d'avoir pu travailler avec une blockchain m'a permis d'en apprendre davantage sur son fonctionnement et de ce qu'elle me permettait de faire. Mais également et surtout de visualiser la structure de données que chaque bloc et transaction contiennent.
 
J'ai également pu approfondir mes connaissances grâce à l'API PHP qui me permettait d'éviter de faire transiter la clé secrète qui est nécessaire à Google Authenticator, et de cette manière j'ai pu apprendre à lier du code PHP avec un client C#.
 
Il se trouve que durant l'élaboration de mon projet, j'ai eu besoin d'une API. De ce fait, j'ai été obligé d'être à l'aise avec la programmation asynchrone afin d'éviter des "DeadLock" au sein de mon application.
 
Je tenais à souligner que ce projet était aussi poussé par une certaine motivation, puisque cette thématique m'intéresse grandement. C'est pour cela que je tenais à mettre en place le plus de fonctionnalités qui s'apparentent aux applications existantes. Je souligne tout de même qu'il s'agit d'une version simplifiée, si j'avais une bonne équipe et plus de temps il aurait été possible de rendre ce projet encore plus conséquent.
 
À l'heure actuelle l'application est partiellement fonctionnelle et permet donc de répondre à la plupart des exigences de mon cahier des charges.J'estime que si j'avais eu un peu plus de temps, certaines petites parties de mon programme auraient pu être optimisées pour mieux répondre aux exigences et permettre que cela soit plus ergonomique pour les utilisateurs. Je pense surtout à la mise en avant des messages d'erreur ou d'affichage de confirmations. Malgré tout cela, j'ai tout de même trouvé pertinent de faire des interfaces simples et qui pouvaient être facilement comprises par l'utilisateur.
 
## Problèmes rencontrés
Je tenais à souligner que la contrainte d'utiliser MarkDown pour la documentation était partiellement compliquée pour des raisons de mise en page d'images, car elle ne me permettait pas de faire autrement que comme j'ai pu le faire tout au long de ce document. Il aurait peut-être été préférable d'utiliser "Word" pour cela.
 
Les requêtes à l'API m'ont donné du travail en plus, puisqu'il donnait l'impression qu'il ne s'arrêtait jamais après avoir reçu la réponse. L'utilisation de Google Authenticator m'était indispensable, mais son fonctionnement n'est pas très clair, ce qui fait que je devais davantage m'informer et tester ses fonctionnalités.
 
J'ai tout de même eu du mal au début avec le code asynchrone qui m'était avant tout inconnu. C'est pourquoi du temps a été consacré à devoir me familiariser à l'utiliser.
 
J'ai eu à maintes reprises dû changer les classes de mon application, puisque la structure des classes n'était pas assez élaborée. La conséquence était, pour le coup, que cela demandait de remanier la plupart de mon code pour adapter mes classes à mon programme.
 
## Améliorations possibles
 
Je le mentionnais ci-dessus, il s'agirait tout d'abord de mettre plus d'affichage de confirmation ou d'erreur. Faire en sorte que les graphiques soient plus explicites et plus précis.
Si on devait mettre cette application en production pour qu'elle soit accessible à tous, je dirais que je devrais tout d'abord changer le mode de cryptage. Le point positif c'est que je me suis déjà renseigné sur ce sujet.
 
La possibilité d'accepter des cryptomonnaies diverses et variées.Il serait intéressant de faire en sorte que différentes blockchains soient compatibles avec mon application. Il serait également important que les différentes cryptomonnaies présentes dans la blockchain Ethereum soient compatibles. Par le biais de ce qu'on appelle "Swap", qui est l'action d'échanger de l'Ethereum contre un autre coin [ERC20](https://fr.wikipedia.org/wiki/ERC20) sur son [smart contract](https://fr.wikipedia.org/wiki/Contrat_intelligent).
 
Je pense qu'il serait important de mettre en place la phrase mnémonique, qui est la suite de mots qui permettraient de récupérer ses portefeuilles.
 
Mon application n'a pas de section qui permette à l'utilisateur de modifier sa sécurité et confidentialité. Il serait intéressant de mettre en place la possibilité d'activer ou pas:
 
- Affichage des transactions entrantes
- Mettre en place une fonction antihameçonnage
- Révéler sa phrase secrète après confirmation de son mot de passe.
 
Ajouter un réseau dans mon programme pourrait laisser plus de liberté à l'utilisateur. Du moment où le réseau est conforme aux normes.
   
Je verrai bien une transformation de mon application pour qu'elle soit aussi sur smartphone.
 
Pour le moment j'affiche des graphiques simples, il serait vraiment bien de permettre à l'utilisateur d'interagir avec le graphique. Pour que cela soit encore plus interactif, je ferai en sorte de lier l'API “Tradingview” à mon application afin de pouvoir dessiner lui-même sur le graphique. Laisser le choix de la tranche horaire pour le graphique. Mettre plus de possibilités de devises. Possibilités d'afficher son compte sur "Etherscan".
 
Lier Binance API serait un choix judicieux pour mon application, puisqu'avec celle-ci on pourrait permettre d'accéder aux données du marché. De plus, celle-ci permettrait de mettre en pratique le trading automatique grâce aux ordres que l'utilisateur pourrait enregistrer.