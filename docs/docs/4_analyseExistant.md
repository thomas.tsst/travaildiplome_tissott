# Analyse de l'existant

Je vais parler d'une application qui sera mon point de repère pour l'élaboration de mon projet qui est "MetaMask".En effet, celle-ci détient la quasi-totalité des fonctionnalités qui m'intéresse.
Ces dernières sont de pouvoir se connecter, de pouvoir envoyer de la cryptomonnaie, de créer/maintenir un cahier de contact, de pouvoir créer et/ou importer des wallets ainsi que de pouvoir jeter un œil sur l'historique des transactions.

{{fig("./img/assets_logo_metamask.jpg", "Logo Metamask", 30)}}

## Importer ou créer un portefeuille

Étant donné que MetaMask est une application/extension que l'on installe sur le navigateur, il est demandé de tout d'abord choisir entre le fait d'importer un portefeuille avec une phrase "mnémotechnique" ou de configurer un nouveau portefeuille. Dans notre cas il s'agirait ici de commencer depuis le début.

{{fig("./img/forumulaireInscription.png", "Formulaire d'inscription Metamask", 60)}}

## Création du Mot de passe

Ensuite il est demandé de choisir un mot de passe. Pour remarque, les navigateurs sont de base connectés avec une adresse mail, qui est donc le point de référence à l'application.

{{fig("./img/mdp.png", "Création d'un mot de passe Metamask", 30)}}

## Connexion

Cette page est toute simple puisqu'il s'agit ici d'entrer le mot de passe spécifié au préalable.

{{fig("./img/connexion.png", "Connexion Metamask", 15)}}

## Acceuil

Cette page représente l'accueil de l'application. Elle offre plusieurs fonctionnalités possibles. Elle permet de savoir sur quel portefeuille on se trouve, de voir la quantité de cryptomonnaies détenue ainsi que sa valeur en "USD", d'échanger de la monnaie fiduciaire contre de l'Ethereum part le biais d'un paiement par carte de crédit, d'envoyer de l'éther, de voir son historique de transaction.

{{fig("./img/accueil.png", "Acceuil Metamask", 15)}}

## Envoyer

Une fois cliqué sur l'option "envoyer", il apparait cette page qui permet de soit envoyer de l'Etherum en utilisant une adresse publique ou de scanner un QR code soit de choisir un contact dans notre liste.

{{fig("./img/envoyer.png", "Création transaction Metamask", 15)}}

## Contact

Comme mentionné tout à l'[heure](#envoyer), on peut créer notre propre liste de contact. Dans cette situation on a la possibilité d'ajouter un contact ou de tout simplement voir nos contacts ou encore de les rechercher. 

{{fig("./img/contact.png", "Liste contacts Metamask", 15)}}

Une fois que l'on décide d'en ajouter un nouveau, il est donc demandé de choisir un nom de contact ainsi que de renseigner son adresse publique ou de scanner avec un QR code. Une fois celle-ci faite, on enregistre ou on annule la démarche.

{{fig("./img/nouveauContact.png", "Création contact Metamask", 15)}}

## Historique

Depuis la page [accueil,](#acceuil) on a la possibilité de voir notre "activité" (historique) de transaction dans l'onglet en bas à droite. C'est-à-dire de voir toutes les transactions effectuées avec le portefeuille sélectionné.

{{fig("./img/historique.png", "Historique des transactions Metamask", 15)}}


## Liste des portefeuilles

En appuyant sur la petite icône ronde en haut à droite on a la possibilité de voir la totalité de nos portefeuilles ainsi que la quantité d'ETH. En plus de cela on a également la possibilité de créer un compte, d'importer un compte ou de connecter un portefeuille matériel (clé USB) par exemple [Ledger Nano S](https://fr.wikipedia.org/wiki/Ledger_(entreprise)) qui est une clé USB qui détient des Wallets avec une clé privée digitale ainsi cela lui permet de se connecter sur son ordinateur ou une plateforme de trading.

{{fig("./img/wallets.png", "Liste des portefeuilles Metamask", 15)}}