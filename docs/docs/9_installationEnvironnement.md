# Installation de l'environnement
## Apache2
Pour installer Apache2, il suffit d'entrer la commande suivante :
```sh
sudo apt install apache2 -y
```
L'option -y permet de valider automatiquement tous les choix proposés par l'installation.

Pour vérifier l'installation d'Apache2, il faut utiliser cette commande :
```sh
sudo systemctl status apache2.service
```

{{fig("./img/verifApache2.png", "Vérification de l'installation Apache2", 50)}}

Afin de permettre d'exécuter des scripts PHP sur ce serveur il est nécessaire d'installer PHP ainsi que le module [libapache2-mod-php](https://packages.debian.org/fr/sid/libapache2-mod-php) qui lie le PHP à apache2 avec cette commande :
```sh
sudo apt install php7.4 libapache2-mod-php7.4 php7.4-mbstring php7.4-mysql php7.4-curl php7.4-gd php7.4-zip -y
```

Le répertoire utilisé par défaut est /var/www/html. Après cette installation, pour vérifier que le serveur peut exécuter du PHP, il suffit de créer un fichier index.php qui contient :
```php
<?php phpinfo(); ?>
```

Une fois ce fichier créé, il suffit d'aller sur n'importe quel navigateur (dans mon cas Chromium) et de rentrer dans l'url 127.0.0.1/index.php si la page s'affiche comme l'image ci-dessous l'installation c'est bien déroulé.

{{fig("./img/phpInfo.png", "Vérification de l'installation PHP", 60)}}

À partir de cette étape, le serveur HTTP est totalement fonctionnel. Néanmoins, pour des raisons de sécurité, il est important d'encrypter l'échange de donnée entre l'utilisateur et le serveur. Pour cela il faut le transformer en HTTPS ce qui forcera une connexion [TLS](https://fr.wikipedia.org/wiki/Transport_Layer_Security).

Pour faire cela, il faut premièrement créer une clé privée ainsi qu'un certificat. Dans mon cas, je vais utiliser OpenSSL pour générer ces éléments avec cette commande.
```sh
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout private.key -out certificate.crt
```

Il y a plusieurs informations demandées lors de la création d'un certificat comme l'image ci-dessous.

{{fig("./img/CreationCertifica.png", "Création des certificats", 50)}}

L'information la plus importante est le paramètre "Common Name" il faut soit entrer le nom de domaine ou sinon directement l'adresse IP du serveur.

Ensuite, pour que ces fichiers soient utilisés par le serveur, il faut les déplacer dans le répertoire /etc/ssl/.../

```sh
sudo mv private.key /etc/ssl/private/
sudo mv certificate.crt /etc/ssl/certs/
```

Les fichiers désormais dans les bons répertoires il faut éditer le fichier de configuration d'Apache2.
```sh
sudo nano /etc/apache2/sites-available/000-default.conf
```

Il y a déjà un bloc "VirtualHost" sur le port 80 (Port par défaut pour le HTTP), en dessous de celui-ci il faut rajouter ce nouveau bloc pointant sur le port 443 (Port par défaut pour le HTTPS).
```sh
<VirtualHost *:443>
        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/html
        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined
        SSLEngine on
        SSLProtocol all -SSLv2
        SSLCipherSuite HIGH:MEDIUM:!aNULL:!MD5
        SSLCertificateFile "/etc/ssl/certs/certificate.crt"
        SSLCertificateKeyFile "/etc/ssl/private/private.key"
</VirtualHost>
```

Afin d'activer le ssl sur le serveur Apache2, il suffit d'utiliser cette commande :
```sh
sudo a2enmod ssl
```

Pour vérifier la syntaxe du fichier de configuration précédemment modifié, il faut utiliser cette commande :
```sh
sudo apache2ctl configtest
```

Exemple avec un fichier valide :

{{fig("./img/VerificationCertificat.png", "Vérification des certificats", 50)}}

Afin de forcer la connexion en HTTPS sur le serveur, il faut ajouter dans le fichier de configuration d'Apache2 cette ligne à l'intérieur du bloc HTTP (Redirection sur le port 80) :
```sh
Redirect "/" "https://IP_DU_SERVEUR/"
```


## Samba
Afin d'avoir un accès simplifié au fichier du Raspberry j'ai installé "Samba" qui me permet de créer un disque réseau directement depuis ma machine.

Voici la commande d'installation.
```sh
sudo apt-get install samba samba-common-bin
```



Ensuite il faut éditer le fichier de configuration de "Samba".
```sh
sudo nano /etc/samba/smb.conf
```

Dans mon cas, j'ai décidé de partager tout à partir de la racine du Raspberry, mais il est également possible de partager uniquement un dossier. C'est pour cette raison que le champ "path" contient uniquement /. Le nom entre crochets va être utilisé pour créer le disque réseau.
```sh
[share]
path = /
writeable=Yes
create mask=0777
directory mask=0777
public=no
```
Pour valider les modifications effectuées dans le fichier de configuration, il est nécessaire de redémarrer le service avec la commande suivante
```sh
sudo service smbd restart
```

Ensuite, il faut créer un utilisateur lié à "Samba" dans mon exemple ci-dessous où le nom de l'utilisateur est "thomapi".
```sh
sudo smbpasswd -a thomapi
```

Après cette étape la configuration de "Samba" est terminée. La suite se déroule sur Windows, il faut ouvrir l'explorateur de fichiers aller sur ce PC et cliquer sur "Connecter un lecteur réseau"

{{fig("./img/sambaExplorateurFichiers.png", "Explorateur de fichiers Windows 10", 60)}}

La fenêtre ci-dessous va s'ouvrir, il faut :

1. Ce premier champ sert à préciser la lettre sous laquelle le lecteur va s'appeler.
2. Ce champ doit contenir l'IP du Raspberry suivi du nom qui a été spécifié dans le fichier de configuration.
3. Après avoir spécifié les 2 champs précédents, il suffit de cliquer sur "Terminer".

{{fig("./img/AjoutDisqueReseau1.png", "Ajout disque réseau", 30)}}

La dernière étape est d'entrer les noms d'utilisateur ainsi que son mot de passe précédemment paramétré.

{{fig("./img/AjoutDisqueReseau2.png", "Connexion à l'utilisateur Samba", 30)}}

Voici ci-dessous les fichiers du Raspberry disponible sous forme de disque réseau sur une machine Windows.

{{fig("./img/AjoutDisqueReseau3.png", "Disque réseau connecté au Raspberry", 40)}}

## MariaDB
### Installation
```bash
sudo apt install mariadb-server
```
### Configuration
Afin de configurer, il est nécessaire d'exécuter le script Shell en utilisant la commande suivante.

```bash
sudo mysql_secure_installation
```

Suite à cette dernière, plusieurs entrées d’utilisateur sont demandées. Il faut accepter toutes les questions.

{{fig("./img/MysqlSecureInstallation.png", "Configuration de MariaDB", 40)}}

Suite a cela, le mot de passe de l'utilisateur root a été modifié et de supprimé les utilisateurs anonymes ainsi que la base de données de tests créés à l'installation du serveur MariaDB.

Pour créer la base de données nécessaire à mon projet, j'utilise les commandes [SQL](https://fr.wikipedia.org/wiki/Structured_Query_Language) suivantes :

```sql
CREATE DATABASE NomBaseDedonnées;
```

Pour créer l'utilisateur local qui se connectera à ma base de données en lui donnant uniquement les droits sur la base de données précédemment créée.

```sql
GRANT ALL ON *NonBaseDeDonnées* TO 'NomUtilisateur'@'localhost' IDENTIFIED BY 'MotDePasse' WITH GRANT OPTION;
```

Cette commande permet de sélectionner la base de données pour que la commande suivante s'exécute sans erreur.

```sql
USE NomBaseDedonnées
```

Pour créer la table "Users" qui contient l'email, le mot de passe et une clé nécessaire à [Google Authenticator](https://fr.wikipedia.org/wiki/Google_Authenticator) j'ai dû entrer cette commande :

```sql
CREATE TABLE `Users` (
  `idUsers` int(11) NOT NULL AUTO_INCREMENT,
  `mail` varchar(100) NOT NULL,
  `password` varchar(40) NOT NULL,
  `secretKey` varchar(16) NOT NULL,
  PRIMARY KEY (`idUsers`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
```

## Hardhat
Comme la plupart des librairies Ethereum, Hardhat est écrit en Javasacript. Afin de pouvoir exécuter ce code en dehors d'un navigateur internet, il est nécessaire d'installer Node.Js à l'aide des 3 commandes suivantes.

```sh
sudo apt install curl git
curl -sL https://deb.nodesource.com/setup_16.x | sudo -E bash -
sudo apt install nodejs
```

Une fois cela terminé, pour installer Hardhat il faut utiliser la commande npm (Node.js package manager). Npm est un manager de paquets et un dépôt en ligne pour des scripts écrits en JavaScript.

Premièrement il faut créer un répertoire qui va contenir le serveur Hardhat. Une fois créer il faut se rendre dans celui-ci à l'aide de la commande cd.  

```sh
mkdir hardhat
cd hardhat
```

Pour installer le paquet Hardhat ainsi que ces dépendances, il faut exécuter les commandes suivantes.

```node
npm init --yes
npm install --save-dev hardhat
```

Pour créer le projet Hardhat, il faut entrer la commande suivante dans le même répertoire créer précédemment. 

```node
npx hardhat
```
Grâce aux flèches du clavier et en appuyant sur la touche "Entrer", il faut sélectionner "Create an empty harhadt.config.js".

{{fig("./img/InstallHardhat.png", "Création du projet Hardhat", 40)}}

Suite à cela, le serveur Hardhat est fonctionnel. Pour lancer le serveur HTTP et le Websocket JSON-RPC, qui nous permettront de communiquer avec la blockchain, il faut exécuter la commande suivante.

```node
npx hardhat node
```

Lors du lancement de Hardhat le serveur se créer avec 20 portefeuilles disposant chacun de 10'000 Ethereum permettant de tester les différentes actions possible sur la blockchain.

{{fig("./img/HardhatCompteGenerer.png", "Compte généré Hardhat", 40)}}
