# Conceptualisation

Avant de me lancer dans la programmation, il m'était tout de même indispensable d'avoir une conception de ce que je voulais réaliser.

C'est pourquoi j'ai divisé ce travail en deux parties. Il s'agissait avant tout d'avoir une "User Stories". Ensuite, de par celles-ci, il m'était plus simple d'avoir une idée générale pour créer des mockups pour l'aspect visuel.

Il est vrai que des mockup sont présents dans mon cahier des charges, mais cela étant trop pauvre en concepts, il m'était évident que je devais pousser mon idée plus loin.

Je me suis donc lancé dans l'élaboration de mockup pour avoir une idée de comment je voyais mon application. J'ai estimé cette démarche très importante puisque de cette manière je minimisais les pertes de temps en validant avant tout l'aspect visuel.

Enfin, dès validation de celles-ci je me suis conceptualisé différentes fonctionnalités que je voulais mettre dans mon application. C'est pourquoi j'ai fait un listing de celles-ci pour avoir une bonne idée générale. Il faut savoir que malgré ce listing d'autres fonctions pourraient être ajoutées en cours de l'élaboration du programme.


## User Stories
### Application C#
|   Nom     |      T1 : Login mot de passe + Google Authenticator |
|---|---|
| Description | En tant qu’utilisateur, lors du lancement de l’application, il m’est demandé de me connecter grâce à un email + mot de passe suivi d’une double authentification. (Google   authenticator). |

|     Nom    |     T2 : importer et créer des portefeuilles    |
|---|---|
|     Description          |     En tant qu’utilisateur,   je peux cliquer sur le bouton « + » pour accéder à la vue d’import   et de création d’un portefeuille.    |

|     Nom    |     T3 :   Visualisation des portefeuilles    |
|---|---|
|     Description    |     En tant qu’utilisateur, je peux voir les   portefeuilles précédemment créé et/ou importé.    |

|     Nom    |     T4 : Sélectionner un portefeuille    |
|---|---|
|     Description    |     En tant qu’utilisateur,   je peux sélectionner un portefeuille parmi la liste.     |

|     Nom    |     T5 : Afficher l’adresse publique du portefeuille    |
|---|---|
|     Description    |     En tant qu’utilisateur,   si un portefeuille est sélectionné, je peux voir l’adresse publique de celui-ci en   format QR code ainsi qu’en texte.    |

|     Nom    |     T6 : Créer une transaction    |
|---|---|
|     Description    |     En tant qu’utilisateur,   je peux créer une transaction si je possède un portefeuille.    |

|     Nom    |     T7 : Évolution du/des portefeuille(s)    |
|---|---|
|     Description    |     En tant qu’utilisateur,   je peux accéder à la vue de l’évolution des portefeuilles.     |

|     Nom    |     T8 : Choix des dates évolution portefeuilles    |
|---|---|
|     Description    |     En tant qu’utilisateur,  je peux choisir une date de début et de fin pour visualiser l’évolution entre   celles-ci.     |

|     Nom    |     T9 : Affichage du graphique sur la vue évolution    |
|---|---|
|     Description    |     En tant qu’utilisateur, du moment où je me trouve sur la vue évolution du/des portefeuille(s) un graphique de la cryptomonnaie s'affiche. Ce graphique s'actualise grâce au choix des dates.     |

| Nom | T10 : Supression d'un portefeuille  |
|---|---|
| Description | En tant qu'utilisateur, je peux cliquer sur le bouton supprimer après avoir sélectionné un portefeuille ce qui me permet de le sortir complètement de mon application. |

|     Nom    |     T11 : Déconnexion    |
|---|---|
|     Description    |     En tant qu’utilisateur, je peux me déconnecter en cliquant sur le bouton « Déconnexion ».     |

### Site Web
|     Nom    |     T1 : Création de comptes    |
|---|---|
|     Description    |     En tant qu’utilisateur, je peux créer un compte en cliquant sur le bouton créer un compte. Il est demandé d'entrer une adresse email ainsi qu'un mot de passe.    |

|     Nom    |     T2 : Connexion    |
|---|---|
|     Description    |     En tant qu’utilisateur, je peux me connecter en cliquant sur le bouton me connecter. Il est demandé à l'utilisateur d'entrer son adresse email et un mot de passe.    |

|     Nom    |     T3 : Déconnexion    |
|---|---|
|     Description    |     En tant qu’utilisateur, je peux me déconnecter en cliquant sur le bouton déconnexion. Ce bouton est présent uniquement si l'utilisateur s'est connecté précédemment.    |

|     Nom    |     T4 : Téléchargement du logiciel d'installation    |
|---|---|
|     Description    |     En tant qu’utilisateur, si je suis connecté sur le site, il m'est possible de télécharger le fichier d'installation du client.    |

<!-- 
### Encryption des fichiers sensible

Pour protéger les wallet stockés localement sur la machine de l'utilisateur, je vais utiliser une encryption AES indépendamment pour chaque fichier (Portefeuille). J'ai décidé d'utiliser cette manière de faire, car même si une personne arrive à casser une clé d'encryption, ce qui est très peu probable, uniquement un portefeuille serait compromis. L'encryption AES (Advanced Encryption Standard) est un algorithme de chiffrement symétrique.
-->

## Interfaces MockUp

{{fig("./img/LienEntrePage.png", "Liens entre les mockup", 60)}}

Pour avoir des interfaces plus modernes, j'ai utilisé le paquet NuGet [MetroSetUI](https://github.com/N-a-r-w-i-n/MetroSet-UI). J'ai essayé de simplifier au maximum les interfaces pour éviter que les utilisateurs de mon application soient dans le flous.

Afin de m'inspirer pour réaliser ces interfaces, je me suis renseigné sur plusieurs applications, dont Metamask. Il s'agit d'une application de gestion de portefeuille. 
### Login
L'utilisateur peut entrer un mail ainsi que son mot de passe afin de se connecter.

{{fig("./img/frmLogin.png", "Mockup login", 30)}}

{{fig("./img/digrammeSequenceConnexionApp.png", "Diagramme connexion", 30)}}

### Vue portefeuille

{{fig("./img/VuePortefeuille.PNG", "Mockup acceuil", 40)}}

#### Sélection du portefeuille
L'utilisateur peut cliquer sur la liste déroulante et sélectionner un portefeuille parmi les différends disponibles.

#### Recevoir
L'utilisateur peut cliquer sur le bouton "Recevoir" pour être redirigé sur la vue [Adresse](#adresse).

#### Recevoir
L'utilisateur peut cliquer sur le bouton "Envoyer" pour être redirigé sur la vue [Envoi](#envoi).

#### Historique
L'utilisateur a une liste à disposition qui contient la liste des transactions réalisées avec le portefeuille sélectionné. 

### Envoi

{{fig("./img/VueEnvoie.png", "Mockup envoi", 30)}}

#### Transaction à une adresse
L'utilisateur entre une adresse Ethereum dans la textebox "Adresse" et clique sur le bouton envoyer pour créer une transaction.

#### Transaction à un portefeuille
L'utilisateur clique sur le bouton "Transfert entre mes comptes" pour sélectionner un de ses portefeuilles personnels ce qui entre automatiquement la clé publique de celui-ci et clique sur le bouton "Envoyer" pour créer la transaction.

{{fig("./img/digrammeSequenceEnvoyer.png", "Diagramme transaction", 50)}}

#### Redirection après transaction
L'utilisateur après avoir cliqué sur le bouton "Envoyer" est redirigé sur la page [Vue portefeuille](#vue-portefeuille).

### Adresse

{{fig("./img/VueRecevoir.png", "Mockup recevoir", 30)}}

#### Copie de l'adresse publique
L'utilisateur peut cliquer sur le bouton "Copier", pour conserver dans le presse-papier, l'adresse publique du portefeuille.

#### QR Code
Quand l'utilisateur arrive sur cette vue, un QR code générer à partir de l'adresse privée est affiché.
### Mon compte

{{fig("./img/VueMonCompte.png", "Mockup mon compte", 30)}}

#### Gérer les portefeuilles
L'utilisateur peut cliquer sur le bouton "Gérer les portefeuilles" pour être redirigé vers la vue [Gestion des portefeuilles](#gestion-des-portefeuilles).

#### Paramètres
L'utilisateur peut cliquer sur le bouton "Paramètres" pour être redirigé vers la vue [Paramètres](#paramètres).

#### Déconnexion
L'utilisateur peut cliquer sur le bouton "Déconnexion" pour être redirigé vers la vue [Login](#login).

### Gestion des portefeuilles

#### Redirection sous-menu
L'utilisateur peut cliquer sur le sous-menu "Nouveau" pour affiché la vue [Nouveau](#nouveau). Ce sous-menu, est celui affiché en arrivant sur la vue [Gestion des portefeuilles](#gestion-des-portefeuilles).

L'utilisateur peut cliquer sur le sous-menu "Importer" pour afficher la vue [Importer](#importer).

L'utilisateur peut cliquer sur le sous-menu "Supprimer" pour afficher la vue [Supprimer](#supprimer).

#### Nouveau

{{fig("./img/VueGestionPortefeuille.PNG", "Mockup gestion des portefeuilles", 30)}}

L'utilisateur peut entrer un nom de portefeuille et cliquer sur le bouton "Créer" pour créer un portefeuille. Après cette action faite, l'utilisateur est redirigé sur la page [Vue portefeuilles](#vue-portefeuille).

{{fig("./img/digrammeSequenceCreeWallet.png", "Diagramme nouveau portefeuilles", 50)}}

#### Importer
{{fig("./img/GestionPortefeuilleImport.PNG", "Mockup import gestion portefeuilles", 50)}}

##### Clé privée
L'utilisateur peut importer un portefeuille existant en entrant dans le champ "clé" la clé privée du portefeuille puis en cliquant sur le bouton "Importer". Après cette action, l'utilisateur est redirigé sur la page [Vue portefeuilles](#vue-portefeuille).

{{fig("./img/digrammeSequenceImporterCle.png", "Diagramme importer portefeuilles (Clé)", 30)}}

##### Fichier JSON
L'utilisateur peut importer un portefeuille existant en cliquant sur le bouton "Importer fichier", puis en sélectionnant le fichier "JSON" contenant le portefeuille. Du moment où l'import s'est bien déroulé, le nom du fichier s'affichera dans l'interface d'import de fichier. Ensuite, l'utilisateur doit entrer le mot de passe qu'il a entré lors de la création du portefeuille et cliquer sur le bouton "Importer". Après cette action, l'utilisateur est redirigé sur la page [Vue portefeuilles](#vue-portefeuille).

{{fig("./img/digrammeSequenceImportFichierJSON.png", "Diagramme importer portefeuilles (JSON)", 30)}}

#### Supprimer

{{fig("./img/VueGestionPortefeuilleSupprimer.PNG", "Mockup suppression gestion portefeuille", 50)}}

L'utilisateur peut sélectionner un portefeuille parmi ceux importés et/ou créés sur l'application. Une fois sélectionné l'utilisateur doit cliquer sur le bouton "Supprimer" pour supprimer son portefeuille du stockage de l'application. Après cette action l'utilisateur est redirigé sur la page [Vue portefeuilles](#vue-portefeuille).

{{fig("./img/digrammeSequenceSupprimerWallet.png", "Diagramme suppression portefeuilles (JSON)", 50)}}

### Paramètres
L'utilisateur peut sélectionner la devise utilisé sur l'application et se rediriger sur la vue "Contact"
{{fig("./img/VueParametres.PNG", "Mockup paramètres", 30)}}