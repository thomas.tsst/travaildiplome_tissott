
<?php
session_start();
  require_once ("../functionConnexionDB.php");
  require_once ("../functions.php");
  include "../lib/google-authenticator-main/vendor/autoload.php";

  $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
  $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
  $submit = filter_has_var(INPUT_POST, 'submit');
  
  $authenticator = filter_has_var(INPUT_POST, 'authenticator');
  $code = filter_input(INPUT_POST, 'code', FILTER_SANITIZE_STRING);
  //Création d'un nouveau compte utilisateur.
  if ($submit){
    if(filter_var($email, FILTER_VALIDATE_EMAIL) && strlen($password) >= 6){
      $secret = createSecretKey($email);
      
      $secretKey = $secret->getSecretKey();
      $_SESSION['secretKey'] = $secretKey;

      $_SESSION['email'] = $email;
      $_SESSION['password'] = sha1($password);

      $qrImageGenerator = new \Dolondro\GoogleAuthenticator\QrImageGenerator\EndroidQrImageGenerator();
    }else{
      echo '<script>alert("Email non valide et/ou mot de passe trop court. (6 caractères minimum)")</script>';
    }
  }
  //Assosciation de Google Authenticator à l'utilisateur.
  if($authenticator){
    $googleAuthenticator = new \Dolondro\GoogleAuthenticator\GoogleAuthenticator();
    if ($googleAuthenticator->authenticate($_SESSION['secretKey'], $code)) {
      createUser($_SESSION['email'], $_SESSION['password'], $_SESSION['secretKey']);
      
      $_SESSION['secretKey'] = "";
      header("Location:../connexion/index.php");
    }
    else {
      echo "Code non valide !!!";
    }
  }
?>
<!doctype html>
<html lang="en" class="no-js">
  <head>
    
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width,initial-scale=1">
      
      
      
      
      <link rel="icon" href="../assets/images/favicon.png">
      <meta name="generator" content="mkdocs-1.2.3, mkdocs-material-7.3.6">
    
    
      
        <title>Créer un compte - SecuWallet</title>
      
    
    
      <link rel="stylesheet" href="../assets/stylesheets/main.a57b2b03.min.css">
      
        
        <link rel="stylesheet" href="../assets/stylesheets/palette.3f5d1f46.min.css">
        
      
    
    
    
      
        
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,700%7CRoboto+Mono&display=fallback">
        <style>:root{--md-text-font-family:"Roboto";--md-code-font-family:"Roboto Mono"}</style>
      
    
    
    
      <link rel="stylesheet" href="../stylesheets/extra.css">
    
    
      


    
    
  </head>
  
  
    
    
    
    
    
    <body dir="ltr" data-md-color-scheme="" data-md-color-primary="none" data-md-color-accent="none">
  
    
    <script>function __prefix(e){return new URL("..",location).pathname+"."+e}function __get(e,t=localStorage){return JSON.parse(t.getItem(__prefix(e)))}</script>
    
    <input class="md-toggle" data-md-toggle="drawer" type="checkbox" id="__drawer" autocomplete="off">
    <input class="md-toggle" data-md-toggle="search" type="checkbox" id="__search" autocomplete="off">
    <label class="md-overlay" for="__drawer"></label>
    <div data-md-component="skip">
      
        
        <a href="#creer-un-compte" class="md-skip">
          Skip to content
        </a>
      
    </div>
    <div data-md-component="announce">
      
    </div>
    
      

<header class="md-header" data-md-component="header">
  <nav class="md-header__inner md-grid" aria-label="Header">
    <a href=".." title="SecuWallet" class="md-header__button md-logo" aria-label="SecuWallet" data-md-component="logo">
      
  
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 8a3 3 0 0 0 3-3 3 3 0 0 0-3-3 3 3 0 0 0-3 3 3 3 0 0 0 3 3m0 3.54C9.64 9.35 6.5 8 3 8v11c3.5 0 6.64 1.35 9 3.54 2.36-2.19 5.5-3.54 9-3.54V8c-3.5 0-6.64 1.35-9 3.54z"/></svg>

    </a>
    <label class="md-header__button md-icon" for="__drawer">
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M3 6h18v2H3V6m0 5h18v2H3v-2m0 5h18v2H3v-2z"/></svg>
    </label>
    <div class="md-header__title" data-md-component="header-title">
      <div class="md-header__ellipsis">
        <div class="md-header__topic">
          <span class="md-ellipsis">
            SecuWallet
          </span>
        </div>
        <div class="md-header__topic" data-md-component="header-topic">
          <span class="md-ellipsis">
            
              Créer un compte
            
          </span>
        </div>
      </div>
    </div>
    
    
    
    
  </nav>
  
</header>
    
    <div class="md-container" data-md-component="container">
      
      
        
          
        
      
      <main class="md-main" data-md-component="main">
        <div class="md-main__inner md-grid">
          
            
              
              <div class="md-sidebar md-sidebar--primary" data-md-component="sidebar" data-md-type="navigation" >
                <div class="md-sidebar__scrollwrap">
                  <div class="md-sidebar__inner">
                    


<nav class="md-nav md-nav--primary" aria-label="Navigation" data-md-level="0">
  <label class="md-nav__title" for="__drawer">
    <a href=".." title="SecuWallet" class="md-nav__button md-logo" aria-label="SecuWallet" data-md-component="logo">
      
  
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 8a3 3 0 0 0 3-3 3 3 0 0 0-3-3 3 3 0 0 0-3 3 3 3 0 0 0 3 3m0 3.54C9.64 9.35 6.5 8 3 8v11c3.5 0 6.64 1.35 9 3.54 2.36-2.19 5.5-3.54 9-3.54V8c-3.5 0-6.64 1.35-9 3.54z"/></svg>

    </a>
    SecuWallet
  </label>
  
  <ul class="md-nav__list" data-md-scrollfix>
    
      
      
      

  
  
  
    <li class="md-nav__item">
      <a href=".." class="md-nav__link">
        Quel but ?
      </a>
    </li>
  

    
      
      
      

  
  
  
    <li class="md-nav__item">
      <a href="../connexion/" class="md-nav__link">
        Connexion
      </a>
    </li>
  

    
      
      
      

  
  
    
  
  
    <li class="md-nav__item md-nav__item--active">
      
      <input class="md-nav__toggle md-toggle" data-md-toggle="toc" type="checkbox" id="__toc">
      
      
        
      
      
      <a href="./" class="md-nav__link md-nav__link--active">
        Créer un compte
      </a>
      
    </li>
  

    
      
      
      

  
  
  
    <li class="md-nav__item">
      <a href="../download/" class="md-nav__link">
        Télécharger
      </a>
    </li>
  

    
  </ul>
</nav>
                  </div>
                </div>
              </div>
            
            
              
              <div class="md-sidebar md-sidebar--secondary" data-md-component="sidebar" data-md-type="toc" >
                <div class="md-sidebar__scrollwrap">
                  <div class="md-sidebar__inner">
                    


<nav class="md-nav md-nav--secondary" aria-label="Table of contents">
  
  
  
    
  
  
</nav>
                  </div>
                </div>
              </div>
            
          
          <div class="md-content" data-md-component="content">
            <article class="md-content__inner md-typeset">
              
                
                
                <h1 id="creer-un-compte">Créer un compte</h1>
<form method="POST">
    Email:<br> <input  placeholder="Email" type="text" name="email"><br>
    Mot de passe:<br> <input  placeholder="Mot de passe" type="password" name="password"><br>
    <br><input type="submit" name="submit">
  </form>
                
  <?php 
  if(strlen($secretKey) == 16){
    echo "<label>Veullez scanner le QR code avec l'application Google Authenticator</label><br>";
    echo "<img id='qrCode' src='".$qrImageGenerator->generateUri($secret)."'>'<br>";
    echo "Voici la clé secrète a sauvegarder pour récupérer votre compte : ".$secretKey."\n";
    echo "<form method='POST' action='index.php'>";
    echo "<input  placeholder='Entré le code' type='text' name='code' >";
    echo "<input type='submit' name='authenticator'>";
    echo "</form>";
  }
  
  
  ?>              
              
                


              
            </article>
          </div>
        </div>
        
      </main>
      
        
<footer class="md-footer">
  
    <nav class="md-footer__inner md-grid" aria-label="Footer">
      
        
        <a href="../connexion/" class="md-footer__link md-footer__link--prev" aria-label="Previous: Connexion" rel="prev">
          <div class="md-footer__button md-icon">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M20 11v2H8l5.5 5.5-1.42 1.42L4.16 12l7.92-7.92L13.5 5.5 8 11h12z"/></svg>
          </div>
          <div class="md-footer__title">
            <div class="md-ellipsis">
              <span class="md-footer__direction">
                Previous
              </span>
              Connexion
            </div>
          </div>
        </a>
      
      
        
        <a href="../download/" class="md-footer__link md-footer__link--next" aria-label="Next: Télécharger" rel="next">
          <div class="md-footer__title">
            <div class="md-ellipsis">
              <span class="md-footer__direction">
                Next
              </span>
              Télécharger
            </div>
          </div>
          <div class="md-footer__button md-icon">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M4 11v2h12l-5.5 5.5 1.42 1.42L19.84 12l-7.92-7.92L10.5 5.5 16 11H4z"/></svg>
          </div>
        </a>
      
    </nav>
  
  <div class="md-footer-meta md-typeset">
    <div class="md-footer-meta__inner md-grid">
      <div class="md-footer-copyright">
        
        
          Made with
          <a href="https://squidfunk.github.io/mkdocs-material/" target="_blank" rel="noopener">
            Material for MkDocs
          </a>
        
        
      </div>
      
    </div>
  </div>
</footer>
      
    </div>
    <div class="md-dialog" data-md-component="dialog">
      <div class="md-dialog__inner md-typeset"></div>
    </div>
    <script id="__config" type="application/json">{"base": "..", "features": [], "translations": {"clipboard.copy": "Copy to clipboard", "clipboard.copied": "Copied to clipboard", "search.config.lang": "en", "search.config.pipeline": "trimmer, stopWordFilter", "search.config.separator": "[\\s\\-]+", "search.placeholder": "Search", "search.result.placeholder": "Type to start searching", "search.result.none": "No matching documents", "search.result.one": "1 matching document", "search.result.other": "# matching documents", "search.result.more.one": "1 more on this page", "search.result.more.other": "# more on this page", "search.result.term.missing": "Missing", "select.version.title": "Select version"}, "search": "../assets/javascripts/workers/search.fcfe8b6d.min.js", "version": null}</script>
    
    
      <script src="../assets/javascripts/bundle.b1047164.min.js"></script>
      
    
  </body>
</html>