
<?php
  require_once ("../functionConnexionDB.php");
  require_once ("../functions.php");
  include_once "../lib/google-authenticator-main/vendor/autoload.php";
/**
 * Génère une clé unique nécessaire à L'API Google Authenticator.
 *
 * @param string  $account  Email de l'utilisateur.
 * 
 * @author Thomas Tissot
 * @return $secret Clé générée grâce à la librairie \Dolondro\GoogleAuthenticator\.
 */ 
function createSecretKey($account){
    $secretFactory = new \Dolondro\GoogleAuthenticator\SecretFactory();
    $secret = $secretFactory->create("SecuWallet", $account);
    $secretKey = $secret->getSecretKey();
    return $secret;
}

/**
 * Génère une clé unique nécessaire à L'API Google Authenticator.
 *
 * @param string  $secretKey  Clé secrète de l'utilisateur.
 * @param string  $code  Code entré par l'utilisateur.
 * 
 * @author Thomas Tissot
 * @return bool TRUE Si le code est validé. FALSE si le code est éronné.
 */ 
function checkAuthenticator($secretKey, $code){
    $googleAuthenticator = new \Dolondro\GoogleAuthenticator\GoogleAuthenticator();
    if ($googleAuthenticator->authenticate($secretKey, $code)) {
            return TRUE;
      }else {
            return FALSE;
      }
      
}
?>