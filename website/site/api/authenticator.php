<?php
    include "../lib/google-authenticator-main/vendor/autoload.php";
    use Emarref\Jwt\Claim;
    require_once ("../functionConnexionDB.php");
    require_once ("../functions.php");

    header('Content-Type: application/json; charset=utf-8');
    //Contrôle que l'API a bien reçu l'email et le code de l'utilisateur
    if(isset($_POST['email']) && isset($_POST['code']))
    {
        //Récupération de la clé secrete
        $secretKey = GetUserSecretKey($_POST['email']);
        //Contrôle de Google Authenticator
        if (checkAuthenticator($secretKey['secretKey'], $_POST['code'])) {
            $value = "ok";
        }else {
            $value = "ko";
        }
        http_response_code(200);
    }else{
        $value = "Error";
    }
    echo json_encode($value);

    
?>