<?php 
require_once ("../functionConnexionDB.php");
include "../lib/google-authenticator-main/vendor/autoload.php";
use Emarref\Jwt\Claim;

if (isset($_POST['token']) && $_POST['token'] != "") {
    $verification = authenticate_token($_POST['token']);
    if ($verification['status'] == 'ok') {
        header('Content-Type: application/json; charset=utf-8');
        http_response_code(200);
        echo json_encode("ok");
    } else if ($verification['status'] == 'error') {
        header('Content-Type: application/json; charset=utf-8');
        http_response_code(403);
        echo json_encode(['data' => $verification['err']]);
    }
} else {
    header('Content-Type: application/json; charset=utf-8');
    http_response_code(401);
    
}

/**
 * Vérifie le token JWT
 *
 * @param string  $serializedToken  Token provenant du client C#
 * 
 * @author Thomas Tissot
 * @return string Ok si le token valise. Si le token est invalide retourne le code d'erreur
 */ 
function authenticate_token($serializedToken)
{
    $jwt = new Emarref\Jwt\Jwt();
    try {
        $token = $jwt->deserialize($serializedToken);
    } catch (Exception $e) {
        return ['status' => 'error', 'err' => $e->getMessage()];
    } catch (Error $e) {
        return ['status' => 'error', 'err' => $e->getMessage()];
    }
    $algorithm = new Emarref\Jwt\Algorithm\Hs256('verysecret');
    $encryption = Emarref\Jwt\Encryption\Factory::create($algorithm);
    $context = new Emarref\Jwt\Verification\Context($encryption);
    $context->setAudience('http://127.0.0.1/site/api/');
    $context->setIssuer('SecuWallet');
    $context->setSubject($_POST['email']);
    
    try {
        $jwt->verify($token, $context);
    } catch (Emarref\Jwt\Exception\VerificationException $e) {
        return ['status' => 'error', 'err' => $e->getMessage()];
    }
    return ['status' => 'ok'];
}
?>