<?php 
    require_once ("../functionConnexionDB.php");
    include "../lib/google-authenticator-main/vendor/autoload.php";
    use Emarref\Jwt\Claim;

    header('Content-Type: application/json; charset=utf-8');
    http_response_code(200);
    echo json_encode(create_token());

/**
 * Génère un token en utilisant l'id et l'email de l'utilisateur
 * 
 * @author Thomas Tissot
 * @return $serializedToken Token JWT généré grace a la librairie Emarref\Jwt
 */     
function create_token(){
    $token = new Emarref\Jwt\Token();
    
    // Standard claims are supported
    $token->addClaim(new Claim\Audience(['http://127.0.0.1/site/api/', 'audience_2']));
    $token->addClaim(new Claim\Expiration(new \DateTime('30 minutes')));
    $token->addClaim(new Claim\IssuedAt(new \DateTime('now')));
    $token->addClaim(new Claim\Issuer('SecuWallet'));
    $token->addClaim(new Claim\JwtId(GetUserId($_POST['email'])));
    $token->addClaim(new Claim\NotBefore(new \DateTime('now')));
    $token->addClaim(new Claim\Subject($_POST['email']));
    
    $jwt = new Emarref\Jwt\Jwt();
    $algorithm = new Emarref\Jwt\Algorithm\Hs256('verysecret');
    $encryption = Emarref\Jwt\Encryption\Factory::create($algorithm);
    $serializedToken = $jwt->serialize($token, $encryption);
    return $serializedToken;
}
?>