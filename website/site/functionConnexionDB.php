<?php
define('DB_HOST', "localhost");
define('DB_NAME', "DBTravailDiplome");
define('DB_USER', "tissott");
define('DB_PASS', "Super");

/**
 * Génère un object PDO connecté à la base de données.
 *
 * 
 * @author Thomas Tissot
 * @return $dbb Object PDO connecté à la base de données
 */ 
function getConnexion() {
    static $dbb = NULL;
    if ($dbb === NULL) {
        try {
            $connectionString = 'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME;
            $dbb = new PDO($connectionString, DB_USER, DB_PASS);
            $dbb->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            die('Erreur : ' . $e->getMessage());
        }
    }
    return $dbb;
}

/**
 * Insert un utilisateur dans la base de données.
 *
 * @param string  $email  Email de l'utilisateur.
 * @param string  $password  mot de passe en SHA1 de l'utilisateur.
 * @param string  $secretKey  Clé secrète associée à l'utilisateur.
 * 
 * @author Thomas Tissot
 */ 
function createUser($email, $password, $secretKey) {
    $connexion = getConnexion();
    $requete = $connexion->prepare("INSERT INTO `DBTravailDiplome`.`Users` (mail, password, secretKey) VALUES (?,?,?);");
    $requete->bindParam(1, $email);
    $requete->bindParam(2, $password);
    $requete->bindParam(3, $secretKey);
    var_dump($requete);
    $requete->execute();
}

/**
 * Vérification les login de l'utilisateur
 *
 * @param string  $email  Email de l'utilisateur.
 * 
 * @author Thomas Tissot
 * @return string Clé secret assosié a l'email utilisateur.
 */ 
function GetUserSecretKey($email){
    $connexion = getConnexion();
    $requete = $connexion->prepare("SELECT `Users`.`secretKey`
    FROM `DBTravailDiplome`.`Users` 
    WHERE mail=\"$email\";");
    $requete->execute();
    return $requete->fetch(PDO::FETCH_ASSOC);
}

/**
 * Vérification les login de l'utilisateur
 *
 * @param string  $email  Email de l'utilisateur
 * @param string  $password  mot de passe SHA1 de l'utilisateur
 * 
 * @author Thomas Tissot
 * @return string Clé secret assosié a l'email et le mot de passe de l'utilisateur.
 */ 
function CheckUserLogin($email, $password){
    $connexion = getConnexion();
    $requete = $connexion->prepare("SELECT `Users`.`secretKey`
    FROM `DBTravailDiplome`.`Users` 
    WHERE mail=\"$email\" AND password=\"$password\";");
    $requete->execute();
    return $requete->fetch(PDO::FETCH_ASSOC);
}

/**
 * Retourne l'idUser associé à l'email de l'utilisateur.
 *
 * @param string  $email  Email de l'utilisateur.
 * 
 * @author Thomas Tissot
 * @return string Id de l'utilisateur.
 */ 
function GetUserId($email){
    $connexion = getConnexion();
    $requete = $connexion->prepare("SELECT idUsers from Users WHERE mail=\"$email\"");
    $requete->execute();
    return $requete->fetch(PDO::FETCH_ASSOC);
}
?>