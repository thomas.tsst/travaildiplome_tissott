# Quel but ?

SecuWallet est une application qui permet de gérer et de stocker des portefeuilles cryptomonnaies sur la blockchain Ethereum. Il y a la possibilité de créer des portefeuilles, importer des portefeuilles déjà existants. La puissance de ce projet est la sécurité apportée lors du stockage des portefeuilles sur la machine. L'utilisateur aura également un suivi de l'évolution des différents portefeuilles importer dans l'application. Il pourra également créer des transactions sur la blockchain.

## Installation

Pour avoir accès au logiciel d'installation il est nécessaire de se [créer un compte](./createAccount.md) et ce connecter a ce dernier.
